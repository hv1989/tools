import re

try:
    text = clipboard.get_clipboard()
except Exception:
    text = ""
if not re.match("^[a-zA-Z0-9_-]$", text):
    text = ""

keyboard.send_keys("for (int i=0; i<%s.size(); i++) {\n\t\n}" % text)
if len(text) == 0:
    keyboard.send_keys("<up><up>")  
    for i in range(len("for (int i=0; i<")-1):
        keyboard.send_keys("<right>")
else:
    keyboard.send_keys("<up>")  