import time

text = clipboard.get_clipboard()

if window.get_active_class() == "gnome-terminal-server.Gnome-terminal":
    keyboard.send_keys(text, 0) # no using paste, since that doesn't work in terminals!
else:
    keyboard.send_keys(text, 1) # using paste, since that is more reliable 