#
# To install: ln -s $(pwd)/bash_aliases ~/.bash_aliases
#

BUILD=$HOME"/"Build 

# Use this alias to reload the definitions upon making changes.
alias alias_reload='source $HOME/.bash_aliases'

# Notify something is done.
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
alias notify='echo "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*notify$//'\'')" >> /tmp/alert'
#TODO; put token in config file!
alias notify='curl --data "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*notify$//'\'')" $"https://uwsoftware.slack.com/services/hooks/slackbot?token=XXXXXXXXXXXXX&channel=%23h4writer-compiler"'

# Engines
alias js='$JS/dist/bin/js'
alias d8='$BUILD/v8/out/ia32.release/d8'

# Benchmarks
alias kraken='cd $BUILD/kraken'
alias octane='cd $BUILD/octane2.0'
alias ss='cd $BUILD/SunSpider'

#Tools
alias m='make -s -C $JS -j8 2>&1 | grep -iE "error|$"; alert "Compilation done"'
alias m='make -s -C $JS -j8 2>&1 | grep -iE "error|$"; notify "Compilation done"'
alias mb='cd $REPO; ./mach build; notify "Compilation done"'
alias val='valgrind --tool=cachegrind'
alias try='hg push -f ssh://hg.mozilla.org/try' 
alias untry='hg phase -f --draft qbase:tip'
alias last='cd ~/Build/NetBeansProjects/DroneCommandline/$(ls ~/Build/NetBeansProjects/DroneCommandline | grep dronelog-20 | tail -n1)'


alias s='~/Build/stackato'

export ANDROID_HOME=/home/h4writer/Android/Sdk/
alias py="python3"
alias show="screen -R -d"

function setgov ()
{
    echo "$1" | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor 
}
