from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import TimeoutException
import time

from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

from getpass import getpass
import os

if not os.path.exists("/tmp/invoices_out"):
    os.mkdir("/tmp/invoices_out")

def create_browser():
    fp = webdriver.FirefoxProfile()
    fp.set_preference("browser.download.folderList", 2)
    fp.set_preference("browser.download.manager.showWhenStarting", False)
    fp.set_preference("browser.download.dir", "/tmp/invoices_out")
    fp.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf,application/x-pdf,application/octet-stream")
    return webdriver.Firefox(fp, executable_path=r'geckodriver')

def fill_slowly(el, text):
    for character in password:
        el.send_keys(character)
        time.sleep(0.3) # pause for 0.3 seconds

def wait_present(browser, what):
    time.sleep(1)
    WebDriverWait(browser, 60).until(
        expected_conditions.presence_of_element_located(
            what
        )
    )

def scroll_to(browser, element):
    x = element.location['x']
    y = element.location['y']
    scroll_by_coord = 'window.scrollTo(%s,%s);' % (
        x,
        y
    )
    scroll_nav_out_of_way = 'window.scrollBy(0, -120);'
    browser.execute_script(scroll_by_coord)
    browser.execute_script(scroll_nav_out_of_way)
    actions = ActionChains(browser)
    actions.move_to_element(element).perform();

def save_page(browser, name):
    import codecs
    with codecs.open("/tmp/site.html", "w", "utf-8") as f:
        f.write(browser.page_source)

    import pdfkit
    pdfkit.from_url('file:///tmp/site.html', name)

def save_image(browser, name):


    base64string = browser.execute_script("var c = document.createElement('canvas');"
                                      + " var ctx = c.getContext('2d');"
                                      + "var img = document.getElementsByTagName('img')[0];"
                                      + "c.height=img.naturalHeight;"
                                      + "c.width=img.naturalWidth;"
                                      + "ctx.drawImage(img, 0, 0,img.naturalWidth, img.naturalHeight);"
                                      + "var base64String = c.toDataURL();"
                                      + "return base64String;");
    import base64
    import math
    print base64string
    with open(name+"out", 'w') as f:
        f.write(base64string)

    from PIL import Image
    from io import BytesIO
    from base64 import b64decode
    im = Image.open(BytesIO(b64decode(base64string.split(',')[1])))
    if "png" not in base64string[:30]:
        error()

    im.save(name)

def inputYN(text):
    print text
    in_ = raw_input()
    return in_ in ["Y","y", ""]

password = getpass()

browser = create_browser()
browser.maximize_window()
browser.get("https://www.rydoo.com/")
browser.find_element(By.CSS_SELECTOR, ".secondary-dark > a").click()
browser.find_element(By.ID, "username").click()
browser.find_element(By.ID, "username").send_keys("hannes@uwsoftware.be")
browser.find_element(By.ID, "nextButton").click()

wait_present(browser, (By.ID, 'password'))
browser.find_element(By.ID, "password").click()
browser.find_element(By.ID, "password").send_keys(password)
browser.find_element(By.ID, "password").send_keys(Keys.ENTER)

time.sleep(5)
browser.get("https://expense.rydoo.com/personal/expenses")

wait_present(browser, (By.ID, "ctl00_ContentPlaceHolder1_overview_aAll"))
browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_aAll").click()

wait_present(browser, (By.ID, "ctl00_ContentPlaceHolder1_overview_aShowFilters"))
time.sleep(4)
browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_aShowFilters").click()

wait_present(browser, (By.ID, "ctl00_ContentPlaceHolder1_overview_tbDate"))
browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_tbDate").click()

browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_tbFrom").click()
browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_tbFrom").send_keys("1/09/2020")
browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_tbFrom").send_keys(Keys.TAB)

browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_tbUntil").click()
browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_tbUntil").send_keys("30/09/2020")
browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_datePickerPopover").click()

browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_tbFrom").click()
browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_tbFrom").send_keys(Keys.CONTROL, "a")
browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_tbFrom").send_keys("1/09/2020")
browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_tbFrom").send_keys(Keys.TAB)

browser.find_element(By.ID, "ctl00_ContentPlaceHolder1_overview_dateFilter_btnCustomGo").click()

i = 1
while True:
    time.sleep(5)
    browser.find_element(By.CSS_SELECTOR, ".table-list-item-header:nth-child("+str(i)+") > .date").click()
    try:
        wait_present(browser, (By.CSS_SELECTOR, ".zoomImg"))
        url = browser.find_element(By.CSS_SELECTOR, ".zoomImg").get_attribute("src");
        url = url.replace("download=true","")
        url = url.replace("size=large","size=original")

        browser.execute_script("window.open('');")
        browser.switch_to.window(browser.window_handles[1])
        browser.get(url)

        time.sleep(3)
        save_image(browser, "/tmp/output.png")

        os.system("convert /tmp/output.png /tmp/invoices_out/output-"+str(i)+".pdf")

        browser.execute_script("window.close();")

        browser.switch_to.window(browser.window_handles[0])

    except TimeoutException:
        pass

    browser.find_element(By.CSS_SELECTOR, ".selected > .date").click()
    i += 1
