
import sys
from pykml import parser
from lxml import etree
from pykml.factory import nsmap
import geopy.distance
import math
from pykml.factory import KML_ElementMaker as KML

def toMeters(ref, value):
    point = [
        geopy.distance.distance(ref, (ref[0], value[1])).m,
        geopy.distance.distance(ref, (value[0], ref[1])).m
    ]

    if ref[1] > value[1]:
        point[0] = -point[0]
    if ref[0] > value[0]:
        point[1] = -point[1]
    return point

def toGeo(ref, value):
    d = geopy.distance.distance(meters=value[0])
    ref = d.destination(point=ref, bearing=90)
    d = geopy.distance.distance(meters=value[1])
    ref = d.destination(point=ref, bearing=0)
    return ref.latitude, ref.longitude

def insidePoly(poly, p):
    from shapely.geometry import Point, Polygon
    poly = Polygon(poly)
    return Point(p).within(poly)

def decreasePoly(poly, meters):
    poly = poly[:-1]
    npoly = []
    for i in range(len(poly)):
        p0 = poly[(i-1)%len(poly)]
        p1 = poly[i]
        p2 = poly[(i+1)%len(poly)]

        # create y= slope*x + b which is meters away
        # for p1, p2
        if p2[0] == p1[0]: 
            slope1 = None
            if insidePoly(poly, [p1[0] + meters, (p1[1]+p2[1])/2]):
                x = p1[0] + meters
            else:
                x = p1[0] - meters
        else:
            slope1 = (p2[1]-p1[1]) / (p2[0]-p1[0])
            dy = math.sqrt(meters**2/(slope1**2+1))
            dx = -slope1*dy
            middle = [(p1[0]+p2[0])/2, (p1[1]+p2[1])/2]
            if insidePoly(poly, [middle[0] + dx, middle[1] + dy]):
                b1 = p1[1] + dy - slope1 * (p1[0] + dx)
            else:
                b1 = p1[1] - dy - slope1 * (p1[0] - dx)

        # create y= slope*x + b which is meters away
        # for p1, p2
        if p0[0] == p1[0]: 
            slope2 = None
            if insidePoly(poly, [p0[0] + meters, (p0[1]+p1[1])/2]):
                x = p0[0] + meters
            else:
                x = p0[0] - meters
        else:
            slope2 = (p1[1]-p0[1]) / (p1[0]-p0[0])
            dy = math.sqrt(meters**2/(slope2**2+1))
            dx = -slope2*dy
            middle = [(p0[0]+p1[0])/2, (p0[1]+p1[1])/2]
            if insidePoly(poly, [middle[0] + dx, middle[1] + dy]):
                b2 = p0[1] + dy - slope2 * (p0[0] + dx)
            else:
                b2 = p0[1] - dy - slope2 * (p0[0] - dx)

        if slope1 == None and slope2 == None:
            print("couldn't calculate polygon")
            return None

        if slope1 == None:
            y = slope2*x+b2
        elif slope2 == None:
            y = slope1*x+b1
        else:
            x = (b2-b1)/(slope1-slope2)
            y = slope1*x+b1
        npoly.append([x,y])
    return npoly + [npoly[-1]]
   
def createMarker(name, styleId, poly):
    return KML.Placemark(
        KML.name(name),
        KML.styleUrl("#"+styleId),
        KML.Polygon(
            KML.outerBoundaryIs(
                KML.LinearRing(
                    KML.tesselate(1),
                    KML.coordinates(
                        "\n".join([str(p[0])+","+str(p[1])+",0" for p in poly])
                        )
                    )
                )
            )
        )
def addstyle(root, name, color):
    root.Document.append(
        KML.Style(
            KML.LineStyle(
                KML.color("ff"+color),
                KML.width(16)
            ),
            KML.PolyStyle(
                KML.color("4a"+color),
                KML.fill(1),
                KML.outline(1)
            ),
            KML.BalloonStyle(
                KML.text("<![CDATA[<h3>$[name]</h3>]]>")
            ),
            id=name+"-normal"
        )
    )
    root.Document.append(
        KML.Style(
            KML.LineStyle(
                KML.color("ff"+color),
                KML.width(24)
            ),
            KML.PolyStyle(
                KML.color("4a"+color),
                KML.fill(1),
                KML.outline(1)
            ),
            id=name+"-highlight"
        )
    )
    root.Document.append(
        KML.StyleMap(
            KML.Pair(
                KML.key("normal"),
                KML.styleUrl("#"+name+"-normal")
            ),
            KML.Pair(
                KML.key("highlight"),
                KML.styleUrl("#"+name+"-highlight")
            ),
            id=name
        )
    )
    

def decdeg2dms(dd):
    negative = dd < 0
    dd = abs(dd)
    minutes,seconds = divmod(dd*3600,60)
    degrees,minutes = divmod(minutes,60)
    if negative:
        if degrees > 0:
            degrees = -degrees
        elif minutes > 0:
            minutes = -minutes
        else:
            seconds = -seconds
    return str(int(degrees))+"º"+str(int(minutes))+"'"+"{:.2f}".format(seconds)+'"'


kml_file = sys.argv[1]
with open(kml_file) as f:
    doc = parser.parse(f)
    root = doc.getroot()

    namespace = {"ns": nsmap[None]}
    pms = root.findall(".//ns:Placemark", namespaces=namespace)

    for pm in pms:
        if "exclusion" in str(pm.name) or "Exclusion" in str(pm.name):
            print("found")
            coordinates = str(pm.Polygon.outerBoundaryIs.LinearRing.coordinates)
            coordinates = [co.split(",") for co in coordinates.split("\n") if "," in co]
            coordinates = [[float(co[1]), float(co[0])] for co in coordinates]
            ref = coordinates[0]
            poly = [toMeters(ref, co) for co in coordinates]

            for co in coordinates:
                print(decdeg2dms(co[0])+"N", decdeg2dms(co[1])+"E")


            zones= [
                ["Flyzone", 15, "589d0f"],
                ["Flyzone 60m", 15+18.5, "2dc0fb"],
                ["Flyzone 120m", 15+27.3, "00eaff"]
            ]
            for name, dist, color in zones: 
                npoly = decreasePoly(poly, dist)
                coordinates = [toGeo(ref, co) for co in npoly]
                coordinates = [[co[1], co[0]] for co in coordinates]
                marker = createMarker(name, "uniquename"+str(dist), coordinates)
                addstyle(root, "uniquename"+str(dist), color)
                pm.getparent().append(marker)
            
with open('/tmp/output.kml', 'wb') as output:
    output.write(etree.tostring(doc, pretty_print=True))
print('writen to /tmp/output.kml')
