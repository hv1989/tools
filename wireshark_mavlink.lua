-- Wireshark dissector for the MAVLink protocol (please see http://qgroundcontrol.org/mavlink/start for details) 

unknownFrameBeginOffset=0


mavlink_proto = Proto("mavlink_proto", "MAVLink protocol")
f = mavlink_proto.fields

payload_fns = {}

messageName = {
    [150] = 'SENSOR_OFFSETS',
    [151] = 'SET_MAG_OFFSETS',
    [152] = 'MEMINFO',
    [153] = 'AP_ADC',
    [154] = 'DIGICAM_CONFIGURE',
    [155] = 'DIGICAM_CONTROL',
    [156] = 'MOUNT_CONFIGURE',
    [157] = 'MOUNT_CONTROL',
    [158] = 'MOUNT_STATUS',
    [160] = 'FENCE_POINT',
    [161] = 'FENCE_FETCH_POINT',
    [162] = 'FENCE_STATUS',
    [163] = 'AHRS',
    [164] = 'SIMSTATE',
    [165] = 'HWSTATUS',
    [166] = 'RADIO',
    [167] = 'LIMITS_STATUS',
    [168] = 'WIND',
    [169] = 'DATA16',
    [170] = 'DATA32',
    [171] = 'DATA64',
    [172] = 'DATA96',
    [173] = 'RANGEFINDER',
    [174] = 'AIRSPEED_AUTOCAL',
    [175] = 'RALLY_POINT',
    [176] = 'RALLY_FETCH_POINT',
    [177] = 'COMPASSMOT_STATUS',
    [178] = 'AHRS2',
    [179] = 'CAMERA_STATUS',
    [180] = 'CAMERA_FEEDBACK',
    [181] = 'BATTERY2',
    [182] = 'AHRS3',
    [183] = 'AUTOPILOT_VERSION_REQUEST',
    [184] = 'REMOTE_LOG_DATA_BLOCK',
    [185] = 'REMOTE_LOG_BLOCK_STATUS',
    [186] = 'LED_CONTROL',
    [191] = 'MAG_CAL_PROGRESS',
    [192] = 'MAG_CAL_REPORT',
    [193] = 'EKF_STATUS_REPORT',
    [194] = 'PID_TUNING',
    [200] = 'GIMBAL_REPORT',
    [201] = 'GIMBAL_CONTROL',
    [214] = 'GIMBAL_TORQUE_CMD_REPORT',
    [215] = 'GOPRO_HEARTBEAT',
    [216] = 'GOPRO_GET_REQUEST',
    [217] = 'GOPRO_GET_RESPONSE',
    [218] = 'GOPRO_SET_REQUEST',
    [219] = 'GOPRO_SET_RESPONSE',
    [226] = 'RPM',
    [0] = 'HEARTBEAT',
    [1] = 'SYS_STATUS',
    [2] = 'SYSTEM_TIME',
    [4] = 'PING',
    [5] = 'CHANGE_OPERATOR_CONTROL',
    [6] = 'CHANGE_OPERATOR_CONTROL_ACK',
    [7] = 'AUTH_KEY',
    [11] = 'SET_MODE',
    [20] = 'PARAM_REQUEST_READ',
    [21] = 'PARAM_REQUEST_LIST',
    [22] = 'PARAM_VALUE',
    [23] = 'PARAM_SET',
    [24] = 'GPS_RAW_INT',
    [25] = 'GPS_STATUS',
    [26] = 'SCALED_IMU',
    [27] = 'RAW_IMU',
    [28] = 'RAW_PRESSURE',
    [29] = 'SCALED_PRESSURE',
    [30] = 'ATTITUDE',
    [31] = 'ATTITUDE_QUATERNION',
    [32] = 'LOCAL_POSITION_NED',
    [33] = 'GLOBAL_POSITION_INT',
    [34] = 'RC_CHANNELS_SCALED',
    [35] = 'RC_CHANNELS_RAW',
    [36] = 'SERVO_OUTPUT_RAW',
    [37] = 'MISSION_REQUEST_PARTIAL_LIST',
    [38] = 'MISSION_WRITE_PARTIAL_LIST',
    [39] = 'MISSION_ITEM',
    [40] = 'MISSION_REQUEST',
    [41] = 'MISSION_SET_CURRENT',
    [42] = 'MISSION_CURRENT',
    [43] = 'MISSION_REQUEST_LIST',
    [44] = 'MISSION_COUNT',
    [45] = 'MISSION_CLEAR_ALL',
    [46] = 'MISSION_ITEM_REACHED',
    [47] = 'MISSION_ACK',
    [48] = 'SET_GPS_GLOBAL_ORIGIN',
    [49] = 'GPS_GLOBAL_ORIGIN',
    [50] = 'PARAM_MAP_RC',
    [51] = 'MISSION_REQUEST_INT',
    [54] = 'SAFETY_SET_ALLOWED_AREA',
    [55] = 'SAFETY_ALLOWED_AREA',
    [61] = 'ATTITUDE_QUATERNION_COV',
    [62] = 'NAV_CONTROLLER_OUTPUT',
    [63] = 'GLOBAL_POSITION_INT_COV',
    [64] = 'LOCAL_POSITION_NED_COV',
    [65] = 'RC_CHANNELS',
    [66] = 'REQUEST_DATA_STREAM',
    [67] = 'DATA_STREAM',
    [69] = 'MANUAL_CONTROL',
    [70] = 'RC_CHANNELS_OVERRIDE',
    [73] = 'MISSION_ITEM_INT',
    [74] = 'VFR_HUD',
    [75] = 'COMMAND_INT',
    [76] = 'COMMAND_LONG',
    [77] = 'COMMAND_ACK',
    [81] = 'MANUAL_SETPOINT',
    [82] = 'SET_ATTITUDE_TARGET',
    [83] = 'ATTITUDE_TARGET',
    [84] = 'SET_POSITION_TARGET_LOCAL_NED',
    [85] = 'POSITION_TARGET_LOCAL_NED',
    [86] = 'SET_POSITION_TARGET_GLOBAL_INT',
    [87] = 'POSITION_TARGET_GLOBAL_INT',
    [89] = 'LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET',
    [90] = 'HIL_STATE',
    [91] = 'HIL_CONTROLS',
    [92] = 'HIL_RC_INPUTS_RAW',
    [93] = 'HIL_ACTUATOR_CONTROLS',
    [100] = 'OPTICAL_FLOW',
    [101] = 'GLOBAL_VISION_POSITION_ESTIMATE',
    [102] = 'VISION_POSITION_ESTIMATE',
    [103] = 'VISION_SPEED_ESTIMATE',
    [104] = 'VICON_POSITION_ESTIMATE',
    [105] = 'HIGHRES_IMU',
    [106] = 'OPTICAL_FLOW_RAD',
    [107] = 'HIL_SENSOR',
    [108] = 'SIM_STATE',
    [109] = 'RADIO_STATUS',
    [110] = 'FILE_TRANSFER_PROTOCOL',
    [111] = 'TIMESYNC',
    [112] = 'CAMERA_TRIGGER',
    [113] = 'HIL_GPS',
    [114] = 'HIL_OPTICAL_FLOW',
    [115] = 'HIL_STATE_QUATERNION',
    [116] = 'SCALED_IMU2',
    [117] = 'LOG_REQUEST_LIST',
    [118] = 'LOG_ENTRY',
    [119] = 'LOG_REQUEST_DATA',
    [120] = 'LOG_DATA',
    [121] = 'LOG_ERASE',
    [122] = 'LOG_REQUEST_END',
    [123] = 'GPS_INJECT_DATA',
    [124] = 'GPS2_RAW',
    [125] = 'POWER_STATUS',
    [126] = 'SERIAL_CONTROL',
    [127] = 'GPS_RTK',
    [128] = 'GPS2_RTK',
    [129] = 'SCALED_IMU3',
    [130] = 'DATA_TRANSMISSION_HANDSHAKE',
    [131] = 'ENCAPSULATED_DATA',
    [132] = 'DISTANCE_SENSOR',
    [133] = 'TERRAIN_REQUEST',
    [134] = 'TERRAIN_DATA',
    [135] = 'TERRAIN_CHECK',
    [136] = 'TERRAIN_REPORT',
    [137] = 'SCALED_PRESSURE2',
    [138] = 'ATT_POS_MOCAP',
    [139] = 'SET_ACTUATOR_CONTROL_TARGET',
    [140] = 'ACTUATOR_CONTROL_TARGET',
    [141] = 'ALTITUDE',
    [142] = 'RESOURCE_REQUEST',
    [143] = 'SCALED_PRESSURE3',
    [144] = 'FOLLOW_TARGET',
    [146] = 'CONTROL_SYSTEM_STATE',
    [147] = 'BATTERY_STATUS',
    [148] = 'AUTOPILOT_VERSION',
    [149] = 'LANDING_TARGET',
    [230] = 'ESTIMATOR_STATUS',
    [231] = 'WIND_COV',
    [232] = 'GPS_INPUT',
    [233] = 'GPS_RTCM_DATA',
    [234] = 'HIGH_LATENCY',
    [241] = 'VIBRATION',
    [242] = 'HOME_POSITION',
    [243] = 'SET_HOME_POSITION',
    [244] = 'MESSAGE_INTERVAL',
    [245] = 'EXTENDED_SYS_STATE',
    [246] = 'ADSB_VEHICLE',
    [247] = 'COLLISION',
    [248] = 'V2_EXTENSION',
    [249] = 'MEMORY_VECT',
    [250] = 'DEBUG_VECT',
    [251] = 'NAMED_VALUE_FLOAT',
    [252] = 'NAMED_VALUE_INT',
    [253] = 'STATUSTEXT',
    [254] = 'DEBUG',
}

f.magic = ProtoField.uint8("mavlink_proto.magic", "Magic value / version", base.HEX)
f.length = ProtoField.uint8("mavlink_proto.length", "Payload length")
f.sequence = ProtoField.uint8("mavlink_proto.sequence", "Packet sequence")
f.sysid = ProtoField.uint8("mavlink_proto.sysid", "System id", base.HEX)
f.compid = ProtoField.uint8("mavlink_proto.compid", "Component id", base.HEX)
f.msgid = ProtoField.uint8("mavlink_proto.msgid", "Message id", base.HEX)
f.crc = ProtoField.uint16("mavlink_proto.crc", "Message CRC", base.HEX)
f.payload = ProtoField.uint8("mavlink_proto.payload", "Payload", base.DEC, messageName)
f.rawheader = ProtoField.bytes("mavlink_proto.rawheader", "Unparsable header fragment")
f.rawpayload = ProtoField.bytes("mavlink_proto.rawpayload", "Unparsable payload")

f.SENSOR_OFFSETS_mag_ofs_x = ProtoField.int16("mavlink_proto.SENSOR_OFFSETS_mag_ofs_x", "mag_ofs_x (int16)")
f.SENSOR_OFFSETS_mag_ofs_y = ProtoField.int16("mavlink_proto.SENSOR_OFFSETS_mag_ofs_y", "mag_ofs_y (int16)")
f.SENSOR_OFFSETS_mag_ofs_z = ProtoField.int16("mavlink_proto.SENSOR_OFFSETS_mag_ofs_z", "mag_ofs_z (int16)")
f.SENSOR_OFFSETS_mag_declination = ProtoField.float("mavlink_proto.SENSOR_OFFSETS_mag_declination", "mag_declination (float)")
f.SENSOR_OFFSETS_raw_press = ProtoField.int32("mavlink_proto.SENSOR_OFFSETS_raw_press", "raw_press (int32)")
f.SENSOR_OFFSETS_raw_temp = ProtoField.int32("mavlink_proto.SENSOR_OFFSETS_raw_temp", "raw_temp (int32)")
f.SENSOR_OFFSETS_gyro_cal_x = ProtoField.float("mavlink_proto.SENSOR_OFFSETS_gyro_cal_x", "gyro_cal_x (float)")
f.SENSOR_OFFSETS_gyro_cal_y = ProtoField.float("mavlink_proto.SENSOR_OFFSETS_gyro_cal_y", "gyro_cal_y (float)")
f.SENSOR_OFFSETS_gyro_cal_z = ProtoField.float("mavlink_proto.SENSOR_OFFSETS_gyro_cal_z", "gyro_cal_z (float)")
f.SENSOR_OFFSETS_accel_cal_x = ProtoField.float("mavlink_proto.SENSOR_OFFSETS_accel_cal_x", "accel_cal_x (float)")
f.SENSOR_OFFSETS_accel_cal_y = ProtoField.float("mavlink_proto.SENSOR_OFFSETS_accel_cal_y", "accel_cal_y (float)")
f.SENSOR_OFFSETS_accel_cal_z = ProtoField.float("mavlink_proto.SENSOR_OFFSETS_accel_cal_z", "accel_cal_z (float)")

f.SET_MAG_OFFSETS_target_system = ProtoField.uint8("mavlink_proto.SET_MAG_OFFSETS_target_system", "target_system (uint8)")
f.SET_MAG_OFFSETS_target_component = ProtoField.uint8("mavlink_proto.SET_MAG_OFFSETS_target_component", "target_component (uint8)")
f.SET_MAG_OFFSETS_mag_ofs_x = ProtoField.int16("mavlink_proto.SET_MAG_OFFSETS_mag_ofs_x", "mag_ofs_x (int16)")
f.SET_MAG_OFFSETS_mag_ofs_y = ProtoField.int16("mavlink_proto.SET_MAG_OFFSETS_mag_ofs_y", "mag_ofs_y (int16)")
f.SET_MAG_OFFSETS_mag_ofs_z = ProtoField.int16("mavlink_proto.SET_MAG_OFFSETS_mag_ofs_z", "mag_ofs_z (int16)")

f.MEMINFO_brkval = ProtoField.uint16("mavlink_proto.MEMINFO_brkval", "brkval (uint16)")
f.MEMINFO_freemem = ProtoField.uint16("mavlink_proto.MEMINFO_freemem", "freemem (uint16)")

f.AP_ADC_adc1 = ProtoField.uint16("mavlink_proto.AP_ADC_adc1", "adc1 (uint16)")
f.AP_ADC_adc2 = ProtoField.uint16("mavlink_proto.AP_ADC_adc2", "adc2 (uint16)")
f.AP_ADC_adc3 = ProtoField.uint16("mavlink_proto.AP_ADC_adc3", "adc3 (uint16)")
f.AP_ADC_adc4 = ProtoField.uint16("mavlink_proto.AP_ADC_adc4", "adc4 (uint16)")
f.AP_ADC_adc5 = ProtoField.uint16("mavlink_proto.AP_ADC_adc5", "adc5 (uint16)")
f.AP_ADC_adc6 = ProtoField.uint16("mavlink_proto.AP_ADC_adc6", "adc6 (uint16)")

f.DIGICAM_CONFIGURE_target_system = ProtoField.uint8("mavlink_proto.DIGICAM_CONFIGURE_target_system", "target_system (uint8)")
f.DIGICAM_CONFIGURE_target_component = ProtoField.uint8("mavlink_proto.DIGICAM_CONFIGURE_target_component", "target_component (uint8)")
f.DIGICAM_CONFIGURE_mode = ProtoField.uint8("mavlink_proto.DIGICAM_CONFIGURE_mode", "mode (uint8)")
f.DIGICAM_CONFIGURE_shutter_speed = ProtoField.uint16("mavlink_proto.DIGICAM_CONFIGURE_shutter_speed", "shutter_speed (uint16)")
f.DIGICAM_CONFIGURE_aperture = ProtoField.uint8("mavlink_proto.DIGICAM_CONFIGURE_aperture", "aperture (uint8)")
f.DIGICAM_CONFIGURE_iso = ProtoField.uint8("mavlink_proto.DIGICAM_CONFIGURE_iso", "iso (uint8)")
f.DIGICAM_CONFIGURE_exposure_type = ProtoField.uint8("mavlink_proto.DIGICAM_CONFIGURE_exposure_type", "exposure_type (uint8)")
f.DIGICAM_CONFIGURE_command_id = ProtoField.uint8("mavlink_proto.DIGICAM_CONFIGURE_command_id", "command_id (uint8)")
f.DIGICAM_CONFIGURE_engine_cut_off = ProtoField.uint8("mavlink_proto.DIGICAM_CONFIGURE_engine_cut_off", "engine_cut_off (uint8)")
f.DIGICAM_CONFIGURE_extra_param = ProtoField.uint8("mavlink_proto.DIGICAM_CONFIGURE_extra_param", "extra_param (uint8)")
f.DIGICAM_CONFIGURE_extra_value = ProtoField.float("mavlink_proto.DIGICAM_CONFIGURE_extra_value", "extra_value (float)")

f.DIGICAM_CONTROL_target_system = ProtoField.uint8("mavlink_proto.DIGICAM_CONTROL_target_system", "target_system (uint8)")
f.DIGICAM_CONTROL_target_component = ProtoField.uint8("mavlink_proto.DIGICAM_CONTROL_target_component", "target_component (uint8)")
f.DIGICAM_CONTROL_session = ProtoField.uint8("mavlink_proto.DIGICAM_CONTROL_session", "session (uint8)")
f.DIGICAM_CONTROL_zoom_pos = ProtoField.uint8("mavlink_proto.DIGICAM_CONTROL_zoom_pos", "zoom_pos (uint8)")
f.DIGICAM_CONTROL_zoom_step = ProtoField.int8("mavlink_proto.DIGICAM_CONTROL_zoom_step", "zoom_step (int8)")
f.DIGICAM_CONTROL_focus_lock = ProtoField.uint8("mavlink_proto.DIGICAM_CONTROL_focus_lock", "focus_lock (uint8)")
f.DIGICAM_CONTROL_shot = ProtoField.uint8("mavlink_proto.DIGICAM_CONTROL_shot", "shot (uint8)")
f.DIGICAM_CONTROL_command_id = ProtoField.uint8("mavlink_proto.DIGICAM_CONTROL_command_id", "command_id (uint8)")
f.DIGICAM_CONTROL_extra_param = ProtoField.uint8("mavlink_proto.DIGICAM_CONTROL_extra_param", "extra_param (uint8)")
f.DIGICAM_CONTROL_extra_value = ProtoField.float("mavlink_proto.DIGICAM_CONTROL_extra_value", "extra_value (float)")

f.MOUNT_CONFIGURE_target_system = ProtoField.uint8("mavlink_proto.MOUNT_CONFIGURE_target_system", "target_system (uint8)")
f.MOUNT_CONFIGURE_target_component = ProtoField.uint8("mavlink_proto.MOUNT_CONFIGURE_target_component", "target_component (uint8)")
f.MOUNT_CONFIGURE_mount_mode = ProtoField.uint8("mavlink_proto.MOUNT_CONFIGURE_mount_mode", "mount_mode (uint8)")
f.MOUNT_CONFIGURE_stab_roll = ProtoField.uint8("mavlink_proto.MOUNT_CONFIGURE_stab_roll", "stab_roll (uint8)")
f.MOUNT_CONFIGURE_stab_pitch = ProtoField.uint8("mavlink_proto.MOUNT_CONFIGURE_stab_pitch", "stab_pitch (uint8)")
f.MOUNT_CONFIGURE_stab_yaw = ProtoField.uint8("mavlink_proto.MOUNT_CONFIGURE_stab_yaw", "stab_yaw (uint8)")

f.MOUNT_CONTROL_target_system = ProtoField.uint8("mavlink_proto.MOUNT_CONTROL_target_system", "target_system (uint8)")
f.MOUNT_CONTROL_target_component = ProtoField.uint8("mavlink_proto.MOUNT_CONTROL_target_component", "target_component (uint8)")
f.MOUNT_CONTROL_input_a = ProtoField.int32("mavlink_proto.MOUNT_CONTROL_input_a", "input_a (int32)")
f.MOUNT_CONTROL_input_b = ProtoField.int32("mavlink_proto.MOUNT_CONTROL_input_b", "input_b (int32)")
f.MOUNT_CONTROL_input_c = ProtoField.int32("mavlink_proto.MOUNT_CONTROL_input_c", "input_c (int32)")
f.MOUNT_CONTROL_save_position = ProtoField.uint8("mavlink_proto.MOUNT_CONTROL_save_position", "save_position (uint8)")

f.MOUNT_STATUS_target_system = ProtoField.uint8("mavlink_proto.MOUNT_STATUS_target_system", "target_system (uint8)")
f.MOUNT_STATUS_target_component = ProtoField.uint8("mavlink_proto.MOUNT_STATUS_target_component", "target_component (uint8)")
f.MOUNT_STATUS_pointing_a = ProtoField.int32("mavlink_proto.MOUNT_STATUS_pointing_a", "pointing_a (int32)")
f.MOUNT_STATUS_pointing_b = ProtoField.int32("mavlink_proto.MOUNT_STATUS_pointing_b", "pointing_b (int32)")
f.MOUNT_STATUS_pointing_c = ProtoField.int32("mavlink_proto.MOUNT_STATUS_pointing_c", "pointing_c (int32)")

f.FENCE_POINT_target_system = ProtoField.uint8("mavlink_proto.FENCE_POINT_target_system", "target_system (uint8)")
f.FENCE_POINT_target_component = ProtoField.uint8("mavlink_proto.FENCE_POINT_target_component", "target_component (uint8)")
f.FENCE_POINT_idx = ProtoField.uint8("mavlink_proto.FENCE_POINT_idx", "idx (uint8)")
f.FENCE_POINT_count = ProtoField.uint8("mavlink_proto.FENCE_POINT_count", "count (uint8)")
f.FENCE_POINT_lat = ProtoField.float("mavlink_proto.FENCE_POINT_lat", "lat (float)")
f.FENCE_POINT_lng = ProtoField.float("mavlink_proto.FENCE_POINT_lng", "lng (float)")

f.FENCE_FETCH_POINT_target_system = ProtoField.uint8("mavlink_proto.FENCE_FETCH_POINT_target_system", "target_system (uint8)")
f.FENCE_FETCH_POINT_target_component = ProtoField.uint8("mavlink_proto.FENCE_FETCH_POINT_target_component", "target_component (uint8)")
f.FENCE_FETCH_POINT_idx = ProtoField.uint8("mavlink_proto.FENCE_FETCH_POINT_idx", "idx (uint8)")

f.FENCE_STATUS_breach_status = ProtoField.uint8("mavlink_proto.FENCE_STATUS_breach_status", "breach_status (uint8)")
f.FENCE_STATUS_breach_count = ProtoField.uint16("mavlink_proto.FENCE_STATUS_breach_count", "breach_count (uint16)")
f.FENCE_STATUS_breach_type = ProtoField.uint8("mavlink_proto.FENCE_STATUS_breach_type", "breach_type (uint8)")
f.FENCE_STATUS_breach_time = ProtoField.uint32("mavlink_proto.FENCE_STATUS_breach_time", "breach_time (uint32)")

f.AHRS_omegaIx = ProtoField.float("mavlink_proto.AHRS_omegaIx", "omegaIx (float)")
f.AHRS_omegaIy = ProtoField.float("mavlink_proto.AHRS_omegaIy", "omegaIy (float)")
f.AHRS_omegaIz = ProtoField.float("mavlink_proto.AHRS_omegaIz", "omegaIz (float)")
f.AHRS_accel_weight = ProtoField.float("mavlink_proto.AHRS_accel_weight", "accel_weight (float)")
f.AHRS_renorm_val = ProtoField.float("mavlink_proto.AHRS_renorm_val", "renorm_val (float)")
f.AHRS_error_rp = ProtoField.float("mavlink_proto.AHRS_error_rp", "error_rp (float)")
f.AHRS_error_yaw = ProtoField.float("mavlink_proto.AHRS_error_yaw", "error_yaw (float)")

f.SIMSTATE_roll = ProtoField.float("mavlink_proto.SIMSTATE_roll", "roll (float)")
f.SIMSTATE_pitch = ProtoField.float("mavlink_proto.SIMSTATE_pitch", "pitch (float)")
f.SIMSTATE_yaw = ProtoField.float("mavlink_proto.SIMSTATE_yaw", "yaw (float)")
f.SIMSTATE_xacc = ProtoField.float("mavlink_proto.SIMSTATE_xacc", "xacc (float)")
f.SIMSTATE_yacc = ProtoField.float("mavlink_proto.SIMSTATE_yacc", "yacc (float)")
f.SIMSTATE_zacc = ProtoField.float("mavlink_proto.SIMSTATE_zacc", "zacc (float)")
f.SIMSTATE_xgyro = ProtoField.float("mavlink_proto.SIMSTATE_xgyro", "xgyro (float)")
f.SIMSTATE_ygyro = ProtoField.float("mavlink_proto.SIMSTATE_ygyro", "ygyro (float)")
f.SIMSTATE_zgyro = ProtoField.float("mavlink_proto.SIMSTATE_zgyro", "zgyro (float)")
f.SIMSTATE_lat = ProtoField.int32("mavlink_proto.SIMSTATE_lat", "lat (int32)")
f.SIMSTATE_lng = ProtoField.int32("mavlink_proto.SIMSTATE_lng", "lng (int32)")

f.HWSTATUS_Vcc = ProtoField.uint16("mavlink_proto.HWSTATUS_Vcc", "Vcc (uint16)")
f.HWSTATUS_I2Cerr = ProtoField.uint8("mavlink_proto.HWSTATUS_I2Cerr", "I2Cerr (uint8)")

f.RADIO_rssi = ProtoField.uint8("mavlink_proto.RADIO_rssi", "rssi (uint8)")
f.RADIO_remrssi = ProtoField.uint8("mavlink_proto.RADIO_remrssi", "remrssi (uint8)")
f.RADIO_txbuf = ProtoField.uint8("mavlink_proto.RADIO_txbuf", "txbuf (uint8)")
f.RADIO_noise = ProtoField.uint8("mavlink_proto.RADIO_noise", "noise (uint8)")
f.RADIO_remnoise = ProtoField.uint8("mavlink_proto.RADIO_remnoise", "remnoise (uint8)")
f.RADIO_rxerrors = ProtoField.uint16("mavlink_proto.RADIO_rxerrors", "rxerrors (uint16)")
f.RADIO_fixed = ProtoField.uint16("mavlink_proto.RADIO_fixed", "fixed (uint16)")

f.LIMITS_STATUS_limits_state = ProtoField.uint8("mavlink_proto.LIMITS_STATUS_limits_state", "limits_state (uint8)")
f.LIMITS_STATUS_last_trigger = ProtoField.uint32("mavlink_proto.LIMITS_STATUS_last_trigger", "last_trigger (uint32)")
f.LIMITS_STATUS_last_action = ProtoField.uint32("mavlink_proto.LIMITS_STATUS_last_action", "last_action (uint32)")
f.LIMITS_STATUS_last_recovery = ProtoField.uint32("mavlink_proto.LIMITS_STATUS_last_recovery", "last_recovery (uint32)")
f.LIMITS_STATUS_last_clear = ProtoField.uint32("mavlink_proto.LIMITS_STATUS_last_clear", "last_clear (uint32)")
f.LIMITS_STATUS_breach_count = ProtoField.uint16("mavlink_proto.LIMITS_STATUS_breach_count", "breach_count (uint16)")
f.LIMITS_STATUS_mods_enabled = ProtoField.uint8("mavlink_proto.LIMITS_STATUS_mods_enabled", "mods_enabled (uint8)")
f.LIMITS_STATUS_mods_required = ProtoField.uint8("mavlink_proto.LIMITS_STATUS_mods_required", "mods_required (uint8)")
f.LIMITS_STATUS_mods_triggered = ProtoField.uint8("mavlink_proto.LIMITS_STATUS_mods_triggered", "mods_triggered (uint8)")

f.WIND_direction = ProtoField.float("mavlink_proto.WIND_direction", "direction (float)")
f.WIND_speed = ProtoField.float("mavlink_proto.WIND_speed", "speed (float)")
f.WIND_speed_z = ProtoField.float("mavlink_proto.WIND_speed_z", "speed_z (float)")

f.DATA16_type = ProtoField.uint8("mavlink_proto.DATA16_type", "type (uint8)")
f.DATA16_len = ProtoField.uint8("mavlink_proto.DATA16_len", "len (uint8)")
f.DATA16_data_0 = ProtoField.uint8("mavlink_proto.DATA16_data_0", "data[0] (uint8)")
f.DATA16_data_1 = ProtoField.uint8("mavlink_proto.DATA16_data_1", "data[1] (uint8)")
f.DATA16_data_2 = ProtoField.uint8("mavlink_proto.DATA16_data_2", "data[2] (uint8)")
f.DATA16_data_3 = ProtoField.uint8("mavlink_proto.DATA16_data_3", "data[3] (uint8)")
f.DATA16_data_4 = ProtoField.uint8("mavlink_proto.DATA16_data_4", "data[4] (uint8)")
f.DATA16_data_5 = ProtoField.uint8("mavlink_proto.DATA16_data_5", "data[5] (uint8)")
f.DATA16_data_6 = ProtoField.uint8("mavlink_proto.DATA16_data_6", "data[6] (uint8)")
f.DATA16_data_7 = ProtoField.uint8("mavlink_proto.DATA16_data_7", "data[7] (uint8)")
f.DATA16_data_8 = ProtoField.uint8("mavlink_proto.DATA16_data_8", "data[8] (uint8)")
f.DATA16_data_9 = ProtoField.uint8("mavlink_proto.DATA16_data_9", "data[9] (uint8)")
f.DATA16_data_10 = ProtoField.uint8("mavlink_proto.DATA16_data_10", "data[10] (uint8)")
f.DATA16_data_11 = ProtoField.uint8("mavlink_proto.DATA16_data_11", "data[11] (uint8)")
f.DATA16_data_12 = ProtoField.uint8("mavlink_proto.DATA16_data_12", "data[12] (uint8)")
f.DATA16_data_13 = ProtoField.uint8("mavlink_proto.DATA16_data_13", "data[13] (uint8)")
f.DATA16_data_14 = ProtoField.uint8("mavlink_proto.DATA16_data_14", "data[14] (uint8)")
f.DATA16_data_15 = ProtoField.uint8("mavlink_proto.DATA16_data_15", "data[15] (uint8)")

f.DATA32_type = ProtoField.uint8("mavlink_proto.DATA32_type", "type (uint8)")
f.DATA32_len = ProtoField.uint8("mavlink_proto.DATA32_len", "len (uint8)")
f.DATA32_data_0 = ProtoField.uint8("mavlink_proto.DATA32_data_0", "data[0] (uint8)")
f.DATA32_data_1 = ProtoField.uint8("mavlink_proto.DATA32_data_1", "data[1] (uint8)")
f.DATA32_data_2 = ProtoField.uint8("mavlink_proto.DATA32_data_2", "data[2] (uint8)")
f.DATA32_data_3 = ProtoField.uint8("mavlink_proto.DATA32_data_3", "data[3] (uint8)")
f.DATA32_data_4 = ProtoField.uint8("mavlink_proto.DATA32_data_4", "data[4] (uint8)")
f.DATA32_data_5 = ProtoField.uint8("mavlink_proto.DATA32_data_5", "data[5] (uint8)")
f.DATA32_data_6 = ProtoField.uint8("mavlink_proto.DATA32_data_6", "data[6] (uint8)")
f.DATA32_data_7 = ProtoField.uint8("mavlink_proto.DATA32_data_7", "data[7] (uint8)")
f.DATA32_data_8 = ProtoField.uint8("mavlink_proto.DATA32_data_8", "data[8] (uint8)")
f.DATA32_data_9 = ProtoField.uint8("mavlink_proto.DATA32_data_9", "data[9] (uint8)")
f.DATA32_data_10 = ProtoField.uint8("mavlink_proto.DATA32_data_10", "data[10] (uint8)")
f.DATA32_data_11 = ProtoField.uint8("mavlink_proto.DATA32_data_11", "data[11] (uint8)")
f.DATA32_data_12 = ProtoField.uint8("mavlink_proto.DATA32_data_12", "data[12] (uint8)")
f.DATA32_data_13 = ProtoField.uint8("mavlink_proto.DATA32_data_13", "data[13] (uint8)")
f.DATA32_data_14 = ProtoField.uint8("mavlink_proto.DATA32_data_14", "data[14] (uint8)")
f.DATA32_data_15 = ProtoField.uint8("mavlink_proto.DATA32_data_15", "data[15] (uint8)")
f.DATA32_data_16 = ProtoField.uint8("mavlink_proto.DATA32_data_16", "data[16] (uint8)")
f.DATA32_data_17 = ProtoField.uint8("mavlink_proto.DATA32_data_17", "data[17] (uint8)")
f.DATA32_data_18 = ProtoField.uint8("mavlink_proto.DATA32_data_18", "data[18] (uint8)")
f.DATA32_data_19 = ProtoField.uint8("mavlink_proto.DATA32_data_19", "data[19] (uint8)")
f.DATA32_data_20 = ProtoField.uint8("mavlink_proto.DATA32_data_20", "data[20] (uint8)")
f.DATA32_data_21 = ProtoField.uint8("mavlink_proto.DATA32_data_21", "data[21] (uint8)")
f.DATA32_data_22 = ProtoField.uint8("mavlink_proto.DATA32_data_22", "data[22] (uint8)")
f.DATA32_data_23 = ProtoField.uint8("mavlink_proto.DATA32_data_23", "data[23] (uint8)")
f.DATA32_data_24 = ProtoField.uint8("mavlink_proto.DATA32_data_24", "data[24] (uint8)")
f.DATA32_data_25 = ProtoField.uint8("mavlink_proto.DATA32_data_25", "data[25] (uint8)")
f.DATA32_data_26 = ProtoField.uint8("mavlink_proto.DATA32_data_26", "data[26] (uint8)")
f.DATA32_data_27 = ProtoField.uint8("mavlink_proto.DATA32_data_27", "data[27] (uint8)")
f.DATA32_data_28 = ProtoField.uint8("mavlink_proto.DATA32_data_28", "data[28] (uint8)")
f.DATA32_data_29 = ProtoField.uint8("mavlink_proto.DATA32_data_29", "data[29] (uint8)")
f.DATA32_data_30 = ProtoField.uint8("mavlink_proto.DATA32_data_30", "data[30] (uint8)")
f.DATA32_data_31 = ProtoField.uint8("mavlink_proto.DATA32_data_31", "data[31] (uint8)")

f.DATA64_type = ProtoField.uint8("mavlink_proto.DATA64_type", "type (uint8)")
f.DATA64_len = ProtoField.uint8("mavlink_proto.DATA64_len", "len (uint8)")
f.DATA64_data_0 = ProtoField.uint8("mavlink_proto.DATA64_data_0", "data[0] (uint8)")
f.DATA64_data_1 = ProtoField.uint8("mavlink_proto.DATA64_data_1", "data[1] (uint8)")
f.DATA64_data_2 = ProtoField.uint8("mavlink_proto.DATA64_data_2", "data[2] (uint8)")
f.DATA64_data_3 = ProtoField.uint8("mavlink_proto.DATA64_data_3", "data[3] (uint8)")
f.DATA64_data_4 = ProtoField.uint8("mavlink_proto.DATA64_data_4", "data[4] (uint8)")
f.DATA64_data_5 = ProtoField.uint8("mavlink_proto.DATA64_data_5", "data[5] (uint8)")
f.DATA64_data_6 = ProtoField.uint8("mavlink_proto.DATA64_data_6", "data[6] (uint8)")
f.DATA64_data_7 = ProtoField.uint8("mavlink_proto.DATA64_data_7", "data[7] (uint8)")
f.DATA64_data_8 = ProtoField.uint8("mavlink_proto.DATA64_data_8", "data[8] (uint8)")
f.DATA64_data_9 = ProtoField.uint8("mavlink_proto.DATA64_data_9", "data[9] (uint8)")
f.DATA64_data_10 = ProtoField.uint8("mavlink_proto.DATA64_data_10", "data[10] (uint8)")
f.DATA64_data_11 = ProtoField.uint8("mavlink_proto.DATA64_data_11", "data[11] (uint8)")
f.DATA64_data_12 = ProtoField.uint8("mavlink_proto.DATA64_data_12", "data[12] (uint8)")
f.DATA64_data_13 = ProtoField.uint8("mavlink_proto.DATA64_data_13", "data[13] (uint8)")
f.DATA64_data_14 = ProtoField.uint8("mavlink_proto.DATA64_data_14", "data[14] (uint8)")
f.DATA64_data_15 = ProtoField.uint8("mavlink_proto.DATA64_data_15", "data[15] (uint8)")
f.DATA64_data_16 = ProtoField.uint8("mavlink_proto.DATA64_data_16", "data[16] (uint8)")
f.DATA64_data_17 = ProtoField.uint8("mavlink_proto.DATA64_data_17", "data[17] (uint8)")
f.DATA64_data_18 = ProtoField.uint8("mavlink_proto.DATA64_data_18", "data[18] (uint8)")
f.DATA64_data_19 = ProtoField.uint8("mavlink_proto.DATA64_data_19", "data[19] (uint8)")
f.DATA64_data_20 = ProtoField.uint8("mavlink_proto.DATA64_data_20", "data[20] (uint8)")
f.DATA64_data_21 = ProtoField.uint8("mavlink_proto.DATA64_data_21", "data[21] (uint8)")
f.DATA64_data_22 = ProtoField.uint8("mavlink_proto.DATA64_data_22", "data[22] (uint8)")
f.DATA64_data_23 = ProtoField.uint8("mavlink_proto.DATA64_data_23", "data[23] (uint8)")
f.DATA64_data_24 = ProtoField.uint8("mavlink_proto.DATA64_data_24", "data[24] (uint8)")
f.DATA64_data_25 = ProtoField.uint8("mavlink_proto.DATA64_data_25", "data[25] (uint8)")
f.DATA64_data_26 = ProtoField.uint8("mavlink_proto.DATA64_data_26", "data[26] (uint8)")
f.DATA64_data_27 = ProtoField.uint8("mavlink_proto.DATA64_data_27", "data[27] (uint8)")
f.DATA64_data_28 = ProtoField.uint8("mavlink_proto.DATA64_data_28", "data[28] (uint8)")
f.DATA64_data_29 = ProtoField.uint8("mavlink_proto.DATA64_data_29", "data[29] (uint8)")
f.DATA64_data_30 = ProtoField.uint8("mavlink_proto.DATA64_data_30", "data[30] (uint8)")
f.DATA64_data_31 = ProtoField.uint8("mavlink_proto.DATA64_data_31", "data[31] (uint8)")
f.DATA64_data_32 = ProtoField.uint8("mavlink_proto.DATA64_data_32", "data[32] (uint8)")
f.DATA64_data_33 = ProtoField.uint8("mavlink_proto.DATA64_data_33", "data[33] (uint8)")
f.DATA64_data_34 = ProtoField.uint8("mavlink_proto.DATA64_data_34", "data[34] (uint8)")
f.DATA64_data_35 = ProtoField.uint8("mavlink_proto.DATA64_data_35", "data[35] (uint8)")
f.DATA64_data_36 = ProtoField.uint8("mavlink_proto.DATA64_data_36", "data[36] (uint8)")
f.DATA64_data_37 = ProtoField.uint8("mavlink_proto.DATA64_data_37", "data[37] (uint8)")
f.DATA64_data_38 = ProtoField.uint8("mavlink_proto.DATA64_data_38", "data[38] (uint8)")
f.DATA64_data_39 = ProtoField.uint8("mavlink_proto.DATA64_data_39", "data[39] (uint8)")
f.DATA64_data_40 = ProtoField.uint8("mavlink_proto.DATA64_data_40", "data[40] (uint8)")
f.DATA64_data_41 = ProtoField.uint8("mavlink_proto.DATA64_data_41", "data[41] (uint8)")
f.DATA64_data_42 = ProtoField.uint8("mavlink_proto.DATA64_data_42", "data[42] (uint8)")
f.DATA64_data_43 = ProtoField.uint8("mavlink_proto.DATA64_data_43", "data[43] (uint8)")
f.DATA64_data_44 = ProtoField.uint8("mavlink_proto.DATA64_data_44", "data[44] (uint8)")
f.DATA64_data_45 = ProtoField.uint8("mavlink_proto.DATA64_data_45", "data[45] (uint8)")
f.DATA64_data_46 = ProtoField.uint8("mavlink_proto.DATA64_data_46", "data[46] (uint8)")
f.DATA64_data_47 = ProtoField.uint8("mavlink_proto.DATA64_data_47", "data[47] (uint8)")
f.DATA64_data_48 = ProtoField.uint8("mavlink_proto.DATA64_data_48", "data[48] (uint8)")
f.DATA64_data_49 = ProtoField.uint8("mavlink_proto.DATA64_data_49", "data[49] (uint8)")
f.DATA64_data_50 = ProtoField.uint8("mavlink_proto.DATA64_data_50", "data[50] (uint8)")
f.DATA64_data_51 = ProtoField.uint8("mavlink_proto.DATA64_data_51", "data[51] (uint8)")
f.DATA64_data_52 = ProtoField.uint8("mavlink_proto.DATA64_data_52", "data[52] (uint8)")
f.DATA64_data_53 = ProtoField.uint8("mavlink_proto.DATA64_data_53", "data[53] (uint8)")
f.DATA64_data_54 = ProtoField.uint8("mavlink_proto.DATA64_data_54", "data[54] (uint8)")
f.DATA64_data_55 = ProtoField.uint8("mavlink_proto.DATA64_data_55", "data[55] (uint8)")
f.DATA64_data_56 = ProtoField.uint8("mavlink_proto.DATA64_data_56", "data[56] (uint8)")
f.DATA64_data_57 = ProtoField.uint8("mavlink_proto.DATA64_data_57", "data[57] (uint8)")
f.DATA64_data_58 = ProtoField.uint8("mavlink_proto.DATA64_data_58", "data[58] (uint8)")
f.DATA64_data_59 = ProtoField.uint8("mavlink_proto.DATA64_data_59", "data[59] (uint8)")
f.DATA64_data_60 = ProtoField.uint8("mavlink_proto.DATA64_data_60", "data[60] (uint8)")
f.DATA64_data_61 = ProtoField.uint8("mavlink_proto.DATA64_data_61", "data[61] (uint8)")
f.DATA64_data_62 = ProtoField.uint8("mavlink_proto.DATA64_data_62", "data[62] (uint8)")
f.DATA64_data_63 = ProtoField.uint8("mavlink_proto.DATA64_data_63", "data[63] (uint8)")

f.DATA96_type = ProtoField.uint8("mavlink_proto.DATA96_type", "type (uint8)")
f.DATA96_len = ProtoField.uint8("mavlink_proto.DATA96_len", "len (uint8)")
f.DATA96_data_0 = ProtoField.uint8("mavlink_proto.DATA96_data_0", "data[0] (uint8)")
f.DATA96_data_1 = ProtoField.uint8("mavlink_proto.DATA96_data_1", "data[1] (uint8)")
f.DATA96_data_2 = ProtoField.uint8("mavlink_proto.DATA96_data_2", "data[2] (uint8)")
f.DATA96_data_3 = ProtoField.uint8("mavlink_proto.DATA96_data_3", "data[3] (uint8)")
f.DATA96_data_4 = ProtoField.uint8("mavlink_proto.DATA96_data_4", "data[4] (uint8)")
f.DATA96_data_5 = ProtoField.uint8("mavlink_proto.DATA96_data_5", "data[5] (uint8)")
f.DATA96_data_6 = ProtoField.uint8("mavlink_proto.DATA96_data_6", "data[6] (uint8)")
f.DATA96_data_7 = ProtoField.uint8("mavlink_proto.DATA96_data_7", "data[7] (uint8)")
f.DATA96_data_8 = ProtoField.uint8("mavlink_proto.DATA96_data_8", "data[8] (uint8)")
f.DATA96_data_9 = ProtoField.uint8("mavlink_proto.DATA96_data_9", "data[9] (uint8)")
f.DATA96_data_10 = ProtoField.uint8("mavlink_proto.DATA96_data_10", "data[10] (uint8)")
f.DATA96_data_11 = ProtoField.uint8("mavlink_proto.DATA96_data_11", "data[11] (uint8)")
f.DATA96_data_12 = ProtoField.uint8("mavlink_proto.DATA96_data_12", "data[12] (uint8)")
f.DATA96_data_13 = ProtoField.uint8("mavlink_proto.DATA96_data_13", "data[13] (uint8)")
f.DATA96_data_14 = ProtoField.uint8("mavlink_proto.DATA96_data_14", "data[14] (uint8)")
f.DATA96_data_15 = ProtoField.uint8("mavlink_proto.DATA96_data_15", "data[15] (uint8)")
f.DATA96_data_16 = ProtoField.uint8("mavlink_proto.DATA96_data_16", "data[16] (uint8)")
f.DATA96_data_17 = ProtoField.uint8("mavlink_proto.DATA96_data_17", "data[17] (uint8)")
f.DATA96_data_18 = ProtoField.uint8("mavlink_proto.DATA96_data_18", "data[18] (uint8)")
f.DATA96_data_19 = ProtoField.uint8("mavlink_proto.DATA96_data_19", "data[19] (uint8)")
f.DATA96_data_20 = ProtoField.uint8("mavlink_proto.DATA96_data_20", "data[20] (uint8)")
f.DATA96_data_21 = ProtoField.uint8("mavlink_proto.DATA96_data_21", "data[21] (uint8)")
f.DATA96_data_22 = ProtoField.uint8("mavlink_proto.DATA96_data_22", "data[22] (uint8)")
f.DATA96_data_23 = ProtoField.uint8("mavlink_proto.DATA96_data_23", "data[23] (uint8)")
f.DATA96_data_24 = ProtoField.uint8("mavlink_proto.DATA96_data_24", "data[24] (uint8)")
f.DATA96_data_25 = ProtoField.uint8("mavlink_proto.DATA96_data_25", "data[25] (uint8)")
f.DATA96_data_26 = ProtoField.uint8("mavlink_proto.DATA96_data_26", "data[26] (uint8)")
f.DATA96_data_27 = ProtoField.uint8("mavlink_proto.DATA96_data_27", "data[27] (uint8)")
f.DATA96_data_28 = ProtoField.uint8("mavlink_proto.DATA96_data_28", "data[28] (uint8)")
f.DATA96_data_29 = ProtoField.uint8("mavlink_proto.DATA96_data_29", "data[29] (uint8)")
f.DATA96_data_30 = ProtoField.uint8("mavlink_proto.DATA96_data_30", "data[30] (uint8)")
f.DATA96_data_31 = ProtoField.uint8("mavlink_proto.DATA96_data_31", "data[31] (uint8)")
f.DATA96_data_32 = ProtoField.uint8("mavlink_proto.DATA96_data_32", "data[32] (uint8)")
f.DATA96_data_33 = ProtoField.uint8("mavlink_proto.DATA96_data_33", "data[33] (uint8)")
f.DATA96_data_34 = ProtoField.uint8("mavlink_proto.DATA96_data_34", "data[34] (uint8)")
f.DATA96_data_35 = ProtoField.uint8("mavlink_proto.DATA96_data_35", "data[35] (uint8)")
f.DATA96_data_36 = ProtoField.uint8("mavlink_proto.DATA96_data_36", "data[36] (uint8)")
f.DATA96_data_37 = ProtoField.uint8("mavlink_proto.DATA96_data_37", "data[37] (uint8)")
f.DATA96_data_38 = ProtoField.uint8("mavlink_proto.DATA96_data_38", "data[38] (uint8)")
f.DATA96_data_39 = ProtoField.uint8("mavlink_proto.DATA96_data_39", "data[39] (uint8)")
f.DATA96_data_40 = ProtoField.uint8("mavlink_proto.DATA96_data_40", "data[40] (uint8)")
f.DATA96_data_41 = ProtoField.uint8("mavlink_proto.DATA96_data_41", "data[41] (uint8)")
f.DATA96_data_42 = ProtoField.uint8("mavlink_proto.DATA96_data_42", "data[42] (uint8)")
f.DATA96_data_43 = ProtoField.uint8("mavlink_proto.DATA96_data_43", "data[43] (uint8)")
f.DATA96_data_44 = ProtoField.uint8("mavlink_proto.DATA96_data_44", "data[44] (uint8)")
f.DATA96_data_45 = ProtoField.uint8("mavlink_proto.DATA96_data_45", "data[45] (uint8)")
f.DATA96_data_46 = ProtoField.uint8("mavlink_proto.DATA96_data_46", "data[46] (uint8)")
f.DATA96_data_47 = ProtoField.uint8("mavlink_proto.DATA96_data_47", "data[47] (uint8)")
f.DATA96_data_48 = ProtoField.uint8("mavlink_proto.DATA96_data_48", "data[48] (uint8)")
f.DATA96_data_49 = ProtoField.uint8("mavlink_proto.DATA96_data_49", "data[49] (uint8)")
f.DATA96_data_50 = ProtoField.uint8("mavlink_proto.DATA96_data_50", "data[50] (uint8)")
f.DATA96_data_51 = ProtoField.uint8("mavlink_proto.DATA96_data_51", "data[51] (uint8)")
f.DATA96_data_52 = ProtoField.uint8("mavlink_proto.DATA96_data_52", "data[52] (uint8)")
f.DATA96_data_53 = ProtoField.uint8("mavlink_proto.DATA96_data_53", "data[53] (uint8)")
f.DATA96_data_54 = ProtoField.uint8("mavlink_proto.DATA96_data_54", "data[54] (uint8)")
f.DATA96_data_55 = ProtoField.uint8("mavlink_proto.DATA96_data_55", "data[55] (uint8)")
f.DATA96_data_56 = ProtoField.uint8("mavlink_proto.DATA96_data_56", "data[56] (uint8)")
f.DATA96_data_57 = ProtoField.uint8("mavlink_proto.DATA96_data_57", "data[57] (uint8)")
f.DATA96_data_58 = ProtoField.uint8("mavlink_proto.DATA96_data_58", "data[58] (uint8)")
f.DATA96_data_59 = ProtoField.uint8("mavlink_proto.DATA96_data_59", "data[59] (uint8)")
f.DATA96_data_60 = ProtoField.uint8("mavlink_proto.DATA96_data_60", "data[60] (uint8)")
f.DATA96_data_61 = ProtoField.uint8("mavlink_proto.DATA96_data_61", "data[61] (uint8)")
f.DATA96_data_62 = ProtoField.uint8("mavlink_proto.DATA96_data_62", "data[62] (uint8)")
f.DATA96_data_63 = ProtoField.uint8("mavlink_proto.DATA96_data_63", "data[63] (uint8)")
f.DATA96_data_64 = ProtoField.uint8("mavlink_proto.DATA96_data_64", "data[64] (uint8)")
f.DATA96_data_65 = ProtoField.uint8("mavlink_proto.DATA96_data_65", "data[65] (uint8)")
f.DATA96_data_66 = ProtoField.uint8("mavlink_proto.DATA96_data_66", "data[66] (uint8)")
f.DATA96_data_67 = ProtoField.uint8("mavlink_proto.DATA96_data_67", "data[67] (uint8)")
f.DATA96_data_68 = ProtoField.uint8("mavlink_proto.DATA96_data_68", "data[68] (uint8)")
f.DATA96_data_69 = ProtoField.uint8("mavlink_proto.DATA96_data_69", "data[69] (uint8)")
f.DATA96_data_70 = ProtoField.uint8("mavlink_proto.DATA96_data_70", "data[70] (uint8)")
f.DATA96_data_71 = ProtoField.uint8("mavlink_proto.DATA96_data_71", "data[71] (uint8)")
f.DATA96_data_72 = ProtoField.uint8("mavlink_proto.DATA96_data_72", "data[72] (uint8)")
f.DATA96_data_73 = ProtoField.uint8("mavlink_proto.DATA96_data_73", "data[73] (uint8)")
f.DATA96_data_74 = ProtoField.uint8("mavlink_proto.DATA96_data_74", "data[74] (uint8)")
f.DATA96_data_75 = ProtoField.uint8("mavlink_proto.DATA96_data_75", "data[75] (uint8)")
f.DATA96_data_76 = ProtoField.uint8("mavlink_proto.DATA96_data_76", "data[76] (uint8)")
f.DATA96_data_77 = ProtoField.uint8("mavlink_proto.DATA96_data_77", "data[77] (uint8)")
f.DATA96_data_78 = ProtoField.uint8("mavlink_proto.DATA96_data_78", "data[78] (uint8)")
f.DATA96_data_79 = ProtoField.uint8("mavlink_proto.DATA96_data_79", "data[79] (uint8)")
f.DATA96_data_80 = ProtoField.uint8("mavlink_proto.DATA96_data_80", "data[80] (uint8)")
f.DATA96_data_81 = ProtoField.uint8("mavlink_proto.DATA96_data_81", "data[81] (uint8)")
f.DATA96_data_82 = ProtoField.uint8("mavlink_proto.DATA96_data_82", "data[82] (uint8)")
f.DATA96_data_83 = ProtoField.uint8("mavlink_proto.DATA96_data_83", "data[83] (uint8)")
f.DATA96_data_84 = ProtoField.uint8("mavlink_proto.DATA96_data_84", "data[84] (uint8)")
f.DATA96_data_85 = ProtoField.uint8("mavlink_proto.DATA96_data_85", "data[85] (uint8)")
f.DATA96_data_86 = ProtoField.uint8("mavlink_proto.DATA96_data_86", "data[86] (uint8)")
f.DATA96_data_87 = ProtoField.uint8("mavlink_proto.DATA96_data_87", "data[87] (uint8)")
f.DATA96_data_88 = ProtoField.uint8("mavlink_proto.DATA96_data_88", "data[88] (uint8)")
f.DATA96_data_89 = ProtoField.uint8("mavlink_proto.DATA96_data_89", "data[89] (uint8)")
f.DATA96_data_90 = ProtoField.uint8("mavlink_proto.DATA96_data_90", "data[90] (uint8)")
f.DATA96_data_91 = ProtoField.uint8("mavlink_proto.DATA96_data_91", "data[91] (uint8)")
f.DATA96_data_92 = ProtoField.uint8("mavlink_proto.DATA96_data_92", "data[92] (uint8)")
f.DATA96_data_93 = ProtoField.uint8("mavlink_proto.DATA96_data_93", "data[93] (uint8)")
f.DATA96_data_94 = ProtoField.uint8("mavlink_proto.DATA96_data_94", "data[94] (uint8)")
f.DATA96_data_95 = ProtoField.uint8("mavlink_proto.DATA96_data_95", "data[95] (uint8)")

f.RANGEFINDER_distance = ProtoField.float("mavlink_proto.RANGEFINDER_distance", "distance (float)")
f.RANGEFINDER_voltage = ProtoField.float("mavlink_proto.RANGEFINDER_voltage", "voltage (float)")

f.AIRSPEED_AUTOCAL_vx = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_vx", "vx (float)")
f.AIRSPEED_AUTOCAL_vy = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_vy", "vy (float)")
f.AIRSPEED_AUTOCAL_vz = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_vz", "vz (float)")
f.AIRSPEED_AUTOCAL_diff_pressure = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_diff_pressure", "diff_pressure (float)")
f.AIRSPEED_AUTOCAL_EAS2TAS = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_EAS2TAS", "EAS2TAS (float)")
f.AIRSPEED_AUTOCAL_ratio = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_ratio", "ratio (float)")
f.AIRSPEED_AUTOCAL_state_x = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_state_x", "state_x (float)")
f.AIRSPEED_AUTOCAL_state_y = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_state_y", "state_y (float)")
f.AIRSPEED_AUTOCAL_state_z = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_state_z", "state_z (float)")
f.AIRSPEED_AUTOCAL_Pax = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_Pax", "Pax (float)")
f.AIRSPEED_AUTOCAL_Pby = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_Pby", "Pby (float)")
f.AIRSPEED_AUTOCAL_Pcz = ProtoField.float("mavlink_proto.AIRSPEED_AUTOCAL_Pcz", "Pcz (float)")

f.RALLY_POINT_target_system = ProtoField.uint8("mavlink_proto.RALLY_POINT_target_system", "target_system (uint8)")
f.RALLY_POINT_target_component = ProtoField.uint8("mavlink_proto.RALLY_POINT_target_component", "target_component (uint8)")
f.RALLY_POINT_idx = ProtoField.uint8("mavlink_proto.RALLY_POINT_idx", "idx (uint8)")
f.RALLY_POINT_count = ProtoField.uint8("mavlink_proto.RALLY_POINT_count", "count (uint8)")
f.RALLY_POINT_lat = ProtoField.int32("mavlink_proto.RALLY_POINT_lat", "lat (int32)")
f.RALLY_POINT_lng = ProtoField.int32("mavlink_proto.RALLY_POINT_lng", "lng (int32)")
f.RALLY_POINT_alt = ProtoField.int16("mavlink_proto.RALLY_POINT_alt", "alt (int16)")
f.RALLY_POINT_break_alt = ProtoField.int16("mavlink_proto.RALLY_POINT_break_alt", "break_alt (int16)")
f.RALLY_POINT_land_dir = ProtoField.uint16("mavlink_proto.RALLY_POINT_land_dir", "land_dir (uint16)")
f.RALLY_POINT_flags = ProtoField.uint8("mavlink_proto.RALLY_POINT_flags", "flags (uint8)")

f.RALLY_FETCH_POINT_target_system = ProtoField.uint8("mavlink_proto.RALLY_FETCH_POINT_target_system", "target_system (uint8)")
f.RALLY_FETCH_POINT_target_component = ProtoField.uint8("mavlink_proto.RALLY_FETCH_POINT_target_component", "target_component (uint8)")
f.RALLY_FETCH_POINT_idx = ProtoField.uint8("mavlink_proto.RALLY_FETCH_POINT_idx", "idx (uint8)")

f.COMPASSMOT_STATUS_throttle = ProtoField.uint16("mavlink_proto.COMPASSMOT_STATUS_throttle", "throttle (uint16)")
f.COMPASSMOT_STATUS_current = ProtoField.float("mavlink_proto.COMPASSMOT_STATUS_current", "current (float)")
f.COMPASSMOT_STATUS_interference = ProtoField.uint16("mavlink_proto.COMPASSMOT_STATUS_interference", "interference (uint16)")
f.COMPASSMOT_STATUS_CompensationX = ProtoField.float("mavlink_proto.COMPASSMOT_STATUS_CompensationX", "CompensationX (float)")
f.COMPASSMOT_STATUS_CompensationY = ProtoField.float("mavlink_proto.COMPASSMOT_STATUS_CompensationY", "CompensationY (float)")
f.COMPASSMOT_STATUS_CompensationZ = ProtoField.float("mavlink_proto.COMPASSMOT_STATUS_CompensationZ", "CompensationZ (float)")

f.AHRS2_roll = ProtoField.float("mavlink_proto.AHRS2_roll", "roll (float)")
f.AHRS2_pitch = ProtoField.float("mavlink_proto.AHRS2_pitch", "pitch (float)")
f.AHRS2_yaw = ProtoField.float("mavlink_proto.AHRS2_yaw", "yaw (float)")
f.AHRS2_altitude = ProtoField.float("mavlink_proto.AHRS2_altitude", "altitude (float)")
f.AHRS2_lat = ProtoField.int32("mavlink_proto.AHRS2_lat", "lat (int32)")
f.AHRS2_lng = ProtoField.int32("mavlink_proto.AHRS2_lng", "lng (int32)")

f.CAMERA_STATUS_time_usec = ProtoField.uint64("mavlink_proto.CAMERA_STATUS_time_usec", "time_usec (uint64)")
f.CAMERA_STATUS_target_system = ProtoField.uint8("mavlink_proto.CAMERA_STATUS_target_system", "target_system (uint8)")
f.CAMERA_STATUS_cam_idx = ProtoField.uint8("mavlink_proto.CAMERA_STATUS_cam_idx", "cam_idx (uint8)")
f.CAMERA_STATUS_img_idx = ProtoField.uint16("mavlink_proto.CAMERA_STATUS_img_idx", "img_idx (uint16)")
f.CAMERA_STATUS_event_id = ProtoField.uint8("mavlink_proto.CAMERA_STATUS_event_id", "event_id (uint8)")
f.CAMERA_STATUS_p1 = ProtoField.float("mavlink_proto.CAMERA_STATUS_p1", "p1 (float)")
f.CAMERA_STATUS_p2 = ProtoField.float("mavlink_proto.CAMERA_STATUS_p2", "p2 (float)")
f.CAMERA_STATUS_p3 = ProtoField.float("mavlink_proto.CAMERA_STATUS_p3", "p3 (float)")
f.CAMERA_STATUS_p4 = ProtoField.float("mavlink_proto.CAMERA_STATUS_p4", "p4 (float)")

f.CAMERA_FEEDBACK_time_usec = ProtoField.uint64("mavlink_proto.CAMERA_FEEDBACK_time_usec", "time_usec (uint64)")
f.CAMERA_FEEDBACK_target_system = ProtoField.uint8("mavlink_proto.CAMERA_FEEDBACK_target_system", "target_system (uint8)")
f.CAMERA_FEEDBACK_cam_idx = ProtoField.uint8("mavlink_proto.CAMERA_FEEDBACK_cam_idx", "cam_idx (uint8)")
f.CAMERA_FEEDBACK_img_idx = ProtoField.uint16("mavlink_proto.CAMERA_FEEDBACK_img_idx", "img_idx (uint16)")
f.CAMERA_FEEDBACK_lat = ProtoField.int32("mavlink_proto.CAMERA_FEEDBACK_lat", "lat (int32)")
f.CAMERA_FEEDBACK_lng = ProtoField.int32("mavlink_proto.CAMERA_FEEDBACK_lng", "lng (int32)")
f.CAMERA_FEEDBACK_alt_msl = ProtoField.float("mavlink_proto.CAMERA_FEEDBACK_alt_msl", "alt_msl (float)")
f.CAMERA_FEEDBACK_alt_rel = ProtoField.float("mavlink_proto.CAMERA_FEEDBACK_alt_rel", "alt_rel (float)")
f.CAMERA_FEEDBACK_roll = ProtoField.float("mavlink_proto.CAMERA_FEEDBACK_roll", "roll (float)")
f.CAMERA_FEEDBACK_pitch = ProtoField.float("mavlink_proto.CAMERA_FEEDBACK_pitch", "pitch (float)")
f.CAMERA_FEEDBACK_yaw = ProtoField.float("mavlink_proto.CAMERA_FEEDBACK_yaw", "yaw (float)")
f.CAMERA_FEEDBACK_foc_len = ProtoField.float("mavlink_proto.CAMERA_FEEDBACK_foc_len", "foc_len (float)")
f.CAMERA_FEEDBACK_flags = ProtoField.uint8("mavlink_proto.CAMERA_FEEDBACK_flags", "flags (uint8)")

f.BATTERY2_voltage = ProtoField.uint16("mavlink_proto.BATTERY2_voltage", "voltage (uint16)")
f.BATTERY2_current_battery = ProtoField.int16("mavlink_proto.BATTERY2_current_battery", "current_battery (int16)")

f.AHRS3_roll = ProtoField.float("mavlink_proto.AHRS3_roll", "roll (float)")
f.AHRS3_pitch = ProtoField.float("mavlink_proto.AHRS3_pitch", "pitch (float)")
f.AHRS3_yaw = ProtoField.float("mavlink_proto.AHRS3_yaw", "yaw (float)")
f.AHRS3_altitude = ProtoField.float("mavlink_proto.AHRS3_altitude", "altitude (float)")
f.AHRS3_lat = ProtoField.int32("mavlink_proto.AHRS3_lat", "lat (int32)")
f.AHRS3_lng = ProtoField.int32("mavlink_proto.AHRS3_lng", "lng (int32)")
f.AHRS3_v1 = ProtoField.float("mavlink_proto.AHRS3_v1", "v1 (float)")
f.AHRS3_v2 = ProtoField.float("mavlink_proto.AHRS3_v2", "v2 (float)")
f.AHRS3_v3 = ProtoField.float("mavlink_proto.AHRS3_v3", "v3 (float)")
f.AHRS3_v4 = ProtoField.float("mavlink_proto.AHRS3_v4", "v4 (float)")

f.AUTOPILOT_VERSION_REQUEST_target_system = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_REQUEST_target_system", "target_system (uint8)")
f.AUTOPILOT_VERSION_REQUEST_target_component = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_REQUEST_target_component", "target_component (uint8)")

f.REMOTE_LOG_DATA_BLOCK_target_system = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_target_system", "target_system (uint8)")
f.REMOTE_LOG_DATA_BLOCK_target_component = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_target_component", "target_component (uint8)")
f.REMOTE_LOG_DATA_BLOCK_seqno = ProtoField.uint32("mavlink_proto.REMOTE_LOG_DATA_BLOCK_seqno", "seqno (uint32)")
f.REMOTE_LOG_DATA_BLOCK_data_0 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_0", "data[0] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_1 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_1", "data[1] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_2 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_2", "data[2] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_3 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_3", "data[3] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_4 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_4", "data[4] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_5 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_5", "data[5] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_6 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_6", "data[6] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_7 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_7", "data[7] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_8 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_8", "data[8] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_9 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_9", "data[9] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_10 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_10", "data[10] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_11 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_11", "data[11] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_12 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_12", "data[12] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_13 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_13", "data[13] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_14 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_14", "data[14] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_15 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_15", "data[15] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_16 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_16", "data[16] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_17 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_17", "data[17] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_18 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_18", "data[18] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_19 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_19", "data[19] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_20 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_20", "data[20] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_21 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_21", "data[21] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_22 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_22", "data[22] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_23 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_23", "data[23] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_24 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_24", "data[24] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_25 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_25", "data[25] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_26 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_26", "data[26] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_27 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_27", "data[27] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_28 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_28", "data[28] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_29 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_29", "data[29] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_30 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_30", "data[30] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_31 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_31", "data[31] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_32 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_32", "data[32] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_33 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_33", "data[33] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_34 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_34", "data[34] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_35 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_35", "data[35] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_36 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_36", "data[36] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_37 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_37", "data[37] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_38 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_38", "data[38] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_39 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_39", "data[39] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_40 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_40", "data[40] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_41 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_41", "data[41] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_42 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_42", "data[42] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_43 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_43", "data[43] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_44 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_44", "data[44] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_45 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_45", "data[45] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_46 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_46", "data[46] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_47 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_47", "data[47] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_48 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_48", "data[48] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_49 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_49", "data[49] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_50 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_50", "data[50] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_51 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_51", "data[51] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_52 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_52", "data[52] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_53 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_53", "data[53] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_54 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_54", "data[54] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_55 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_55", "data[55] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_56 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_56", "data[56] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_57 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_57", "data[57] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_58 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_58", "data[58] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_59 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_59", "data[59] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_60 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_60", "data[60] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_61 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_61", "data[61] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_62 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_62", "data[62] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_63 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_63", "data[63] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_64 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_64", "data[64] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_65 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_65", "data[65] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_66 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_66", "data[66] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_67 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_67", "data[67] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_68 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_68", "data[68] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_69 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_69", "data[69] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_70 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_70", "data[70] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_71 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_71", "data[71] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_72 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_72", "data[72] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_73 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_73", "data[73] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_74 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_74", "data[74] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_75 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_75", "data[75] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_76 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_76", "data[76] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_77 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_77", "data[77] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_78 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_78", "data[78] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_79 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_79", "data[79] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_80 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_80", "data[80] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_81 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_81", "data[81] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_82 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_82", "data[82] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_83 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_83", "data[83] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_84 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_84", "data[84] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_85 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_85", "data[85] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_86 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_86", "data[86] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_87 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_87", "data[87] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_88 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_88", "data[88] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_89 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_89", "data[89] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_90 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_90", "data[90] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_91 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_91", "data[91] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_92 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_92", "data[92] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_93 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_93", "data[93] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_94 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_94", "data[94] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_95 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_95", "data[95] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_96 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_96", "data[96] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_97 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_97", "data[97] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_98 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_98", "data[98] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_99 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_99", "data[99] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_100 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_100", "data[100] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_101 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_101", "data[101] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_102 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_102", "data[102] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_103 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_103", "data[103] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_104 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_104", "data[104] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_105 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_105", "data[105] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_106 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_106", "data[106] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_107 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_107", "data[107] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_108 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_108", "data[108] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_109 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_109", "data[109] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_110 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_110", "data[110] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_111 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_111", "data[111] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_112 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_112", "data[112] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_113 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_113", "data[113] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_114 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_114", "data[114] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_115 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_115", "data[115] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_116 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_116", "data[116] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_117 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_117", "data[117] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_118 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_118", "data[118] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_119 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_119", "data[119] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_120 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_120", "data[120] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_121 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_121", "data[121] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_122 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_122", "data[122] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_123 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_123", "data[123] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_124 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_124", "data[124] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_125 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_125", "data[125] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_126 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_126", "data[126] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_127 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_127", "data[127] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_128 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_128", "data[128] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_129 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_129", "data[129] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_130 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_130", "data[130] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_131 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_131", "data[131] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_132 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_132", "data[132] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_133 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_133", "data[133] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_134 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_134", "data[134] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_135 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_135", "data[135] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_136 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_136", "data[136] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_137 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_137", "data[137] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_138 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_138", "data[138] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_139 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_139", "data[139] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_140 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_140", "data[140] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_141 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_141", "data[141] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_142 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_142", "data[142] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_143 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_143", "data[143] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_144 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_144", "data[144] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_145 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_145", "data[145] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_146 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_146", "data[146] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_147 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_147", "data[147] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_148 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_148", "data[148] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_149 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_149", "data[149] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_150 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_150", "data[150] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_151 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_151", "data[151] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_152 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_152", "data[152] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_153 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_153", "data[153] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_154 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_154", "data[154] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_155 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_155", "data[155] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_156 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_156", "data[156] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_157 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_157", "data[157] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_158 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_158", "data[158] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_159 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_159", "data[159] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_160 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_160", "data[160] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_161 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_161", "data[161] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_162 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_162", "data[162] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_163 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_163", "data[163] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_164 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_164", "data[164] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_165 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_165", "data[165] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_166 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_166", "data[166] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_167 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_167", "data[167] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_168 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_168", "data[168] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_169 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_169", "data[169] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_170 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_170", "data[170] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_171 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_171", "data[171] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_172 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_172", "data[172] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_173 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_173", "data[173] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_174 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_174", "data[174] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_175 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_175", "data[175] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_176 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_176", "data[176] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_177 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_177", "data[177] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_178 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_178", "data[178] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_179 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_179", "data[179] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_180 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_180", "data[180] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_181 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_181", "data[181] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_182 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_182", "data[182] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_183 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_183", "data[183] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_184 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_184", "data[184] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_185 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_185", "data[185] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_186 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_186", "data[186] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_187 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_187", "data[187] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_188 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_188", "data[188] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_189 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_189", "data[189] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_190 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_190", "data[190] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_191 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_191", "data[191] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_192 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_192", "data[192] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_193 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_193", "data[193] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_194 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_194", "data[194] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_195 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_195", "data[195] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_196 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_196", "data[196] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_197 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_197", "data[197] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_198 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_198", "data[198] (uint8)")
f.REMOTE_LOG_DATA_BLOCK_data_199 = ProtoField.uint8("mavlink_proto.REMOTE_LOG_DATA_BLOCK_data_199", "data[199] (uint8)")

f.REMOTE_LOG_BLOCK_STATUS_target_system = ProtoField.uint8("mavlink_proto.REMOTE_LOG_BLOCK_STATUS_target_system", "target_system (uint8)")
f.REMOTE_LOG_BLOCK_STATUS_target_component = ProtoField.uint8("mavlink_proto.REMOTE_LOG_BLOCK_STATUS_target_component", "target_component (uint8)")
f.REMOTE_LOG_BLOCK_STATUS_seqno = ProtoField.uint32("mavlink_proto.REMOTE_LOG_BLOCK_STATUS_seqno", "seqno (uint32)")
f.REMOTE_LOG_BLOCK_STATUS_status = ProtoField.uint8("mavlink_proto.REMOTE_LOG_BLOCK_STATUS_status", "status (uint8)")

f.LED_CONTROL_target_system = ProtoField.uint8("mavlink_proto.LED_CONTROL_target_system", "target_system (uint8)")
f.LED_CONTROL_target_component = ProtoField.uint8("mavlink_proto.LED_CONTROL_target_component", "target_component (uint8)")
f.LED_CONTROL_instance = ProtoField.uint8("mavlink_proto.LED_CONTROL_instance", "instance (uint8)")
f.LED_CONTROL_pattern = ProtoField.uint8("mavlink_proto.LED_CONTROL_pattern", "pattern (uint8)")
f.LED_CONTROL_custom_len = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_len", "custom_len (uint8)")
f.LED_CONTROL_custom_bytes_0 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_0", "custom_bytes[0] (uint8)")
f.LED_CONTROL_custom_bytes_1 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_1", "custom_bytes[1] (uint8)")
f.LED_CONTROL_custom_bytes_2 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_2", "custom_bytes[2] (uint8)")
f.LED_CONTROL_custom_bytes_3 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_3", "custom_bytes[3] (uint8)")
f.LED_CONTROL_custom_bytes_4 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_4", "custom_bytes[4] (uint8)")
f.LED_CONTROL_custom_bytes_5 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_5", "custom_bytes[5] (uint8)")
f.LED_CONTROL_custom_bytes_6 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_6", "custom_bytes[6] (uint8)")
f.LED_CONTROL_custom_bytes_7 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_7", "custom_bytes[7] (uint8)")
f.LED_CONTROL_custom_bytes_8 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_8", "custom_bytes[8] (uint8)")
f.LED_CONTROL_custom_bytes_9 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_9", "custom_bytes[9] (uint8)")
f.LED_CONTROL_custom_bytes_10 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_10", "custom_bytes[10] (uint8)")
f.LED_CONTROL_custom_bytes_11 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_11", "custom_bytes[11] (uint8)")
f.LED_CONTROL_custom_bytes_12 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_12", "custom_bytes[12] (uint8)")
f.LED_CONTROL_custom_bytes_13 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_13", "custom_bytes[13] (uint8)")
f.LED_CONTROL_custom_bytes_14 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_14", "custom_bytes[14] (uint8)")
f.LED_CONTROL_custom_bytes_15 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_15", "custom_bytes[15] (uint8)")
f.LED_CONTROL_custom_bytes_16 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_16", "custom_bytes[16] (uint8)")
f.LED_CONTROL_custom_bytes_17 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_17", "custom_bytes[17] (uint8)")
f.LED_CONTROL_custom_bytes_18 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_18", "custom_bytes[18] (uint8)")
f.LED_CONTROL_custom_bytes_19 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_19", "custom_bytes[19] (uint8)")
f.LED_CONTROL_custom_bytes_20 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_20", "custom_bytes[20] (uint8)")
f.LED_CONTROL_custom_bytes_21 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_21", "custom_bytes[21] (uint8)")
f.LED_CONTROL_custom_bytes_22 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_22", "custom_bytes[22] (uint8)")
f.LED_CONTROL_custom_bytes_23 = ProtoField.uint8("mavlink_proto.LED_CONTROL_custom_bytes_23", "custom_bytes[23] (uint8)")

f.MAG_CAL_PROGRESS_compass_id = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_compass_id", "compass_id (uint8)")
f.MAG_CAL_PROGRESS_cal_mask = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_cal_mask", "cal_mask (uint8)")
f.MAG_CAL_PROGRESS_cal_status = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_cal_status", "cal_status (uint8)")
f.MAG_CAL_PROGRESS_attempt = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_attempt", "attempt (uint8)")
f.MAG_CAL_PROGRESS_completion_pct = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_pct", "completion_pct (uint8)")
f.MAG_CAL_PROGRESS_completion_mask_0 = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_mask_0", "completion_mask[0] (uint8)")
f.MAG_CAL_PROGRESS_completion_mask_1 = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_mask_1", "completion_mask[1] (uint8)")
f.MAG_CAL_PROGRESS_completion_mask_2 = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_mask_2", "completion_mask[2] (uint8)")
f.MAG_CAL_PROGRESS_completion_mask_3 = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_mask_3", "completion_mask[3] (uint8)")
f.MAG_CAL_PROGRESS_completion_mask_4 = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_mask_4", "completion_mask[4] (uint8)")
f.MAG_CAL_PROGRESS_completion_mask_5 = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_mask_5", "completion_mask[5] (uint8)")
f.MAG_CAL_PROGRESS_completion_mask_6 = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_mask_6", "completion_mask[6] (uint8)")
f.MAG_CAL_PROGRESS_completion_mask_7 = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_mask_7", "completion_mask[7] (uint8)")
f.MAG_CAL_PROGRESS_completion_mask_8 = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_mask_8", "completion_mask[8] (uint8)")
f.MAG_CAL_PROGRESS_completion_mask_9 = ProtoField.uint8("mavlink_proto.MAG_CAL_PROGRESS_completion_mask_9", "completion_mask[9] (uint8)")
f.MAG_CAL_PROGRESS_direction_x = ProtoField.float("mavlink_proto.MAG_CAL_PROGRESS_direction_x", "direction_x (float)")
f.MAG_CAL_PROGRESS_direction_y = ProtoField.float("mavlink_proto.MAG_CAL_PROGRESS_direction_y", "direction_y (float)")
f.MAG_CAL_PROGRESS_direction_z = ProtoField.float("mavlink_proto.MAG_CAL_PROGRESS_direction_z", "direction_z (float)")

f.MAG_CAL_REPORT_compass_id = ProtoField.uint8("mavlink_proto.MAG_CAL_REPORT_compass_id", "compass_id (uint8)")
f.MAG_CAL_REPORT_cal_mask = ProtoField.uint8("mavlink_proto.MAG_CAL_REPORT_cal_mask", "cal_mask (uint8)")
f.MAG_CAL_REPORT_cal_status = ProtoField.uint8("mavlink_proto.MAG_CAL_REPORT_cal_status", "cal_status (uint8)")
f.MAG_CAL_REPORT_autosaved = ProtoField.uint8("mavlink_proto.MAG_CAL_REPORT_autosaved", "autosaved (uint8)")
f.MAG_CAL_REPORT_fitness = ProtoField.float("mavlink_proto.MAG_CAL_REPORT_fitness", "fitness (float)")
f.MAG_CAL_REPORT_ofs_x = ProtoField.float("mavlink_proto.MAG_CAL_REPORT_ofs_x", "ofs_x (float)")
f.MAG_CAL_REPORT_ofs_y = ProtoField.float("mavlink_proto.MAG_CAL_REPORT_ofs_y", "ofs_y (float)")
f.MAG_CAL_REPORT_ofs_z = ProtoField.float("mavlink_proto.MAG_CAL_REPORT_ofs_z", "ofs_z (float)")
f.MAG_CAL_REPORT_diag_x = ProtoField.float("mavlink_proto.MAG_CAL_REPORT_diag_x", "diag_x (float)")
f.MAG_CAL_REPORT_diag_y = ProtoField.float("mavlink_proto.MAG_CAL_REPORT_diag_y", "diag_y (float)")
f.MAG_CAL_REPORT_diag_z = ProtoField.float("mavlink_proto.MAG_CAL_REPORT_diag_z", "diag_z (float)")
f.MAG_CAL_REPORT_offdiag_x = ProtoField.float("mavlink_proto.MAG_CAL_REPORT_offdiag_x", "offdiag_x (float)")
f.MAG_CAL_REPORT_offdiag_y = ProtoField.float("mavlink_proto.MAG_CAL_REPORT_offdiag_y", "offdiag_y (float)")
f.MAG_CAL_REPORT_offdiag_z = ProtoField.float("mavlink_proto.MAG_CAL_REPORT_offdiag_z", "offdiag_z (float)")

f.EKF_STATUS_REPORT_flags = ProtoField.uint16("mavlink_proto.EKF_STATUS_REPORT_flags", "flags (uint16)")
f.EKF_STATUS_REPORT_velocity_variance = ProtoField.float("mavlink_proto.EKF_STATUS_REPORT_velocity_variance", "velocity_variance (float)")
f.EKF_STATUS_REPORT_pos_horiz_variance = ProtoField.float("mavlink_proto.EKF_STATUS_REPORT_pos_horiz_variance", "pos_horiz_variance (float)")
f.EKF_STATUS_REPORT_pos_vert_variance = ProtoField.float("mavlink_proto.EKF_STATUS_REPORT_pos_vert_variance", "pos_vert_variance (float)")
f.EKF_STATUS_REPORT_compass_variance = ProtoField.float("mavlink_proto.EKF_STATUS_REPORT_compass_variance", "compass_variance (float)")
f.EKF_STATUS_REPORT_terrain_alt_variance = ProtoField.float("mavlink_proto.EKF_STATUS_REPORT_terrain_alt_variance", "terrain_alt_variance (float)")

f.PID_TUNING_axis = ProtoField.uint8("mavlink_proto.PID_TUNING_axis", "axis (uint8)")
f.PID_TUNING_desired = ProtoField.float("mavlink_proto.PID_TUNING_desired", "desired (float)")
f.PID_TUNING_achieved = ProtoField.float("mavlink_proto.PID_TUNING_achieved", "achieved (float)")
f.PID_TUNING_FF = ProtoField.float("mavlink_proto.PID_TUNING_FF", "FF (float)")
f.PID_TUNING_P = ProtoField.float("mavlink_proto.PID_TUNING_P", "P (float)")
f.PID_TUNING_I = ProtoField.float("mavlink_proto.PID_TUNING_I", "I (float)")
f.PID_TUNING_D = ProtoField.float("mavlink_proto.PID_TUNING_D", "D (float)")

f.GIMBAL_REPORT_target_system = ProtoField.uint8("mavlink_proto.GIMBAL_REPORT_target_system", "target_system (uint8)")
f.GIMBAL_REPORT_target_component = ProtoField.uint8("mavlink_proto.GIMBAL_REPORT_target_component", "target_component (uint8)")
f.GIMBAL_REPORT_delta_time = ProtoField.float("mavlink_proto.GIMBAL_REPORT_delta_time", "delta_time (float)")
f.GIMBAL_REPORT_delta_angle_x = ProtoField.float("mavlink_proto.GIMBAL_REPORT_delta_angle_x", "delta_angle_x (float)")
f.GIMBAL_REPORT_delta_angle_y = ProtoField.float("mavlink_proto.GIMBAL_REPORT_delta_angle_y", "delta_angle_y (float)")
f.GIMBAL_REPORT_delta_angle_z = ProtoField.float("mavlink_proto.GIMBAL_REPORT_delta_angle_z", "delta_angle_z (float)")
f.GIMBAL_REPORT_delta_velocity_x = ProtoField.float("mavlink_proto.GIMBAL_REPORT_delta_velocity_x", "delta_velocity_x (float)")
f.GIMBAL_REPORT_delta_velocity_y = ProtoField.float("mavlink_proto.GIMBAL_REPORT_delta_velocity_y", "delta_velocity_y (float)")
f.GIMBAL_REPORT_delta_velocity_z = ProtoField.float("mavlink_proto.GIMBAL_REPORT_delta_velocity_z", "delta_velocity_z (float)")
f.GIMBAL_REPORT_joint_roll = ProtoField.float("mavlink_proto.GIMBAL_REPORT_joint_roll", "joint_roll (float)")
f.GIMBAL_REPORT_joint_el = ProtoField.float("mavlink_proto.GIMBAL_REPORT_joint_el", "joint_el (float)")
f.GIMBAL_REPORT_joint_az = ProtoField.float("mavlink_proto.GIMBAL_REPORT_joint_az", "joint_az (float)")

f.GIMBAL_CONTROL_target_system = ProtoField.uint8("mavlink_proto.GIMBAL_CONTROL_target_system", "target_system (uint8)")
f.GIMBAL_CONTROL_target_component = ProtoField.uint8("mavlink_proto.GIMBAL_CONTROL_target_component", "target_component (uint8)")
f.GIMBAL_CONTROL_demanded_rate_x = ProtoField.float("mavlink_proto.GIMBAL_CONTROL_demanded_rate_x", "demanded_rate_x (float)")
f.GIMBAL_CONTROL_demanded_rate_y = ProtoField.float("mavlink_proto.GIMBAL_CONTROL_demanded_rate_y", "demanded_rate_y (float)")
f.GIMBAL_CONTROL_demanded_rate_z = ProtoField.float("mavlink_proto.GIMBAL_CONTROL_demanded_rate_z", "demanded_rate_z (float)")

f.GIMBAL_TORQUE_CMD_REPORT_target_system = ProtoField.uint8("mavlink_proto.GIMBAL_TORQUE_CMD_REPORT_target_system", "target_system (uint8)")
f.GIMBAL_TORQUE_CMD_REPORT_target_component = ProtoField.uint8("mavlink_proto.GIMBAL_TORQUE_CMD_REPORT_target_component", "target_component (uint8)")
f.GIMBAL_TORQUE_CMD_REPORT_rl_torque_cmd = ProtoField.int16("mavlink_proto.GIMBAL_TORQUE_CMD_REPORT_rl_torque_cmd", "rl_torque_cmd (int16)")
f.GIMBAL_TORQUE_CMD_REPORT_el_torque_cmd = ProtoField.int16("mavlink_proto.GIMBAL_TORQUE_CMD_REPORT_el_torque_cmd", "el_torque_cmd (int16)")
f.GIMBAL_TORQUE_CMD_REPORT_az_torque_cmd = ProtoField.int16("mavlink_proto.GIMBAL_TORQUE_CMD_REPORT_az_torque_cmd", "az_torque_cmd (int16)")

f.GOPRO_HEARTBEAT_status = ProtoField.uint8("mavlink_proto.GOPRO_HEARTBEAT_status", "status (uint8)")
f.GOPRO_HEARTBEAT_capture_mode = ProtoField.uint8("mavlink_proto.GOPRO_HEARTBEAT_capture_mode", "capture_mode (uint8)")
f.GOPRO_HEARTBEAT_flags = ProtoField.uint8("mavlink_proto.GOPRO_HEARTBEAT_flags", "flags (uint8)")

f.GOPRO_GET_REQUEST_target_system = ProtoField.uint8("mavlink_proto.GOPRO_GET_REQUEST_target_system", "target_system (uint8)")
f.GOPRO_GET_REQUEST_target_component = ProtoField.uint8("mavlink_proto.GOPRO_GET_REQUEST_target_component", "target_component (uint8)")
f.GOPRO_GET_REQUEST_cmd_id = ProtoField.uint8("mavlink_proto.GOPRO_GET_REQUEST_cmd_id", "cmd_id (uint8)")

f.GOPRO_GET_RESPONSE_cmd_id = ProtoField.uint8("mavlink_proto.GOPRO_GET_RESPONSE_cmd_id", "cmd_id (uint8)")
f.GOPRO_GET_RESPONSE_status = ProtoField.uint8("mavlink_proto.GOPRO_GET_RESPONSE_status", "status (uint8)")
f.GOPRO_GET_RESPONSE_value_0 = ProtoField.uint8("mavlink_proto.GOPRO_GET_RESPONSE_value_0", "value[0] (uint8)")
f.GOPRO_GET_RESPONSE_value_1 = ProtoField.uint8("mavlink_proto.GOPRO_GET_RESPONSE_value_1", "value[1] (uint8)")
f.GOPRO_GET_RESPONSE_value_2 = ProtoField.uint8("mavlink_proto.GOPRO_GET_RESPONSE_value_2", "value[2] (uint8)")
f.GOPRO_GET_RESPONSE_value_3 = ProtoField.uint8("mavlink_proto.GOPRO_GET_RESPONSE_value_3", "value[3] (uint8)")

f.GOPRO_SET_REQUEST_target_system = ProtoField.uint8("mavlink_proto.GOPRO_SET_REQUEST_target_system", "target_system (uint8)")
f.GOPRO_SET_REQUEST_target_component = ProtoField.uint8("mavlink_proto.GOPRO_SET_REQUEST_target_component", "target_component (uint8)")
f.GOPRO_SET_REQUEST_cmd_id = ProtoField.uint8("mavlink_proto.GOPRO_SET_REQUEST_cmd_id", "cmd_id (uint8)")
f.GOPRO_SET_REQUEST_value_0 = ProtoField.uint8("mavlink_proto.GOPRO_SET_REQUEST_value_0", "value[0] (uint8)")
f.GOPRO_SET_REQUEST_value_1 = ProtoField.uint8("mavlink_proto.GOPRO_SET_REQUEST_value_1", "value[1] (uint8)")
f.GOPRO_SET_REQUEST_value_2 = ProtoField.uint8("mavlink_proto.GOPRO_SET_REQUEST_value_2", "value[2] (uint8)")
f.GOPRO_SET_REQUEST_value_3 = ProtoField.uint8("mavlink_proto.GOPRO_SET_REQUEST_value_3", "value[3] (uint8)")

f.GOPRO_SET_RESPONSE_cmd_id = ProtoField.uint8("mavlink_proto.GOPRO_SET_RESPONSE_cmd_id", "cmd_id (uint8)")
f.GOPRO_SET_RESPONSE_status = ProtoField.uint8("mavlink_proto.GOPRO_SET_RESPONSE_status", "status (uint8)")

f.RPM_rpm1 = ProtoField.float("mavlink_proto.RPM_rpm1", "rpm1 (float)")
f.RPM_rpm2 = ProtoField.float("mavlink_proto.RPM_rpm2", "rpm2 (float)")

f.HEARTBEAT_type = ProtoField.uint8("mavlink_proto.HEARTBEAT_type", "type (uint8)")
f.HEARTBEAT_autopilot = ProtoField.uint8("mavlink_proto.HEARTBEAT_autopilot", "autopilot (uint8)")
f.HEARTBEAT_base_mode = ProtoField.uint8("mavlink_proto.HEARTBEAT_base_mode", "base_mode (uint8)")
f.HEARTBEAT_custom_mode = ProtoField.uint32("mavlink_proto.HEARTBEAT_custom_mode", "custom_mode (uint32)")
f.HEARTBEAT_system_status = ProtoField.uint8("mavlink_proto.HEARTBEAT_system_status", "system_status (uint8)")
f.HEARTBEAT_mavlink_version = ProtoField.uint8("mavlink_proto.HEARTBEAT_mavlink_version", "mavlink_version (uint8)")

f.SYS_STATUS_onboard_control_sensors_present = ProtoField.uint32("mavlink_proto.SYS_STATUS_onboard_control_sensors_present", "onboard_control_sensors_present (uint32)")
f.SYS_STATUS_onboard_control_sensors_enabled = ProtoField.uint32("mavlink_proto.SYS_STATUS_onboard_control_sensors_enabled", "onboard_control_sensors_enabled (uint32)")
f.SYS_STATUS_onboard_control_sensors_health = ProtoField.uint32("mavlink_proto.SYS_STATUS_onboard_control_sensors_health", "onboard_control_sensors_health (uint32)")
f.SYS_STATUS_load = ProtoField.uint16("mavlink_proto.SYS_STATUS_load", "load (uint16)")
f.SYS_STATUS_voltage_battery = ProtoField.uint16("mavlink_proto.SYS_STATUS_voltage_battery", "voltage_battery (uint16)")
f.SYS_STATUS_current_battery = ProtoField.int16("mavlink_proto.SYS_STATUS_current_battery", "current_battery (int16)")
f.SYS_STATUS_battery_remaining = ProtoField.int8("mavlink_proto.SYS_STATUS_battery_remaining", "battery_remaining (int8)")
f.SYS_STATUS_drop_rate_comm = ProtoField.uint16("mavlink_proto.SYS_STATUS_drop_rate_comm", "drop_rate_comm (uint16)")
f.SYS_STATUS_errors_comm = ProtoField.uint16("mavlink_proto.SYS_STATUS_errors_comm", "errors_comm (uint16)")
f.SYS_STATUS_errors_count1 = ProtoField.uint16("mavlink_proto.SYS_STATUS_errors_count1", "errors_count1 (uint16)")
f.SYS_STATUS_errors_count2 = ProtoField.uint16("mavlink_proto.SYS_STATUS_errors_count2", "errors_count2 (uint16)")
f.SYS_STATUS_errors_count3 = ProtoField.uint16("mavlink_proto.SYS_STATUS_errors_count3", "errors_count3 (uint16)")
f.SYS_STATUS_errors_count4 = ProtoField.uint16("mavlink_proto.SYS_STATUS_errors_count4", "errors_count4 (uint16)")

f.SYSTEM_TIME_time_unix_usec = ProtoField.uint64("mavlink_proto.SYSTEM_TIME_time_unix_usec", "time_unix_usec (uint64)")
f.SYSTEM_TIME_time_boot_ms = ProtoField.uint32("mavlink_proto.SYSTEM_TIME_time_boot_ms", "time_boot_ms (uint32)")

f.PING_time_usec = ProtoField.uint64("mavlink_proto.PING_time_usec", "time_usec (uint64)")
f.PING_seq = ProtoField.uint32("mavlink_proto.PING_seq", "seq (uint32)")
f.PING_target_system = ProtoField.uint8("mavlink_proto.PING_target_system", "target_system (uint8)")
f.PING_target_component = ProtoField.uint8("mavlink_proto.PING_target_component", "target_component (uint8)")

f.CHANGE_OPERATOR_CONTROL_target_system = ProtoField.uint8("mavlink_proto.CHANGE_OPERATOR_CONTROL_target_system", "target_system (uint8)")
f.CHANGE_OPERATOR_CONTROL_control_request = ProtoField.uint8("mavlink_proto.CHANGE_OPERATOR_CONTROL_control_request", "control_request (uint8)")
f.CHANGE_OPERATOR_CONTROL_version = ProtoField.uint8("mavlink_proto.CHANGE_OPERATOR_CONTROL_version", "version (uint8)")
f.CHANGE_OPERATOR_CONTROL_passkey = ProtoField.string("mavlink_proto.CHANGE_OPERATOR_CONTROL_passkey", "passkey (string)")

f.CHANGE_OPERATOR_CONTROL_ACK_gcs_system_id = ProtoField.uint8("mavlink_proto.CHANGE_OPERATOR_CONTROL_ACK_gcs_system_id", "gcs_system_id (uint8)")
f.CHANGE_OPERATOR_CONTROL_ACK_control_request = ProtoField.uint8("mavlink_proto.CHANGE_OPERATOR_CONTROL_ACK_control_request", "control_request (uint8)")
f.CHANGE_OPERATOR_CONTROL_ACK_ack = ProtoField.uint8("mavlink_proto.CHANGE_OPERATOR_CONTROL_ACK_ack", "ack (uint8)")

f.AUTH_KEY_key = ProtoField.string("mavlink_proto.AUTH_KEY_key", "key (string)")

f.SET_MODE_target_system = ProtoField.uint8("mavlink_proto.SET_MODE_target_system", "target_system (uint8)")
f.SET_MODE_base_mode = ProtoField.uint8("mavlink_proto.SET_MODE_base_mode", "base_mode (uint8)")
f.SET_MODE_custom_mode = ProtoField.uint32("mavlink_proto.SET_MODE_custom_mode", "custom_mode (uint32)")

f.PARAM_REQUEST_READ_target_system = ProtoField.uint8("mavlink_proto.PARAM_REQUEST_READ_target_system", "target_system (uint8)")
f.PARAM_REQUEST_READ_target_component = ProtoField.uint8("mavlink_proto.PARAM_REQUEST_READ_target_component", "target_component (uint8)")
f.PARAM_REQUEST_READ_param_id = ProtoField.string("mavlink_proto.PARAM_REQUEST_READ_param_id", "param_id (string)")
f.PARAM_REQUEST_READ_param_index = ProtoField.int16("mavlink_proto.PARAM_REQUEST_READ_param_index", "param_index (int16)")

f.PARAM_REQUEST_LIST_target_system = ProtoField.uint8("mavlink_proto.PARAM_REQUEST_LIST_target_system", "target_system (uint8)")
f.PARAM_REQUEST_LIST_target_component = ProtoField.uint8("mavlink_proto.PARAM_REQUEST_LIST_target_component", "target_component (uint8)")

f.PARAM_VALUE_param_id = ProtoField.string("mavlink_proto.PARAM_VALUE_param_id", "param_id (string)")
f.PARAM_VALUE_param_value = ProtoField.float("mavlink_proto.PARAM_VALUE_param_value", "param_value (float)")
f.PARAM_VALUE_param_type = ProtoField.uint8("mavlink_proto.PARAM_VALUE_param_type", "param_type (uint8)")
f.PARAM_VALUE_param_count = ProtoField.uint16("mavlink_proto.PARAM_VALUE_param_count", "param_count (uint16)")
f.PARAM_VALUE_param_index = ProtoField.uint16("mavlink_proto.PARAM_VALUE_param_index", "param_index (uint16)")

f.PARAM_SET_target_system = ProtoField.uint8("mavlink_proto.PARAM_SET_target_system", "target_system (uint8)")
f.PARAM_SET_target_component = ProtoField.uint8("mavlink_proto.PARAM_SET_target_component", "target_component (uint8)")
f.PARAM_SET_param_id = ProtoField.string("mavlink_proto.PARAM_SET_param_id", "param_id (string)")
f.PARAM_SET_param_value = ProtoField.float("mavlink_proto.PARAM_SET_param_value", "param_value (float)")
f.PARAM_SET_param_type = ProtoField.uint8("mavlink_proto.PARAM_SET_param_type", "param_type (uint8)")

f.GPS_RAW_INT_time_usec = ProtoField.uint64("mavlink_proto.GPS_RAW_INT_time_usec", "time_usec (uint64)")
f.GPS_RAW_INT_fix_type = ProtoField.uint8("mavlink_proto.GPS_RAW_INT_fix_type", "fix_type (uint8)")
f.GPS_RAW_INT_lat = ProtoField.int32("mavlink_proto.GPS_RAW_INT_lat", "lat (int32)")
f.GPS_RAW_INT_lon = ProtoField.int32("mavlink_proto.GPS_RAW_INT_lon", "lon (int32)")
f.GPS_RAW_INT_alt = ProtoField.int32("mavlink_proto.GPS_RAW_INT_alt", "alt (int32)")
f.GPS_RAW_INT_eph = ProtoField.uint16("mavlink_proto.GPS_RAW_INT_eph", "eph (uint16)")
f.GPS_RAW_INT_epv = ProtoField.uint16("mavlink_proto.GPS_RAW_INT_epv", "epv (uint16)")
f.GPS_RAW_INT_vel = ProtoField.uint16("mavlink_proto.GPS_RAW_INT_vel", "vel (uint16)")
f.GPS_RAW_INT_cog = ProtoField.uint16("mavlink_proto.GPS_RAW_INT_cog", "cog (uint16)")
f.GPS_RAW_INT_satellites_visible = ProtoField.uint8("mavlink_proto.GPS_RAW_INT_satellites_visible", "satellites_visible (uint8)")

f.GPS_STATUS_satellites_visible = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellites_visible", "satellites_visible (uint8)")
f.GPS_STATUS_satellite_prn_0 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_0", "satellite_prn[0] (uint8)")
f.GPS_STATUS_satellite_prn_1 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_1", "satellite_prn[1] (uint8)")
f.GPS_STATUS_satellite_prn_2 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_2", "satellite_prn[2] (uint8)")
f.GPS_STATUS_satellite_prn_3 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_3", "satellite_prn[3] (uint8)")
f.GPS_STATUS_satellite_prn_4 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_4", "satellite_prn[4] (uint8)")
f.GPS_STATUS_satellite_prn_5 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_5", "satellite_prn[5] (uint8)")
f.GPS_STATUS_satellite_prn_6 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_6", "satellite_prn[6] (uint8)")
f.GPS_STATUS_satellite_prn_7 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_7", "satellite_prn[7] (uint8)")
f.GPS_STATUS_satellite_prn_8 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_8", "satellite_prn[8] (uint8)")
f.GPS_STATUS_satellite_prn_9 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_9", "satellite_prn[9] (uint8)")
f.GPS_STATUS_satellite_prn_10 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_10", "satellite_prn[10] (uint8)")
f.GPS_STATUS_satellite_prn_11 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_11", "satellite_prn[11] (uint8)")
f.GPS_STATUS_satellite_prn_12 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_12", "satellite_prn[12] (uint8)")
f.GPS_STATUS_satellite_prn_13 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_13", "satellite_prn[13] (uint8)")
f.GPS_STATUS_satellite_prn_14 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_14", "satellite_prn[14] (uint8)")
f.GPS_STATUS_satellite_prn_15 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_15", "satellite_prn[15] (uint8)")
f.GPS_STATUS_satellite_prn_16 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_16", "satellite_prn[16] (uint8)")
f.GPS_STATUS_satellite_prn_17 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_17", "satellite_prn[17] (uint8)")
f.GPS_STATUS_satellite_prn_18 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_18", "satellite_prn[18] (uint8)")
f.GPS_STATUS_satellite_prn_19 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_prn_19", "satellite_prn[19] (uint8)")
f.GPS_STATUS_satellite_used_0 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_0", "satellite_used[0] (uint8)")
f.GPS_STATUS_satellite_used_1 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_1", "satellite_used[1] (uint8)")
f.GPS_STATUS_satellite_used_2 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_2", "satellite_used[2] (uint8)")
f.GPS_STATUS_satellite_used_3 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_3", "satellite_used[3] (uint8)")
f.GPS_STATUS_satellite_used_4 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_4", "satellite_used[4] (uint8)")
f.GPS_STATUS_satellite_used_5 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_5", "satellite_used[5] (uint8)")
f.GPS_STATUS_satellite_used_6 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_6", "satellite_used[6] (uint8)")
f.GPS_STATUS_satellite_used_7 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_7", "satellite_used[7] (uint8)")
f.GPS_STATUS_satellite_used_8 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_8", "satellite_used[8] (uint8)")
f.GPS_STATUS_satellite_used_9 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_9", "satellite_used[9] (uint8)")
f.GPS_STATUS_satellite_used_10 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_10", "satellite_used[10] (uint8)")
f.GPS_STATUS_satellite_used_11 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_11", "satellite_used[11] (uint8)")
f.GPS_STATUS_satellite_used_12 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_12", "satellite_used[12] (uint8)")
f.GPS_STATUS_satellite_used_13 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_13", "satellite_used[13] (uint8)")
f.GPS_STATUS_satellite_used_14 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_14", "satellite_used[14] (uint8)")
f.GPS_STATUS_satellite_used_15 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_15", "satellite_used[15] (uint8)")
f.GPS_STATUS_satellite_used_16 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_16", "satellite_used[16] (uint8)")
f.GPS_STATUS_satellite_used_17 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_17", "satellite_used[17] (uint8)")
f.GPS_STATUS_satellite_used_18 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_18", "satellite_used[18] (uint8)")
f.GPS_STATUS_satellite_used_19 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_used_19", "satellite_used[19] (uint8)")
f.GPS_STATUS_satellite_elevation_0 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_0", "satellite_elevation[0] (uint8)")
f.GPS_STATUS_satellite_elevation_1 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_1", "satellite_elevation[1] (uint8)")
f.GPS_STATUS_satellite_elevation_2 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_2", "satellite_elevation[2] (uint8)")
f.GPS_STATUS_satellite_elevation_3 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_3", "satellite_elevation[3] (uint8)")
f.GPS_STATUS_satellite_elevation_4 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_4", "satellite_elevation[4] (uint8)")
f.GPS_STATUS_satellite_elevation_5 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_5", "satellite_elevation[5] (uint8)")
f.GPS_STATUS_satellite_elevation_6 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_6", "satellite_elevation[6] (uint8)")
f.GPS_STATUS_satellite_elevation_7 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_7", "satellite_elevation[7] (uint8)")
f.GPS_STATUS_satellite_elevation_8 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_8", "satellite_elevation[8] (uint8)")
f.GPS_STATUS_satellite_elevation_9 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_9", "satellite_elevation[9] (uint8)")
f.GPS_STATUS_satellite_elevation_10 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_10", "satellite_elevation[10] (uint8)")
f.GPS_STATUS_satellite_elevation_11 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_11", "satellite_elevation[11] (uint8)")
f.GPS_STATUS_satellite_elevation_12 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_12", "satellite_elevation[12] (uint8)")
f.GPS_STATUS_satellite_elevation_13 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_13", "satellite_elevation[13] (uint8)")
f.GPS_STATUS_satellite_elevation_14 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_14", "satellite_elevation[14] (uint8)")
f.GPS_STATUS_satellite_elevation_15 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_15", "satellite_elevation[15] (uint8)")
f.GPS_STATUS_satellite_elevation_16 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_16", "satellite_elevation[16] (uint8)")
f.GPS_STATUS_satellite_elevation_17 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_17", "satellite_elevation[17] (uint8)")
f.GPS_STATUS_satellite_elevation_18 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_18", "satellite_elevation[18] (uint8)")
f.GPS_STATUS_satellite_elevation_19 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_elevation_19", "satellite_elevation[19] (uint8)")
f.GPS_STATUS_satellite_azimuth_0 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_0", "satellite_azimuth[0] (uint8)")
f.GPS_STATUS_satellite_azimuth_1 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_1", "satellite_azimuth[1] (uint8)")
f.GPS_STATUS_satellite_azimuth_2 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_2", "satellite_azimuth[2] (uint8)")
f.GPS_STATUS_satellite_azimuth_3 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_3", "satellite_azimuth[3] (uint8)")
f.GPS_STATUS_satellite_azimuth_4 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_4", "satellite_azimuth[4] (uint8)")
f.GPS_STATUS_satellite_azimuth_5 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_5", "satellite_azimuth[5] (uint8)")
f.GPS_STATUS_satellite_azimuth_6 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_6", "satellite_azimuth[6] (uint8)")
f.GPS_STATUS_satellite_azimuth_7 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_7", "satellite_azimuth[7] (uint8)")
f.GPS_STATUS_satellite_azimuth_8 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_8", "satellite_azimuth[8] (uint8)")
f.GPS_STATUS_satellite_azimuth_9 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_9", "satellite_azimuth[9] (uint8)")
f.GPS_STATUS_satellite_azimuth_10 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_10", "satellite_azimuth[10] (uint8)")
f.GPS_STATUS_satellite_azimuth_11 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_11", "satellite_azimuth[11] (uint8)")
f.GPS_STATUS_satellite_azimuth_12 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_12", "satellite_azimuth[12] (uint8)")
f.GPS_STATUS_satellite_azimuth_13 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_13", "satellite_azimuth[13] (uint8)")
f.GPS_STATUS_satellite_azimuth_14 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_14", "satellite_azimuth[14] (uint8)")
f.GPS_STATUS_satellite_azimuth_15 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_15", "satellite_azimuth[15] (uint8)")
f.GPS_STATUS_satellite_azimuth_16 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_16", "satellite_azimuth[16] (uint8)")
f.GPS_STATUS_satellite_azimuth_17 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_17", "satellite_azimuth[17] (uint8)")
f.GPS_STATUS_satellite_azimuth_18 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_18", "satellite_azimuth[18] (uint8)")
f.GPS_STATUS_satellite_azimuth_19 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_azimuth_19", "satellite_azimuth[19] (uint8)")
f.GPS_STATUS_satellite_snr_0 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_0", "satellite_snr[0] (uint8)")
f.GPS_STATUS_satellite_snr_1 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_1", "satellite_snr[1] (uint8)")
f.GPS_STATUS_satellite_snr_2 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_2", "satellite_snr[2] (uint8)")
f.GPS_STATUS_satellite_snr_3 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_3", "satellite_snr[3] (uint8)")
f.GPS_STATUS_satellite_snr_4 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_4", "satellite_snr[4] (uint8)")
f.GPS_STATUS_satellite_snr_5 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_5", "satellite_snr[5] (uint8)")
f.GPS_STATUS_satellite_snr_6 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_6", "satellite_snr[6] (uint8)")
f.GPS_STATUS_satellite_snr_7 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_7", "satellite_snr[7] (uint8)")
f.GPS_STATUS_satellite_snr_8 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_8", "satellite_snr[8] (uint8)")
f.GPS_STATUS_satellite_snr_9 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_9", "satellite_snr[9] (uint8)")
f.GPS_STATUS_satellite_snr_10 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_10", "satellite_snr[10] (uint8)")
f.GPS_STATUS_satellite_snr_11 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_11", "satellite_snr[11] (uint8)")
f.GPS_STATUS_satellite_snr_12 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_12", "satellite_snr[12] (uint8)")
f.GPS_STATUS_satellite_snr_13 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_13", "satellite_snr[13] (uint8)")
f.GPS_STATUS_satellite_snr_14 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_14", "satellite_snr[14] (uint8)")
f.GPS_STATUS_satellite_snr_15 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_15", "satellite_snr[15] (uint8)")
f.GPS_STATUS_satellite_snr_16 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_16", "satellite_snr[16] (uint8)")
f.GPS_STATUS_satellite_snr_17 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_17", "satellite_snr[17] (uint8)")
f.GPS_STATUS_satellite_snr_18 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_18", "satellite_snr[18] (uint8)")
f.GPS_STATUS_satellite_snr_19 = ProtoField.uint8("mavlink_proto.GPS_STATUS_satellite_snr_19", "satellite_snr[19] (uint8)")

f.SCALED_IMU_time_boot_ms = ProtoField.uint32("mavlink_proto.SCALED_IMU_time_boot_ms", "time_boot_ms (uint32)")
f.SCALED_IMU_xacc = ProtoField.int16("mavlink_proto.SCALED_IMU_xacc", "xacc (int16)")
f.SCALED_IMU_yacc = ProtoField.int16("mavlink_proto.SCALED_IMU_yacc", "yacc (int16)")
f.SCALED_IMU_zacc = ProtoField.int16("mavlink_proto.SCALED_IMU_zacc", "zacc (int16)")
f.SCALED_IMU_xgyro = ProtoField.int16("mavlink_proto.SCALED_IMU_xgyro", "xgyro (int16)")
f.SCALED_IMU_ygyro = ProtoField.int16("mavlink_proto.SCALED_IMU_ygyro", "ygyro (int16)")
f.SCALED_IMU_zgyro = ProtoField.int16("mavlink_proto.SCALED_IMU_zgyro", "zgyro (int16)")
f.SCALED_IMU_xmag = ProtoField.int16("mavlink_proto.SCALED_IMU_xmag", "xmag (int16)")
f.SCALED_IMU_ymag = ProtoField.int16("mavlink_proto.SCALED_IMU_ymag", "ymag (int16)")
f.SCALED_IMU_zmag = ProtoField.int16("mavlink_proto.SCALED_IMU_zmag", "zmag (int16)")

f.RAW_IMU_time_usec = ProtoField.uint64("mavlink_proto.RAW_IMU_time_usec", "time_usec (uint64)")
f.RAW_IMU_xacc = ProtoField.int16("mavlink_proto.RAW_IMU_xacc", "xacc (int16)")
f.RAW_IMU_yacc = ProtoField.int16("mavlink_proto.RAW_IMU_yacc", "yacc (int16)")
f.RAW_IMU_zacc = ProtoField.int16("mavlink_proto.RAW_IMU_zacc", "zacc (int16)")
f.RAW_IMU_xgyro = ProtoField.int16("mavlink_proto.RAW_IMU_xgyro", "xgyro (int16)")
f.RAW_IMU_ygyro = ProtoField.int16("mavlink_proto.RAW_IMU_ygyro", "ygyro (int16)")
f.RAW_IMU_zgyro = ProtoField.int16("mavlink_proto.RAW_IMU_zgyro", "zgyro (int16)")
f.RAW_IMU_xmag = ProtoField.int16("mavlink_proto.RAW_IMU_xmag", "xmag (int16)")
f.RAW_IMU_ymag = ProtoField.int16("mavlink_proto.RAW_IMU_ymag", "ymag (int16)")
f.RAW_IMU_zmag = ProtoField.int16("mavlink_proto.RAW_IMU_zmag", "zmag (int16)")

f.RAW_PRESSURE_time_usec = ProtoField.uint64("mavlink_proto.RAW_PRESSURE_time_usec", "time_usec (uint64)")
f.RAW_PRESSURE_press_abs = ProtoField.int16("mavlink_proto.RAW_PRESSURE_press_abs", "press_abs (int16)")
f.RAW_PRESSURE_press_diff1 = ProtoField.int16("mavlink_proto.RAW_PRESSURE_press_diff1", "press_diff1 (int16)")
f.RAW_PRESSURE_press_diff2 = ProtoField.int16("mavlink_proto.RAW_PRESSURE_press_diff2", "press_diff2 (int16)")
f.RAW_PRESSURE_temperature = ProtoField.int16("mavlink_proto.RAW_PRESSURE_temperature", "temperature (int16)")

f.SCALED_PRESSURE_time_boot_ms = ProtoField.uint32("mavlink_proto.SCALED_PRESSURE_time_boot_ms", "time_boot_ms (uint32)")
f.SCALED_PRESSURE_press_abs = ProtoField.float("mavlink_proto.SCALED_PRESSURE_press_abs", "press_abs (float)")
f.SCALED_PRESSURE_press_diff = ProtoField.float("mavlink_proto.SCALED_PRESSURE_press_diff", "press_diff (float)")
f.SCALED_PRESSURE_temperature = ProtoField.int16("mavlink_proto.SCALED_PRESSURE_temperature", "temperature (int16)")

f.ATTITUDE_time_boot_ms = ProtoField.uint32("mavlink_proto.ATTITUDE_time_boot_ms", "time_boot_ms (uint32)")
f.ATTITUDE_roll = ProtoField.float("mavlink_proto.ATTITUDE_roll", "roll (float)")
f.ATTITUDE_pitch = ProtoField.float("mavlink_proto.ATTITUDE_pitch", "pitch (float)")
f.ATTITUDE_yaw = ProtoField.float("mavlink_proto.ATTITUDE_yaw", "yaw (float)")
f.ATTITUDE_rollspeed = ProtoField.float("mavlink_proto.ATTITUDE_rollspeed", "rollspeed (float)")
f.ATTITUDE_pitchspeed = ProtoField.float("mavlink_proto.ATTITUDE_pitchspeed", "pitchspeed (float)")
f.ATTITUDE_yawspeed = ProtoField.float("mavlink_proto.ATTITUDE_yawspeed", "yawspeed (float)")

f.ATTITUDE_QUATERNION_time_boot_ms = ProtoField.uint32("mavlink_proto.ATTITUDE_QUATERNION_time_boot_ms", "time_boot_ms (uint32)")
f.ATTITUDE_QUATERNION_q1 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_q1", "q1 (float)")
f.ATTITUDE_QUATERNION_q2 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_q2", "q2 (float)")
f.ATTITUDE_QUATERNION_q3 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_q3", "q3 (float)")
f.ATTITUDE_QUATERNION_q4 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_q4", "q4 (float)")
f.ATTITUDE_QUATERNION_rollspeed = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_rollspeed", "rollspeed (float)")
f.ATTITUDE_QUATERNION_pitchspeed = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_pitchspeed", "pitchspeed (float)")
f.ATTITUDE_QUATERNION_yawspeed = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_yawspeed", "yawspeed (float)")

f.LOCAL_POSITION_NED_time_boot_ms = ProtoField.uint32("mavlink_proto.LOCAL_POSITION_NED_time_boot_ms", "time_boot_ms (uint32)")
f.LOCAL_POSITION_NED_x = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_x", "x (float)")
f.LOCAL_POSITION_NED_y = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_y", "y (float)")
f.LOCAL_POSITION_NED_z = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_z", "z (float)")
f.LOCAL_POSITION_NED_vx = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_vx", "vx (float)")
f.LOCAL_POSITION_NED_vy = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_vy", "vy (float)")
f.LOCAL_POSITION_NED_vz = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_vz", "vz (float)")

f.GLOBAL_POSITION_INT_time_boot_ms = ProtoField.uint32("mavlink_proto.GLOBAL_POSITION_INT_time_boot_ms", "time_boot_ms (uint32)")
f.GLOBAL_POSITION_INT_lat = ProtoField.int32("mavlink_proto.GLOBAL_POSITION_INT_lat", "lat (int32)")
f.GLOBAL_POSITION_INT_lon = ProtoField.int32("mavlink_proto.GLOBAL_POSITION_INT_lon", "lon (int32)")
f.GLOBAL_POSITION_INT_alt = ProtoField.int32("mavlink_proto.GLOBAL_POSITION_INT_alt", "alt (int32)")
f.GLOBAL_POSITION_INT_relative_alt = ProtoField.int32("mavlink_proto.GLOBAL_POSITION_INT_relative_alt", "relative_alt (int32)")
f.GLOBAL_POSITION_INT_vx = ProtoField.int16("mavlink_proto.GLOBAL_POSITION_INT_vx", "vx (int16)")
f.GLOBAL_POSITION_INT_vy = ProtoField.int16("mavlink_proto.GLOBAL_POSITION_INT_vy", "vy (int16)")
f.GLOBAL_POSITION_INT_vz = ProtoField.int16("mavlink_proto.GLOBAL_POSITION_INT_vz", "vz (int16)")
f.GLOBAL_POSITION_INT_hdg = ProtoField.uint16("mavlink_proto.GLOBAL_POSITION_INT_hdg", "hdg (uint16)")

f.RC_CHANNELS_SCALED_time_boot_ms = ProtoField.uint32("mavlink_proto.RC_CHANNELS_SCALED_time_boot_ms", "time_boot_ms (uint32)")
f.RC_CHANNELS_SCALED_port = ProtoField.uint8("mavlink_proto.RC_CHANNELS_SCALED_port", "port (uint8)")
f.RC_CHANNELS_SCALED_chan1_scaled = ProtoField.int16("mavlink_proto.RC_CHANNELS_SCALED_chan1_scaled", "chan1_scaled (int16)")
f.RC_CHANNELS_SCALED_chan2_scaled = ProtoField.int16("mavlink_proto.RC_CHANNELS_SCALED_chan2_scaled", "chan2_scaled (int16)")
f.RC_CHANNELS_SCALED_chan3_scaled = ProtoField.int16("mavlink_proto.RC_CHANNELS_SCALED_chan3_scaled", "chan3_scaled (int16)")
f.RC_CHANNELS_SCALED_chan4_scaled = ProtoField.int16("mavlink_proto.RC_CHANNELS_SCALED_chan4_scaled", "chan4_scaled (int16)")
f.RC_CHANNELS_SCALED_chan5_scaled = ProtoField.int16("mavlink_proto.RC_CHANNELS_SCALED_chan5_scaled", "chan5_scaled (int16)")
f.RC_CHANNELS_SCALED_chan6_scaled = ProtoField.int16("mavlink_proto.RC_CHANNELS_SCALED_chan6_scaled", "chan6_scaled (int16)")
f.RC_CHANNELS_SCALED_chan7_scaled = ProtoField.int16("mavlink_proto.RC_CHANNELS_SCALED_chan7_scaled", "chan7_scaled (int16)")
f.RC_CHANNELS_SCALED_chan8_scaled = ProtoField.int16("mavlink_proto.RC_CHANNELS_SCALED_chan8_scaled", "chan8_scaled (int16)")
f.RC_CHANNELS_SCALED_rssi = ProtoField.uint8("mavlink_proto.RC_CHANNELS_SCALED_rssi", "rssi (uint8)")

f.RC_CHANNELS_RAW_time_boot_ms = ProtoField.uint32("mavlink_proto.RC_CHANNELS_RAW_time_boot_ms", "time_boot_ms (uint32)")
f.RC_CHANNELS_RAW_port = ProtoField.uint8("mavlink_proto.RC_CHANNELS_RAW_port", "port (uint8)")
f.RC_CHANNELS_RAW_chan1_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_RAW_chan1_raw", "chan1_raw (uint16)")
f.RC_CHANNELS_RAW_chan2_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_RAW_chan2_raw", "chan2_raw (uint16)")
f.RC_CHANNELS_RAW_chan3_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_RAW_chan3_raw", "chan3_raw (uint16)")
f.RC_CHANNELS_RAW_chan4_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_RAW_chan4_raw", "chan4_raw (uint16)")
f.RC_CHANNELS_RAW_chan5_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_RAW_chan5_raw", "chan5_raw (uint16)")
f.RC_CHANNELS_RAW_chan6_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_RAW_chan6_raw", "chan6_raw (uint16)")
f.RC_CHANNELS_RAW_chan7_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_RAW_chan7_raw", "chan7_raw (uint16)")
f.RC_CHANNELS_RAW_chan8_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_RAW_chan8_raw", "chan8_raw (uint16)")
f.RC_CHANNELS_RAW_rssi = ProtoField.uint8("mavlink_proto.RC_CHANNELS_RAW_rssi", "rssi (uint8)")

f.SERVO_OUTPUT_RAW_time_usec = ProtoField.uint32("mavlink_proto.SERVO_OUTPUT_RAW_time_usec", "time_usec (uint32)")
f.SERVO_OUTPUT_RAW_port = ProtoField.uint8("mavlink_proto.SERVO_OUTPUT_RAW_port", "port (uint8)")
f.SERVO_OUTPUT_RAW_servo1_raw = ProtoField.uint16("mavlink_proto.SERVO_OUTPUT_RAW_servo1_raw", "servo1_raw (uint16)")
f.SERVO_OUTPUT_RAW_servo2_raw = ProtoField.uint16("mavlink_proto.SERVO_OUTPUT_RAW_servo2_raw", "servo2_raw (uint16)")
f.SERVO_OUTPUT_RAW_servo3_raw = ProtoField.uint16("mavlink_proto.SERVO_OUTPUT_RAW_servo3_raw", "servo3_raw (uint16)")
f.SERVO_OUTPUT_RAW_servo4_raw = ProtoField.uint16("mavlink_proto.SERVO_OUTPUT_RAW_servo4_raw", "servo4_raw (uint16)")
f.SERVO_OUTPUT_RAW_servo5_raw = ProtoField.uint16("mavlink_proto.SERVO_OUTPUT_RAW_servo5_raw", "servo5_raw (uint16)")
f.SERVO_OUTPUT_RAW_servo6_raw = ProtoField.uint16("mavlink_proto.SERVO_OUTPUT_RAW_servo6_raw", "servo6_raw (uint16)")
f.SERVO_OUTPUT_RAW_servo7_raw = ProtoField.uint16("mavlink_proto.SERVO_OUTPUT_RAW_servo7_raw", "servo7_raw (uint16)")
f.SERVO_OUTPUT_RAW_servo8_raw = ProtoField.uint16("mavlink_proto.SERVO_OUTPUT_RAW_servo8_raw", "servo8_raw (uint16)")

f.MISSION_REQUEST_PARTIAL_LIST_target_system = ProtoField.uint8("mavlink_proto.MISSION_REQUEST_PARTIAL_LIST_target_system", "target_system (uint8)")
f.MISSION_REQUEST_PARTIAL_LIST_target_component = ProtoField.uint8("mavlink_proto.MISSION_REQUEST_PARTIAL_LIST_target_component", "target_component (uint8)")
f.MISSION_REQUEST_PARTIAL_LIST_start_index = ProtoField.int16("mavlink_proto.MISSION_REQUEST_PARTIAL_LIST_start_index", "start_index (int16)")
f.MISSION_REQUEST_PARTIAL_LIST_end_index = ProtoField.int16("mavlink_proto.MISSION_REQUEST_PARTIAL_LIST_end_index", "end_index (int16)")

f.MISSION_WRITE_PARTIAL_LIST_target_system = ProtoField.uint8("mavlink_proto.MISSION_WRITE_PARTIAL_LIST_target_system", "target_system (uint8)")
f.MISSION_WRITE_PARTIAL_LIST_target_component = ProtoField.uint8("mavlink_proto.MISSION_WRITE_PARTIAL_LIST_target_component", "target_component (uint8)")
f.MISSION_WRITE_PARTIAL_LIST_start_index = ProtoField.int16("mavlink_proto.MISSION_WRITE_PARTIAL_LIST_start_index", "start_index (int16)")
f.MISSION_WRITE_PARTIAL_LIST_end_index = ProtoField.int16("mavlink_proto.MISSION_WRITE_PARTIAL_LIST_end_index", "end_index (int16)")

f.MISSION_ITEM_target_system = ProtoField.uint8("mavlink_proto.MISSION_ITEM_target_system", "target_system (uint8)")
f.MISSION_ITEM_target_component = ProtoField.uint8("mavlink_proto.MISSION_ITEM_target_component", "target_component (uint8)")
f.MISSION_ITEM_seq = ProtoField.uint16("mavlink_proto.MISSION_ITEM_seq", "seq (uint16)")
f.MISSION_ITEM_frame = ProtoField.uint8("mavlink_proto.MISSION_ITEM_frame", "frame (uint8)")
f.MISSION_ITEM_command = ProtoField.uint16("mavlink_proto.MISSION_ITEM_command", "command (uint16)")
f.MISSION_ITEM_current = ProtoField.uint8("mavlink_proto.MISSION_ITEM_current", "current (uint8)")
f.MISSION_ITEM_autocontinue = ProtoField.uint8("mavlink_proto.MISSION_ITEM_autocontinue", "autocontinue (uint8)")
f.MISSION_ITEM_param1 = ProtoField.float("mavlink_proto.MISSION_ITEM_param1", "param1 (float)")
f.MISSION_ITEM_param2 = ProtoField.float("mavlink_proto.MISSION_ITEM_param2", "param2 (float)")
f.MISSION_ITEM_param3 = ProtoField.float("mavlink_proto.MISSION_ITEM_param3", "param3 (float)")
f.MISSION_ITEM_param4 = ProtoField.float("mavlink_proto.MISSION_ITEM_param4", "param4 (float)")
f.MISSION_ITEM_x = ProtoField.float("mavlink_proto.MISSION_ITEM_x", "x (float)")
f.MISSION_ITEM_y = ProtoField.float("mavlink_proto.MISSION_ITEM_y", "y (float)")
f.MISSION_ITEM_z = ProtoField.float("mavlink_proto.MISSION_ITEM_z", "z (float)")

f.MISSION_REQUEST_target_system = ProtoField.uint8("mavlink_proto.MISSION_REQUEST_target_system", "target_system (uint8)")
f.MISSION_REQUEST_target_component = ProtoField.uint8("mavlink_proto.MISSION_REQUEST_target_component", "target_component (uint8)")
f.MISSION_REQUEST_seq = ProtoField.uint16("mavlink_proto.MISSION_REQUEST_seq", "seq (uint16)")

f.MISSION_SET_CURRENT_target_system = ProtoField.uint8("mavlink_proto.MISSION_SET_CURRENT_target_system", "target_system (uint8)")
f.MISSION_SET_CURRENT_target_component = ProtoField.uint8("mavlink_proto.MISSION_SET_CURRENT_target_component", "target_component (uint8)")
f.MISSION_SET_CURRENT_seq = ProtoField.uint16("mavlink_proto.MISSION_SET_CURRENT_seq", "seq (uint16)")

f.MISSION_CURRENT_seq = ProtoField.uint16("mavlink_proto.MISSION_CURRENT_seq", "seq (uint16)")

f.MISSION_REQUEST_LIST_target_system = ProtoField.uint8("mavlink_proto.MISSION_REQUEST_LIST_target_system", "target_system (uint8)")
f.MISSION_REQUEST_LIST_target_component = ProtoField.uint8("mavlink_proto.MISSION_REQUEST_LIST_target_component", "target_component (uint8)")

f.MISSION_COUNT_target_system = ProtoField.uint8("mavlink_proto.MISSION_COUNT_target_system", "target_system (uint8)")
f.MISSION_COUNT_target_component = ProtoField.uint8("mavlink_proto.MISSION_COUNT_target_component", "target_component (uint8)")
f.MISSION_COUNT_count = ProtoField.uint16("mavlink_proto.MISSION_COUNT_count", "count (uint16)")

f.MISSION_CLEAR_ALL_target_system = ProtoField.uint8("mavlink_proto.MISSION_CLEAR_ALL_target_system", "target_system (uint8)")
f.MISSION_CLEAR_ALL_target_component = ProtoField.uint8("mavlink_proto.MISSION_CLEAR_ALL_target_component", "target_component (uint8)")

f.MISSION_ITEM_REACHED_seq = ProtoField.uint16("mavlink_proto.MISSION_ITEM_REACHED_seq", "seq (uint16)")

f.MISSION_ACK_target_system = ProtoField.uint8("mavlink_proto.MISSION_ACK_target_system", "target_system (uint8)")
f.MISSION_ACK_target_component = ProtoField.uint8("mavlink_proto.MISSION_ACK_target_component", "target_component (uint8)")
f.MISSION_ACK_type = ProtoField.uint8("mavlink_proto.MISSION_ACK_type", "type (uint8)")

f.SET_GPS_GLOBAL_ORIGIN_target_system = ProtoField.uint8("mavlink_proto.SET_GPS_GLOBAL_ORIGIN_target_system", "target_system (uint8)")
f.SET_GPS_GLOBAL_ORIGIN_latitude = ProtoField.int32("mavlink_proto.SET_GPS_GLOBAL_ORIGIN_latitude", "latitude (int32)")
f.SET_GPS_GLOBAL_ORIGIN_longitude = ProtoField.int32("mavlink_proto.SET_GPS_GLOBAL_ORIGIN_longitude", "longitude (int32)")
f.SET_GPS_GLOBAL_ORIGIN_altitude = ProtoField.int32("mavlink_proto.SET_GPS_GLOBAL_ORIGIN_altitude", "altitude (int32)")

f.GPS_GLOBAL_ORIGIN_latitude = ProtoField.int32("mavlink_proto.GPS_GLOBAL_ORIGIN_latitude", "latitude (int32)")
f.GPS_GLOBAL_ORIGIN_longitude = ProtoField.int32("mavlink_proto.GPS_GLOBAL_ORIGIN_longitude", "longitude (int32)")
f.GPS_GLOBAL_ORIGIN_altitude = ProtoField.int32("mavlink_proto.GPS_GLOBAL_ORIGIN_altitude", "altitude (int32)")

f.PARAM_MAP_RC_target_system = ProtoField.uint8("mavlink_proto.PARAM_MAP_RC_target_system", "target_system (uint8)")
f.PARAM_MAP_RC_target_component = ProtoField.uint8("mavlink_proto.PARAM_MAP_RC_target_component", "target_component (uint8)")
f.PARAM_MAP_RC_param_id = ProtoField.string("mavlink_proto.PARAM_MAP_RC_param_id", "param_id (string)")
f.PARAM_MAP_RC_param_index = ProtoField.int16("mavlink_proto.PARAM_MAP_RC_param_index", "param_index (int16)")
f.PARAM_MAP_RC_parameter_rc_channel_index = ProtoField.uint8("mavlink_proto.PARAM_MAP_RC_parameter_rc_channel_index", "parameter_rc_channel_index (uint8)")
f.PARAM_MAP_RC_param_value0 = ProtoField.float("mavlink_proto.PARAM_MAP_RC_param_value0", "param_value0 (float)")
f.PARAM_MAP_RC_scale = ProtoField.float("mavlink_proto.PARAM_MAP_RC_scale", "scale (float)")
f.PARAM_MAP_RC_param_value_min = ProtoField.float("mavlink_proto.PARAM_MAP_RC_param_value_min", "param_value_min (float)")
f.PARAM_MAP_RC_param_value_max = ProtoField.float("mavlink_proto.PARAM_MAP_RC_param_value_max", "param_value_max (float)")

f.MISSION_REQUEST_INT_target_system = ProtoField.uint8("mavlink_proto.MISSION_REQUEST_INT_target_system", "target_system (uint8)")
f.MISSION_REQUEST_INT_target_component = ProtoField.uint8("mavlink_proto.MISSION_REQUEST_INT_target_component", "target_component (uint8)")
f.MISSION_REQUEST_INT_seq = ProtoField.uint16("mavlink_proto.MISSION_REQUEST_INT_seq", "seq (uint16)")

f.SAFETY_SET_ALLOWED_AREA_target_system = ProtoField.uint8("mavlink_proto.SAFETY_SET_ALLOWED_AREA_target_system", "target_system (uint8)")
f.SAFETY_SET_ALLOWED_AREA_target_component = ProtoField.uint8("mavlink_proto.SAFETY_SET_ALLOWED_AREA_target_component", "target_component (uint8)")
f.SAFETY_SET_ALLOWED_AREA_frame = ProtoField.uint8("mavlink_proto.SAFETY_SET_ALLOWED_AREA_frame", "frame (uint8)")
f.SAFETY_SET_ALLOWED_AREA_p1x = ProtoField.float("mavlink_proto.SAFETY_SET_ALLOWED_AREA_p1x", "p1x (float)")
f.SAFETY_SET_ALLOWED_AREA_p1y = ProtoField.float("mavlink_proto.SAFETY_SET_ALLOWED_AREA_p1y", "p1y (float)")
f.SAFETY_SET_ALLOWED_AREA_p1z = ProtoField.float("mavlink_proto.SAFETY_SET_ALLOWED_AREA_p1z", "p1z (float)")
f.SAFETY_SET_ALLOWED_AREA_p2x = ProtoField.float("mavlink_proto.SAFETY_SET_ALLOWED_AREA_p2x", "p2x (float)")
f.SAFETY_SET_ALLOWED_AREA_p2y = ProtoField.float("mavlink_proto.SAFETY_SET_ALLOWED_AREA_p2y", "p2y (float)")
f.SAFETY_SET_ALLOWED_AREA_p2z = ProtoField.float("mavlink_proto.SAFETY_SET_ALLOWED_AREA_p2z", "p2z (float)")

f.SAFETY_ALLOWED_AREA_frame = ProtoField.uint8("mavlink_proto.SAFETY_ALLOWED_AREA_frame", "frame (uint8)")
f.SAFETY_ALLOWED_AREA_p1x = ProtoField.float("mavlink_proto.SAFETY_ALLOWED_AREA_p1x", "p1x (float)")
f.SAFETY_ALLOWED_AREA_p1y = ProtoField.float("mavlink_proto.SAFETY_ALLOWED_AREA_p1y", "p1y (float)")
f.SAFETY_ALLOWED_AREA_p1z = ProtoField.float("mavlink_proto.SAFETY_ALLOWED_AREA_p1z", "p1z (float)")
f.SAFETY_ALLOWED_AREA_p2x = ProtoField.float("mavlink_proto.SAFETY_ALLOWED_AREA_p2x", "p2x (float)")
f.SAFETY_ALLOWED_AREA_p2y = ProtoField.float("mavlink_proto.SAFETY_ALLOWED_AREA_p2y", "p2y (float)")
f.SAFETY_ALLOWED_AREA_p2z = ProtoField.float("mavlink_proto.SAFETY_ALLOWED_AREA_p2z", "p2z (float)")

f.ATTITUDE_QUATERNION_COV_time_usec = ProtoField.uint64("mavlink_proto.ATTITUDE_QUATERNION_COV_time_usec", "time_usec (uint64)")
f.ATTITUDE_QUATERNION_COV_q_0 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_q_0", "q[0] (float)")
f.ATTITUDE_QUATERNION_COV_q_1 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_q_1", "q[1] (float)")
f.ATTITUDE_QUATERNION_COV_q_2 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_q_2", "q[2] (float)")
f.ATTITUDE_QUATERNION_COV_q_3 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_q_3", "q[3] (float)")
f.ATTITUDE_QUATERNION_COV_rollspeed = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_rollspeed", "rollspeed (float)")
f.ATTITUDE_QUATERNION_COV_pitchspeed = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_pitchspeed", "pitchspeed (float)")
f.ATTITUDE_QUATERNION_COV_yawspeed = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_yawspeed", "yawspeed (float)")
f.ATTITUDE_QUATERNION_COV_covariance_0 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_covariance_0", "covariance[0] (float)")
f.ATTITUDE_QUATERNION_COV_covariance_1 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_covariance_1", "covariance[1] (float)")
f.ATTITUDE_QUATERNION_COV_covariance_2 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_covariance_2", "covariance[2] (float)")
f.ATTITUDE_QUATERNION_COV_covariance_3 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_covariance_3", "covariance[3] (float)")
f.ATTITUDE_QUATERNION_COV_covariance_4 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_covariance_4", "covariance[4] (float)")
f.ATTITUDE_QUATERNION_COV_covariance_5 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_covariance_5", "covariance[5] (float)")
f.ATTITUDE_QUATERNION_COV_covariance_6 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_covariance_6", "covariance[6] (float)")
f.ATTITUDE_QUATERNION_COV_covariance_7 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_covariance_7", "covariance[7] (float)")
f.ATTITUDE_QUATERNION_COV_covariance_8 = ProtoField.float("mavlink_proto.ATTITUDE_QUATERNION_COV_covariance_8", "covariance[8] (float)")

f.NAV_CONTROLLER_OUTPUT_nav_roll = ProtoField.float("mavlink_proto.NAV_CONTROLLER_OUTPUT_nav_roll", "nav_roll (float)")
f.NAV_CONTROLLER_OUTPUT_nav_pitch = ProtoField.float("mavlink_proto.NAV_CONTROLLER_OUTPUT_nav_pitch", "nav_pitch (float)")
f.NAV_CONTROLLER_OUTPUT_nav_bearing = ProtoField.int16("mavlink_proto.NAV_CONTROLLER_OUTPUT_nav_bearing", "nav_bearing (int16)")
f.NAV_CONTROLLER_OUTPUT_target_bearing = ProtoField.int16("mavlink_proto.NAV_CONTROLLER_OUTPUT_target_bearing", "target_bearing (int16)")
f.NAV_CONTROLLER_OUTPUT_wp_dist = ProtoField.uint16("mavlink_proto.NAV_CONTROLLER_OUTPUT_wp_dist", "wp_dist (uint16)")
f.NAV_CONTROLLER_OUTPUT_alt_error = ProtoField.float("mavlink_proto.NAV_CONTROLLER_OUTPUT_alt_error", "alt_error (float)")
f.NAV_CONTROLLER_OUTPUT_aspd_error = ProtoField.float("mavlink_proto.NAV_CONTROLLER_OUTPUT_aspd_error", "aspd_error (float)")
f.NAV_CONTROLLER_OUTPUT_xtrack_error = ProtoField.float("mavlink_proto.NAV_CONTROLLER_OUTPUT_xtrack_error", "xtrack_error (float)")

f.GLOBAL_POSITION_INT_COV_time_usec = ProtoField.uint64("mavlink_proto.GLOBAL_POSITION_INT_COV_time_usec", "time_usec (uint64)")
f.GLOBAL_POSITION_INT_COV_estimator_type = ProtoField.uint8("mavlink_proto.GLOBAL_POSITION_INT_COV_estimator_type", "estimator_type (uint8)")
f.GLOBAL_POSITION_INT_COV_lat = ProtoField.int32("mavlink_proto.GLOBAL_POSITION_INT_COV_lat", "lat (int32)")
f.GLOBAL_POSITION_INT_COV_lon = ProtoField.int32("mavlink_proto.GLOBAL_POSITION_INT_COV_lon", "lon (int32)")
f.GLOBAL_POSITION_INT_COV_alt = ProtoField.int32("mavlink_proto.GLOBAL_POSITION_INT_COV_alt", "alt (int32)")
f.GLOBAL_POSITION_INT_COV_relative_alt = ProtoField.int32("mavlink_proto.GLOBAL_POSITION_INT_COV_relative_alt", "relative_alt (int32)")
f.GLOBAL_POSITION_INT_COV_vx = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_vx", "vx (float)")
f.GLOBAL_POSITION_INT_COV_vy = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_vy", "vy (float)")
f.GLOBAL_POSITION_INT_COV_vz = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_vz", "vz (float)")
f.GLOBAL_POSITION_INT_COV_covariance_0 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_0", "covariance[0] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_1 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_1", "covariance[1] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_2 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_2", "covariance[2] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_3 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_3", "covariance[3] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_4 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_4", "covariance[4] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_5 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_5", "covariance[5] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_6 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_6", "covariance[6] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_7 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_7", "covariance[7] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_8 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_8", "covariance[8] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_9 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_9", "covariance[9] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_10 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_10", "covariance[10] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_11 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_11", "covariance[11] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_12 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_12", "covariance[12] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_13 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_13", "covariance[13] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_14 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_14", "covariance[14] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_15 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_15", "covariance[15] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_16 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_16", "covariance[16] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_17 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_17", "covariance[17] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_18 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_18", "covariance[18] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_19 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_19", "covariance[19] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_20 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_20", "covariance[20] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_21 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_21", "covariance[21] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_22 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_22", "covariance[22] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_23 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_23", "covariance[23] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_24 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_24", "covariance[24] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_25 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_25", "covariance[25] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_26 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_26", "covariance[26] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_27 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_27", "covariance[27] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_28 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_28", "covariance[28] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_29 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_29", "covariance[29] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_30 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_30", "covariance[30] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_31 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_31", "covariance[31] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_32 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_32", "covariance[32] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_33 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_33", "covariance[33] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_34 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_34", "covariance[34] (float)")
f.GLOBAL_POSITION_INT_COV_covariance_35 = ProtoField.float("mavlink_proto.GLOBAL_POSITION_INT_COV_covariance_35", "covariance[35] (float)")

f.LOCAL_POSITION_NED_COV_time_usec = ProtoField.uint64("mavlink_proto.LOCAL_POSITION_NED_COV_time_usec", "time_usec (uint64)")
f.LOCAL_POSITION_NED_COV_estimator_type = ProtoField.uint8("mavlink_proto.LOCAL_POSITION_NED_COV_estimator_type", "estimator_type (uint8)")
f.LOCAL_POSITION_NED_COV_x = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_x", "x (float)")
f.LOCAL_POSITION_NED_COV_y = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_y", "y (float)")
f.LOCAL_POSITION_NED_COV_z = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_z", "z (float)")
f.LOCAL_POSITION_NED_COV_vx = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_vx", "vx (float)")
f.LOCAL_POSITION_NED_COV_vy = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_vy", "vy (float)")
f.LOCAL_POSITION_NED_COV_vz = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_vz", "vz (float)")
f.LOCAL_POSITION_NED_COV_ax = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_ax", "ax (float)")
f.LOCAL_POSITION_NED_COV_ay = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_ay", "ay (float)")
f.LOCAL_POSITION_NED_COV_az = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_az", "az (float)")
f.LOCAL_POSITION_NED_COV_covariance_0 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_0", "covariance[0] (float)")
f.LOCAL_POSITION_NED_COV_covariance_1 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_1", "covariance[1] (float)")
f.LOCAL_POSITION_NED_COV_covariance_2 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_2", "covariance[2] (float)")
f.LOCAL_POSITION_NED_COV_covariance_3 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_3", "covariance[3] (float)")
f.LOCAL_POSITION_NED_COV_covariance_4 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_4", "covariance[4] (float)")
f.LOCAL_POSITION_NED_COV_covariance_5 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_5", "covariance[5] (float)")
f.LOCAL_POSITION_NED_COV_covariance_6 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_6", "covariance[6] (float)")
f.LOCAL_POSITION_NED_COV_covariance_7 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_7", "covariance[7] (float)")
f.LOCAL_POSITION_NED_COV_covariance_8 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_8", "covariance[8] (float)")
f.LOCAL_POSITION_NED_COV_covariance_9 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_9", "covariance[9] (float)")
f.LOCAL_POSITION_NED_COV_covariance_10 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_10", "covariance[10] (float)")
f.LOCAL_POSITION_NED_COV_covariance_11 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_11", "covariance[11] (float)")
f.LOCAL_POSITION_NED_COV_covariance_12 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_12", "covariance[12] (float)")
f.LOCAL_POSITION_NED_COV_covariance_13 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_13", "covariance[13] (float)")
f.LOCAL_POSITION_NED_COV_covariance_14 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_14", "covariance[14] (float)")
f.LOCAL_POSITION_NED_COV_covariance_15 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_15", "covariance[15] (float)")
f.LOCAL_POSITION_NED_COV_covariance_16 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_16", "covariance[16] (float)")
f.LOCAL_POSITION_NED_COV_covariance_17 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_17", "covariance[17] (float)")
f.LOCAL_POSITION_NED_COV_covariance_18 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_18", "covariance[18] (float)")
f.LOCAL_POSITION_NED_COV_covariance_19 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_19", "covariance[19] (float)")
f.LOCAL_POSITION_NED_COV_covariance_20 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_20", "covariance[20] (float)")
f.LOCAL_POSITION_NED_COV_covariance_21 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_21", "covariance[21] (float)")
f.LOCAL_POSITION_NED_COV_covariance_22 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_22", "covariance[22] (float)")
f.LOCAL_POSITION_NED_COV_covariance_23 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_23", "covariance[23] (float)")
f.LOCAL_POSITION_NED_COV_covariance_24 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_24", "covariance[24] (float)")
f.LOCAL_POSITION_NED_COV_covariance_25 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_25", "covariance[25] (float)")
f.LOCAL_POSITION_NED_COV_covariance_26 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_26", "covariance[26] (float)")
f.LOCAL_POSITION_NED_COV_covariance_27 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_27", "covariance[27] (float)")
f.LOCAL_POSITION_NED_COV_covariance_28 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_28", "covariance[28] (float)")
f.LOCAL_POSITION_NED_COV_covariance_29 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_29", "covariance[29] (float)")
f.LOCAL_POSITION_NED_COV_covariance_30 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_30", "covariance[30] (float)")
f.LOCAL_POSITION_NED_COV_covariance_31 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_31", "covariance[31] (float)")
f.LOCAL_POSITION_NED_COV_covariance_32 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_32", "covariance[32] (float)")
f.LOCAL_POSITION_NED_COV_covariance_33 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_33", "covariance[33] (float)")
f.LOCAL_POSITION_NED_COV_covariance_34 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_34", "covariance[34] (float)")
f.LOCAL_POSITION_NED_COV_covariance_35 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_35", "covariance[35] (float)")
f.LOCAL_POSITION_NED_COV_covariance_36 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_36", "covariance[36] (float)")
f.LOCAL_POSITION_NED_COV_covariance_37 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_37", "covariance[37] (float)")
f.LOCAL_POSITION_NED_COV_covariance_38 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_38", "covariance[38] (float)")
f.LOCAL_POSITION_NED_COV_covariance_39 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_39", "covariance[39] (float)")
f.LOCAL_POSITION_NED_COV_covariance_40 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_40", "covariance[40] (float)")
f.LOCAL_POSITION_NED_COV_covariance_41 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_41", "covariance[41] (float)")
f.LOCAL_POSITION_NED_COV_covariance_42 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_42", "covariance[42] (float)")
f.LOCAL_POSITION_NED_COV_covariance_43 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_43", "covariance[43] (float)")
f.LOCAL_POSITION_NED_COV_covariance_44 = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_COV_covariance_44", "covariance[44] (float)")

f.RC_CHANNELS_time_boot_ms = ProtoField.uint32("mavlink_proto.RC_CHANNELS_time_boot_ms", "time_boot_ms (uint32)")
f.RC_CHANNELS_chancount = ProtoField.uint8("mavlink_proto.RC_CHANNELS_chancount", "chancount (uint8)")
f.RC_CHANNELS_chan1_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan1_raw", "chan1_raw (uint16)")
f.RC_CHANNELS_chan2_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan2_raw", "chan2_raw (uint16)")
f.RC_CHANNELS_chan3_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan3_raw", "chan3_raw (uint16)")
f.RC_CHANNELS_chan4_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan4_raw", "chan4_raw (uint16)")
f.RC_CHANNELS_chan5_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan5_raw", "chan5_raw (uint16)")
f.RC_CHANNELS_chan6_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan6_raw", "chan6_raw (uint16)")
f.RC_CHANNELS_chan7_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan7_raw", "chan7_raw (uint16)")
f.RC_CHANNELS_chan8_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan8_raw", "chan8_raw (uint16)")
f.RC_CHANNELS_chan9_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan9_raw", "chan9_raw (uint16)")
f.RC_CHANNELS_chan10_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan10_raw", "chan10_raw (uint16)")
f.RC_CHANNELS_chan11_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan11_raw", "chan11_raw (uint16)")
f.RC_CHANNELS_chan12_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan12_raw", "chan12_raw (uint16)")
f.RC_CHANNELS_chan13_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan13_raw", "chan13_raw (uint16)")
f.RC_CHANNELS_chan14_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan14_raw", "chan14_raw (uint16)")
f.RC_CHANNELS_chan15_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan15_raw", "chan15_raw (uint16)")
f.RC_CHANNELS_chan16_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan16_raw", "chan16_raw (uint16)")
f.RC_CHANNELS_chan17_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan17_raw", "chan17_raw (uint16)")
f.RC_CHANNELS_chan18_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_chan18_raw", "chan18_raw (uint16)")
f.RC_CHANNELS_rssi = ProtoField.uint8("mavlink_proto.RC_CHANNELS_rssi", "rssi (uint8)")

f.REQUEST_DATA_STREAM_target_system = ProtoField.uint8("mavlink_proto.REQUEST_DATA_STREAM_target_system", "target_system (uint8)")
f.REQUEST_DATA_STREAM_target_component = ProtoField.uint8("mavlink_proto.REQUEST_DATA_STREAM_target_component", "target_component (uint8)")
f.REQUEST_DATA_STREAM_req_stream_id = ProtoField.uint8("mavlink_proto.REQUEST_DATA_STREAM_req_stream_id", "req_stream_id (uint8)")
f.REQUEST_DATA_STREAM_req_message_rate = ProtoField.uint16("mavlink_proto.REQUEST_DATA_STREAM_req_message_rate", "req_message_rate (uint16)")
f.REQUEST_DATA_STREAM_start_stop = ProtoField.uint8("mavlink_proto.REQUEST_DATA_STREAM_start_stop", "start_stop (uint8)")

f.DATA_STREAM_stream_id = ProtoField.uint8("mavlink_proto.DATA_STREAM_stream_id", "stream_id (uint8)")
f.DATA_STREAM_message_rate = ProtoField.uint16("mavlink_proto.DATA_STREAM_message_rate", "message_rate (uint16)")
f.DATA_STREAM_on_off = ProtoField.uint8("mavlink_proto.DATA_STREAM_on_off", "on_off (uint8)")

f.MANUAL_CONTROL_target = ProtoField.uint8("mavlink_proto.MANUAL_CONTROL_target", "target (uint8)")
f.MANUAL_CONTROL_x = ProtoField.int16("mavlink_proto.MANUAL_CONTROL_x", "x (int16)")
f.MANUAL_CONTROL_y = ProtoField.int16("mavlink_proto.MANUAL_CONTROL_y", "y (int16)")
f.MANUAL_CONTROL_z = ProtoField.int16("mavlink_proto.MANUAL_CONTROL_z", "z (int16)")
f.MANUAL_CONTROL_r = ProtoField.int16("mavlink_proto.MANUAL_CONTROL_r", "r (int16)")
f.MANUAL_CONTROL_buttons = ProtoField.uint16("mavlink_proto.MANUAL_CONTROL_buttons", "buttons (uint16)")

f.RC_CHANNELS_OVERRIDE_target_system = ProtoField.uint8("mavlink_proto.RC_CHANNELS_OVERRIDE_target_system", "target_system (uint8)")
f.RC_CHANNELS_OVERRIDE_target_component = ProtoField.uint8("mavlink_proto.RC_CHANNELS_OVERRIDE_target_component", "target_component (uint8)")
f.RC_CHANNELS_OVERRIDE_chan1_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_OVERRIDE_chan1_raw", "chan1_raw (uint16)")
f.RC_CHANNELS_OVERRIDE_chan2_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_OVERRIDE_chan2_raw", "chan2_raw (uint16)")
f.RC_CHANNELS_OVERRIDE_chan3_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_OVERRIDE_chan3_raw", "chan3_raw (uint16)")
f.RC_CHANNELS_OVERRIDE_chan4_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_OVERRIDE_chan4_raw", "chan4_raw (uint16)")
f.RC_CHANNELS_OVERRIDE_chan5_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_OVERRIDE_chan5_raw", "chan5_raw (uint16)")
f.RC_CHANNELS_OVERRIDE_chan6_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_OVERRIDE_chan6_raw", "chan6_raw (uint16)")
f.RC_CHANNELS_OVERRIDE_chan7_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_OVERRIDE_chan7_raw", "chan7_raw (uint16)")
f.RC_CHANNELS_OVERRIDE_chan8_raw = ProtoField.uint16("mavlink_proto.RC_CHANNELS_OVERRIDE_chan8_raw", "chan8_raw (uint16)")

f.MISSION_ITEM_INT_target_system = ProtoField.uint8("mavlink_proto.MISSION_ITEM_INT_target_system", "target_system (uint8)")
f.MISSION_ITEM_INT_target_component = ProtoField.uint8("mavlink_proto.MISSION_ITEM_INT_target_component", "target_component (uint8)")
f.MISSION_ITEM_INT_seq = ProtoField.uint16("mavlink_proto.MISSION_ITEM_INT_seq", "seq (uint16)")
f.MISSION_ITEM_INT_frame = ProtoField.uint8("mavlink_proto.MISSION_ITEM_INT_frame", "frame (uint8)")
f.MISSION_ITEM_INT_command = ProtoField.uint16("mavlink_proto.MISSION_ITEM_INT_command", "command (uint16)")
f.MISSION_ITEM_INT_current = ProtoField.uint8("mavlink_proto.MISSION_ITEM_INT_current", "current (uint8)")
f.MISSION_ITEM_INT_autocontinue = ProtoField.uint8("mavlink_proto.MISSION_ITEM_INT_autocontinue", "autocontinue (uint8)")
f.MISSION_ITEM_INT_param1 = ProtoField.float("mavlink_proto.MISSION_ITEM_INT_param1", "param1 (float)")
f.MISSION_ITEM_INT_param2 = ProtoField.float("mavlink_proto.MISSION_ITEM_INT_param2", "param2 (float)")
f.MISSION_ITEM_INT_param3 = ProtoField.float("mavlink_proto.MISSION_ITEM_INT_param3", "param3 (float)")
f.MISSION_ITEM_INT_param4 = ProtoField.float("mavlink_proto.MISSION_ITEM_INT_param4", "param4 (float)")
f.MISSION_ITEM_INT_x = ProtoField.int32("mavlink_proto.MISSION_ITEM_INT_x", "x (int32)")
f.MISSION_ITEM_INT_y = ProtoField.int32("mavlink_proto.MISSION_ITEM_INT_y", "y (int32)")
f.MISSION_ITEM_INT_z = ProtoField.float("mavlink_proto.MISSION_ITEM_INT_z", "z (float)")

f.VFR_HUD_airspeed = ProtoField.float("mavlink_proto.VFR_HUD_airspeed", "airspeed (float)")
f.VFR_HUD_groundspeed = ProtoField.float("mavlink_proto.VFR_HUD_groundspeed", "groundspeed (float)")
f.VFR_HUD_heading = ProtoField.int16("mavlink_proto.VFR_HUD_heading", "heading (int16)")
f.VFR_HUD_throttle = ProtoField.uint16("mavlink_proto.VFR_HUD_throttle", "throttle (uint16)")
f.VFR_HUD_alt = ProtoField.float("mavlink_proto.VFR_HUD_alt", "alt (float)")
f.VFR_HUD_climb = ProtoField.float("mavlink_proto.VFR_HUD_climb", "climb (float)")

f.COMMAND_INT_target_system = ProtoField.uint8("mavlink_proto.COMMAND_INT_target_system", "target_system (uint8)")
f.COMMAND_INT_target_component = ProtoField.uint8("mavlink_proto.COMMAND_INT_target_component", "target_component (uint8)")
f.COMMAND_INT_frame = ProtoField.uint8("mavlink_proto.COMMAND_INT_frame", "frame (uint8)")
f.COMMAND_INT_command = ProtoField.uint16("mavlink_proto.COMMAND_INT_command", "command (uint16)")
f.COMMAND_INT_current = ProtoField.uint8("mavlink_proto.COMMAND_INT_current", "current (uint8)")
f.COMMAND_INT_autocontinue = ProtoField.uint8("mavlink_proto.COMMAND_INT_autocontinue", "autocontinue (uint8)")
f.COMMAND_INT_param1 = ProtoField.float("mavlink_proto.COMMAND_INT_param1", "param1 (float)")
f.COMMAND_INT_param2 = ProtoField.float("mavlink_proto.COMMAND_INT_param2", "param2 (float)")
f.COMMAND_INT_param3 = ProtoField.float("mavlink_proto.COMMAND_INT_param3", "param3 (float)")
f.COMMAND_INT_param4 = ProtoField.float("mavlink_proto.COMMAND_INT_param4", "param4 (float)")
f.COMMAND_INT_x = ProtoField.int32("mavlink_proto.COMMAND_INT_x", "x (int32)")
f.COMMAND_INT_y = ProtoField.int32("mavlink_proto.COMMAND_INT_y", "y (int32)")
f.COMMAND_INT_z = ProtoField.float("mavlink_proto.COMMAND_INT_z", "z (float)")

f.COMMAND_LONG_target_system = ProtoField.uint8("mavlink_proto.COMMAND_LONG_target_system", "target_system (uint8)")
f.COMMAND_LONG_target_component = ProtoField.uint8("mavlink_proto.COMMAND_LONG_target_component", "target_component (uint8)")
f.COMMAND_LONG_command = ProtoField.uint16("mavlink_proto.COMMAND_LONG_command", "command (uint16)")
f.COMMAND_LONG_confirmation = ProtoField.uint8("mavlink_proto.COMMAND_LONG_confirmation", "confirmation (uint8)")
f.COMMAND_LONG_param1 = ProtoField.float("mavlink_proto.COMMAND_LONG_param1", "param1 (float)")
f.COMMAND_LONG_param2 = ProtoField.float("mavlink_proto.COMMAND_LONG_param2", "param2 (float)")
f.COMMAND_LONG_param3 = ProtoField.float("mavlink_proto.COMMAND_LONG_param3", "param3 (float)")
f.COMMAND_LONG_param4 = ProtoField.float("mavlink_proto.COMMAND_LONG_param4", "param4 (float)")
f.COMMAND_LONG_param5 = ProtoField.float("mavlink_proto.COMMAND_LONG_param5", "param5 (float)")
f.COMMAND_LONG_param6 = ProtoField.float("mavlink_proto.COMMAND_LONG_param6", "param6 (float)")
f.COMMAND_LONG_param7 = ProtoField.float("mavlink_proto.COMMAND_LONG_param7", "param7 (float)")

f.COMMAND_ACK_command = ProtoField.uint16("mavlink_proto.COMMAND_ACK_command", "command (uint16)")
f.COMMAND_ACK_result = ProtoField.uint8("mavlink_proto.COMMAND_ACK_result", "result (uint8)")

f.MANUAL_SETPOINT_time_boot_ms = ProtoField.uint32("mavlink_proto.MANUAL_SETPOINT_time_boot_ms", "time_boot_ms (uint32)")
f.MANUAL_SETPOINT_roll = ProtoField.float("mavlink_proto.MANUAL_SETPOINT_roll", "roll (float)")
f.MANUAL_SETPOINT_pitch = ProtoField.float("mavlink_proto.MANUAL_SETPOINT_pitch", "pitch (float)")
f.MANUAL_SETPOINT_yaw = ProtoField.float("mavlink_proto.MANUAL_SETPOINT_yaw", "yaw (float)")
f.MANUAL_SETPOINT_thrust = ProtoField.float("mavlink_proto.MANUAL_SETPOINT_thrust", "thrust (float)")
f.MANUAL_SETPOINT_mode_switch = ProtoField.uint8("mavlink_proto.MANUAL_SETPOINT_mode_switch", "mode_switch (uint8)")
f.MANUAL_SETPOINT_manual_override_switch = ProtoField.uint8("mavlink_proto.MANUAL_SETPOINT_manual_override_switch", "manual_override_switch (uint8)")

f.SET_ATTITUDE_TARGET_time_boot_ms = ProtoField.uint32("mavlink_proto.SET_ATTITUDE_TARGET_time_boot_ms", "time_boot_ms (uint32)")
f.SET_ATTITUDE_TARGET_target_system = ProtoField.uint8("mavlink_proto.SET_ATTITUDE_TARGET_target_system", "target_system (uint8)")
f.SET_ATTITUDE_TARGET_target_component = ProtoField.uint8("mavlink_proto.SET_ATTITUDE_TARGET_target_component", "target_component (uint8)")
f.SET_ATTITUDE_TARGET_type_mask = ProtoField.uint8("mavlink_proto.SET_ATTITUDE_TARGET_type_mask", "type_mask (uint8)")
f.SET_ATTITUDE_TARGET_q_0 = ProtoField.float("mavlink_proto.SET_ATTITUDE_TARGET_q_0", "q[0] (float)")
f.SET_ATTITUDE_TARGET_q_1 = ProtoField.float("mavlink_proto.SET_ATTITUDE_TARGET_q_1", "q[1] (float)")
f.SET_ATTITUDE_TARGET_q_2 = ProtoField.float("mavlink_proto.SET_ATTITUDE_TARGET_q_2", "q[2] (float)")
f.SET_ATTITUDE_TARGET_q_3 = ProtoField.float("mavlink_proto.SET_ATTITUDE_TARGET_q_3", "q[3] (float)")
f.SET_ATTITUDE_TARGET_body_roll_rate = ProtoField.float("mavlink_proto.SET_ATTITUDE_TARGET_body_roll_rate", "body_roll_rate (float)")
f.SET_ATTITUDE_TARGET_body_pitch_rate = ProtoField.float("mavlink_proto.SET_ATTITUDE_TARGET_body_pitch_rate", "body_pitch_rate (float)")
f.SET_ATTITUDE_TARGET_body_yaw_rate = ProtoField.float("mavlink_proto.SET_ATTITUDE_TARGET_body_yaw_rate", "body_yaw_rate (float)")
f.SET_ATTITUDE_TARGET_thrust = ProtoField.float("mavlink_proto.SET_ATTITUDE_TARGET_thrust", "thrust (float)")

f.ATTITUDE_TARGET_time_boot_ms = ProtoField.uint32("mavlink_proto.ATTITUDE_TARGET_time_boot_ms", "time_boot_ms (uint32)")
f.ATTITUDE_TARGET_type_mask = ProtoField.uint8("mavlink_proto.ATTITUDE_TARGET_type_mask", "type_mask (uint8)")
f.ATTITUDE_TARGET_q_0 = ProtoField.float("mavlink_proto.ATTITUDE_TARGET_q_0", "q[0] (float)")
f.ATTITUDE_TARGET_q_1 = ProtoField.float("mavlink_proto.ATTITUDE_TARGET_q_1", "q[1] (float)")
f.ATTITUDE_TARGET_q_2 = ProtoField.float("mavlink_proto.ATTITUDE_TARGET_q_2", "q[2] (float)")
f.ATTITUDE_TARGET_q_3 = ProtoField.float("mavlink_proto.ATTITUDE_TARGET_q_3", "q[3] (float)")
f.ATTITUDE_TARGET_body_roll_rate = ProtoField.float("mavlink_proto.ATTITUDE_TARGET_body_roll_rate", "body_roll_rate (float)")
f.ATTITUDE_TARGET_body_pitch_rate = ProtoField.float("mavlink_proto.ATTITUDE_TARGET_body_pitch_rate", "body_pitch_rate (float)")
f.ATTITUDE_TARGET_body_yaw_rate = ProtoField.float("mavlink_proto.ATTITUDE_TARGET_body_yaw_rate", "body_yaw_rate (float)")
f.ATTITUDE_TARGET_thrust = ProtoField.float("mavlink_proto.ATTITUDE_TARGET_thrust", "thrust (float)")

f.SET_POSITION_TARGET_LOCAL_NED_time_boot_ms = ProtoField.uint32("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_time_boot_ms", "time_boot_ms (uint32)")
f.SET_POSITION_TARGET_LOCAL_NED_target_system = ProtoField.uint8("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_target_system", "target_system (uint8)")
f.SET_POSITION_TARGET_LOCAL_NED_target_component = ProtoField.uint8("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_target_component", "target_component (uint8)")
f.SET_POSITION_TARGET_LOCAL_NED_coordinate_frame = ProtoField.uint8("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_coordinate_frame", "coordinate_frame (uint8)")
f.SET_POSITION_TARGET_LOCAL_NED_type_mask = ProtoField.uint16("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_type_mask", "type_mask (uint16)")
f.SET_POSITION_TARGET_LOCAL_NED_x = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_x", "x (float)")
f.SET_POSITION_TARGET_LOCAL_NED_y = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_y", "y (float)")
f.SET_POSITION_TARGET_LOCAL_NED_z = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_z", "z (float)")
f.SET_POSITION_TARGET_LOCAL_NED_vx = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_vx", "vx (float)")
f.SET_POSITION_TARGET_LOCAL_NED_vy = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_vy", "vy (float)")
f.SET_POSITION_TARGET_LOCAL_NED_vz = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_vz", "vz (float)")
f.SET_POSITION_TARGET_LOCAL_NED_afx = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_afx", "afx (float)")
f.SET_POSITION_TARGET_LOCAL_NED_afy = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_afy", "afy (float)")
f.SET_POSITION_TARGET_LOCAL_NED_afz = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_afz", "afz (float)")
f.SET_POSITION_TARGET_LOCAL_NED_yaw = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_yaw", "yaw (float)")
f.SET_POSITION_TARGET_LOCAL_NED_yaw_rate = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_LOCAL_NED_yaw_rate", "yaw_rate (float)")

f.POSITION_TARGET_LOCAL_NED_time_boot_ms = ProtoField.uint32("mavlink_proto.POSITION_TARGET_LOCAL_NED_time_boot_ms", "time_boot_ms (uint32)")
f.POSITION_TARGET_LOCAL_NED_coordinate_frame = ProtoField.uint8("mavlink_proto.POSITION_TARGET_LOCAL_NED_coordinate_frame", "coordinate_frame (uint8)")
f.POSITION_TARGET_LOCAL_NED_type_mask = ProtoField.uint16("mavlink_proto.POSITION_TARGET_LOCAL_NED_type_mask", "type_mask (uint16)")
f.POSITION_TARGET_LOCAL_NED_x = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_x", "x (float)")
f.POSITION_TARGET_LOCAL_NED_y = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_y", "y (float)")
f.POSITION_TARGET_LOCAL_NED_z = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_z", "z (float)")
f.POSITION_TARGET_LOCAL_NED_vx = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_vx", "vx (float)")
f.POSITION_TARGET_LOCAL_NED_vy = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_vy", "vy (float)")
f.POSITION_TARGET_LOCAL_NED_vz = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_vz", "vz (float)")
f.POSITION_TARGET_LOCAL_NED_afx = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_afx", "afx (float)")
f.POSITION_TARGET_LOCAL_NED_afy = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_afy", "afy (float)")
f.POSITION_TARGET_LOCAL_NED_afz = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_afz", "afz (float)")
f.POSITION_TARGET_LOCAL_NED_yaw = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_yaw", "yaw (float)")
f.POSITION_TARGET_LOCAL_NED_yaw_rate = ProtoField.float("mavlink_proto.POSITION_TARGET_LOCAL_NED_yaw_rate", "yaw_rate (float)")

f.SET_POSITION_TARGET_GLOBAL_INT_time_boot_ms = ProtoField.uint32("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_time_boot_ms", "time_boot_ms (uint32)")
f.SET_POSITION_TARGET_GLOBAL_INT_target_system = ProtoField.uint8("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_target_system", "target_system (uint8)")
f.SET_POSITION_TARGET_GLOBAL_INT_target_component = ProtoField.uint8("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_target_component", "target_component (uint8)")
f.SET_POSITION_TARGET_GLOBAL_INT_coordinate_frame = ProtoField.uint8("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_coordinate_frame", "coordinate_frame (uint8)")
f.SET_POSITION_TARGET_GLOBAL_INT_type_mask = ProtoField.uint16("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_type_mask", "type_mask (uint16)")
f.SET_POSITION_TARGET_GLOBAL_INT_lat_int = ProtoField.int32("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_lat_int", "lat_int (int32)")
f.SET_POSITION_TARGET_GLOBAL_INT_lon_int = ProtoField.int32("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_lon_int", "lon_int (int32)")
f.SET_POSITION_TARGET_GLOBAL_INT_alt = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_alt", "alt (float)")
f.SET_POSITION_TARGET_GLOBAL_INT_vx = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_vx", "vx (float)")
f.SET_POSITION_TARGET_GLOBAL_INT_vy = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_vy", "vy (float)")
f.SET_POSITION_TARGET_GLOBAL_INT_vz = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_vz", "vz (float)")
f.SET_POSITION_TARGET_GLOBAL_INT_afx = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_afx", "afx (float)")
f.SET_POSITION_TARGET_GLOBAL_INT_afy = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_afy", "afy (float)")
f.SET_POSITION_TARGET_GLOBAL_INT_afz = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_afz", "afz (float)")
f.SET_POSITION_TARGET_GLOBAL_INT_yaw = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_yaw", "yaw (float)")
f.SET_POSITION_TARGET_GLOBAL_INT_yaw_rate = ProtoField.float("mavlink_proto.SET_POSITION_TARGET_GLOBAL_INT_yaw_rate", "yaw_rate (float)")

f.POSITION_TARGET_GLOBAL_INT_time_boot_ms = ProtoField.uint32("mavlink_proto.POSITION_TARGET_GLOBAL_INT_time_boot_ms", "time_boot_ms (uint32)")
f.POSITION_TARGET_GLOBAL_INT_coordinate_frame = ProtoField.uint8("mavlink_proto.POSITION_TARGET_GLOBAL_INT_coordinate_frame", "coordinate_frame (uint8)")
f.POSITION_TARGET_GLOBAL_INT_type_mask = ProtoField.uint16("mavlink_proto.POSITION_TARGET_GLOBAL_INT_type_mask", "type_mask (uint16)")
f.POSITION_TARGET_GLOBAL_INT_lat_int = ProtoField.int32("mavlink_proto.POSITION_TARGET_GLOBAL_INT_lat_int", "lat_int (int32)")
f.POSITION_TARGET_GLOBAL_INT_lon_int = ProtoField.int32("mavlink_proto.POSITION_TARGET_GLOBAL_INT_lon_int", "lon_int (int32)")
f.POSITION_TARGET_GLOBAL_INT_alt = ProtoField.float("mavlink_proto.POSITION_TARGET_GLOBAL_INT_alt", "alt (float)")
f.POSITION_TARGET_GLOBAL_INT_vx = ProtoField.float("mavlink_proto.POSITION_TARGET_GLOBAL_INT_vx", "vx (float)")
f.POSITION_TARGET_GLOBAL_INT_vy = ProtoField.float("mavlink_proto.POSITION_TARGET_GLOBAL_INT_vy", "vy (float)")
f.POSITION_TARGET_GLOBAL_INT_vz = ProtoField.float("mavlink_proto.POSITION_TARGET_GLOBAL_INT_vz", "vz (float)")
f.POSITION_TARGET_GLOBAL_INT_afx = ProtoField.float("mavlink_proto.POSITION_TARGET_GLOBAL_INT_afx", "afx (float)")
f.POSITION_TARGET_GLOBAL_INT_afy = ProtoField.float("mavlink_proto.POSITION_TARGET_GLOBAL_INT_afy", "afy (float)")
f.POSITION_TARGET_GLOBAL_INT_afz = ProtoField.float("mavlink_proto.POSITION_TARGET_GLOBAL_INT_afz", "afz (float)")
f.POSITION_TARGET_GLOBAL_INT_yaw = ProtoField.float("mavlink_proto.POSITION_TARGET_GLOBAL_INT_yaw", "yaw (float)")
f.POSITION_TARGET_GLOBAL_INT_yaw_rate = ProtoField.float("mavlink_proto.POSITION_TARGET_GLOBAL_INT_yaw_rate", "yaw_rate (float)")

f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_time_boot_ms = ProtoField.uint32("mavlink_proto.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_time_boot_ms", "time_boot_ms (uint32)")
f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_x = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_x", "x (float)")
f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_y = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_y", "y (float)")
f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_z = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_z", "z (float)")
f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_roll = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_roll", "roll (float)")
f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_pitch = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_pitch", "pitch (float)")
f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_yaw = ProtoField.float("mavlink_proto.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_yaw", "yaw (float)")

f.HIL_STATE_time_usec = ProtoField.uint64("mavlink_proto.HIL_STATE_time_usec", "time_usec (uint64)")
f.HIL_STATE_roll = ProtoField.float("mavlink_proto.HIL_STATE_roll", "roll (float)")
f.HIL_STATE_pitch = ProtoField.float("mavlink_proto.HIL_STATE_pitch", "pitch (float)")
f.HIL_STATE_yaw = ProtoField.float("mavlink_proto.HIL_STATE_yaw", "yaw (float)")
f.HIL_STATE_rollspeed = ProtoField.float("mavlink_proto.HIL_STATE_rollspeed", "rollspeed (float)")
f.HIL_STATE_pitchspeed = ProtoField.float("mavlink_proto.HIL_STATE_pitchspeed", "pitchspeed (float)")
f.HIL_STATE_yawspeed = ProtoField.float("mavlink_proto.HIL_STATE_yawspeed", "yawspeed (float)")
f.HIL_STATE_lat = ProtoField.int32("mavlink_proto.HIL_STATE_lat", "lat (int32)")
f.HIL_STATE_lon = ProtoField.int32("mavlink_proto.HIL_STATE_lon", "lon (int32)")
f.HIL_STATE_alt = ProtoField.int32("mavlink_proto.HIL_STATE_alt", "alt (int32)")
f.HIL_STATE_vx = ProtoField.int16("mavlink_proto.HIL_STATE_vx", "vx (int16)")
f.HIL_STATE_vy = ProtoField.int16("mavlink_proto.HIL_STATE_vy", "vy (int16)")
f.HIL_STATE_vz = ProtoField.int16("mavlink_proto.HIL_STATE_vz", "vz (int16)")
f.HIL_STATE_xacc = ProtoField.int16("mavlink_proto.HIL_STATE_xacc", "xacc (int16)")
f.HIL_STATE_yacc = ProtoField.int16("mavlink_proto.HIL_STATE_yacc", "yacc (int16)")
f.HIL_STATE_zacc = ProtoField.int16("mavlink_proto.HIL_STATE_zacc", "zacc (int16)")

f.HIL_CONTROLS_time_usec = ProtoField.uint64("mavlink_proto.HIL_CONTROLS_time_usec", "time_usec (uint64)")
f.HIL_CONTROLS_roll_ailerons = ProtoField.float("mavlink_proto.HIL_CONTROLS_roll_ailerons", "roll_ailerons (float)")
f.HIL_CONTROLS_pitch_elevator = ProtoField.float("mavlink_proto.HIL_CONTROLS_pitch_elevator", "pitch_elevator (float)")
f.HIL_CONTROLS_yaw_rudder = ProtoField.float("mavlink_proto.HIL_CONTROLS_yaw_rudder", "yaw_rudder (float)")
f.HIL_CONTROLS_throttle = ProtoField.float("mavlink_proto.HIL_CONTROLS_throttle", "throttle (float)")
f.HIL_CONTROLS_aux1 = ProtoField.float("mavlink_proto.HIL_CONTROLS_aux1", "aux1 (float)")
f.HIL_CONTROLS_aux2 = ProtoField.float("mavlink_proto.HIL_CONTROLS_aux2", "aux2 (float)")
f.HIL_CONTROLS_aux3 = ProtoField.float("mavlink_proto.HIL_CONTROLS_aux3", "aux3 (float)")
f.HIL_CONTROLS_aux4 = ProtoField.float("mavlink_proto.HIL_CONTROLS_aux4", "aux4 (float)")
f.HIL_CONTROLS_mode = ProtoField.uint8("mavlink_proto.HIL_CONTROLS_mode", "mode (uint8)")
f.HIL_CONTROLS_nav_mode = ProtoField.uint8("mavlink_proto.HIL_CONTROLS_nav_mode", "nav_mode (uint8)")

f.HIL_RC_INPUTS_RAW_time_usec = ProtoField.uint64("mavlink_proto.HIL_RC_INPUTS_RAW_time_usec", "time_usec (uint64)")
f.HIL_RC_INPUTS_RAW_chan1_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan1_raw", "chan1_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan2_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan2_raw", "chan2_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan3_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan3_raw", "chan3_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan4_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan4_raw", "chan4_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan5_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan5_raw", "chan5_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan6_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan6_raw", "chan6_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan7_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan7_raw", "chan7_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan8_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan8_raw", "chan8_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan9_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan9_raw", "chan9_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan10_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan10_raw", "chan10_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan11_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan11_raw", "chan11_raw (uint16)")
f.HIL_RC_INPUTS_RAW_chan12_raw = ProtoField.uint16("mavlink_proto.HIL_RC_INPUTS_RAW_chan12_raw", "chan12_raw (uint16)")
f.HIL_RC_INPUTS_RAW_rssi = ProtoField.uint8("mavlink_proto.HIL_RC_INPUTS_RAW_rssi", "rssi (uint8)")

f.HIL_ACTUATOR_CONTROLS_time_usec = ProtoField.uint64("mavlink_proto.HIL_ACTUATOR_CONTROLS_time_usec", "time_usec (uint64)")
f.HIL_ACTUATOR_CONTROLS_controls_0 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_0", "controls[0] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_1 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_1", "controls[1] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_2 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_2", "controls[2] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_3 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_3", "controls[3] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_4 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_4", "controls[4] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_5 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_5", "controls[5] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_6 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_6", "controls[6] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_7 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_7", "controls[7] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_8 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_8", "controls[8] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_9 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_9", "controls[9] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_10 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_10", "controls[10] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_11 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_11", "controls[11] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_12 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_12", "controls[12] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_13 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_13", "controls[13] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_14 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_14", "controls[14] (float)")
f.HIL_ACTUATOR_CONTROLS_controls_15 = ProtoField.float("mavlink_proto.HIL_ACTUATOR_CONTROLS_controls_15", "controls[15] (float)")
f.HIL_ACTUATOR_CONTROLS_mode = ProtoField.uint8("mavlink_proto.HIL_ACTUATOR_CONTROLS_mode", "mode (uint8)")
f.HIL_ACTUATOR_CONTROLS_flags = ProtoField.uint64("mavlink_proto.HIL_ACTUATOR_CONTROLS_flags", "flags (uint64)")

f.OPTICAL_FLOW_time_usec = ProtoField.uint64("mavlink_proto.OPTICAL_FLOW_time_usec", "time_usec (uint64)")
f.OPTICAL_FLOW_sensor_id = ProtoField.uint8("mavlink_proto.OPTICAL_FLOW_sensor_id", "sensor_id (uint8)")
f.OPTICAL_FLOW_flow_x = ProtoField.int16("mavlink_proto.OPTICAL_FLOW_flow_x", "flow_x (int16)")
f.OPTICAL_FLOW_flow_y = ProtoField.int16("mavlink_proto.OPTICAL_FLOW_flow_y", "flow_y (int16)")
f.OPTICAL_FLOW_flow_comp_m_x = ProtoField.float("mavlink_proto.OPTICAL_FLOW_flow_comp_m_x", "flow_comp_m_x (float)")
f.OPTICAL_FLOW_flow_comp_m_y = ProtoField.float("mavlink_proto.OPTICAL_FLOW_flow_comp_m_y", "flow_comp_m_y (float)")
f.OPTICAL_FLOW_quality = ProtoField.uint8("mavlink_proto.OPTICAL_FLOW_quality", "quality (uint8)")
f.OPTICAL_FLOW_ground_distance = ProtoField.float("mavlink_proto.OPTICAL_FLOW_ground_distance", "ground_distance (float)")

f.GLOBAL_VISION_POSITION_ESTIMATE_usec = ProtoField.uint64("mavlink_proto.GLOBAL_VISION_POSITION_ESTIMATE_usec", "usec (uint64)")
f.GLOBAL_VISION_POSITION_ESTIMATE_x = ProtoField.float("mavlink_proto.GLOBAL_VISION_POSITION_ESTIMATE_x", "x (float)")
f.GLOBAL_VISION_POSITION_ESTIMATE_y = ProtoField.float("mavlink_proto.GLOBAL_VISION_POSITION_ESTIMATE_y", "y (float)")
f.GLOBAL_VISION_POSITION_ESTIMATE_z = ProtoField.float("mavlink_proto.GLOBAL_VISION_POSITION_ESTIMATE_z", "z (float)")
f.GLOBAL_VISION_POSITION_ESTIMATE_roll = ProtoField.float("mavlink_proto.GLOBAL_VISION_POSITION_ESTIMATE_roll", "roll (float)")
f.GLOBAL_VISION_POSITION_ESTIMATE_pitch = ProtoField.float("mavlink_proto.GLOBAL_VISION_POSITION_ESTIMATE_pitch", "pitch (float)")
f.GLOBAL_VISION_POSITION_ESTIMATE_yaw = ProtoField.float("mavlink_proto.GLOBAL_VISION_POSITION_ESTIMATE_yaw", "yaw (float)")

f.VISION_POSITION_ESTIMATE_usec = ProtoField.uint64("mavlink_proto.VISION_POSITION_ESTIMATE_usec", "usec (uint64)")
f.VISION_POSITION_ESTIMATE_x = ProtoField.float("mavlink_proto.VISION_POSITION_ESTIMATE_x", "x (float)")
f.VISION_POSITION_ESTIMATE_y = ProtoField.float("mavlink_proto.VISION_POSITION_ESTIMATE_y", "y (float)")
f.VISION_POSITION_ESTIMATE_z = ProtoField.float("mavlink_proto.VISION_POSITION_ESTIMATE_z", "z (float)")
f.VISION_POSITION_ESTIMATE_roll = ProtoField.float("mavlink_proto.VISION_POSITION_ESTIMATE_roll", "roll (float)")
f.VISION_POSITION_ESTIMATE_pitch = ProtoField.float("mavlink_proto.VISION_POSITION_ESTIMATE_pitch", "pitch (float)")
f.VISION_POSITION_ESTIMATE_yaw = ProtoField.float("mavlink_proto.VISION_POSITION_ESTIMATE_yaw", "yaw (float)")

f.VISION_SPEED_ESTIMATE_usec = ProtoField.uint64("mavlink_proto.VISION_SPEED_ESTIMATE_usec", "usec (uint64)")
f.VISION_SPEED_ESTIMATE_x = ProtoField.float("mavlink_proto.VISION_SPEED_ESTIMATE_x", "x (float)")
f.VISION_SPEED_ESTIMATE_y = ProtoField.float("mavlink_proto.VISION_SPEED_ESTIMATE_y", "y (float)")
f.VISION_SPEED_ESTIMATE_z = ProtoField.float("mavlink_proto.VISION_SPEED_ESTIMATE_z", "z (float)")

f.VICON_POSITION_ESTIMATE_usec = ProtoField.uint64("mavlink_proto.VICON_POSITION_ESTIMATE_usec", "usec (uint64)")
f.VICON_POSITION_ESTIMATE_x = ProtoField.float("mavlink_proto.VICON_POSITION_ESTIMATE_x", "x (float)")
f.VICON_POSITION_ESTIMATE_y = ProtoField.float("mavlink_proto.VICON_POSITION_ESTIMATE_y", "y (float)")
f.VICON_POSITION_ESTIMATE_z = ProtoField.float("mavlink_proto.VICON_POSITION_ESTIMATE_z", "z (float)")
f.VICON_POSITION_ESTIMATE_roll = ProtoField.float("mavlink_proto.VICON_POSITION_ESTIMATE_roll", "roll (float)")
f.VICON_POSITION_ESTIMATE_pitch = ProtoField.float("mavlink_proto.VICON_POSITION_ESTIMATE_pitch", "pitch (float)")
f.VICON_POSITION_ESTIMATE_yaw = ProtoField.float("mavlink_proto.VICON_POSITION_ESTIMATE_yaw", "yaw (float)")

f.HIGHRES_IMU_time_usec = ProtoField.uint64("mavlink_proto.HIGHRES_IMU_time_usec", "time_usec (uint64)")
f.HIGHRES_IMU_xacc = ProtoField.float("mavlink_proto.HIGHRES_IMU_xacc", "xacc (float)")
f.HIGHRES_IMU_yacc = ProtoField.float("mavlink_proto.HIGHRES_IMU_yacc", "yacc (float)")
f.HIGHRES_IMU_zacc = ProtoField.float("mavlink_proto.HIGHRES_IMU_zacc", "zacc (float)")
f.HIGHRES_IMU_xgyro = ProtoField.float("mavlink_proto.HIGHRES_IMU_xgyro", "xgyro (float)")
f.HIGHRES_IMU_ygyro = ProtoField.float("mavlink_proto.HIGHRES_IMU_ygyro", "ygyro (float)")
f.HIGHRES_IMU_zgyro = ProtoField.float("mavlink_proto.HIGHRES_IMU_zgyro", "zgyro (float)")
f.HIGHRES_IMU_xmag = ProtoField.float("mavlink_proto.HIGHRES_IMU_xmag", "xmag (float)")
f.HIGHRES_IMU_ymag = ProtoField.float("mavlink_proto.HIGHRES_IMU_ymag", "ymag (float)")
f.HIGHRES_IMU_zmag = ProtoField.float("mavlink_proto.HIGHRES_IMU_zmag", "zmag (float)")
f.HIGHRES_IMU_abs_pressure = ProtoField.float("mavlink_proto.HIGHRES_IMU_abs_pressure", "abs_pressure (float)")
f.HIGHRES_IMU_diff_pressure = ProtoField.float("mavlink_proto.HIGHRES_IMU_diff_pressure", "diff_pressure (float)")
f.HIGHRES_IMU_pressure_alt = ProtoField.float("mavlink_proto.HIGHRES_IMU_pressure_alt", "pressure_alt (float)")
f.HIGHRES_IMU_temperature = ProtoField.float("mavlink_proto.HIGHRES_IMU_temperature", "temperature (float)")
f.HIGHRES_IMU_fields_updated = ProtoField.uint16("mavlink_proto.HIGHRES_IMU_fields_updated", "fields_updated (uint16)")

f.OPTICAL_FLOW_RAD_time_usec = ProtoField.uint64("mavlink_proto.OPTICAL_FLOW_RAD_time_usec", "time_usec (uint64)")
f.OPTICAL_FLOW_RAD_sensor_id = ProtoField.uint8("mavlink_proto.OPTICAL_FLOW_RAD_sensor_id", "sensor_id (uint8)")
f.OPTICAL_FLOW_RAD_integration_time_us = ProtoField.uint32("mavlink_proto.OPTICAL_FLOW_RAD_integration_time_us", "integration_time_us (uint32)")
f.OPTICAL_FLOW_RAD_integrated_x = ProtoField.float("mavlink_proto.OPTICAL_FLOW_RAD_integrated_x", "integrated_x (float)")
f.OPTICAL_FLOW_RAD_integrated_y = ProtoField.float("mavlink_proto.OPTICAL_FLOW_RAD_integrated_y", "integrated_y (float)")
f.OPTICAL_FLOW_RAD_integrated_xgyro = ProtoField.float("mavlink_proto.OPTICAL_FLOW_RAD_integrated_xgyro", "integrated_xgyro (float)")
f.OPTICAL_FLOW_RAD_integrated_ygyro = ProtoField.float("mavlink_proto.OPTICAL_FLOW_RAD_integrated_ygyro", "integrated_ygyro (float)")
f.OPTICAL_FLOW_RAD_integrated_zgyro = ProtoField.float("mavlink_proto.OPTICAL_FLOW_RAD_integrated_zgyro", "integrated_zgyro (float)")
f.OPTICAL_FLOW_RAD_temperature = ProtoField.int16("mavlink_proto.OPTICAL_FLOW_RAD_temperature", "temperature (int16)")
f.OPTICAL_FLOW_RAD_quality = ProtoField.uint8("mavlink_proto.OPTICAL_FLOW_RAD_quality", "quality (uint8)")
f.OPTICAL_FLOW_RAD_time_delta_distance_us = ProtoField.uint32("mavlink_proto.OPTICAL_FLOW_RAD_time_delta_distance_us", "time_delta_distance_us (uint32)")
f.OPTICAL_FLOW_RAD_distance = ProtoField.float("mavlink_proto.OPTICAL_FLOW_RAD_distance", "distance (float)")

f.HIL_SENSOR_time_usec = ProtoField.uint64("mavlink_proto.HIL_SENSOR_time_usec", "time_usec (uint64)")
f.HIL_SENSOR_xacc = ProtoField.float("mavlink_proto.HIL_SENSOR_xacc", "xacc (float)")
f.HIL_SENSOR_yacc = ProtoField.float("mavlink_proto.HIL_SENSOR_yacc", "yacc (float)")
f.HIL_SENSOR_zacc = ProtoField.float("mavlink_proto.HIL_SENSOR_zacc", "zacc (float)")
f.HIL_SENSOR_xgyro = ProtoField.float("mavlink_proto.HIL_SENSOR_xgyro", "xgyro (float)")
f.HIL_SENSOR_ygyro = ProtoField.float("mavlink_proto.HIL_SENSOR_ygyro", "ygyro (float)")
f.HIL_SENSOR_zgyro = ProtoField.float("mavlink_proto.HIL_SENSOR_zgyro", "zgyro (float)")
f.HIL_SENSOR_xmag = ProtoField.float("mavlink_proto.HIL_SENSOR_xmag", "xmag (float)")
f.HIL_SENSOR_ymag = ProtoField.float("mavlink_proto.HIL_SENSOR_ymag", "ymag (float)")
f.HIL_SENSOR_zmag = ProtoField.float("mavlink_proto.HIL_SENSOR_zmag", "zmag (float)")
f.HIL_SENSOR_abs_pressure = ProtoField.float("mavlink_proto.HIL_SENSOR_abs_pressure", "abs_pressure (float)")
f.HIL_SENSOR_diff_pressure = ProtoField.float("mavlink_proto.HIL_SENSOR_diff_pressure", "diff_pressure (float)")
f.HIL_SENSOR_pressure_alt = ProtoField.float("mavlink_proto.HIL_SENSOR_pressure_alt", "pressure_alt (float)")
f.HIL_SENSOR_temperature = ProtoField.float("mavlink_proto.HIL_SENSOR_temperature", "temperature (float)")
f.HIL_SENSOR_fields_updated = ProtoField.uint32("mavlink_proto.HIL_SENSOR_fields_updated", "fields_updated (uint32)")

f.SIM_STATE_q1 = ProtoField.float("mavlink_proto.SIM_STATE_q1", "q1 (float)")
f.SIM_STATE_q2 = ProtoField.float("mavlink_proto.SIM_STATE_q2", "q2 (float)")
f.SIM_STATE_q3 = ProtoField.float("mavlink_proto.SIM_STATE_q3", "q3 (float)")
f.SIM_STATE_q4 = ProtoField.float("mavlink_proto.SIM_STATE_q4", "q4 (float)")
f.SIM_STATE_roll = ProtoField.float("mavlink_proto.SIM_STATE_roll", "roll (float)")
f.SIM_STATE_pitch = ProtoField.float("mavlink_proto.SIM_STATE_pitch", "pitch (float)")
f.SIM_STATE_yaw = ProtoField.float("mavlink_proto.SIM_STATE_yaw", "yaw (float)")
f.SIM_STATE_xacc = ProtoField.float("mavlink_proto.SIM_STATE_xacc", "xacc (float)")
f.SIM_STATE_yacc = ProtoField.float("mavlink_proto.SIM_STATE_yacc", "yacc (float)")
f.SIM_STATE_zacc = ProtoField.float("mavlink_proto.SIM_STATE_zacc", "zacc (float)")
f.SIM_STATE_xgyro = ProtoField.float("mavlink_proto.SIM_STATE_xgyro", "xgyro (float)")
f.SIM_STATE_ygyro = ProtoField.float("mavlink_proto.SIM_STATE_ygyro", "ygyro (float)")
f.SIM_STATE_zgyro = ProtoField.float("mavlink_proto.SIM_STATE_zgyro", "zgyro (float)")
f.SIM_STATE_lat = ProtoField.float("mavlink_proto.SIM_STATE_lat", "lat (float)")
f.SIM_STATE_lon = ProtoField.float("mavlink_proto.SIM_STATE_lon", "lon (float)")
f.SIM_STATE_alt = ProtoField.float("mavlink_proto.SIM_STATE_alt", "alt (float)")
f.SIM_STATE_std_dev_horz = ProtoField.float("mavlink_proto.SIM_STATE_std_dev_horz", "std_dev_horz (float)")
f.SIM_STATE_std_dev_vert = ProtoField.float("mavlink_proto.SIM_STATE_std_dev_vert", "std_dev_vert (float)")
f.SIM_STATE_vn = ProtoField.float("mavlink_proto.SIM_STATE_vn", "vn (float)")
f.SIM_STATE_ve = ProtoField.float("mavlink_proto.SIM_STATE_ve", "ve (float)")
f.SIM_STATE_vd = ProtoField.float("mavlink_proto.SIM_STATE_vd", "vd (float)")

f.RADIO_STATUS_rssi = ProtoField.uint8("mavlink_proto.RADIO_STATUS_rssi", "rssi (uint8)")
f.RADIO_STATUS_remrssi = ProtoField.uint8("mavlink_proto.RADIO_STATUS_remrssi", "remrssi (uint8)")
f.RADIO_STATUS_txbuf = ProtoField.uint8("mavlink_proto.RADIO_STATUS_txbuf", "txbuf (uint8)")
f.RADIO_STATUS_noise = ProtoField.uint8("mavlink_proto.RADIO_STATUS_noise", "noise (uint8)")
f.RADIO_STATUS_remnoise = ProtoField.uint8("mavlink_proto.RADIO_STATUS_remnoise", "remnoise (uint8)")
f.RADIO_STATUS_rxerrors = ProtoField.uint16("mavlink_proto.RADIO_STATUS_rxerrors", "rxerrors (uint16)")
f.RADIO_STATUS_fixed = ProtoField.uint16("mavlink_proto.RADIO_STATUS_fixed", "fixed (uint16)")

f.FILE_TRANSFER_PROTOCOL_target_network = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_target_network", "target_network (uint8)")
f.FILE_TRANSFER_PROTOCOL_target_system = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_target_system", "target_system (uint8)")
f.FILE_TRANSFER_PROTOCOL_target_component = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_target_component", "target_component (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_0 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_0", "payload[0] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_1 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_1", "payload[1] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_2 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_2", "payload[2] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_3 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_3", "payload[3] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_4 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_4", "payload[4] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_5 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_5", "payload[5] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_6 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_6", "payload[6] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_7 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_7", "payload[7] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_8 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_8", "payload[8] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_9 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_9", "payload[9] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_10 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_10", "payload[10] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_11 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_11", "payload[11] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_12 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_12", "payload[12] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_13 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_13", "payload[13] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_14 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_14", "payload[14] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_15 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_15", "payload[15] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_16 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_16", "payload[16] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_17 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_17", "payload[17] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_18 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_18", "payload[18] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_19 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_19", "payload[19] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_20 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_20", "payload[20] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_21 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_21", "payload[21] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_22 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_22", "payload[22] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_23 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_23", "payload[23] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_24 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_24", "payload[24] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_25 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_25", "payload[25] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_26 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_26", "payload[26] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_27 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_27", "payload[27] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_28 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_28", "payload[28] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_29 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_29", "payload[29] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_30 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_30", "payload[30] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_31 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_31", "payload[31] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_32 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_32", "payload[32] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_33 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_33", "payload[33] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_34 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_34", "payload[34] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_35 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_35", "payload[35] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_36 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_36", "payload[36] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_37 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_37", "payload[37] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_38 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_38", "payload[38] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_39 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_39", "payload[39] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_40 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_40", "payload[40] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_41 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_41", "payload[41] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_42 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_42", "payload[42] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_43 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_43", "payload[43] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_44 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_44", "payload[44] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_45 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_45", "payload[45] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_46 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_46", "payload[46] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_47 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_47", "payload[47] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_48 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_48", "payload[48] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_49 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_49", "payload[49] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_50 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_50", "payload[50] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_51 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_51", "payload[51] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_52 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_52", "payload[52] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_53 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_53", "payload[53] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_54 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_54", "payload[54] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_55 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_55", "payload[55] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_56 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_56", "payload[56] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_57 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_57", "payload[57] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_58 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_58", "payload[58] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_59 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_59", "payload[59] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_60 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_60", "payload[60] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_61 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_61", "payload[61] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_62 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_62", "payload[62] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_63 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_63", "payload[63] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_64 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_64", "payload[64] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_65 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_65", "payload[65] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_66 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_66", "payload[66] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_67 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_67", "payload[67] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_68 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_68", "payload[68] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_69 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_69", "payload[69] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_70 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_70", "payload[70] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_71 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_71", "payload[71] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_72 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_72", "payload[72] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_73 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_73", "payload[73] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_74 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_74", "payload[74] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_75 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_75", "payload[75] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_76 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_76", "payload[76] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_77 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_77", "payload[77] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_78 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_78", "payload[78] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_79 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_79", "payload[79] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_80 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_80", "payload[80] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_81 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_81", "payload[81] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_82 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_82", "payload[82] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_83 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_83", "payload[83] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_84 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_84", "payload[84] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_85 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_85", "payload[85] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_86 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_86", "payload[86] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_87 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_87", "payload[87] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_88 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_88", "payload[88] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_89 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_89", "payload[89] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_90 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_90", "payload[90] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_91 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_91", "payload[91] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_92 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_92", "payload[92] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_93 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_93", "payload[93] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_94 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_94", "payload[94] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_95 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_95", "payload[95] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_96 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_96", "payload[96] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_97 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_97", "payload[97] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_98 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_98", "payload[98] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_99 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_99", "payload[99] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_100 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_100", "payload[100] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_101 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_101", "payload[101] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_102 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_102", "payload[102] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_103 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_103", "payload[103] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_104 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_104", "payload[104] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_105 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_105", "payload[105] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_106 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_106", "payload[106] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_107 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_107", "payload[107] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_108 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_108", "payload[108] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_109 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_109", "payload[109] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_110 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_110", "payload[110] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_111 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_111", "payload[111] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_112 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_112", "payload[112] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_113 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_113", "payload[113] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_114 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_114", "payload[114] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_115 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_115", "payload[115] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_116 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_116", "payload[116] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_117 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_117", "payload[117] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_118 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_118", "payload[118] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_119 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_119", "payload[119] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_120 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_120", "payload[120] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_121 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_121", "payload[121] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_122 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_122", "payload[122] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_123 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_123", "payload[123] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_124 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_124", "payload[124] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_125 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_125", "payload[125] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_126 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_126", "payload[126] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_127 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_127", "payload[127] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_128 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_128", "payload[128] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_129 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_129", "payload[129] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_130 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_130", "payload[130] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_131 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_131", "payload[131] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_132 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_132", "payload[132] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_133 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_133", "payload[133] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_134 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_134", "payload[134] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_135 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_135", "payload[135] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_136 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_136", "payload[136] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_137 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_137", "payload[137] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_138 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_138", "payload[138] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_139 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_139", "payload[139] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_140 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_140", "payload[140] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_141 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_141", "payload[141] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_142 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_142", "payload[142] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_143 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_143", "payload[143] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_144 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_144", "payload[144] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_145 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_145", "payload[145] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_146 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_146", "payload[146] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_147 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_147", "payload[147] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_148 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_148", "payload[148] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_149 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_149", "payload[149] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_150 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_150", "payload[150] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_151 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_151", "payload[151] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_152 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_152", "payload[152] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_153 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_153", "payload[153] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_154 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_154", "payload[154] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_155 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_155", "payload[155] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_156 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_156", "payload[156] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_157 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_157", "payload[157] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_158 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_158", "payload[158] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_159 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_159", "payload[159] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_160 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_160", "payload[160] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_161 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_161", "payload[161] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_162 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_162", "payload[162] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_163 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_163", "payload[163] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_164 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_164", "payload[164] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_165 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_165", "payload[165] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_166 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_166", "payload[166] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_167 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_167", "payload[167] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_168 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_168", "payload[168] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_169 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_169", "payload[169] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_170 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_170", "payload[170] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_171 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_171", "payload[171] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_172 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_172", "payload[172] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_173 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_173", "payload[173] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_174 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_174", "payload[174] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_175 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_175", "payload[175] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_176 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_176", "payload[176] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_177 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_177", "payload[177] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_178 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_178", "payload[178] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_179 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_179", "payload[179] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_180 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_180", "payload[180] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_181 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_181", "payload[181] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_182 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_182", "payload[182] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_183 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_183", "payload[183] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_184 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_184", "payload[184] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_185 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_185", "payload[185] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_186 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_186", "payload[186] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_187 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_187", "payload[187] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_188 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_188", "payload[188] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_189 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_189", "payload[189] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_190 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_190", "payload[190] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_191 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_191", "payload[191] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_192 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_192", "payload[192] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_193 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_193", "payload[193] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_194 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_194", "payload[194] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_195 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_195", "payload[195] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_196 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_196", "payload[196] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_197 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_197", "payload[197] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_198 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_198", "payload[198] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_199 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_199", "payload[199] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_200 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_200", "payload[200] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_201 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_201", "payload[201] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_202 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_202", "payload[202] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_203 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_203", "payload[203] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_204 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_204", "payload[204] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_205 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_205", "payload[205] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_206 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_206", "payload[206] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_207 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_207", "payload[207] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_208 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_208", "payload[208] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_209 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_209", "payload[209] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_210 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_210", "payload[210] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_211 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_211", "payload[211] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_212 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_212", "payload[212] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_213 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_213", "payload[213] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_214 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_214", "payload[214] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_215 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_215", "payload[215] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_216 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_216", "payload[216] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_217 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_217", "payload[217] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_218 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_218", "payload[218] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_219 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_219", "payload[219] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_220 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_220", "payload[220] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_221 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_221", "payload[221] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_222 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_222", "payload[222] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_223 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_223", "payload[223] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_224 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_224", "payload[224] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_225 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_225", "payload[225] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_226 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_226", "payload[226] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_227 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_227", "payload[227] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_228 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_228", "payload[228] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_229 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_229", "payload[229] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_230 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_230", "payload[230] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_231 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_231", "payload[231] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_232 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_232", "payload[232] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_233 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_233", "payload[233] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_234 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_234", "payload[234] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_235 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_235", "payload[235] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_236 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_236", "payload[236] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_237 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_237", "payload[237] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_238 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_238", "payload[238] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_239 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_239", "payload[239] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_240 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_240", "payload[240] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_241 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_241", "payload[241] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_242 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_242", "payload[242] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_243 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_243", "payload[243] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_244 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_244", "payload[244] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_245 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_245", "payload[245] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_246 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_246", "payload[246] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_247 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_247", "payload[247] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_248 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_248", "payload[248] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_249 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_249", "payload[249] (uint8)")
f.FILE_TRANSFER_PROTOCOL_payload_250 = ProtoField.uint8("mavlink_proto.FILE_TRANSFER_PROTOCOL_payload_250", "payload[250] (uint8)")

f.TIMESYNC_tc1 = ProtoField.int64("mavlink_proto.TIMESYNC_tc1", "tc1 (int64)")
f.TIMESYNC_ts1 = ProtoField.int64("mavlink_proto.TIMESYNC_ts1", "ts1 (int64)")

f.CAMERA_TRIGGER_time_usec = ProtoField.uint64("mavlink_proto.CAMERA_TRIGGER_time_usec", "time_usec (uint64)")
f.CAMERA_TRIGGER_seq = ProtoField.uint32("mavlink_proto.CAMERA_TRIGGER_seq", "seq (uint32)")

f.HIL_GPS_time_usec = ProtoField.uint64("mavlink_proto.HIL_GPS_time_usec", "time_usec (uint64)")
f.HIL_GPS_fix_type = ProtoField.uint8("mavlink_proto.HIL_GPS_fix_type", "fix_type (uint8)")
f.HIL_GPS_lat = ProtoField.int32("mavlink_proto.HIL_GPS_lat", "lat (int32)")
f.HIL_GPS_lon = ProtoField.int32("mavlink_proto.HIL_GPS_lon", "lon (int32)")
f.HIL_GPS_alt = ProtoField.int32("mavlink_proto.HIL_GPS_alt", "alt (int32)")
f.HIL_GPS_eph = ProtoField.uint16("mavlink_proto.HIL_GPS_eph", "eph (uint16)")
f.HIL_GPS_epv = ProtoField.uint16("mavlink_proto.HIL_GPS_epv", "epv (uint16)")
f.HIL_GPS_vel = ProtoField.uint16("mavlink_proto.HIL_GPS_vel", "vel (uint16)")
f.HIL_GPS_vn = ProtoField.int16("mavlink_proto.HIL_GPS_vn", "vn (int16)")
f.HIL_GPS_ve = ProtoField.int16("mavlink_proto.HIL_GPS_ve", "ve (int16)")
f.HIL_GPS_vd = ProtoField.int16("mavlink_proto.HIL_GPS_vd", "vd (int16)")
f.HIL_GPS_cog = ProtoField.uint16("mavlink_proto.HIL_GPS_cog", "cog (uint16)")
f.HIL_GPS_satellites_visible = ProtoField.uint8("mavlink_proto.HIL_GPS_satellites_visible", "satellites_visible (uint8)")

f.HIL_OPTICAL_FLOW_time_usec = ProtoField.uint64("mavlink_proto.HIL_OPTICAL_FLOW_time_usec", "time_usec (uint64)")
f.HIL_OPTICAL_FLOW_sensor_id = ProtoField.uint8("mavlink_proto.HIL_OPTICAL_FLOW_sensor_id", "sensor_id (uint8)")
f.HIL_OPTICAL_FLOW_integration_time_us = ProtoField.uint32("mavlink_proto.HIL_OPTICAL_FLOW_integration_time_us", "integration_time_us (uint32)")
f.HIL_OPTICAL_FLOW_integrated_x = ProtoField.float("mavlink_proto.HIL_OPTICAL_FLOW_integrated_x", "integrated_x (float)")
f.HIL_OPTICAL_FLOW_integrated_y = ProtoField.float("mavlink_proto.HIL_OPTICAL_FLOW_integrated_y", "integrated_y (float)")
f.HIL_OPTICAL_FLOW_integrated_xgyro = ProtoField.float("mavlink_proto.HIL_OPTICAL_FLOW_integrated_xgyro", "integrated_xgyro (float)")
f.HIL_OPTICAL_FLOW_integrated_ygyro = ProtoField.float("mavlink_proto.HIL_OPTICAL_FLOW_integrated_ygyro", "integrated_ygyro (float)")
f.HIL_OPTICAL_FLOW_integrated_zgyro = ProtoField.float("mavlink_proto.HIL_OPTICAL_FLOW_integrated_zgyro", "integrated_zgyro (float)")
f.HIL_OPTICAL_FLOW_temperature = ProtoField.int16("mavlink_proto.HIL_OPTICAL_FLOW_temperature", "temperature (int16)")
f.HIL_OPTICAL_FLOW_quality = ProtoField.uint8("mavlink_proto.HIL_OPTICAL_FLOW_quality", "quality (uint8)")
f.HIL_OPTICAL_FLOW_time_delta_distance_us = ProtoField.uint32("mavlink_proto.HIL_OPTICAL_FLOW_time_delta_distance_us", "time_delta_distance_us (uint32)")
f.HIL_OPTICAL_FLOW_distance = ProtoField.float("mavlink_proto.HIL_OPTICAL_FLOW_distance", "distance (float)")

f.HIL_STATE_QUATERNION_time_usec = ProtoField.uint64("mavlink_proto.HIL_STATE_QUATERNION_time_usec", "time_usec (uint64)")
f.HIL_STATE_QUATERNION_attitude_quaternion_0 = ProtoField.float("mavlink_proto.HIL_STATE_QUATERNION_attitude_quaternion_0", "attitude_quaternion[0] (float)")
f.HIL_STATE_QUATERNION_attitude_quaternion_1 = ProtoField.float("mavlink_proto.HIL_STATE_QUATERNION_attitude_quaternion_1", "attitude_quaternion[1] (float)")
f.HIL_STATE_QUATERNION_attitude_quaternion_2 = ProtoField.float("mavlink_proto.HIL_STATE_QUATERNION_attitude_quaternion_2", "attitude_quaternion[2] (float)")
f.HIL_STATE_QUATERNION_attitude_quaternion_3 = ProtoField.float("mavlink_proto.HIL_STATE_QUATERNION_attitude_quaternion_3", "attitude_quaternion[3] (float)")
f.HIL_STATE_QUATERNION_rollspeed = ProtoField.float("mavlink_proto.HIL_STATE_QUATERNION_rollspeed", "rollspeed (float)")
f.HIL_STATE_QUATERNION_pitchspeed = ProtoField.float("mavlink_proto.HIL_STATE_QUATERNION_pitchspeed", "pitchspeed (float)")
f.HIL_STATE_QUATERNION_yawspeed = ProtoField.float("mavlink_proto.HIL_STATE_QUATERNION_yawspeed", "yawspeed (float)")
f.HIL_STATE_QUATERNION_lat = ProtoField.int32("mavlink_proto.HIL_STATE_QUATERNION_lat", "lat (int32)")
f.HIL_STATE_QUATERNION_lon = ProtoField.int32("mavlink_proto.HIL_STATE_QUATERNION_lon", "lon (int32)")
f.HIL_STATE_QUATERNION_alt = ProtoField.int32("mavlink_proto.HIL_STATE_QUATERNION_alt", "alt (int32)")
f.HIL_STATE_QUATERNION_vx = ProtoField.int16("mavlink_proto.HIL_STATE_QUATERNION_vx", "vx (int16)")
f.HIL_STATE_QUATERNION_vy = ProtoField.int16("mavlink_proto.HIL_STATE_QUATERNION_vy", "vy (int16)")
f.HIL_STATE_QUATERNION_vz = ProtoField.int16("mavlink_proto.HIL_STATE_QUATERNION_vz", "vz (int16)")
f.HIL_STATE_QUATERNION_ind_airspeed = ProtoField.uint16("mavlink_proto.HIL_STATE_QUATERNION_ind_airspeed", "ind_airspeed (uint16)")
f.HIL_STATE_QUATERNION_true_airspeed = ProtoField.uint16("mavlink_proto.HIL_STATE_QUATERNION_true_airspeed", "true_airspeed (uint16)")
f.HIL_STATE_QUATERNION_xacc = ProtoField.int16("mavlink_proto.HIL_STATE_QUATERNION_xacc", "xacc (int16)")
f.HIL_STATE_QUATERNION_yacc = ProtoField.int16("mavlink_proto.HIL_STATE_QUATERNION_yacc", "yacc (int16)")
f.HIL_STATE_QUATERNION_zacc = ProtoField.int16("mavlink_proto.HIL_STATE_QUATERNION_zacc", "zacc (int16)")

f.SCALED_IMU2_time_boot_ms = ProtoField.uint32("mavlink_proto.SCALED_IMU2_time_boot_ms", "time_boot_ms (uint32)")
f.SCALED_IMU2_xacc = ProtoField.int16("mavlink_proto.SCALED_IMU2_xacc", "xacc (int16)")
f.SCALED_IMU2_yacc = ProtoField.int16("mavlink_proto.SCALED_IMU2_yacc", "yacc (int16)")
f.SCALED_IMU2_zacc = ProtoField.int16("mavlink_proto.SCALED_IMU2_zacc", "zacc (int16)")
f.SCALED_IMU2_xgyro = ProtoField.int16("mavlink_proto.SCALED_IMU2_xgyro", "xgyro (int16)")
f.SCALED_IMU2_ygyro = ProtoField.int16("mavlink_proto.SCALED_IMU2_ygyro", "ygyro (int16)")
f.SCALED_IMU2_zgyro = ProtoField.int16("mavlink_proto.SCALED_IMU2_zgyro", "zgyro (int16)")
f.SCALED_IMU2_xmag = ProtoField.int16("mavlink_proto.SCALED_IMU2_xmag", "xmag (int16)")
f.SCALED_IMU2_ymag = ProtoField.int16("mavlink_proto.SCALED_IMU2_ymag", "ymag (int16)")
f.SCALED_IMU2_zmag = ProtoField.int16("mavlink_proto.SCALED_IMU2_zmag", "zmag (int16)")

f.LOG_REQUEST_LIST_target_system = ProtoField.uint8("mavlink_proto.LOG_REQUEST_LIST_target_system", "target_system (uint8)")
f.LOG_REQUEST_LIST_target_component = ProtoField.uint8("mavlink_proto.LOG_REQUEST_LIST_target_component", "target_component (uint8)")
f.LOG_REQUEST_LIST_start = ProtoField.uint16("mavlink_proto.LOG_REQUEST_LIST_start", "start (uint16)")
f.LOG_REQUEST_LIST_end = ProtoField.uint16("mavlink_proto.LOG_REQUEST_LIST_end", "end (uint16)")

f.LOG_ENTRY_id = ProtoField.uint16("mavlink_proto.LOG_ENTRY_id", "id (uint16)")
f.LOG_ENTRY_num_logs = ProtoField.uint16("mavlink_proto.LOG_ENTRY_num_logs", "num_logs (uint16)")
f.LOG_ENTRY_last_log_num = ProtoField.uint16("mavlink_proto.LOG_ENTRY_last_log_num", "last_log_num (uint16)")
f.LOG_ENTRY_time_utc = ProtoField.uint32("mavlink_proto.LOG_ENTRY_time_utc", "time_utc (uint32)")
f.LOG_ENTRY_size = ProtoField.uint32("mavlink_proto.LOG_ENTRY_size", "size (uint32)")

f.LOG_REQUEST_DATA_target_system = ProtoField.uint8("mavlink_proto.LOG_REQUEST_DATA_target_system", "target_system (uint8)")
f.LOG_REQUEST_DATA_target_component = ProtoField.uint8("mavlink_proto.LOG_REQUEST_DATA_target_component", "target_component (uint8)")
f.LOG_REQUEST_DATA_id = ProtoField.uint16("mavlink_proto.LOG_REQUEST_DATA_id", "id (uint16)")
f.LOG_REQUEST_DATA_ofs = ProtoField.uint32("mavlink_proto.LOG_REQUEST_DATA_ofs", "ofs (uint32)")
f.LOG_REQUEST_DATA_count = ProtoField.uint32("mavlink_proto.LOG_REQUEST_DATA_count", "count (uint32)")

f.LOG_DATA_id = ProtoField.uint16("mavlink_proto.LOG_DATA_id", "id (uint16)")
f.LOG_DATA_ofs = ProtoField.uint32("mavlink_proto.LOG_DATA_ofs", "ofs (uint32)")
f.LOG_DATA_count = ProtoField.uint8("mavlink_proto.LOG_DATA_count", "count (uint8)")
f.LOG_DATA_data_0 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_0", "data[0] (uint8)")
f.LOG_DATA_data_1 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_1", "data[1] (uint8)")
f.LOG_DATA_data_2 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_2", "data[2] (uint8)")
f.LOG_DATA_data_3 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_3", "data[3] (uint8)")
f.LOG_DATA_data_4 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_4", "data[4] (uint8)")
f.LOG_DATA_data_5 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_5", "data[5] (uint8)")
f.LOG_DATA_data_6 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_6", "data[6] (uint8)")
f.LOG_DATA_data_7 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_7", "data[7] (uint8)")
f.LOG_DATA_data_8 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_8", "data[8] (uint8)")
f.LOG_DATA_data_9 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_9", "data[9] (uint8)")
f.LOG_DATA_data_10 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_10", "data[10] (uint8)")
f.LOG_DATA_data_11 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_11", "data[11] (uint8)")
f.LOG_DATA_data_12 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_12", "data[12] (uint8)")
f.LOG_DATA_data_13 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_13", "data[13] (uint8)")
f.LOG_DATA_data_14 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_14", "data[14] (uint8)")
f.LOG_DATA_data_15 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_15", "data[15] (uint8)")
f.LOG_DATA_data_16 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_16", "data[16] (uint8)")
f.LOG_DATA_data_17 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_17", "data[17] (uint8)")
f.LOG_DATA_data_18 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_18", "data[18] (uint8)")
f.LOG_DATA_data_19 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_19", "data[19] (uint8)")
f.LOG_DATA_data_20 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_20", "data[20] (uint8)")
f.LOG_DATA_data_21 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_21", "data[21] (uint8)")
f.LOG_DATA_data_22 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_22", "data[22] (uint8)")
f.LOG_DATA_data_23 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_23", "data[23] (uint8)")
f.LOG_DATA_data_24 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_24", "data[24] (uint8)")
f.LOG_DATA_data_25 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_25", "data[25] (uint8)")
f.LOG_DATA_data_26 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_26", "data[26] (uint8)")
f.LOG_DATA_data_27 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_27", "data[27] (uint8)")
f.LOG_DATA_data_28 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_28", "data[28] (uint8)")
f.LOG_DATA_data_29 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_29", "data[29] (uint8)")
f.LOG_DATA_data_30 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_30", "data[30] (uint8)")
f.LOG_DATA_data_31 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_31", "data[31] (uint8)")
f.LOG_DATA_data_32 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_32", "data[32] (uint8)")
f.LOG_DATA_data_33 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_33", "data[33] (uint8)")
f.LOG_DATA_data_34 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_34", "data[34] (uint8)")
f.LOG_DATA_data_35 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_35", "data[35] (uint8)")
f.LOG_DATA_data_36 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_36", "data[36] (uint8)")
f.LOG_DATA_data_37 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_37", "data[37] (uint8)")
f.LOG_DATA_data_38 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_38", "data[38] (uint8)")
f.LOG_DATA_data_39 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_39", "data[39] (uint8)")
f.LOG_DATA_data_40 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_40", "data[40] (uint8)")
f.LOG_DATA_data_41 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_41", "data[41] (uint8)")
f.LOG_DATA_data_42 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_42", "data[42] (uint8)")
f.LOG_DATA_data_43 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_43", "data[43] (uint8)")
f.LOG_DATA_data_44 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_44", "data[44] (uint8)")
f.LOG_DATA_data_45 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_45", "data[45] (uint8)")
f.LOG_DATA_data_46 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_46", "data[46] (uint8)")
f.LOG_DATA_data_47 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_47", "data[47] (uint8)")
f.LOG_DATA_data_48 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_48", "data[48] (uint8)")
f.LOG_DATA_data_49 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_49", "data[49] (uint8)")
f.LOG_DATA_data_50 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_50", "data[50] (uint8)")
f.LOG_DATA_data_51 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_51", "data[51] (uint8)")
f.LOG_DATA_data_52 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_52", "data[52] (uint8)")
f.LOG_DATA_data_53 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_53", "data[53] (uint8)")
f.LOG_DATA_data_54 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_54", "data[54] (uint8)")
f.LOG_DATA_data_55 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_55", "data[55] (uint8)")
f.LOG_DATA_data_56 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_56", "data[56] (uint8)")
f.LOG_DATA_data_57 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_57", "data[57] (uint8)")
f.LOG_DATA_data_58 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_58", "data[58] (uint8)")
f.LOG_DATA_data_59 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_59", "data[59] (uint8)")
f.LOG_DATA_data_60 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_60", "data[60] (uint8)")
f.LOG_DATA_data_61 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_61", "data[61] (uint8)")
f.LOG_DATA_data_62 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_62", "data[62] (uint8)")
f.LOG_DATA_data_63 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_63", "data[63] (uint8)")
f.LOG_DATA_data_64 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_64", "data[64] (uint8)")
f.LOG_DATA_data_65 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_65", "data[65] (uint8)")
f.LOG_DATA_data_66 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_66", "data[66] (uint8)")
f.LOG_DATA_data_67 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_67", "data[67] (uint8)")
f.LOG_DATA_data_68 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_68", "data[68] (uint8)")
f.LOG_DATA_data_69 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_69", "data[69] (uint8)")
f.LOG_DATA_data_70 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_70", "data[70] (uint8)")
f.LOG_DATA_data_71 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_71", "data[71] (uint8)")
f.LOG_DATA_data_72 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_72", "data[72] (uint8)")
f.LOG_DATA_data_73 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_73", "data[73] (uint8)")
f.LOG_DATA_data_74 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_74", "data[74] (uint8)")
f.LOG_DATA_data_75 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_75", "data[75] (uint8)")
f.LOG_DATA_data_76 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_76", "data[76] (uint8)")
f.LOG_DATA_data_77 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_77", "data[77] (uint8)")
f.LOG_DATA_data_78 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_78", "data[78] (uint8)")
f.LOG_DATA_data_79 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_79", "data[79] (uint8)")
f.LOG_DATA_data_80 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_80", "data[80] (uint8)")
f.LOG_DATA_data_81 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_81", "data[81] (uint8)")
f.LOG_DATA_data_82 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_82", "data[82] (uint8)")
f.LOG_DATA_data_83 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_83", "data[83] (uint8)")
f.LOG_DATA_data_84 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_84", "data[84] (uint8)")
f.LOG_DATA_data_85 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_85", "data[85] (uint8)")
f.LOG_DATA_data_86 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_86", "data[86] (uint8)")
f.LOG_DATA_data_87 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_87", "data[87] (uint8)")
f.LOG_DATA_data_88 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_88", "data[88] (uint8)")
f.LOG_DATA_data_89 = ProtoField.uint8("mavlink_proto.LOG_DATA_data_89", "data[89] (uint8)")

f.LOG_ERASE_target_system = ProtoField.uint8("mavlink_proto.LOG_ERASE_target_system", "target_system (uint8)")
f.LOG_ERASE_target_component = ProtoField.uint8("mavlink_proto.LOG_ERASE_target_component", "target_component (uint8)")

f.LOG_REQUEST_END_target_system = ProtoField.uint8("mavlink_proto.LOG_REQUEST_END_target_system", "target_system (uint8)")
f.LOG_REQUEST_END_target_component = ProtoField.uint8("mavlink_proto.LOG_REQUEST_END_target_component", "target_component (uint8)")

f.GPS_INJECT_DATA_target_system = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_target_system", "target_system (uint8)")
f.GPS_INJECT_DATA_target_component = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_target_component", "target_component (uint8)")
f.GPS_INJECT_DATA_len = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_len", "len (uint8)")
f.GPS_INJECT_DATA_data_0 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_0", "data[0] (uint8)")
f.GPS_INJECT_DATA_data_1 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_1", "data[1] (uint8)")
f.GPS_INJECT_DATA_data_2 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_2", "data[2] (uint8)")
f.GPS_INJECT_DATA_data_3 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_3", "data[3] (uint8)")
f.GPS_INJECT_DATA_data_4 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_4", "data[4] (uint8)")
f.GPS_INJECT_DATA_data_5 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_5", "data[5] (uint8)")
f.GPS_INJECT_DATA_data_6 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_6", "data[6] (uint8)")
f.GPS_INJECT_DATA_data_7 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_7", "data[7] (uint8)")
f.GPS_INJECT_DATA_data_8 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_8", "data[8] (uint8)")
f.GPS_INJECT_DATA_data_9 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_9", "data[9] (uint8)")
f.GPS_INJECT_DATA_data_10 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_10", "data[10] (uint8)")
f.GPS_INJECT_DATA_data_11 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_11", "data[11] (uint8)")
f.GPS_INJECT_DATA_data_12 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_12", "data[12] (uint8)")
f.GPS_INJECT_DATA_data_13 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_13", "data[13] (uint8)")
f.GPS_INJECT_DATA_data_14 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_14", "data[14] (uint8)")
f.GPS_INJECT_DATA_data_15 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_15", "data[15] (uint8)")
f.GPS_INJECT_DATA_data_16 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_16", "data[16] (uint8)")
f.GPS_INJECT_DATA_data_17 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_17", "data[17] (uint8)")
f.GPS_INJECT_DATA_data_18 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_18", "data[18] (uint8)")
f.GPS_INJECT_DATA_data_19 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_19", "data[19] (uint8)")
f.GPS_INJECT_DATA_data_20 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_20", "data[20] (uint8)")
f.GPS_INJECT_DATA_data_21 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_21", "data[21] (uint8)")
f.GPS_INJECT_DATA_data_22 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_22", "data[22] (uint8)")
f.GPS_INJECT_DATA_data_23 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_23", "data[23] (uint8)")
f.GPS_INJECT_DATA_data_24 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_24", "data[24] (uint8)")
f.GPS_INJECT_DATA_data_25 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_25", "data[25] (uint8)")
f.GPS_INJECT_DATA_data_26 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_26", "data[26] (uint8)")
f.GPS_INJECT_DATA_data_27 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_27", "data[27] (uint8)")
f.GPS_INJECT_DATA_data_28 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_28", "data[28] (uint8)")
f.GPS_INJECT_DATA_data_29 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_29", "data[29] (uint8)")
f.GPS_INJECT_DATA_data_30 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_30", "data[30] (uint8)")
f.GPS_INJECT_DATA_data_31 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_31", "data[31] (uint8)")
f.GPS_INJECT_DATA_data_32 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_32", "data[32] (uint8)")
f.GPS_INJECT_DATA_data_33 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_33", "data[33] (uint8)")
f.GPS_INJECT_DATA_data_34 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_34", "data[34] (uint8)")
f.GPS_INJECT_DATA_data_35 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_35", "data[35] (uint8)")
f.GPS_INJECT_DATA_data_36 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_36", "data[36] (uint8)")
f.GPS_INJECT_DATA_data_37 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_37", "data[37] (uint8)")
f.GPS_INJECT_DATA_data_38 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_38", "data[38] (uint8)")
f.GPS_INJECT_DATA_data_39 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_39", "data[39] (uint8)")
f.GPS_INJECT_DATA_data_40 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_40", "data[40] (uint8)")
f.GPS_INJECT_DATA_data_41 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_41", "data[41] (uint8)")
f.GPS_INJECT_DATA_data_42 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_42", "data[42] (uint8)")
f.GPS_INJECT_DATA_data_43 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_43", "data[43] (uint8)")
f.GPS_INJECT_DATA_data_44 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_44", "data[44] (uint8)")
f.GPS_INJECT_DATA_data_45 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_45", "data[45] (uint8)")
f.GPS_INJECT_DATA_data_46 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_46", "data[46] (uint8)")
f.GPS_INJECT_DATA_data_47 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_47", "data[47] (uint8)")
f.GPS_INJECT_DATA_data_48 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_48", "data[48] (uint8)")
f.GPS_INJECT_DATA_data_49 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_49", "data[49] (uint8)")
f.GPS_INJECT_DATA_data_50 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_50", "data[50] (uint8)")
f.GPS_INJECT_DATA_data_51 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_51", "data[51] (uint8)")
f.GPS_INJECT_DATA_data_52 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_52", "data[52] (uint8)")
f.GPS_INJECT_DATA_data_53 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_53", "data[53] (uint8)")
f.GPS_INJECT_DATA_data_54 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_54", "data[54] (uint8)")
f.GPS_INJECT_DATA_data_55 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_55", "data[55] (uint8)")
f.GPS_INJECT_DATA_data_56 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_56", "data[56] (uint8)")
f.GPS_INJECT_DATA_data_57 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_57", "data[57] (uint8)")
f.GPS_INJECT_DATA_data_58 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_58", "data[58] (uint8)")
f.GPS_INJECT_DATA_data_59 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_59", "data[59] (uint8)")
f.GPS_INJECT_DATA_data_60 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_60", "data[60] (uint8)")
f.GPS_INJECT_DATA_data_61 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_61", "data[61] (uint8)")
f.GPS_INJECT_DATA_data_62 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_62", "data[62] (uint8)")
f.GPS_INJECT_DATA_data_63 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_63", "data[63] (uint8)")
f.GPS_INJECT_DATA_data_64 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_64", "data[64] (uint8)")
f.GPS_INJECT_DATA_data_65 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_65", "data[65] (uint8)")
f.GPS_INJECT_DATA_data_66 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_66", "data[66] (uint8)")
f.GPS_INJECT_DATA_data_67 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_67", "data[67] (uint8)")
f.GPS_INJECT_DATA_data_68 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_68", "data[68] (uint8)")
f.GPS_INJECT_DATA_data_69 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_69", "data[69] (uint8)")
f.GPS_INJECT_DATA_data_70 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_70", "data[70] (uint8)")
f.GPS_INJECT_DATA_data_71 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_71", "data[71] (uint8)")
f.GPS_INJECT_DATA_data_72 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_72", "data[72] (uint8)")
f.GPS_INJECT_DATA_data_73 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_73", "data[73] (uint8)")
f.GPS_INJECT_DATA_data_74 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_74", "data[74] (uint8)")
f.GPS_INJECT_DATA_data_75 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_75", "data[75] (uint8)")
f.GPS_INJECT_DATA_data_76 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_76", "data[76] (uint8)")
f.GPS_INJECT_DATA_data_77 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_77", "data[77] (uint8)")
f.GPS_INJECT_DATA_data_78 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_78", "data[78] (uint8)")
f.GPS_INJECT_DATA_data_79 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_79", "data[79] (uint8)")
f.GPS_INJECT_DATA_data_80 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_80", "data[80] (uint8)")
f.GPS_INJECT_DATA_data_81 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_81", "data[81] (uint8)")
f.GPS_INJECT_DATA_data_82 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_82", "data[82] (uint8)")
f.GPS_INJECT_DATA_data_83 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_83", "data[83] (uint8)")
f.GPS_INJECT_DATA_data_84 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_84", "data[84] (uint8)")
f.GPS_INJECT_DATA_data_85 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_85", "data[85] (uint8)")
f.GPS_INJECT_DATA_data_86 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_86", "data[86] (uint8)")
f.GPS_INJECT_DATA_data_87 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_87", "data[87] (uint8)")
f.GPS_INJECT_DATA_data_88 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_88", "data[88] (uint8)")
f.GPS_INJECT_DATA_data_89 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_89", "data[89] (uint8)")
f.GPS_INJECT_DATA_data_90 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_90", "data[90] (uint8)")
f.GPS_INJECT_DATA_data_91 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_91", "data[91] (uint8)")
f.GPS_INJECT_DATA_data_92 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_92", "data[92] (uint8)")
f.GPS_INJECT_DATA_data_93 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_93", "data[93] (uint8)")
f.GPS_INJECT_DATA_data_94 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_94", "data[94] (uint8)")
f.GPS_INJECT_DATA_data_95 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_95", "data[95] (uint8)")
f.GPS_INJECT_DATA_data_96 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_96", "data[96] (uint8)")
f.GPS_INJECT_DATA_data_97 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_97", "data[97] (uint8)")
f.GPS_INJECT_DATA_data_98 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_98", "data[98] (uint8)")
f.GPS_INJECT_DATA_data_99 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_99", "data[99] (uint8)")
f.GPS_INJECT_DATA_data_100 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_100", "data[100] (uint8)")
f.GPS_INJECT_DATA_data_101 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_101", "data[101] (uint8)")
f.GPS_INJECT_DATA_data_102 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_102", "data[102] (uint8)")
f.GPS_INJECT_DATA_data_103 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_103", "data[103] (uint8)")
f.GPS_INJECT_DATA_data_104 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_104", "data[104] (uint8)")
f.GPS_INJECT_DATA_data_105 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_105", "data[105] (uint8)")
f.GPS_INJECT_DATA_data_106 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_106", "data[106] (uint8)")
f.GPS_INJECT_DATA_data_107 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_107", "data[107] (uint8)")
f.GPS_INJECT_DATA_data_108 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_108", "data[108] (uint8)")
f.GPS_INJECT_DATA_data_109 = ProtoField.uint8("mavlink_proto.GPS_INJECT_DATA_data_109", "data[109] (uint8)")

f.GPS2_RAW_time_usec = ProtoField.uint64("mavlink_proto.GPS2_RAW_time_usec", "time_usec (uint64)")
f.GPS2_RAW_fix_type = ProtoField.uint8("mavlink_proto.GPS2_RAW_fix_type", "fix_type (uint8)")
f.GPS2_RAW_lat = ProtoField.int32("mavlink_proto.GPS2_RAW_lat", "lat (int32)")
f.GPS2_RAW_lon = ProtoField.int32("mavlink_proto.GPS2_RAW_lon", "lon (int32)")
f.GPS2_RAW_alt = ProtoField.int32("mavlink_proto.GPS2_RAW_alt", "alt (int32)")
f.GPS2_RAW_eph = ProtoField.uint16("mavlink_proto.GPS2_RAW_eph", "eph (uint16)")
f.GPS2_RAW_epv = ProtoField.uint16("mavlink_proto.GPS2_RAW_epv", "epv (uint16)")
f.GPS2_RAW_vel = ProtoField.uint16("mavlink_proto.GPS2_RAW_vel", "vel (uint16)")
f.GPS2_RAW_cog = ProtoField.uint16("mavlink_proto.GPS2_RAW_cog", "cog (uint16)")
f.GPS2_RAW_satellites_visible = ProtoField.uint8("mavlink_proto.GPS2_RAW_satellites_visible", "satellites_visible (uint8)")
f.GPS2_RAW_dgps_numch = ProtoField.uint8("mavlink_proto.GPS2_RAW_dgps_numch", "dgps_numch (uint8)")
f.GPS2_RAW_dgps_age = ProtoField.uint32("mavlink_proto.GPS2_RAW_dgps_age", "dgps_age (uint32)")

f.POWER_STATUS_Vcc = ProtoField.uint16("mavlink_proto.POWER_STATUS_Vcc", "Vcc (uint16)")
f.POWER_STATUS_Vservo = ProtoField.uint16("mavlink_proto.POWER_STATUS_Vservo", "Vservo (uint16)")
f.POWER_STATUS_flags = ProtoField.uint16("mavlink_proto.POWER_STATUS_flags", "flags (uint16)")

f.SERIAL_CONTROL_device = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_device", "device (uint8)")
f.SERIAL_CONTROL_flags = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_flags", "flags (uint8)")
f.SERIAL_CONTROL_timeout = ProtoField.uint16("mavlink_proto.SERIAL_CONTROL_timeout", "timeout (uint16)")
f.SERIAL_CONTROL_baudrate = ProtoField.uint32("mavlink_proto.SERIAL_CONTROL_baudrate", "baudrate (uint32)")
f.SERIAL_CONTROL_count = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_count", "count (uint8)")
f.SERIAL_CONTROL_data_0 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_0", "data[0] (uint8)")
f.SERIAL_CONTROL_data_1 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_1", "data[1] (uint8)")
f.SERIAL_CONTROL_data_2 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_2", "data[2] (uint8)")
f.SERIAL_CONTROL_data_3 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_3", "data[3] (uint8)")
f.SERIAL_CONTROL_data_4 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_4", "data[4] (uint8)")
f.SERIAL_CONTROL_data_5 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_5", "data[5] (uint8)")
f.SERIAL_CONTROL_data_6 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_6", "data[6] (uint8)")
f.SERIAL_CONTROL_data_7 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_7", "data[7] (uint8)")
f.SERIAL_CONTROL_data_8 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_8", "data[8] (uint8)")
f.SERIAL_CONTROL_data_9 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_9", "data[9] (uint8)")
f.SERIAL_CONTROL_data_10 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_10", "data[10] (uint8)")
f.SERIAL_CONTROL_data_11 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_11", "data[11] (uint8)")
f.SERIAL_CONTROL_data_12 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_12", "data[12] (uint8)")
f.SERIAL_CONTROL_data_13 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_13", "data[13] (uint8)")
f.SERIAL_CONTROL_data_14 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_14", "data[14] (uint8)")
f.SERIAL_CONTROL_data_15 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_15", "data[15] (uint8)")
f.SERIAL_CONTROL_data_16 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_16", "data[16] (uint8)")
f.SERIAL_CONTROL_data_17 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_17", "data[17] (uint8)")
f.SERIAL_CONTROL_data_18 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_18", "data[18] (uint8)")
f.SERIAL_CONTROL_data_19 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_19", "data[19] (uint8)")
f.SERIAL_CONTROL_data_20 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_20", "data[20] (uint8)")
f.SERIAL_CONTROL_data_21 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_21", "data[21] (uint8)")
f.SERIAL_CONTROL_data_22 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_22", "data[22] (uint8)")
f.SERIAL_CONTROL_data_23 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_23", "data[23] (uint8)")
f.SERIAL_CONTROL_data_24 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_24", "data[24] (uint8)")
f.SERIAL_CONTROL_data_25 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_25", "data[25] (uint8)")
f.SERIAL_CONTROL_data_26 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_26", "data[26] (uint8)")
f.SERIAL_CONTROL_data_27 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_27", "data[27] (uint8)")
f.SERIAL_CONTROL_data_28 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_28", "data[28] (uint8)")
f.SERIAL_CONTROL_data_29 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_29", "data[29] (uint8)")
f.SERIAL_CONTROL_data_30 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_30", "data[30] (uint8)")
f.SERIAL_CONTROL_data_31 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_31", "data[31] (uint8)")
f.SERIAL_CONTROL_data_32 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_32", "data[32] (uint8)")
f.SERIAL_CONTROL_data_33 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_33", "data[33] (uint8)")
f.SERIAL_CONTROL_data_34 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_34", "data[34] (uint8)")
f.SERIAL_CONTROL_data_35 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_35", "data[35] (uint8)")
f.SERIAL_CONTROL_data_36 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_36", "data[36] (uint8)")
f.SERIAL_CONTROL_data_37 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_37", "data[37] (uint8)")
f.SERIAL_CONTROL_data_38 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_38", "data[38] (uint8)")
f.SERIAL_CONTROL_data_39 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_39", "data[39] (uint8)")
f.SERIAL_CONTROL_data_40 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_40", "data[40] (uint8)")
f.SERIAL_CONTROL_data_41 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_41", "data[41] (uint8)")
f.SERIAL_CONTROL_data_42 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_42", "data[42] (uint8)")
f.SERIAL_CONTROL_data_43 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_43", "data[43] (uint8)")
f.SERIAL_CONTROL_data_44 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_44", "data[44] (uint8)")
f.SERIAL_CONTROL_data_45 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_45", "data[45] (uint8)")
f.SERIAL_CONTROL_data_46 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_46", "data[46] (uint8)")
f.SERIAL_CONTROL_data_47 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_47", "data[47] (uint8)")
f.SERIAL_CONTROL_data_48 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_48", "data[48] (uint8)")
f.SERIAL_CONTROL_data_49 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_49", "data[49] (uint8)")
f.SERIAL_CONTROL_data_50 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_50", "data[50] (uint8)")
f.SERIAL_CONTROL_data_51 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_51", "data[51] (uint8)")
f.SERIAL_CONTROL_data_52 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_52", "data[52] (uint8)")
f.SERIAL_CONTROL_data_53 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_53", "data[53] (uint8)")
f.SERIAL_CONTROL_data_54 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_54", "data[54] (uint8)")
f.SERIAL_CONTROL_data_55 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_55", "data[55] (uint8)")
f.SERIAL_CONTROL_data_56 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_56", "data[56] (uint8)")
f.SERIAL_CONTROL_data_57 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_57", "data[57] (uint8)")
f.SERIAL_CONTROL_data_58 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_58", "data[58] (uint8)")
f.SERIAL_CONTROL_data_59 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_59", "data[59] (uint8)")
f.SERIAL_CONTROL_data_60 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_60", "data[60] (uint8)")
f.SERIAL_CONTROL_data_61 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_61", "data[61] (uint8)")
f.SERIAL_CONTROL_data_62 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_62", "data[62] (uint8)")
f.SERIAL_CONTROL_data_63 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_63", "data[63] (uint8)")
f.SERIAL_CONTROL_data_64 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_64", "data[64] (uint8)")
f.SERIAL_CONTROL_data_65 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_65", "data[65] (uint8)")
f.SERIAL_CONTROL_data_66 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_66", "data[66] (uint8)")
f.SERIAL_CONTROL_data_67 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_67", "data[67] (uint8)")
f.SERIAL_CONTROL_data_68 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_68", "data[68] (uint8)")
f.SERIAL_CONTROL_data_69 = ProtoField.uint8("mavlink_proto.SERIAL_CONTROL_data_69", "data[69] (uint8)")

f.GPS_RTK_time_last_baseline_ms = ProtoField.uint32("mavlink_proto.GPS_RTK_time_last_baseline_ms", "time_last_baseline_ms (uint32)")
f.GPS_RTK_rtk_receiver_id = ProtoField.uint8("mavlink_proto.GPS_RTK_rtk_receiver_id", "rtk_receiver_id (uint8)")
f.GPS_RTK_wn = ProtoField.uint16("mavlink_proto.GPS_RTK_wn", "wn (uint16)")
f.GPS_RTK_tow = ProtoField.uint32("mavlink_proto.GPS_RTK_tow", "tow (uint32)")
f.GPS_RTK_rtk_health = ProtoField.uint8("mavlink_proto.GPS_RTK_rtk_health", "rtk_health (uint8)")
f.GPS_RTK_rtk_rate = ProtoField.uint8("mavlink_proto.GPS_RTK_rtk_rate", "rtk_rate (uint8)")
f.GPS_RTK_nsats = ProtoField.uint8("mavlink_proto.GPS_RTK_nsats", "nsats (uint8)")
f.GPS_RTK_baseline_coords_type = ProtoField.uint8("mavlink_proto.GPS_RTK_baseline_coords_type", "baseline_coords_type (uint8)")
f.GPS_RTK_baseline_a_mm = ProtoField.int32("mavlink_proto.GPS_RTK_baseline_a_mm", "baseline_a_mm (int32)")
f.GPS_RTK_baseline_b_mm = ProtoField.int32("mavlink_proto.GPS_RTK_baseline_b_mm", "baseline_b_mm (int32)")
f.GPS_RTK_baseline_c_mm = ProtoField.int32("mavlink_proto.GPS_RTK_baseline_c_mm", "baseline_c_mm (int32)")
f.GPS_RTK_accuracy = ProtoField.uint32("mavlink_proto.GPS_RTK_accuracy", "accuracy (uint32)")
f.GPS_RTK_iar_num_hypotheses = ProtoField.int32("mavlink_proto.GPS_RTK_iar_num_hypotheses", "iar_num_hypotheses (int32)")

f.GPS2_RTK_time_last_baseline_ms = ProtoField.uint32("mavlink_proto.GPS2_RTK_time_last_baseline_ms", "time_last_baseline_ms (uint32)")
f.GPS2_RTK_rtk_receiver_id = ProtoField.uint8("mavlink_proto.GPS2_RTK_rtk_receiver_id", "rtk_receiver_id (uint8)")
f.GPS2_RTK_wn = ProtoField.uint16("mavlink_proto.GPS2_RTK_wn", "wn (uint16)")
f.GPS2_RTK_tow = ProtoField.uint32("mavlink_proto.GPS2_RTK_tow", "tow (uint32)")
f.GPS2_RTK_rtk_health = ProtoField.uint8("mavlink_proto.GPS2_RTK_rtk_health", "rtk_health (uint8)")
f.GPS2_RTK_rtk_rate = ProtoField.uint8("mavlink_proto.GPS2_RTK_rtk_rate", "rtk_rate (uint8)")
f.GPS2_RTK_nsats = ProtoField.uint8("mavlink_proto.GPS2_RTK_nsats", "nsats (uint8)")
f.GPS2_RTK_baseline_coords_type = ProtoField.uint8("mavlink_proto.GPS2_RTK_baseline_coords_type", "baseline_coords_type (uint8)")
f.GPS2_RTK_baseline_a_mm = ProtoField.int32("mavlink_proto.GPS2_RTK_baseline_a_mm", "baseline_a_mm (int32)")
f.GPS2_RTK_baseline_b_mm = ProtoField.int32("mavlink_proto.GPS2_RTK_baseline_b_mm", "baseline_b_mm (int32)")
f.GPS2_RTK_baseline_c_mm = ProtoField.int32("mavlink_proto.GPS2_RTK_baseline_c_mm", "baseline_c_mm (int32)")
f.GPS2_RTK_accuracy = ProtoField.uint32("mavlink_proto.GPS2_RTK_accuracy", "accuracy (uint32)")
f.GPS2_RTK_iar_num_hypotheses = ProtoField.int32("mavlink_proto.GPS2_RTK_iar_num_hypotheses", "iar_num_hypotheses (int32)")

f.SCALED_IMU3_time_boot_ms = ProtoField.uint32("mavlink_proto.SCALED_IMU3_time_boot_ms", "time_boot_ms (uint32)")
f.SCALED_IMU3_xacc = ProtoField.int16("mavlink_proto.SCALED_IMU3_xacc", "xacc (int16)")
f.SCALED_IMU3_yacc = ProtoField.int16("mavlink_proto.SCALED_IMU3_yacc", "yacc (int16)")
f.SCALED_IMU3_zacc = ProtoField.int16("mavlink_proto.SCALED_IMU3_zacc", "zacc (int16)")
f.SCALED_IMU3_xgyro = ProtoField.int16("mavlink_proto.SCALED_IMU3_xgyro", "xgyro (int16)")
f.SCALED_IMU3_ygyro = ProtoField.int16("mavlink_proto.SCALED_IMU3_ygyro", "ygyro (int16)")
f.SCALED_IMU3_zgyro = ProtoField.int16("mavlink_proto.SCALED_IMU3_zgyro", "zgyro (int16)")
f.SCALED_IMU3_xmag = ProtoField.int16("mavlink_proto.SCALED_IMU3_xmag", "xmag (int16)")
f.SCALED_IMU3_ymag = ProtoField.int16("mavlink_proto.SCALED_IMU3_ymag", "ymag (int16)")
f.SCALED_IMU3_zmag = ProtoField.int16("mavlink_proto.SCALED_IMU3_zmag", "zmag (int16)")

f.DATA_TRANSMISSION_HANDSHAKE_type = ProtoField.uint8("mavlink_proto.DATA_TRANSMISSION_HANDSHAKE_type", "type (uint8)")
f.DATA_TRANSMISSION_HANDSHAKE_size = ProtoField.uint32("mavlink_proto.DATA_TRANSMISSION_HANDSHAKE_size", "size (uint32)")
f.DATA_TRANSMISSION_HANDSHAKE_width = ProtoField.uint16("mavlink_proto.DATA_TRANSMISSION_HANDSHAKE_width", "width (uint16)")
f.DATA_TRANSMISSION_HANDSHAKE_height = ProtoField.uint16("mavlink_proto.DATA_TRANSMISSION_HANDSHAKE_height", "height (uint16)")
f.DATA_TRANSMISSION_HANDSHAKE_packets = ProtoField.uint16("mavlink_proto.DATA_TRANSMISSION_HANDSHAKE_packets", "packets (uint16)")
f.DATA_TRANSMISSION_HANDSHAKE_payload = ProtoField.uint8("mavlink_proto.DATA_TRANSMISSION_HANDSHAKE_payload", "payload (uint8)")
f.DATA_TRANSMISSION_HANDSHAKE_jpg_quality = ProtoField.uint8("mavlink_proto.DATA_TRANSMISSION_HANDSHAKE_jpg_quality", "jpg_quality (uint8)")

f.ENCAPSULATED_DATA_seqnr = ProtoField.uint16("mavlink_proto.ENCAPSULATED_DATA_seqnr", "seqnr (uint16)")
f.ENCAPSULATED_DATA_data_0 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_0", "data[0] (uint8)")
f.ENCAPSULATED_DATA_data_1 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_1", "data[1] (uint8)")
f.ENCAPSULATED_DATA_data_2 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_2", "data[2] (uint8)")
f.ENCAPSULATED_DATA_data_3 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_3", "data[3] (uint8)")
f.ENCAPSULATED_DATA_data_4 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_4", "data[4] (uint8)")
f.ENCAPSULATED_DATA_data_5 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_5", "data[5] (uint8)")
f.ENCAPSULATED_DATA_data_6 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_6", "data[6] (uint8)")
f.ENCAPSULATED_DATA_data_7 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_7", "data[7] (uint8)")
f.ENCAPSULATED_DATA_data_8 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_8", "data[8] (uint8)")
f.ENCAPSULATED_DATA_data_9 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_9", "data[9] (uint8)")
f.ENCAPSULATED_DATA_data_10 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_10", "data[10] (uint8)")
f.ENCAPSULATED_DATA_data_11 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_11", "data[11] (uint8)")
f.ENCAPSULATED_DATA_data_12 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_12", "data[12] (uint8)")
f.ENCAPSULATED_DATA_data_13 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_13", "data[13] (uint8)")
f.ENCAPSULATED_DATA_data_14 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_14", "data[14] (uint8)")
f.ENCAPSULATED_DATA_data_15 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_15", "data[15] (uint8)")
f.ENCAPSULATED_DATA_data_16 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_16", "data[16] (uint8)")
f.ENCAPSULATED_DATA_data_17 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_17", "data[17] (uint8)")
f.ENCAPSULATED_DATA_data_18 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_18", "data[18] (uint8)")
f.ENCAPSULATED_DATA_data_19 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_19", "data[19] (uint8)")
f.ENCAPSULATED_DATA_data_20 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_20", "data[20] (uint8)")
f.ENCAPSULATED_DATA_data_21 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_21", "data[21] (uint8)")
f.ENCAPSULATED_DATA_data_22 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_22", "data[22] (uint8)")
f.ENCAPSULATED_DATA_data_23 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_23", "data[23] (uint8)")
f.ENCAPSULATED_DATA_data_24 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_24", "data[24] (uint8)")
f.ENCAPSULATED_DATA_data_25 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_25", "data[25] (uint8)")
f.ENCAPSULATED_DATA_data_26 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_26", "data[26] (uint8)")
f.ENCAPSULATED_DATA_data_27 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_27", "data[27] (uint8)")
f.ENCAPSULATED_DATA_data_28 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_28", "data[28] (uint8)")
f.ENCAPSULATED_DATA_data_29 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_29", "data[29] (uint8)")
f.ENCAPSULATED_DATA_data_30 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_30", "data[30] (uint8)")
f.ENCAPSULATED_DATA_data_31 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_31", "data[31] (uint8)")
f.ENCAPSULATED_DATA_data_32 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_32", "data[32] (uint8)")
f.ENCAPSULATED_DATA_data_33 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_33", "data[33] (uint8)")
f.ENCAPSULATED_DATA_data_34 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_34", "data[34] (uint8)")
f.ENCAPSULATED_DATA_data_35 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_35", "data[35] (uint8)")
f.ENCAPSULATED_DATA_data_36 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_36", "data[36] (uint8)")
f.ENCAPSULATED_DATA_data_37 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_37", "data[37] (uint8)")
f.ENCAPSULATED_DATA_data_38 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_38", "data[38] (uint8)")
f.ENCAPSULATED_DATA_data_39 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_39", "data[39] (uint8)")
f.ENCAPSULATED_DATA_data_40 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_40", "data[40] (uint8)")
f.ENCAPSULATED_DATA_data_41 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_41", "data[41] (uint8)")
f.ENCAPSULATED_DATA_data_42 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_42", "data[42] (uint8)")
f.ENCAPSULATED_DATA_data_43 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_43", "data[43] (uint8)")
f.ENCAPSULATED_DATA_data_44 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_44", "data[44] (uint8)")
f.ENCAPSULATED_DATA_data_45 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_45", "data[45] (uint8)")
f.ENCAPSULATED_DATA_data_46 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_46", "data[46] (uint8)")
f.ENCAPSULATED_DATA_data_47 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_47", "data[47] (uint8)")
f.ENCAPSULATED_DATA_data_48 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_48", "data[48] (uint8)")
f.ENCAPSULATED_DATA_data_49 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_49", "data[49] (uint8)")
f.ENCAPSULATED_DATA_data_50 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_50", "data[50] (uint8)")
f.ENCAPSULATED_DATA_data_51 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_51", "data[51] (uint8)")
f.ENCAPSULATED_DATA_data_52 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_52", "data[52] (uint8)")
f.ENCAPSULATED_DATA_data_53 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_53", "data[53] (uint8)")
f.ENCAPSULATED_DATA_data_54 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_54", "data[54] (uint8)")
f.ENCAPSULATED_DATA_data_55 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_55", "data[55] (uint8)")
f.ENCAPSULATED_DATA_data_56 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_56", "data[56] (uint8)")
f.ENCAPSULATED_DATA_data_57 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_57", "data[57] (uint8)")
f.ENCAPSULATED_DATA_data_58 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_58", "data[58] (uint8)")
f.ENCAPSULATED_DATA_data_59 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_59", "data[59] (uint8)")
f.ENCAPSULATED_DATA_data_60 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_60", "data[60] (uint8)")
f.ENCAPSULATED_DATA_data_61 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_61", "data[61] (uint8)")
f.ENCAPSULATED_DATA_data_62 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_62", "data[62] (uint8)")
f.ENCAPSULATED_DATA_data_63 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_63", "data[63] (uint8)")
f.ENCAPSULATED_DATA_data_64 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_64", "data[64] (uint8)")
f.ENCAPSULATED_DATA_data_65 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_65", "data[65] (uint8)")
f.ENCAPSULATED_DATA_data_66 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_66", "data[66] (uint8)")
f.ENCAPSULATED_DATA_data_67 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_67", "data[67] (uint8)")
f.ENCAPSULATED_DATA_data_68 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_68", "data[68] (uint8)")
f.ENCAPSULATED_DATA_data_69 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_69", "data[69] (uint8)")
f.ENCAPSULATED_DATA_data_70 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_70", "data[70] (uint8)")
f.ENCAPSULATED_DATA_data_71 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_71", "data[71] (uint8)")
f.ENCAPSULATED_DATA_data_72 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_72", "data[72] (uint8)")
f.ENCAPSULATED_DATA_data_73 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_73", "data[73] (uint8)")
f.ENCAPSULATED_DATA_data_74 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_74", "data[74] (uint8)")
f.ENCAPSULATED_DATA_data_75 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_75", "data[75] (uint8)")
f.ENCAPSULATED_DATA_data_76 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_76", "data[76] (uint8)")
f.ENCAPSULATED_DATA_data_77 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_77", "data[77] (uint8)")
f.ENCAPSULATED_DATA_data_78 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_78", "data[78] (uint8)")
f.ENCAPSULATED_DATA_data_79 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_79", "data[79] (uint8)")
f.ENCAPSULATED_DATA_data_80 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_80", "data[80] (uint8)")
f.ENCAPSULATED_DATA_data_81 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_81", "data[81] (uint8)")
f.ENCAPSULATED_DATA_data_82 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_82", "data[82] (uint8)")
f.ENCAPSULATED_DATA_data_83 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_83", "data[83] (uint8)")
f.ENCAPSULATED_DATA_data_84 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_84", "data[84] (uint8)")
f.ENCAPSULATED_DATA_data_85 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_85", "data[85] (uint8)")
f.ENCAPSULATED_DATA_data_86 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_86", "data[86] (uint8)")
f.ENCAPSULATED_DATA_data_87 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_87", "data[87] (uint8)")
f.ENCAPSULATED_DATA_data_88 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_88", "data[88] (uint8)")
f.ENCAPSULATED_DATA_data_89 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_89", "data[89] (uint8)")
f.ENCAPSULATED_DATA_data_90 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_90", "data[90] (uint8)")
f.ENCAPSULATED_DATA_data_91 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_91", "data[91] (uint8)")
f.ENCAPSULATED_DATA_data_92 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_92", "data[92] (uint8)")
f.ENCAPSULATED_DATA_data_93 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_93", "data[93] (uint8)")
f.ENCAPSULATED_DATA_data_94 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_94", "data[94] (uint8)")
f.ENCAPSULATED_DATA_data_95 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_95", "data[95] (uint8)")
f.ENCAPSULATED_DATA_data_96 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_96", "data[96] (uint8)")
f.ENCAPSULATED_DATA_data_97 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_97", "data[97] (uint8)")
f.ENCAPSULATED_DATA_data_98 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_98", "data[98] (uint8)")
f.ENCAPSULATED_DATA_data_99 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_99", "data[99] (uint8)")
f.ENCAPSULATED_DATA_data_100 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_100", "data[100] (uint8)")
f.ENCAPSULATED_DATA_data_101 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_101", "data[101] (uint8)")
f.ENCAPSULATED_DATA_data_102 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_102", "data[102] (uint8)")
f.ENCAPSULATED_DATA_data_103 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_103", "data[103] (uint8)")
f.ENCAPSULATED_DATA_data_104 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_104", "data[104] (uint8)")
f.ENCAPSULATED_DATA_data_105 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_105", "data[105] (uint8)")
f.ENCAPSULATED_DATA_data_106 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_106", "data[106] (uint8)")
f.ENCAPSULATED_DATA_data_107 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_107", "data[107] (uint8)")
f.ENCAPSULATED_DATA_data_108 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_108", "data[108] (uint8)")
f.ENCAPSULATED_DATA_data_109 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_109", "data[109] (uint8)")
f.ENCAPSULATED_DATA_data_110 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_110", "data[110] (uint8)")
f.ENCAPSULATED_DATA_data_111 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_111", "data[111] (uint8)")
f.ENCAPSULATED_DATA_data_112 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_112", "data[112] (uint8)")
f.ENCAPSULATED_DATA_data_113 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_113", "data[113] (uint8)")
f.ENCAPSULATED_DATA_data_114 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_114", "data[114] (uint8)")
f.ENCAPSULATED_DATA_data_115 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_115", "data[115] (uint8)")
f.ENCAPSULATED_DATA_data_116 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_116", "data[116] (uint8)")
f.ENCAPSULATED_DATA_data_117 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_117", "data[117] (uint8)")
f.ENCAPSULATED_DATA_data_118 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_118", "data[118] (uint8)")
f.ENCAPSULATED_DATA_data_119 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_119", "data[119] (uint8)")
f.ENCAPSULATED_DATA_data_120 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_120", "data[120] (uint8)")
f.ENCAPSULATED_DATA_data_121 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_121", "data[121] (uint8)")
f.ENCAPSULATED_DATA_data_122 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_122", "data[122] (uint8)")
f.ENCAPSULATED_DATA_data_123 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_123", "data[123] (uint8)")
f.ENCAPSULATED_DATA_data_124 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_124", "data[124] (uint8)")
f.ENCAPSULATED_DATA_data_125 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_125", "data[125] (uint8)")
f.ENCAPSULATED_DATA_data_126 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_126", "data[126] (uint8)")
f.ENCAPSULATED_DATA_data_127 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_127", "data[127] (uint8)")
f.ENCAPSULATED_DATA_data_128 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_128", "data[128] (uint8)")
f.ENCAPSULATED_DATA_data_129 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_129", "data[129] (uint8)")
f.ENCAPSULATED_DATA_data_130 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_130", "data[130] (uint8)")
f.ENCAPSULATED_DATA_data_131 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_131", "data[131] (uint8)")
f.ENCAPSULATED_DATA_data_132 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_132", "data[132] (uint8)")
f.ENCAPSULATED_DATA_data_133 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_133", "data[133] (uint8)")
f.ENCAPSULATED_DATA_data_134 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_134", "data[134] (uint8)")
f.ENCAPSULATED_DATA_data_135 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_135", "data[135] (uint8)")
f.ENCAPSULATED_DATA_data_136 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_136", "data[136] (uint8)")
f.ENCAPSULATED_DATA_data_137 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_137", "data[137] (uint8)")
f.ENCAPSULATED_DATA_data_138 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_138", "data[138] (uint8)")
f.ENCAPSULATED_DATA_data_139 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_139", "data[139] (uint8)")
f.ENCAPSULATED_DATA_data_140 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_140", "data[140] (uint8)")
f.ENCAPSULATED_DATA_data_141 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_141", "data[141] (uint8)")
f.ENCAPSULATED_DATA_data_142 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_142", "data[142] (uint8)")
f.ENCAPSULATED_DATA_data_143 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_143", "data[143] (uint8)")
f.ENCAPSULATED_DATA_data_144 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_144", "data[144] (uint8)")
f.ENCAPSULATED_DATA_data_145 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_145", "data[145] (uint8)")
f.ENCAPSULATED_DATA_data_146 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_146", "data[146] (uint8)")
f.ENCAPSULATED_DATA_data_147 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_147", "data[147] (uint8)")
f.ENCAPSULATED_DATA_data_148 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_148", "data[148] (uint8)")
f.ENCAPSULATED_DATA_data_149 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_149", "data[149] (uint8)")
f.ENCAPSULATED_DATA_data_150 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_150", "data[150] (uint8)")
f.ENCAPSULATED_DATA_data_151 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_151", "data[151] (uint8)")
f.ENCAPSULATED_DATA_data_152 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_152", "data[152] (uint8)")
f.ENCAPSULATED_DATA_data_153 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_153", "data[153] (uint8)")
f.ENCAPSULATED_DATA_data_154 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_154", "data[154] (uint8)")
f.ENCAPSULATED_DATA_data_155 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_155", "data[155] (uint8)")
f.ENCAPSULATED_DATA_data_156 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_156", "data[156] (uint8)")
f.ENCAPSULATED_DATA_data_157 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_157", "data[157] (uint8)")
f.ENCAPSULATED_DATA_data_158 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_158", "data[158] (uint8)")
f.ENCAPSULATED_DATA_data_159 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_159", "data[159] (uint8)")
f.ENCAPSULATED_DATA_data_160 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_160", "data[160] (uint8)")
f.ENCAPSULATED_DATA_data_161 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_161", "data[161] (uint8)")
f.ENCAPSULATED_DATA_data_162 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_162", "data[162] (uint8)")
f.ENCAPSULATED_DATA_data_163 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_163", "data[163] (uint8)")
f.ENCAPSULATED_DATA_data_164 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_164", "data[164] (uint8)")
f.ENCAPSULATED_DATA_data_165 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_165", "data[165] (uint8)")
f.ENCAPSULATED_DATA_data_166 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_166", "data[166] (uint8)")
f.ENCAPSULATED_DATA_data_167 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_167", "data[167] (uint8)")
f.ENCAPSULATED_DATA_data_168 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_168", "data[168] (uint8)")
f.ENCAPSULATED_DATA_data_169 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_169", "data[169] (uint8)")
f.ENCAPSULATED_DATA_data_170 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_170", "data[170] (uint8)")
f.ENCAPSULATED_DATA_data_171 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_171", "data[171] (uint8)")
f.ENCAPSULATED_DATA_data_172 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_172", "data[172] (uint8)")
f.ENCAPSULATED_DATA_data_173 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_173", "data[173] (uint8)")
f.ENCAPSULATED_DATA_data_174 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_174", "data[174] (uint8)")
f.ENCAPSULATED_DATA_data_175 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_175", "data[175] (uint8)")
f.ENCAPSULATED_DATA_data_176 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_176", "data[176] (uint8)")
f.ENCAPSULATED_DATA_data_177 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_177", "data[177] (uint8)")
f.ENCAPSULATED_DATA_data_178 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_178", "data[178] (uint8)")
f.ENCAPSULATED_DATA_data_179 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_179", "data[179] (uint8)")
f.ENCAPSULATED_DATA_data_180 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_180", "data[180] (uint8)")
f.ENCAPSULATED_DATA_data_181 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_181", "data[181] (uint8)")
f.ENCAPSULATED_DATA_data_182 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_182", "data[182] (uint8)")
f.ENCAPSULATED_DATA_data_183 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_183", "data[183] (uint8)")
f.ENCAPSULATED_DATA_data_184 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_184", "data[184] (uint8)")
f.ENCAPSULATED_DATA_data_185 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_185", "data[185] (uint8)")
f.ENCAPSULATED_DATA_data_186 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_186", "data[186] (uint8)")
f.ENCAPSULATED_DATA_data_187 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_187", "data[187] (uint8)")
f.ENCAPSULATED_DATA_data_188 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_188", "data[188] (uint8)")
f.ENCAPSULATED_DATA_data_189 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_189", "data[189] (uint8)")
f.ENCAPSULATED_DATA_data_190 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_190", "data[190] (uint8)")
f.ENCAPSULATED_DATA_data_191 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_191", "data[191] (uint8)")
f.ENCAPSULATED_DATA_data_192 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_192", "data[192] (uint8)")
f.ENCAPSULATED_DATA_data_193 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_193", "data[193] (uint8)")
f.ENCAPSULATED_DATA_data_194 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_194", "data[194] (uint8)")
f.ENCAPSULATED_DATA_data_195 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_195", "data[195] (uint8)")
f.ENCAPSULATED_DATA_data_196 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_196", "data[196] (uint8)")
f.ENCAPSULATED_DATA_data_197 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_197", "data[197] (uint8)")
f.ENCAPSULATED_DATA_data_198 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_198", "data[198] (uint8)")
f.ENCAPSULATED_DATA_data_199 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_199", "data[199] (uint8)")
f.ENCAPSULATED_DATA_data_200 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_200", "data[200] (uint8)")
f.ENCAPSULATED_DATA_data_201 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_201", "data[201] (uint8)")
f.ENCAPSULATED_DATA_data_202 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_202", "data[202] (uint8)")
f.ENCAPSULATED_DATA_data_203 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_203", "data[203] (uint8)")
f.ENCAPSULATED_DATA_data_204 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_204", "data[204] (uint8)")
f.ENCAPSULATED_DATA_data_205 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_205", "data[205] (uint8)")
f.ENCAPSULATED_DATA_data_206 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_206", "data[206] (uint8)")
f.ENCAPSULATED_DATA_data_207 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_207", "data[207] (uint8)")
f.ENCAPSULATED_DATA_data_208 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_208", "data[208] (uint8)")
f.ENCAPSULATED_DATA_data_209 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_209", "data[209] (uint8)")
f.ENCAPSULATED_DATA_data_210 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_210", "data[210] (uint8)")
f.ENCAPSULATED_DATA_data_211 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_211", "data[211] (uint8)")
f.ENCAPSULATED_DATA_data_212 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_212", "data[212] (uint8)")
f.ENCAPSULATED_DATA_data_213 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_213", "data[213] (uint8)")
f.ENCAPSULATED_DATA_data_214 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_214", "data[214] (uint8)")
f.ENCAPSULATED_DATA_data_215 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_215", "data[215] (uint8)")
f.ENCAPSULATED_DATA_data_216 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_216", "data[216] (uint8)")
f.ENCAPSULATED_DATA_data_217 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_217", "data[217] (uint8)")
f.ENCAPSULATED_DATA_data_218 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_218", "data[218] (uint8)")
f.ENCAPSULATED_DATA_data_219 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_219", "data[219] (uint8)")
f.ENCAPSULATED_DATA_data_220 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_220", "data[220] (uint8)")
f.ENCAPSULATED_DATA_data_221 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_221", "data[221] (uint8)")
f.ENCAPSULATED_DATA_data_222 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_222", "data[222] (uint8)")
f.ENCAPSULATED_DATA_data_223 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_223", "data[223] (uint8)")
f.ENCAPSULATED_DATA_data_224 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_224", "data[224] (uint8)")
f.ENCAPSULATED_DATA_data_225 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_225", "data[225] (uint8)")
f.ENCAPSULATED_DATA_data_226 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_226", "data[226] (uint8)")
f.ENCAPSULATED_DATA_data_227 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_227", "data[227] (uint8)")
f.ENCAPSULATED_DATA_data_228 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_228", "data[228] (uint8)")
f.ENCAPSULATED_DATA_data_229 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_229", "data[229] (uint8)")
f.ENCAPSULATED_DATA_data_230 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_230", "data[230] (uint8)")
f.ENCAPSULATED_DATA_data_231 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_231", "data[231] (uint8)")
f.ENCAPSULATED_DATA_data_232 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_232", "data[232] (uint8)")
f.ENCAPSULATED_DATA_data_233 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_233", "data[233] (uint8)")
f.ENCAPSULATED_DATA_data_234 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_234", "data[234] (uint8)")
f.ENCAPSULATED_DATA_data_235 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_235", "data[235] (uint8)")
f.ENCAPSULATED_DATA_data_236 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_236", "data[236] (uint8)")
f.ENCAPSULATED_DATA_data_237 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_237", "data[237] (uint8)")
f.ENCAPSULATED_DATA_data_238 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_238", "data[238] (uint8)")
f.ENCAPSULATED_DATA_data_239 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_239", "data[239] (uint8)")
f.ENCAPSULATED_DATA_data_240 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_240", "data[240] (uint8)")
f.ENCAPSULATED_DATA_data_241 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_241", "data[241] (uint8)")
f.ENCAPSULATED_DATA_data_242 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_242", "data[242] (uint8)")
f.ENCAPSULATED_DATA_data_243 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_243", "data[243] (uint8)")
f.ENCAPSULATED_DATA_data_244 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_244", "data[244] (uint8)")
f.ENCAPSULATED_DATA_data_245 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_245", "data[245] (uint8)")
f.ENCAPSULATED_DATA_data_246 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_246", "data[246] (uint8)")
f.ENCAPSULATED_DATA_data_247 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_247", "data[247] (uint8)")
f.ENCAPSULATED_DATA_data_248 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_248", "data[248] (uint8)")
f.ENCAPSULATED_DATA_data_249 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_249", "data[249] (uint8)")
f.ENCAPSULATED_DATA_data_250 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_250", "data[250] (uint8)")
f.ENCAPSULATED_DATA_data_251 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_251", "data[251] (uint8)")
f.ENCAPSULATED_DATA_data_252 = ProtoField.uint8("mavlink_proto.ENCAPSULATED_DATA_data_252", "data[252] (uint8)")

f.DISTANCE_SENSOR_time_boot_ms = ProtoField.uint32("mavlink_proto.DISTANCE_SENSOR_time_boot_ms", "time_boot_ms (uint32)")
f.DISTANCE_SENSOR_min_distance = ProtoField.uint16("mavlink_proto.DISTANCE_SENSOR_min_distance", "min_distance (uint16)")
f.DISTANCE_SENSOR_max_distance = ProtoField.uint16("mavlink_proto.DISTANCE_SENSOR_max_distance", "max_distance (uint16)")
f.DISTANCE_SENSOR_current_distance = ProtoField.uint16("mavlink_proto.DISTANCE_SENSOR_current_distance", "current_distance (uint16)")
f.DISTANCE_SENSOR_type = ProtoField.uint8("mavlink_proto.DISTANCE_SENSOR_type", "type (uint8)")
f.DISTANCE_SENSOR_id = ProtoField.uint8("mavlink_proto.DISTANCE_SENSOR_id", "id (uint8)")
f.DISTANCE_SENSOR_orientation = ProtoField.uint8("mavlink_proto.DISTANCE_SENSOR_orientation", "orientation (uint8)")
f.DISTANCE_SENSOR_covariance = ProtoField.uint8("mavlink_proto.DISTANCE_SENSOR_covariance", "covariance (uint8)")

f.TERRAIN_REQUEST_lat = ProtoField.int32("mavlink_proto.TERRAIN_REQUEST_lat", "lat (int32)")
f.TERRAIN_REQUEST_lon = ProtoField.int32("mavlink_proto.TERRAIN_REQUEST_lon", "lon (int32)")
f.TERRAIN_REQUEST_grid_spacing = ProtoField.uint16("mavlink_proto.TERRAIN_REQUEST_grid_spacing", "grid_spacing (uint16)")
f.TERRAIN_REQUEST_mask = ProtoField.uint64("mavlink_proto.TERRAIN_REQUEST_mask", "mask (uint64)")

f.TERRAIN_DATA_lat = ProtoField.int32("mavlink_proto.TERRAIN_DATA_lat", "lat (int32)")
f.TERRAIN_DATA_lon = ProtoField.int32("mavlink_proto.TERRAIN_DATA_lon", "lon (int32)")
f.TERRAIN_DATA_grid_spacing = ProtoField.uint16("mavlink_proto.TERRAIN_DATA_grid_spacing", "grid_spacing (uint16)")
f.TERRAIN_DATA_gridbit = ProtoField.uint8("mavlink_proto.TERRAIN_DATA_gridbit", "gridbit (uint8)")
f.TERRAIN_DATA_data_0 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_0", "data[0] (int16)")
f.TERRAIN_DATA_data_1 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_1", "data[1] (int16)")
f.TERRAIN_DATA_data_2 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_2", "data[2] (int16)")
f.TERRAIN_DATA_data_3 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_3", "data[3] (int16)")
f.TERRAIN_DATA_data_4 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_4", "data[4] (int16)")
f.TERRAIN_DATA_data_5 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_5", "data[5] (int16)")
f.TERRAIN_DATA_data_6 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_6", "data[6] (int16)")
f.TERRAIN_DATA_data_7 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_7", "data[7] (int16)")
f.TERRAIN_DATA_data_8 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_8", "data[8] (int16)")
f.TERRAIN_DATA_data_9 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_9", "data[9] (int16)")
f.TERRAIN_DATA_data_10 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_10", "data[10] (int16)")
f.TERRAIN_DATA_data_11 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_11", "data[11] (int16)")
f.TERRAIN_DATA_data_12 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_12", "data[12] (int16)")
f.TERRAIN_DATA_data_13 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_13", "data[13] (int16)")
f.TERRAIN_DATA_data_14 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_14", "data[14] (int16)")
f.TERRAIN_DATA_data_15 = ProtoField.int16("mavlink_proto.TERRAIN_DATA_data_15", "data[15] (int16)")

f.TERRAIN_CHECK_lat = ProtoField.int32("mavlink_proto.TERRAIN_CHECK_lat", "lat (int32)")
f.TERRAIN_CHECK_lon = ProtoField.int32("mavlink_proto.TERRAIN_CHECK_lon", "lon (int32)")

f.TERRAIN_REPORT_lat = ProtoField.int32("mavlink_proto.TERRAIN_REPORT_lat", "lat (int32)")
f.TERRAIN_REPORT_lon = ProtoField.int32("mavlink_proto.TERRAIN_REPORT_lon", "lon (int32)")
f.TERRAIN_REPORT_spacing = ProtoField.uint16("mavlink_proto.TERRAIN_REPORT_spacing", "spacing (uint16)")
f.TERRAIN_REPORT_terrain_height = ProtoField.float("mavlink_proto.TERRAIN_REPORT_terrain_height", "terrain_height (float)")
f.TERRAIN_REPORT_current_height = ProtoField.float("mavlink_proto.TERRAIN_REPORT_current_height", "current_height (float)")
f.TERRAIN_REPORT_pending = ProtoField.uint16("mavlink_proto.TERRAIN_REPORT_pending", "pending (uint16)")
f.TERRAIN_REPORT_loaded = ProtoField.uint16("mavlink_proto.TERRAIN_REPORT_loaded", "loaded (uint16)")

f.SCALED_PRESSURE2_time_boot_ms = ProtoField.uint32("mavlink_proto.SCALED_PRESSURE2_time_boot_ms", "time_boot_ms (uint32)")
f.SCALED_PRESSURE2_press_abs = ProtoField.float("mavlink_proto.SCALED_PRESSURE2_press_abs", "press_abs (float)")
f.SCALED_PRESSURE2_press_diff = ProtoField.float("mavlink_proto.SCALED_PRESSURE2_press_diff", "press_diff (float)")
f.SCALED_PRESSURE2_temperature = ProtoField.int16("mavlink_proto.SCALED_PRESSURE2_temperature", "temperature (int16)")

f.ATT_POS_MOCAP_time_usec = ProtoField.uint64("mavlink_proto.ATT_POS_MOCAP_time_usec", "time_usec (uint64)")
f.ATT_POS_MOCAP_q_0 = ProtoField.float("mavlink_proto.ATT_POS_MOCAP_q_0", "q[0] (float)")
f.ATT_POS_MOCAP_q_1 = ProtoField.float("mavlink_proto.ATT_POS_MOCAP_q_1", "q[1] (float)")
f.ATT_POS_MOCAP_q_2 = ProtoField.float("mavlink_proto.ATT_POS_MOCAP_q_2", "q[2] (float)")
f.ATT_POS_MOCAP_q_3 = ProtoField.float("mavlink_proto.ATT_POS_MOCAP_q_3", "q[3] (float)")
f.ATT_POS_MOCAP_x = ProtoField.float("mavlink_proto.ATT_POS_MOCAP_x", "x (float)")
f.ATT_POS_MOCAP_y = ProtoField.float("mavlink_proto.ATT_POS_MOCAP_y", "y (float)")
f.ATT_POS_MOCAP_z = ProtoField.float("mavlink_proto.ATT_POS_MOCAP_z", "z (float)")

f.SET_ACTUATOR_CONTROL_TARGET_time_usec = ProtoField.uint64("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_time_usec", "time_usec (uint64)")
f.SET_ACTUATOR_CONTROL_TARGET_group_mlx = ProtoField.uint8("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_group_mlx", "group_mlx (uint8)")
f.SET_ACTUATOR_CONTROL_TARGET_target_system = ProtoField.uint8("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_target_system", "target_system (uint8)")
f.SET_ACTUATOR_CONTROL_TARGET_target_component = ProtoField.uint8("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_target_component", "target_component (uint8)")
f.SET_ACTUATOR_CONTROL_TARGET_controls_0 = ProtoField.float("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_controls_0", "controls[0] (float)")
f.SET_ACTUATOR_CONTROL_TARGET_controls_1 = ProtoField.float("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_controls_1", "controls[1] (float)")
f.SET_ACTUATOR_CONTROL_TARGET_controls_2 = ProtoField.float("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_controls_2", "controls[2] (float)")
f.SET_ACTUATOR_CONTROL_TARGET_controls_3 = ProtoField.float("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_controls_3", "controls[3] (float)")
f.SET_ACTUATOR_CONTROL_TARGET_controls_4 = ProtoField.float("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_controls_4", "controls[4] (float)")
f.SET_ACTUATOR_CONTROL_TARGET_controls_5 = ProtoField.float("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_controls_5", "controls[5] (float)")
f.SET_ACTUATOR_CONTROL_TARGET_controls_6 = ProtoField.float("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_controls_6", "controls[6] (float)")
f.SET_ACTUATOR_CONTROL_TARGET_controls_7 = ProtoField.float("mavlink_proto.SET_ACTUATOR_CONTROL_TARGET_controls_7", "controls[7] (float)")

f.ACTUATOR_CONTROL_TARGET_time_usec = ProtoField.uint64("mavlink_proto.ACTUATOR_CONTROL_TARGET_time_usec", "time_usec (uint64)")
f.ACTUATOR_CONTROL_TARGET_group_mlx = ProtoField.uint8("mavlink_proto.ACTUATOR_CONTROL_TARGET_group_mlx", "group_mlx (uint8)")
f.ACTUATOR_CONTROL_TARGET_controls_0 = ProtoField.float("mavlink_proto.ACTUATOR_CONTROL_TARGET_controls_0", "controls[0] (float)")
f.ACTUATOR_CONTROL_TARGET_controls_1 = ProtoField.float("mavlink_proto.ACTUATOR_CONTROL_TARGET_controls_1", "controls[1] (float)")
f.ACTUATOR_CONTROL_TARGET_controls_2 = ProtoField.float("mavlink_proto.ACTUATOR_CONTROL_TARGET_controls_2", "controls[2] (float)")
f.ACTUATOR_CONTROL_TARGET_controls_3 = ProtoField.float("mavlink_proto.ACTUATOR_CONTROL_TARGET_controls_3", "controls[3] (float)")
f.ACTUATOR_CONTROL_TARGET_controls_4 = ProtoField.float("mavlink_proto.ACTUATOR_CONTROL_TARGET_controls_4", "controls[4] (float)")
f.ACTUATOR_CONTROL_TARGET_controls_5 = ProtoField.float("mavlink_proto.ACTUATOR_CONTROL_TARGET_controls_5", "controls[5] (float)")
f.ACTUATOR_CONTROL_TARGET_controls_6 = ProtoField.float("mavlink_proto.ACTUATOR_CONTROL_TARGET_controls_6", "controls[6] (float)")
f.ACTUATOR_CONTROL_TARGET_controls_7 = ProtoField.float("mavlink_proto.ACTUATOR_CONTROL_TARGET_controls_7", "controls[7] (float)")

f.ALTITUDE_time_usec = ProtoField.uint64("mavlink_proto.ALTITUDE_time_usec", "time_usec (uint64)")
f.ALTITUDE_altitude_monotonic = ProtoField.float("mavlink_proto.ALTITUDE_altitude_monotonic", "altitude_monotonic (float)")
f.ALTITUDE_altitude_amsl = ProtoField.float("mavlink_proto.ALTITUDE_altitude_amsl", "altitude_amsl (float)")
f.ALTITUDE_altitude_local = ProtoField.float("mavlink_proto.ALTITUDE_altitude_local", "altitude_local (float)")
f.ALTITUDE_altitude_relative = ProtoField.float("mavlink_proto.ALTITUDE_altitude_relative", "altitude_relative (float)")
f.ALTITUDE_altitude_terrain = ProtoField.float("mavlink_proto.ALTITUDE_altitude_terrain", "altitude_terrain (float)")
f.ALTITUDE_bottom_clearance = ProtoField.float("mavlink_proto.ALTITUDE_bottom_clearance", "bottom_clearance (float)")

f.RESOURCE_REQUEST_request_id = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_request_id", "request_id (uint8)")
f.RESOURCE_REQUEST_uri_type = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_type", "uri_type (uint8)")
f.RESOURCE_REQUEST_uri_0 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_0", "uri[0] (uint8)")
f.RESOURCE_REQUEST_uri_1 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_1", "uri[1] (uint8)")
f.RESOURCE_REQUEST_uri_2 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_2", "uri[2] (uint8)")
f.RESOURCE_REQUEST_uri_3 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_3", "uri[3] (uint8)")
f.RESOURCE_REQUEST_uri_4 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_4", "uri[4] (uint8)")
f.RESOURCE_REQUEST_uri_5 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_5", "uri[5] (uint8)")
f.RESOURCE_REQUEST_uri_6 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_6", "uri[6] (uint8)")
f.RESOURCE_REQUEST_uri_7 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_7", "uri[7] (uint8)")
f.RESOURCE_REQUEST_uri_8 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_8", "uri[8] (uint8)")
f.RESOURCE_REQUEST_uri_9 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_9", "uri[9] (uint8)")
f.RESOURCE_REQUEST_uri_10 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_10", "uri[10] (uint8)")
f.RESOURCE_REQUEST_uri_11 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_11", "uri[11] (uint8)")
f.RESOURCE_REQUEST_uri_12 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_12", "uri[12] (uint8)")
f.RESOURCE_REQUEST_uri_13 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_13", "uri[13] (uint8)")
f.RESOURCE_REQUEST_uri_14 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_14", "uri[14] (uint8)")
f.RESOURCE_REQUEST_uri_15 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_15", "uri[15] (uint8)")
f.RESOURCE_REQUEST_uri_16 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_16", "uri[16] (uint8)")
f.RESOURCE_REQUEST_uri_17 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_17", "uri[17] (uint8)")
f.RESOURCE_REQUEST_uri_18 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_18", "uri[18] (uint8)")
f.RESOURCE_REQUEST_uri_19 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_19", "uri[19] (uint8)")
f.RESOURCE_REQUEST_uri_20 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_20", "uri[20] (uint8)")
f.RESOURCE_REQUEST_uri_21 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_21", "uri[21] (uint8)")
f.RESOURCE_REQUEST_uri_22 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_22", "uri[22] (uint8)")
f.RESOURCE_REQUEST_uri_23 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_23", "uri[23] (uint8)")
f.RESOURCE_REQUEST_uri_24 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_24", "uri[24] (uint8)")
f.RESOURCE_REQUEST_uri_25 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_25", "uri[25] (uint8)")
f.RESOURCE_REQUEST_uri_26 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_26", "uri[26] (uint8)")
f.RESOURCE_REQUEST_uri_27 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_27", "uri[27] (uint8)")
f.RESOURCE_REQUEST_uri_28 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_28", "uri[28] (uint8)")
f.RESOURCE_REQUEST_uri_29 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_29", "uri[29] (uint8)")
f.RESOURCE_REQUEST_uri_30 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_30", "uri[30] (uint8)")
f.RESOURCE_REQUEST_uri_31 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_31", "uri[31] (uint8)")
f.RESOURCE_REQUEST_uri_32 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_32", "uri[32] (uint8)")
f.RESOURCE_REQUEST_uri_33 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_33", "uri[33] (uint8)")
f.RESOURCE_REQUEST_uri_34 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_34", "uri[34] (uint8)")
f.RESOURCE_REQUEST_uri_35 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_35", "uri[35] (uint8)")
f.RESOURCE_REQUEST_uri_36 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_36", "uri[36] (uint8)")
f.RESOURCE_REQUEST_uri_37 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_37", "uri[37] (uint8)")
f.RESOURCE_REQUEST_uri_38 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_38", "uri[38] (uint8)")
f.RESOURCE_REQUEST_uri_39 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_39", "uri[39] (uint8)")
f.RESOURCE_REQUEST_uri_40 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_40", "uri[40] (uint8)")
f.RESOURCE_REQUEST_uri_41 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_41", "uri[41] (uint8)")
f.RESOURCE_REQUEST_uri_42 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_42", "uri[42] (uint8)")
f.RESOURCE_REQUEST_uri_43 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_43", "uri[43] (uint8)")
f.RESOURCE_REQUEST_uri_44 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_44", "uri[44] (uint8)")
f.RESOURCE_REQUEST_uri_45 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_45", "uri[45] (uint8)")
f.RESOURCE_REQUEST_uri_46 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_46", "uri[46] (uint8)")
f.RESOURCE_REQUEST_uri_47 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_47", "uri[47] (uint8)")
f.RESOURCE_REQUEST_uri_48 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_48", "uri[48] (uint8)")
f.RESOURCE_REQUEST_uri_49 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_49", "uri[49] (uint8)")
f.RESOURCE_REQUEST_uri_50 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_50", "uri[50] (uint8)")
f.RESOURCE_REQUEST_uri_51 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_51", "uri[51] (uint8)")
f.RESOURCE_REQUEST_uri_52 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_52", "uri[52] (uint8)")
f.RESOURCE_REQUEST_uri_53 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_53", "uri[53] (uint8)")
f.RESOURCE_REQUEST_uri_54 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_54", "uri[54] (uint8)")
f.RESOURCE_REQUEST_uri_55 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_55", "uri[55] (uint8)")
f.RESOURCE_REQUEST_uri_56 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_56", "uri[56] (uint8)")
f.RESOURCE_REQUEST_uri_57 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_57", "uri[57] (uint8)")
f.RESOURCE_REQUEST_uri_58 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_58", "uri[58] (uint8)")
f.RESOURCE_REQUEST_uri_59 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_59", "uri[59] (uint8)")
f.RESOURCE_REQUEST_uri_60 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_60", "uri[60] (uint8)")
f.RESOURCE_REQUEST_uri_61 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_61", "uri[61] (uint8)")
f.RESOURCE_REQUEST_uri_62 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_62", "uri[62] (uint8)")
f.RESOURCE_REQUEST_uri_63 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_63", "uri[63] (uint8)")
f.RESOURCE_REQUEST_uri_64 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_64", "uri[64] (uint8)")
f.RESOURCE_REQUEST_uri_65 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_65", "uri[65] (uint8)")
f.RESOURCE_REQUEST_uri_66 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_66", "uri[66] (uint8)")
f.RESOURCE_REQUEST_uri_67 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_67", "uri[67] (uint8)")
f.RESOURCE_REQUEST_uri_68 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_68", "uri[68] (uint8)")
f.RESOURCE_REQUEST_uri_69 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_69", "uri[69] (uint8)")
f.RESOURCE_REQUEST_uri_70 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_70", "uri[70] (uint8)")
f.RESOURCE_REQUEST_uri_71 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_71", "uri[71] (uint8)")
f.RESOURCE_REQUEST_uri_72 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_72", "uri[72] (uint8)")
f.RESOURCE_REQUEST_uri_73 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_73", "uri[73] (uint8)")
f.RESOURCE_REQUEST_uri_74 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_74", "uri[74] (uint8)")
f.RESOURCE_REQUEST_uri_75 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_75", "uri[75] (uint8)")
f.RESOURCE_REQUEST_uri_76 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_76", "uri[76] (uint8)")
f.RESOURCE_REQUEST_uri_77 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_77", "uri[77] (uint8)")
f.RESOURCE_REQUEST_uri_78 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_78", "uri[78] (uint8)")
f.RESOURCE_REQUEST_uri_79 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_79", "uri[79] (uint8)")
f.RESOURCE_REQUEST_uri_80 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_80", "uri[80] (uint8)")
f.RESOURCE_REQUEST_uri_81 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_81", "uri[81] (uint8)")
f.RESOURCE_REQUEST_uri_82 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_82", "uri[82] (uint8)")
f.RESOURCE_REQUEST_uri_83 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_83", "uri[83] (uint8)")
f.RESOURCE_REQUEST_uri_84 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_84", "uri[84] (uint8)")
f.RESOURCE_REQUEST_uri_85 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_85", "uri[85] (uint8)")
f.RESOURCE_REQUEST_uri_86 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_86", "uri[86] (uint8)")
f.RESOURCE_REQUEST_uri_87 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_87", "uri[87] (uint8)")
f.RESOURCE_REQUEST_uri_88 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_88", "uri[88] (uint8)")
f.RESOURCE_REQUEST_uri_89 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_89", "uri[89] (uint8)")
f.RESOURCE_REQUEST_uri_90 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_90", "uri[90] (uint8)")
f.RESOURCE_REQUEST_uri_91 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_91", "uri[91] (uint8)")
f.RESOURCE_REQUEST_uri_92 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_92", "uri[92] (uint8)")
f.RESOURCE_REQUEST_uri_93 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_93", "uri[93] (uint8)")
f.RESOURCE_REQUEST_uri_94 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_94", "uri[94] (uint8)")
f.RESOURCE_REQUEST_uri_95 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_95", "uri[95] (uint8)")
f.RESOURCE_REQUEST_uri_96 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_96", "uri[96] (uint8)")
f.RESOURCE_REQUEST_uri_97 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_97", "uri[97] (uint8)")
f.RESOURCE_REQUEST_uri_98 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_98", "uri[98] (uint8)")
f.RESOURCE_REQUEST_uri_99 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_99", "uri[99] (uint8)")
f.RESOURCE_REQUEST_uri_100 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_100", "uri[100] (uint8)")
f.RESOURCE_REQUEST_uri_101 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_101", "uri[101] (uint8)")
f.RESOURCE_REQUEST_uri_102 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_102", "uri[102] (uint8)")
f.RESOURCE_REQUEST_uri_103 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_103", "uri[103] (uint8)")
f.RESOURCE_REQUEST_uri_104 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_104", "uri[104] (uint8)")
f.RESOURCE_REQUEST_uri_105 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_105", "uri[105] (uint8)")
f.RESOURCE_REQUEST_uri_106 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_106", "uri[106] (uint8)")
f.RESOURCE_REQUEST_uri_107 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_107", "uri[107] (uint8)")
f.RESOURCE_REQUEST_uri_108 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_108", "uri[108] (uint8)")
f.RESOURCE_REQUEST_uri_109 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_109", "uri[109] (uint8)")
f.RESOURCE_REQUEST_uri_110 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_110", "uri[110] (uint8)")
f.RESOURCE_REQUEST_uri_111 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_111", "uri[111] (uint8)")
f.RESOURCE_REQUEST_uri_112 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_112", "uri[112] (uint8)")
f.RESOURCE_REQUEST_uri_113 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_113", "uri[113] (uint8)")
f.RESOURCE_REQUEST_uri_114 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_114", "uri[114] (uint8)")
f.RESOURCE_REQUEST_uri_115 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_115", "uri[115] (uint8)")
f.RESOURCE_REQUEST_uri_116 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_116", "uri[116] (uint8)")
f.RESOURCE_REQUEST_uri_117 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_117", "uri[117] (uint8)")
f.RESOURCE_REQUEST_uri_118 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_118", "uri[118] (uint8)")
f.RESOURCE_REQUEST_uri_119 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_uri_119", "uri[119] (uint8)")
f.RESOURCE_REQUEST_transfer_type = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_transfer_type", "transfer_type (uint8)")
f.RESOURCE_REQUEST_storage_0 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_0", "storage[0] (uint8)")
f.RESOURCE_REQUEST_storage_1 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_1", "storage[1] (uint8)")
f.RESOURCE_REQUEST_storage_2 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_2", "storage[2] (uint8)")
f.RESOURCE_REQUEST_storage_3 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_3", "storage[3] (uint8)")
f.RESOURCE_REQUEST_storage_4 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_4", "storage[4] (uint8)")
f.RESOURCE_REQUEST_storage_5 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_5", "storage[5] (uint8)")
f.RESOURCE_REQUEST_storage_6 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_6", "storage[6] (uint8)")
f.RESOURCE_REQUEST_storage_7 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_7", "storage[7] (uint8)")
f.RESOURCE_REQUEST_storage_8 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_8", "storage[8] (uint8)")
f.RESOURCE_REQUEST_storage_9 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_9", "storage[9] (uint8)")
f.RESOURCE_REQUEST_storage_10 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_10", "storage[10] (uint8)")
f.RESOURCE_REQUEST_storage_11 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_11", "storage[11] (uint8)")
f.RESOURCE_REQUEST_storage_12 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_12", "storage[12] (uint8)")
f.RESOURCE_REQUEST_storage_13 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_13", "storage[13] (uint8)")
f.RESOURCE_REQUEST_storage_14 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_14", "storage[14] (uint8)")
f.RESOURCE_REQUEST_storage_15 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_15", "storage[15] (uint8)")
f.RESOURCE_REQUEST_storage_16 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_16", "storage[16] (uint8)")
f.RESOURCE_REQUEST_storage_17 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_17", "storage[17] (uint8)")
f.RESOURCE_REQUEST_storage_18 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_18", "storage[18] (uint8)")
f.RESOURCE_REQUEST_storage_19 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_19", "storage[19] (uint8)")
f.RESOURCE_REQUEST_storage_20 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_20", "storage[20] (uint8)")
f.RESOURCE_REQUEST_storage_21 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_21", "storage[21] (uint8)")
f.RESOURCE_REQUEST_storage_22 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_22", "storage[22] (uint8)")
f.RESOURCE_REQUEST_storage_23 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_23", "storage[23] (uint8)")
f.RESOURCE_REQUEST_storage_24 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_24", "storage[24] (uint8)")
f.RESOURCE_REQUEST_storage_25 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_25", "storage[25] (uint8)")
f.RESOURCE_REQUEST_storage_26 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_26", "storage[26] (uint8)")
f.RESOURCE_REQUEST_storage_27 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_27", "storage[27] (uint8)")
f.RESOURCE_REQUEST_storage_28 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_28", "storage[28] (uint8)")
f.RESOURCE_REQUEST_storage_29 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_29", "storage[29] (uint8)")
f.RESOURCE_REQUEST_storage_30 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_30", "storage[30] (uint8)")
f.RESOURCE_REQUEST_storage_31 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_31", "storage[31] (uint8)")
f.RESOURCE_REQUEST_storage_32 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_32", "storage[32] (uint8)")
f.RESOURCE_REQUEST_storage_33 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_33", "storage[33] (uint8)")
f.RESOURCE_REQUEST_storage_34 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_34", "storage[34] (uint8)")
f.RESOURCE_REQUEST_storage_35 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_35", "storage[35] (uint8)")
f.RESOURCE_REQUEST_storage_36 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_36", "storage[36] (uint8)")
f.RESOURCE_REQUEST_storage_37 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_37", "storage[37] (uint8)")
f.RESOURCE_REQUEST_storage_38 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_38", "storage[38] (uint8)")
f.RESOURCE_REQUEST_storage_39 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_39", "storage[39] (uint8)")
f.RESOURCE_REQUEST_storage_40 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_40", "storage[40] (uint8)")
f.RESOURCE_REQUEST_storage_41 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_41", "storage[41] (uint8)")
f.RESOURCE_REQUEST_storage_42 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_42", "storage[42] (uint8)")
f.RESOURCE_REQUEST_storage_43 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_43", "storage[43] (uint8)")
f.RESOURCE_REQUEST_storage_44 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_44", "storage[44] (uint8)")
f.RESOURCE_REQUEST_storage_45 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_45", "storage[45] (uint8)")
f.RESOURCE_REQUEST_storage_46 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_46", "storage[46] (uint8)")
f.RESOURCE_REQUEST_storage_47 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_47", "storage[47] (uint8)")
f.RESOURCE_REQUEST_storage_48 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_48", "storage[48] (uint8)")
f.RESOURCE_REQUEST_storage_49 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_49", "storage[49] (uint8)")
f.RESOURCE_REQUEST_storage_50 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_50", "storage[50] (uint8)")
f.RESOURCE_REQUEST_storage_51 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_51", "storage[51] (uint8)")
f.RESOURCE_REQUEST_storage_52 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_52", "storage[52] (uint8)")
f.RESOURCE_REQUEST_storage_53 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_53", "storage[53] (uint8)")
f.RESOURCE_REQUEST_storage_54 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_54", "storage[54] (uint8)")
f.RESOURCE_REQUEST_storage_55 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_55", "storage[55] (uint8)")
f.RESOURCE_REQUEST_storage_56 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_56", "storage[56] (uint8)")
f.RESOURCE_REQUEST_storage_57 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_57", "storage[57] (uint8)")
f.RESOURCE_REQUEST_storage_58 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_58", "storage[58] (uint8)")
f.RESOURCE_REQUEST_storage_59 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_59", "storage[59] (uint8)")
f.RESOURCE_REQUEST_storage_60 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_60", "storage[60] (uint8)")
f.RESOURCE_REQUEST_storage_61 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_61", "storage[61] (uint8)")
f.RESOURCE_REQUEST_storage_62 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_62", "storage[62] (uint8)")
f.RESOURCE_REQUEST_storage_63 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_63", "storage[63] (uint8)")
f.RESOURCE_REQUEST_storage_64 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_64", "storage[64] (uint8)")
f.RESOURCE_REQUEST_storage_65 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_65", "storage[65] (uint8)")
f.RESOURCE_REQUEST_storage_66 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_66", "storage[66] (uint8)")
f.RESOURCE_REQUEST_storage_67 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_67", "storage[67] (uint8)")
f.RESOURCE_REQUEST_storage_68 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_68", "storage[68] (uint8)")
f.RESOURCE_REQUEST_storage_69 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_69", "storage[69] (uint8)")
f.RESOURCE_REQUEST_storage_70 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_70", "storage[70] (uint8)")
f.RESOURCE_REQUEST_storage_71 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_71", "storage[71] (uint8)")
f.RESOURCE_REQUEST_storage_72 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_72", "storage[72] (uint8)")
f.RESOURCE_REQUEST_storage_73 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_73", "storage[73] (uint8)")
f.RESOURCE_REQUEST_storage_74 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_74", "storage[74] (uint8)")
f.RESOURCE_REQUEST_storage_75 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_75", "storage[75] (uint8)")
f.RESOURCE_REQUEST_storage_76 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_76", "storage[76] (uint8)")
f.RESOURCE_REQUEST_storage_77 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_77", "storage[77] (uint8)")
f.RESOURCE_REQUEST_storage_78 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_78", "storage[78] (uint8)")
f.RESOURCE_REQUEST_storage_79 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_79", "storage[79] (uint8)")
f.RESOURCE_REQUEST_storage_80 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_80", "storage[80] (uint8)")
f.RESOURCE_REQUEST_storage_81 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_81", "storage[81] (uint8)")
f.RESOURCE_REQUEST_storage_82 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_82", "storage[82] (uint8)")
f.RESOURCE_REQUEST_storage_83 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_83", "storage[83] (uint8)")
f.RESOURCE_REQUEST_storage_84 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_84", "storage[84] (uint8)")
f.RESOURCE_REQUEST_storage_85 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_85", "storage[85] (uint8)")
f.RESOURCE_REQUEST_storage_86 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_86", "storage[86] (uint8)")
f.RESOURCE_REQUEST_storage_87 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_87", "storage[87] (uint8)")
f.RESOURCE_REQUEST_storage_88 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_88", "storage[88] (uint8)")
f.RESOURCE_REQUEST_storage_89 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_89", "storage[89] (uint8)")
f.RESOURCE_REQUEST_storage_90 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_90", "storage[90] (uint8)")
f.RESOURCE_REQUEST_storage_91 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_91", "storage[91] (uint8)")
f.RESOURCE_REQUEST_storage_92 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_92", "storage[92] (uint8)")
f.RESOURCE_REQUEST_storage_93 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_93", "storage[93] (uint8)")
f.RESOURCE_REQUEST_storage_94 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_94", "storage[94] (uint8)")
f.RESOURCE_REQUEST_storage_95 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_95", "storage[95] (uint8)")
f.RESOURCE_REQUEST_storage_96 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_96", "storage[96] (uint8)")
f.RESOURCE_REQUEST_storage_97 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_97", "storage[97] (uint8)")
f.RESOURCE_REQUEST_storage_98 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_98", "storage[98] (uint8)")
f.RESOURCE_REQUEST_storage_99 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_99", "storage[99] (uint8)")
f.RESOURCE_REQUEST_storage_100 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_100", "storage[100] (uint8)")
f.RESOURCE_REQUEST_storage_101 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_101", "storage[101] (uint8)")
f.RESOURCE_REQUEST_storage_102 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_102", "storage[102] (uint8)")
f.RESOURCE_REQUEST_storage_103 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_103", "storage[103] (uint8)")
f.RESOURCE_REQUEST_storage_104 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_104", "storage[104] (uint8)")
f.RESOURCE_REQUEST_storage_105 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_105", "storage[105] (uint8)")
f.RESOURCE_REQUEST_storage_106 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_106", "storage[106] (uint8)")
f.RESOURCE_REQUEST_storage_107 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_107", "storage[107] (uint8)")
f.RESOURCE_REQUEST_storage_108 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_108", "storage[108] (uint8)")
f.RESOURCE_REQUEST_storage_109 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_109", "storage[109] (uint8)")
f.RESOURCE_REQUEST_storage_110 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_110", "storage[110] (uint8)")
f.RESOURCE_REQUEST_storage_111 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_111", "storage[111] (uint8)")
f.RESOURCE_REQUEST_storage_112 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_112", "storage[112] (uint8)")
f.RESOURCE_REQUEST_storage_113 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_113", "storage[113] (uint8)")
f.RESOURCE_REQUEST_storage_114 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_114", "storage[114] (uint8)")
f.RESOURCE_REQUEST_storage_115 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_115", "storage[115] (uint8)")
f.RESOURCE_REQUEST_storage_116 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_116", "storage[116] (uint8)")
f.RESOURCE_REQUEST_storage_117 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_117", "storage[117] (uint8)")
f.RESOURCE_REQUEST_storage_118 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_118", "storage[118] (uint8)")
f.RESOURCE_REQUEST_storage_119 = ProtoField.uint8("mavlink_proto.RESOURCE_REQUEST_storage_119", "storage[119] (uint8)")

f.SCALED_PRESSURE3_time_boot_ms = ProtoField.uint32("mavlink_proto.SCALED_PRESSURE3_time_boot_ms", "time_boot_ms (uint32)")
f.SCALED_PRESSURE3_press_abs = ProtoField.float("mavlink_proto.SCALED_PRESSURE3_press_abs", "press_abs (float)")
f.SCALED_PRESSURE3_press_diff = ProtoField.float("mavlink_proto.SCALED_PRESSURE3_press_diff", "press_diff (float)")
f.SCALED_PRESSURE3_temperature = ProtoField.int16("mavlink_proto.SCALED_PRESSURE3_temperature", "temperature (int16)")

f.FOLLOW_TARGET_timestamp = ProtoField.uint64("mavlink_proto.FOLLOW_TARGET_timestamp", "timestamp (uint64)")
f.FOLLOW_TARGET_est_capabilities = ProtoField.uint8("mavlink_proto.FOLLOW_TARGET_est_capabilities", "est_capabilities (uint8)")
f.FOLLOW_TARGET_lat = ProtoField.int32("mavlink_proto.FOLLOW_TARGET_lat", "lat (int32)")
f.FOLLOW_TARGET_lon = ProtoField.int32("mavlink_proto.FOLLOW_TARGET_lon", "lon (int32)")
f.FOLLOW_TARGET_alt = ProtoField.float("mavlink_proto.FOLLOW_TARGET_alt", "alt (float)")
f.FOLLOW_TARGET_vel_0 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_vel_0", "vel[0] (float)")
f.FOLLOW_TARGET_vel_1 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_vel_1", "vel[1] (float)")
f.FOLLOW_TARGET_vel_2 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_vel_2", "vel[2] (float)")
f.FOLLOW_TARGET_acc_0 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_acc_0", "acc[0] (float)")
f.FOLLOW_TARGET_acc_1 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_acc_1", "acc[1] (float)")
f.FOLLOW_TARGET_acc_2 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_acc_2", "acc[2] (float)")
f.FOLLOW_TARGET_attitude_q_0 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_attitude_q_0", "attitude_q[0] (float)")
f.FOLLOW_TARGET_attitude_q_1 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_attitude_q_1", "attitude_q[1] (float)")
f.FOLLOW_TARGET_attitude_q_2 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_attitude_q_2", "attitude_q[2] (float)")
f.FOLLOW_TARGET_attitude_q_3 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_attitude_q_3", "attitude_q[3] (float)")
f.FOLLOW_TARGET_rates_0 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_rates_0", "rates[0] (float)")
f.FOLLOW_TARGET_rates_1 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_rates_1", "rates[1] (float)")
f.FOLLOW_TARGET_rates_2 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_rates_2", "rates[2] (float)")
f.FOLLOW_TARGET_position_cov_0 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_position_cov_0", "position_cov[0] (float)")
f.FOLLOW_TARGET_position_cov_1 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_position_cov_1", "position_cov[1] (float)")
f.FOLLOW_TARGET_position_cov_2 = ProtoField.float("mavlink_proto.FOLLOW_TARGET_position_cov_2", "position_cov[2] (float)")
f.FOLLOW_TARGET_custom_state = ProtoField.uint64("mavlink_proto.FOLLOW_TARGET_custom_state", "custom_state (uint64)")

f.CONTROL_SYSTEM_STATE_time_usec = ProtoField.uint64("mavlink_proto.CONTROL_SYSTEM_STATE_time_usec", "time_usec (uint64)")
f.CONTROL_SYSTEM_STATE_x_acc = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_x_acc", "x_acc (float)")
f.CONTROL_SYSTEM_STATE_y_acc = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_y_acc", "y_acc (float)")
f.CONTROL_SYSTEM_STATE_z_acc = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_z_acc", "z_acc (float)")
f.CONTROL_SYSTEM_STATE_x_vel = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_x_vel", "x_vel (float)")
f.CONTROL_SYSTEM_STATE_y_vel = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_y_vel", "y_vel (float)")
f.CONTROL_SYSTEM_STATE_z_vel = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_z_vel", "z_vel (float)")
f.CONTROL_SYSTEM_STATE_x_pos = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_x_pos", "x_pos (float)")
f.CONTROL_SYSTEM_STATE_y_pos = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_y_pos", "y_pos (float)")
f.CONTROL_SYSTEM_STATE_z_pos = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_z_pos", "z_pos (float)")
f.CONTROL_SYSTEM_STATE_airspeed = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_airspeed", "airspeed (float)")
f.CONTROL_SYSTEM_STATE_vel_variance_0 = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_vel_variance_0", "vel_variance[0] (float)")
f.CONTROL_SYSTEM_STATE_vel_variance_1 = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_vel_variance_1", "vel_variance[1] (float)")
f.CONTROL_SYSTEM_STATE_vel_variance_2 = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_vel_variance_2", "vel_variance[2] (float)")
f.CONTROL_SYSTEM_STATE_pos_variance_0 = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_pos_variance_0", "pos_variance[0] (float)")
f.CONTROL_SYSTEM_STATE_pos_variance_1 = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_pos_variance_1", "pos_variance[1] (float)")
f.CONTROL_SYSTEM_STATE_pos_variance_2 = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_pos_variance_2", "pos_variance[2] (float)")
f.CONTROL_SYSTEM_STATE_q_0 = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_q_0", "q[0] (float)")
f.CONTROL_SYSTEM_STATE_q_1 = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_q_1", "q[1] (float)")
f.CONTROL_SYSTEM_STATE_q_2 = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_q_2", "q[2] (float)")
f.CONTROL_SYSTEM_STATE_q_3 = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_q_3", "q[3] (float)")
f.CONTROL_SYSTEM_STATE_roll_rate = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_roll_rate", "roll_rate (float)")
f.CONTROL_SYSTEM_STATE_pitch_rate = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_pitch_rate", "pitch_rate (float)")
f.CONTROL_SYSTEM_STATE_yaw_rate = ProtoField.float("mavlink_proto.CONTROL_SYSTEM_STATE_yaw_rate", "yaw_rate (float)")

f.BATTERY_STATUS_id = ProtoField.uint8("mavlink_proto.BATTERY_STATUS_id", "id (uint8)")
f.BATTERY_STATUS_battery_function = ProtoField.uint8("mavlink_proto.BATTERY_STATUS_battery_function", "battery_function (uint8)")
f.BATTERY_STATUS_type = ProtoField.uint8("mavlink_proto.BATTERY_STATUS_type", "type (uint8)")
f.BATTERY_STATUS_temperature = ProtoField.int16("mavlink_proto.BATTERY_STATUS_temperature", "temperature (int16)")
f.BATTERY_STATUS_voltages_0 = ProtoField.uint16("mavlink_proto.BATTERY_STATUS_voltages_0", "voltages[0] (uint16)")
f.BATTERY_STATUS_voltages_1 = ProtoField.uint16("mavlink_proto.BATTERY_STATUS_voltages_1", "voltages[1] (uint16)")
f.BATTERY_STATUS_voltages_2 = ProtoField.uint16("mavlink_proto.BATTERY_STATUS_voltages_2", "voltages[2] (uint16)")
f.BATTERY_STATUS_voltages_3 = ProtoField.uint16("mavlink_proto.BATTERY_STATUS_voltages_3", "voltages[3] (uint16)")
f.BATTERY_STATUS_voltages_4 = ProtoField.uint16("mavlink_proto.BATTERY_STATUS_voltages_4", "voltages[4] (uint16)")
f.BATTERY_STATUS_voltages_5 = ProtoField.uint16("mavlink_proto.BATTERY_STATUS_voltages_5", "voltages[5] (uint16)")
f.BATTERY_STATUS_voltages_6 = ProtoField.uint16("mavlink_proto.BATTERY_STATUS_voltages_6", "voltages[6] (uint16)")
f.BATTERY_STATUS_voltages_7 = ProtoField.uint16("mavlink_proto.BATTERY_STATUS_voltages_7", "voltages[7] (uint16)")
f.BATTERY_STATUS_voltages_8 = ProtoField.uint16("mavlink_proto.BATTERY_STATUS_voltages_8", "voltages[8] (uint16)")
f.BATTERY_STATUS_voltages_9 = ProtoField.uint16("mavlink_proto.BATTERY_STATUS_voltages_9", "voltages[9] (uint16)")
f.BATTERY_STATUS_current_battery = ProtoField.int16("mavlink_proto.BATTERY_STATUS_current_battery", "current_battery (int16)")
f.BATTERY_STATUS_current_consumed = ProtoField.int32("mavlink_proto.BATTERY_STATUS_current_consumed", "current_consumed (int32)")
f.BATTERY_STATUS_energy_consumed = ProtoField.int32("mavlink_proto.BATTERY_STATUS_energy_consumed", "energy_consumed (int32)")
f.BATTERY_STATUS_battery_remaining = ProtoField.int8("mavlink_proto.BATTERY_STATUS_battery_remaining", "battery_remaining (int8)")

f.AUTOPILOT_VERSION_capabilities = ProtoField.uint64("mavlink_proto.AUTOPILOT_VERSION_capabilities", "capabilities (uint64)")
f.AUTOPILOT_VERSION_flight_sw_version = ProtoField.uint32("mavlink_proto.AUTOPILOT_VERSION_flight_sw_version", "flight_sw_version (uint32)")
f.AUTOPILOT_VERSION_middleware_sw_version = ProtoField.uint32("mavlink_proto.AUTOPILOT_VERSION_middleware_sw_version", "middleware_sw_version (uint32)")
f.AUTOPILOT_VERSION_os_sw_version = ProtoField.uint32("mavlink_proto.AUTOPILOT_VERSION_os_sw_version", "os_sw_version (uint32)")
f.AUTOPILOT_VERSION_board_version = ProtoField.uint32("mavlink_proto.AUTOPILOT_VERSION_board_version", "board_version (uint32)")
f.AUTOPILOT_VERSION_flight_custom_version_0 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_flight_custom_version_0", "flight_custom_version[0] (uint8)")
f.AUTOPILOT_VERSION_flight_custom_version_1 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_flight_custom_version_1", "flight_custom_version[1] (uint8)")
f.AUTOPILOT_VERSION_flight_custom_version_2 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_flight_custom_version_2", "flight_custom_version[2] (uint8)")
f.AUTOPILOT_VERSION_flight_custom_version_3 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_flight_custom_version_3", "flight_custom_version[3] (uint8)")
f.AUTOPILOT_VERSION_flight_custom_version_4 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_flight_custom_version_4", "flight_custom_version[4] (uint8)")
f.AUTOPILOT_VERSION_flight_custom_version_5 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_flight_custom_version_5", "flight_custom_version[5] (uint8)")
f.AUTOPILOT_VERSION_flight_custom_version_6 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_flight_custom_version_6", "flight_custom_version[6] (uint8)")
f.AUTOPILOT_VERSION_flight_custom_version_7 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_flight_custom_version_7", "flight_custom_version[7] (uint8)")
f.AUTOPILOT_VERSION_middleware_custom_version_0 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_middleware_custom_version_0", "middleware_custom_version[0] (uint8)")
f.AUTOPILOT_VERSION_middleware_custom_version_1 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_middleware_custom_version_1", "middleware_custom_version[1] (uint8)")
f.AUTOPILOT_VERSION_middleware_custom_version_2 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_middleware_custom_version_2", "middleware_custom_version[2] (uint8)")
f.AUTOPILOT_VERSION_middleware_custom_version_3 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_middleware_custom_version_3", "middleware_custom_version[3] (uint8)")
f.AUTOPILOT_VERSION_middleware_custom_version_4 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_middleware_custom_version_4", "middleware_custom_version[4] (uint8)")
f.AUTOPILOT_VERSION_middleware_custom_version_5 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_middleware_custom_version_5", "middleware_custom_version[5] (uint8)")
f.AUTOPILOT_VERSION_middleware_custom_version_6 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_middleware_custom_version_6", "middleware_custom_version[6] (uint8)")
f.AUTOPILOT_VERSION_middleware_custom_version_7 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_middleware_custom_version_7", "middleware_custom_version[7] (uint8)")
f.AUTOPILOT_VERSION_os_custom_version_0 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_os_custom_version_0", "os_custom_version[0] (uint8)")
f.AUTOPILOT_VERSION_os_custom_version_1 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_os_custom_version_1", "os_custom_version[1] (uint8)")
f.AUTOPILOT_VERSION_os_custom_version_2 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_os_custom_version_2", "os_custom_version[2] (uint8)")
f.AUTOPILOT_VERSION_os_custom_version_3 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_os_custom_version_3", "os_custom_version[3] (uint8)")
f.AUTOPILOT_VERSION_os_custom_version_4 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_os_custom_version_4", "os_custom_version[4] (uint8)")
f.AUTOPILOT_VERSION_os_custom_version_5 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_os_custom_version_5", "os_custom_version[5] (uint8)")
f.AUTOPILOT_VERSION_os_custom_version_6 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_os_custom_version_6", "os_custom_version[6] (uint8)")
f.AUTOPILOT_VERSION_os_custom_version_7 = ProtoField.uint8("mavlink_proto.AUTOPILOT_VERSION_os_custom_version_7", "os_custom_version[7] (uint8)")
f.AUTOPILOT_VERSION_vendor_id = ProtoField.uint16("mavlink_proto.AUTOPILOT_VERSION_vendor_id", "vendor_id (uint16)")
f.AUTOPILOT_VERSION_product_id = ProtoField.uint16("mavlink_proto.AUTOPILOT_VERSION_product_id", "product_id (uint16)")
f.AUTOPILOT_VERSION_uid = ProtoField.uint64("mavlink_proto.AUTOPILOT_VERSION_uid", "uid (uint64)")

f.LANDING_TARGET_time_usec = ProtoField.uint64("mavlink_proto.LANDING_TARGET_time_usec", "time_usec (uint64)")
f.LANDING_TARGET_target_num = ProtoField.uint8("mavlink_proto.LANDING_TARGET_target_num", "target_num (uint8)")
f.LANDING_TARGET_frame = ProtoField.uint8("mavlink_proto.LANDING_TARGET_frame", "frame (uint8)")
f.LANDING_TARGET_angle_x = ProtoField.float("mavlink_proto.LANDING_TARGET_angle_x", "angle_x (float)")
f.LANDING_TARGET_angle_y = ProtoField.float("mavlink_proto.LANDING_TARGET_angle_y", "angle_y (float)")
f.LANDING_TARGET_distance = ProtoField.float("mavlink_proto.LANDING_TARGET_distance", "distance (float)")
f.LANDING_TARGET_size_x = ProtoField.float("mavlink_proto.LANDING_TARGET_size_x", "size_x (float)")
f.LANDING_TARGET_size_y = ProtoField.float("mavlink_proto.LANDING_TARGET_size_y", "size_y (float)")

f.ESTIMATOR_STATUS_time_usec = ProtoField.uint64("mavlink_proto.ESTIMATOR_STATUS_time_usec", "time_usec (uint64)")
f.ESTIMATOR_STATUS_flags = ProtoField.uint16("mavlink_proto.ESTIMATOR_STATUS_flags", "flags (uint16)")
f.ESTIMATOR_STATUS_vel_ratio = ProtoField.float("mavlink_proto.ESTIMATOR_STATUS_vel_ratio", "vel_ratio (float)")
f.ESTIMATOR_STATUS_pos_horiz_ratio = ProtoField.float("mavlink_proto.ESTIMATOR_STATUS_pos_horiz_ratio", "pos_horiz_ratio (float)")
f.ESTIMATOR_STATUS_pos_vert_ratio = ProtoField.float("mavlink_proto.ESTIMATOR_STATUS_pos_vert_ratio", "pos_vert_ratio (float)")
f.ESTIMATOR_STATUS_mag_ratio = ProtoField.float("mavlink_proto.ESTIMATOR_STATUS_mag_ratio", "mag_ratio (float)")
f.ESTIMATOR_STATUS_hagl_ratio = ProtoField.float("mavlink_proto.ESTIMATOR_STATUS_hagl_ratio", "hagl_ratio (float)")
f.ESTIMATOR_STATUS_tas_ratio = ProtoField.float("mavlink_proto.ESTIMATOR_STATUS_tas_ratio", "tas_ratio (float)")
f.ESTIMATOR_STATUS_pos_horiz_accuracy = ProtoField.float("mavlink_proto.ESTIMATOR_STATUS_pos_horiz_accuracy", "pos_horiz_accuracy (float)")
f.ESTIMATOR_STATUS_pos_vert_accuracy = ProtoField.float("mavlink_proto.ESTIMATOR_STATUS_pos_vert_accuracy", "pos_vert_accuracy (float)")

f.WIND_COV_time_usec = ProtoField.uint64("mavlink_proto.WIND_COV_time_usec", "time_usec (uint64)")
f.WIND_COV_wind_x = ProtoField.float("mavlink_proto.WIND_COV_wind_x", "wind_x (float)")
f.WIND_COV_wind_y = ProtoField.float("mavlink_proto.WIND_COV_wind_y", "wind_y (float)")
f.WIND_COV_wind_z = ProtoField.float("mavlink_proto.WIND_COV_wind_z", "wind_z (float)")
f.WIND_COV_var_horiz = ProtoField.float("mavlink_proto.WIND_COV_var_horiz", "var_horiz (float)")
f.WIND_COV_var_vert = ProtoField.float("mavlink_proto.WIND_COV_var_vert", "var_vert (float)")
f.WIND_COV_wind_alt = ProtoField.float("mavlink_proto.WIND_COV_wind_alt", "wind_alt (float)")
f.WIND_COV_horiz_accuracy = ProtoField.float("mavlink_proto.WIND_COV_horiz_accuracy", "horiz_accuracy (float)")
f.WIND_COV_vert_accuracy = ProtoField.float("mavlink_proto.WIND_COV_vert_accuracy", "vert_accuracy (float)")

f.GPS_INPUT_time_usec = ProtoField.uint64("mavlink_proto.GPS_INPUT_time_usec", "time_usec (uint64)")
f.GPS_INPUT_gps_id = ProtoField.uint8("mavlink_proto.GPS_INPUT_gps_id", "gps_id (uint8)")
f.GPS_INPUT_ignore_flags = ProtoField.uint16("mavlink_proto.GPS_INPUT_ignore_flags", "ignore_flags (uint16)")
f.GPS_INPUT_time_week_ms = ProtoField.uint32("mavlink_proto.GPS_INPUT_time_week_ms", "time_week_ms (uint32)")
f.GPS_INPUT_time_week = ProtoField.uint16("mavlink_proto.GPS_INPUT_time_week", "time_week (uint16)")
f.GPS_INPUT_fix_type = ProtoField.uint8("mavlink_proto.GPS_INPUT_fix_type", "fix_type (uint8)")
f.GPS_INPUT_lat = ProtoField.int32("mavlink_proto.GPS_INPUT_lat", "lat (int32)")
f.GPS_INPUT_lon = ProtoField.int32("mavlink_proto.GPS_INPUT_lon", "lon (int32)")
f.GPS_INPUT_alt = ProtoField.float("mavlink_proto.GPS_INPUT_alt", "alt (float)")
f.GPS_INPUT_hdop = ProtoField.float("mavlink_proto.GPS_INPUT_hdop", "hdop (float)")
f.GPS_INPUT_vdop = ProtoField.float("mavlink_proto.GPS_INPUT_vdop", "vdop (float)")
f.GPS_INPUT_vn = ProtoField.float("mavlink_proto.GPS_INPUT_vn", "vn (float)")
f.GPS_INPUT_ve = ProtoField.float("mavlink_proto.GPS_INPUT_ve", "ve (float)")
f.GPS_INPUT_vd = ProtoField.float("mavlink_proto.GPS_INPUT_vd", "vd (float)")
f.GPS_INPUT_speed_accuracy = ProtoField.float("mavlink_proto.GPS_INPUT_speed_accuracy", "speed_accuracy (float)")
f.GPS_INPUT_horiz_accuracy = ProtoField.float("mavlink_proto.GPS_INPUT_horiz_accuracy", "horiz_accuracy (float)")
f.GPS_INPUT_vert_accuracy = ProtoField.float("mavlink_proto.GPS_INPUT_vert_accuracy", "vert_accuracy (float)")
f.GPS_INPUT_satellites_visible = ProtoField.uint8("mavlink_proto.GPS_INPUT_satellites_visible", "satellites_visible (uint8)")

f.GPS_RTCM_DATA_flags = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_flags", "flags (uint8)")
f.GPS_RTCM_DATA_len = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_len", "len (uint8)")
f.GPS_RTCM_DATA_data_0 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_0", "data[0] (uint8)")
f.GPS_RTCM_DATA_data_1 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_1", "data[1] (uint8)")
f.GPS_RTCM_DATA_data_2 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_2", "data[2] (uint8)")
f.GPS_RTCM_DATA_data_3 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_3", "data[3] (uint8)")
f.GPS_RTCM_DATA_data_4 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_4", "data[4] (uint8)")
f.GPS_RTCM_DATA_data_5 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_5", "data[5] (uint8)")
f.GPS_RTCM_DATA_data_6 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_6", "data[6] (uint8)")
f.GPS_RTCM_DATA_data_7 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_7", "data[7] (uint8)")
f.GPS_RTCM_DATA_data_8 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_8", "data[8] (uint8)")
f.GPS_RTCM_DATA_data_9 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_9", "data[9] (uint8)")
f.GPS_RTCM_DATA_data_10 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_10", "data[10] (uint8)")
f.GPS_RTCM_DATA_data_11 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_11", "data[11] (uint8)")
f.GPS_RTCM_DATA_data_12 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_12", "data[12] (uint8)")
f.GPS_RTCM_DATA_data_13 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_13", "data[13] (uint8)")
f.GPS_RTCM_DATA_data_14 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_14", "data[14] (uint8)")
f.GPS_RTCM_DATA_data_15 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_15", "data[15] (uint8)")
f.GPS_RTCM_DATA_data_16 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_16", "data[16] (uint8)")
f.GPS_RTCM_DATA_data_17 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_17", "data[17] (uint8)")
f.GPS_RTCM_DATA_data_18 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_18", "data[18] (uint8)")
f.GPS_RTCM_DATA_data_19 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_19", "data[19] (uint8)")
f.GPS_RTCM_DATA_data_20 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_20", "data[20] (uint8)")
f.GPS_RTCM_DATA_data_21 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_21", "data[21] (uint8)")
f.GPS_RTCM_DATA_data_22 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_22", "data[22] (uint8)")
f.GPS_RTCM_DATA_data_23 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_23", "data[23] (uint8)")
f.GPS_RTCM_DATA_data_24 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_24", "data[24] (uint8)")
f.GPS_RTCM_DATA_data_25 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_25", "data[25] (uint8)")
f.GPS_RTCM_DATA_data_26 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_26", "data[26] (uint8)")
f.GPS_RTCM_DATA_data_27 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_27", "data[27] (uint8)")
f.GPS_RTCM_DATA_data_28 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_28", "data[28] (uint8)")
f.GPS_RTCM_DATA_data_29 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_29", "data[29] (uint8)")
f.GPS_RTCM_DATA_data_30 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_30", "data[30] (uint8)")
f.GPS_RTCM_DATA_data_31 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_31", "data[31] (uint8)")
f.GPS_RTCM_DATA_data_32 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_32", "data[32] (uint8)")
f.GPS_RTCM_DATA_data_33 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_33", "data[33] (uint8)")
f.GPS_RTCM_DATA_data_34 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_34", "data[34] (uint8)")
f.GPS_RTCM_DATA_data_35 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_35", "data[35] (uint8)")
f.GPS_RTCM_DATA_data_36 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_36", "data[36] (uint8)")
f.GPS_RTCM_DATA_data_37 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_37", "data[37] (uint8)")
f.GPS_RTCM_DATA_data_38 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_38", "data[38] (uint8)")
f.GPS_RTCM_DATA_data_39 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_39", "data[39] (uint8)")
f.GPS_RTCM_DATA_data_40 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_40", "data[40] (uint8)")
f.GPS_RTCM_DATA_data_41 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_41", "data[41] (uint8)")
f.GPS_RTCM_DATA_data_42 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_42", "data[42] (uint8)")
f.GPS_RTCM_DATA_data_43 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_43", "data[43] (uint8)")
f.GPS_RTCM_DATA_data_44 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_44", "data[44] (uint8)")
f.GPS_RTCM_DATA_data_45 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_45", "data[45] (uint8)")
f.GPS_RTCM_DATA_data_46 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_46", "data[46] (uint8)")
f.GPS_RTCM_DATA_data_47 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_47", "data[47] (uint8)")
f.GPS_RTCM_DATA_data_48 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_48", "data[48] (uint8)")
f.GPS_RTCM_DATA_data_49 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_49", "data[49] (uint8)")
f.GPS_RTCM_DATA_data_50 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_50", "data[50] (uint8)")
f.GPS_RTCM_DATA_data_51 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_51", "data[51] (uint8)")
f.GPS_RTCM_DATA_data_52 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_52", "data[52] (uint8)")
f.GPS_RTCM_DATA_data_53 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_53", "data[53] (uint8)")
f.GPS_RTCM_DATA_data_54 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_54", "data[54] (uint8)")
f.GPS_RTCM_DATA_data_55 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_55", "data[55] (uint8)")
f.GPS_RTCM_DATA_data_56 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_56", "data[56] (uint8)")
f.GPS_RTCM_DATA_data_57 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_57", "data[57] (uint8)")
f.GPS_RTCM_DATA_data_58 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_58", "data[58] (uint8)")
f.GPS_RTCM_DATA_data_59 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_59", "data[59] (uint8)")
f.GPS_RTCM_DATA_data_60 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_60", "data[60] (uint8)")
f.GPS_RTCM_DATA_data_61 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_61", "data[61] (uint8)")
f.GPS_RTCM_DATA_data_62 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_62", "data[62] (uint8)")
f.GPS_RTCM_DATA_data_63 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_63", "data[63] (uint8)")
f.GPS_RTCM_DATA_data_64 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_64", "data[64] (uint8)")
f.GPS_RTCM_DATA_data_65 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_65", "data[65] (uint8)")
f.GPS_RTCM_DATA_data_66 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_66", "data[66] (uint8)")
f.GPS_RTCM_DATA_data_67 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_67", "data[67] (uint8)")
f.GPS_RTCM_DATA_data_68 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_68", "data[68] (uint8)")
f.GPS_RTCM_DATA_data_69 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_69", "data[69] (uint8)")
f.GPS_RTCM_DATA_data_70 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_70", "data[70] (uint8)")
f.GPS_RTCM_DATA_data_71 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_71", "data[71] (uint8)")
f.GPS_RTCM_DATA_data_72 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_72", "data[72] (uint8)")
f.GPS_RTCM_DATA_data_73 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_73", "data[73] (uint8)")
f.GPS_RTCM_DATA_data_74 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_74", "data[74] (uint8)")
f.GPS_RTCM_DATA_data_75 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_75", "data[75] (uint8)")
f.GPS_RTCM_DATA_data_76 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_76", "data[76] (uint8)")
f.GPS_RTCM_DATA_data_77 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_77", "data[77] (uint8)")
f.GPS_RTCM_DATA_data_78 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_78", "data[78] (uint8)")
f.GPS_RTCM_DATA_data_79 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_79", "data[79] (uint8)")
f.GPS_RTCM_DATA_data_80 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_80", "data[80] (uint8)")
f.GPS_RTCM_DATA_data_81 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_81", "data[81] (uint8)")
f.GPS_RTCM_DATA_data_82 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_82", "data[82] (uint8)")
f.GPS_RTCM_DATA_data_83 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_83", "data[83] (uint8)")
f.GPS_RTCM_DATA_data_84 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_84", "data[84] (uint8)")
f.GPS_RTCM_DATA_data_85 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_85", "data[85] (uint8)")
f.GPS_RTCM_DATA_data_86 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_86", "data[86] (uint8)")
f.GPS_RTCM_DATA_data_87 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_87", "data[87] (uint8)")
f.GPS_RTCM_DATA_data_88 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_88", "data[88] (uint8)")
f.GPS_RTCM_DATA_data_89 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_89", "data[89] (uint8)")
f.GPS_RTCM_DATA_data_90 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_90", "data[90] (uint8)")
f.GPS_RTCM_DATA_data_91 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_91", "data[91] (uint8)")
f.GPS_RTCM_DATA_data_92 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_92", "data[92] (uint8)")
f.GPS_RTCM_DATA_data_93 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_93", "data[93] (uint8)")
f.GPS_RTCM_DATA_data_94 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_94", "data[94] (uint8)")
f.GPS_RTCM_DATA_data_95 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_95", "data[95] (uint8)")
f.GPS_RTCM_DATA_data_96 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_96", "data[96] (uint8)")
f.GPS_RTCM_DATA_data_97 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_97", "data[97] (uint8)")
f.GPS_RTCM_DATA_data_98 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_98", "data[98] (uint8)")
f.GPS_RTCM_DATA_data_99 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_99", "data[99] (uint8)")
f.GPS_RTCM_DATA_data_100 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_100", "data[100] (uint8)")
f.GPS_RTCM_DATA_data_101 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_101", "data[101] (uint8)")
f.GPS_RTCM_DATA_data_102 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_102", "data[102] (uint8)")
f.GPS_RTCM_DATA_data_103 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_103", "data[103] (uint8)")
f.GPS_RTCM_DATA_data_104 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_104", "data[104] (uint8)")
f.GPS_RTCM_DATA_data_105 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_105", "data[105] (uint8)")
f.GPS_RTCM_DATA_data_106 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_106", "data[106] (uint8)")
f.GPS_RTCM_DATA_data_107 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_107", "data[107] (uint8)")
f.GPS_RTCM_DATA_data_108 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_108", "data[108] (uint8)")
f.GPS_RTCM_DATA_data_109 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_109", "data[109] (uint8)")
f.GPS_RTCM_DATA_data_110 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_110", "data[110] (uint8)")
f.GPS_RTCM_DATA_data_111 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_111", "data[111] (uint8)")
f.GPS_RTCM_DATA_data_112 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_112", "data[112] (uint8)")
f.GPS_RTCM_DATA_data_113 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_113", "data[113] (uint8)")
f.GPS_RTCM_DATA_data_114 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_114", "data[114] (uint8)")
f.GPS_RTCM_DATA_data_115 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_115", "data[115] (uint8)")
f.GPS_RTCM_DATA_data_116 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_116", "data[116] (uint8)")
f.GPS_RTCM_DATA_data_117 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_117", "data[117] (uint8)")
f.GPS_RTCM_DATA_data_118 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_118", "data[118] (uint8)")
f.GPS_RTCM_DATA_data_119 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_119", "data[119] (uint8)")
f.GPS_RTCM_DATA_data_120 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_120", "data[120] (uint8)")
f.GPS_RTCM_DATA_data_121 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_121", "data[121] (uint8)")
f.GPS_RTCM_DATA_data_122 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_122", "data[122] (uint8)")
f.GPS_RTCM_DATA_data_123 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_123", "data[123] (uint8)")
f.GPS_RTCM_DATA_data_124 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_124", "data[124] (uint8)")
f.GPS_RTCM_DATA_data_125 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_125", "data[125] (uint8)")
f.GPS_RTCM_DATA_data_126 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_126", "data[126] (uint8)")
f.GPS_RTCM_DATA_data_127 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_127", "data[127] (uint8)")
f.GPS_RTCM_DATA_data_128 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_128", "data[128] (uint8)")
f.GPS_RTCM_DATA_data_129 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_129", "data[129] (uint8)")
f.GPS_RTCM_DATA_data_130 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_130", "data[130] (uint8)")
f.GPS_RTCM_DATA_data_131 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_131", "data[131] (uint8)")
f.GPS_RTCM_DATA_data_132 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_132", "data[132] (uint8)")
f.GPS_RTCM_DATA_data_133 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_133", "data[133] (uint8)")
f.GPS_RTCM_DATA_data_134 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_134", "data[134] (uint8)")
f.GPS_RTCM_DATA_data_135 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_135", "data[135] (uint8)")
f.GPS_RTCM_DATA_data_136 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_136", "data[136] (uint8)")
f.GPS_RTCM_DATA_data_137 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_137", "data[137] (uint8)")
f.GPS_RTCM_DATA_data_138 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_138", "data[138] (uint8)")
f.GPS_RTCM_DATA_data_139 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_139", "data[139] (uint8)")
f.GPS_RTCM_DATA_data_140 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_140", "data[140] (uint8)")
f.GPS_RTCM_DATA_data_141 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_141", "data[141] (uint8)")
f.GPS_RTCM_DATA_data_142 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_142", "data[142] (uint8)")
f.GPS_RTCM_DATA_data_143 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_143", "data[143] (uint8)")
f.GPS_RTCM_DATA_data_144 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_144", "data[144] (uint8)")
f.GPS_RTCM_DATA_data_145 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_145", "data[145] (uint8)")
f.GPS_RTCM_DATA_data_146 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_146", "data[146] (uint8)")
f.GPS_RTCM_DATA_data_147 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_147", "data[147] (uint8)")
f.GPS_RTCM_DATA_data_148 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_148", "data[148] (uint8)")
f.GPS_RTCM_DATA_data_149 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_149", "data[149] (uint8)")
f.GPS_RTCM_DATA_data_150 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_150", "data[150] (uint8)")
f.GPS_RTCM_DATA_data_151 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_151", "data[151] (uint8)")
f.GPS_RTCM_DATA_data_152 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_152", "data[152] (uint8)")
f.GPS_RTCM_DATA_data_153 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_153", "data[153] (uint8)")
f.GPS_RTCM_DATA_data_154 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_154", "data[154] (uint8)")
f.GPS_RTCM_DATA_data_155 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_155", "data[155] (uint8)")
f.GPS_RTCM_DATA_data_156 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_156", "data[156] (uint8)")
f.GPS_RTCM_DATA_data_157 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_157", "data[157] (uint8)")
f.GPS_RTCM_DATA_data_158 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_158", "data[158] (uint8)")
f.GPS_RTCM_DATA_data_159 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_159", "data[159] (uint8)")
f.GPS_RTCM_DATA_data_160 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_160", "data[160] (uint8)")
f.GPS_RTCM_DATA_data_161 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_161", "data[161] (uint8)")
f.GPS_RTCM_DATA_data_162 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_162", "data[162] (uint8)")
f.GPS_RTCM_DATA_data_163 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_163", "data[163] (uint8)")
f.GPS_RTCM_DATA_data_164 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_164", "data[164] (uint8)")
f.GPS_RTCM_DATA_data_165 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_165", "data[165] (uint8)")
f.GPS_RTCM_DATA_data_166 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_166", "data[166] (uint8)")
f.GPS_RTCM_DATA_data_167 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_167", "data[167] (uint8)")
f.GPS_RTCM_DATA_data_168 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_168", "data[168] (uint8)")
f.GPS_RTCM_DATA_data_169 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_169", "data[169] (uint8)")
f.GPS_RTCM_DATA_data_170 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_170", "data[170] (uint8)")
f.GPS_RTCM_DATA_data_171 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_171", "data[171] (uint8)")
f.GPS_RTCM_DATA_data_172 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_172", "data[172] (uint8)")
f.GPS_RTCM_DATA_data_173 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_173", "data[173] (uint8)")
f.GPS_RTCM_DATA_data_174 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_174", "data[174] (uint8)")
f.GPS_RTCM_DATA_data_175 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_175", "data[175] (uint8)")
f.GPS_RTCM_DATA_data_176 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_176", "data[176] (uint8)")
f.GPS_RTCM_DATA_data_177 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_177", "data[177] (uint8)")
f.GPS_RTCM_DATA_data_178 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_178", "data[178] (uint8)")
f.GPS_RTCM_DATA_data_179 = ProtoField.uint8("mavlink_proto.GPS_RTCM_DATA_data_179", "data[179] (uint8)")

f.HIGH_LATENCY_base_mode = ProtoField.uint8("mavlink_proto.HIGH_LATENCY_base_mode", "base_mode (uint8)")
f.HIGH_LATENCY_custom_mode = ProtoField.uint32("mavlink_proto.HIGH_LATENCY_custom_mode", "custom_mode (uint32)")
f.HIGH_LATENCY_landed_state = ProtoField.uint8("mavlink_proto.HIGH_LATENCY_landed_state", "landed_state (uint8)")
f.HIGH_LATENCY_roll = ProtoField.int16("mavlink_proto.HIGH_LATENCY_roll", "roll (int16)")
f.HIGH_LATENCY_pitch = ProtoField.int16("mavlink_proto.HIGH_LATENCY_pitch", "pitch (int16)")
f.HIGH_LATENCY_heading = ProtoField.uint16("mavlink_proto.HIGH_LATENCY_heading", "heading (uint16)")
f.HIGH_LATENCY_throttle = ProtoField.int8("mavlink_proto.HIGH_LATENCY_throttle", "throttle (int8)")
f.HIGH_LATENCY_heading_sp = ProtoField.int16("mavlink_proto.HIGH_LATENCY_heading_sp", "heading_sp (int16)")
f.HIGH_LATENCY_latitude = ProtoField.int32("mavlink_proto.HIGH_LATENCY_latitude", "latitude (int32)")
f.HIGH_LATENCY_longitude = ProtoField.int32("mavlink_proto.HIGH_LATENCY_longitude", "longitude (int32)")
f.HIGH_LATENCY_altitude_amsl = ProtoField.int16("mavlink_proto.HIGH_LATENCY_altitude_amsl", "altitude_amsl (int16)")
f.HIGH_LATENCY_altitude_sp = ProtoField.int16("mavlink_proto.HIGH_LATENCY_altitude_sp", "altitude_sp (int16)")
f.HIGH_LATENCY_airspeed = ProtoField.uint8("mavlink_proto.HIGH_LATENCY_airspeed", "airspeed (uint8)")
f.HIGH_LATENCY_airspeed_sp = ProtoField.uint8("mavlink_proto.HIGH_LATENCY_airspeed_sp", "airspeed_sp (uint8)")
f.HIGH_LATENCY_groundspeed = ProtoField.uint8("mavlink_proto.HIGH_LATENCY_groundspeed", "groundspeed (uint8)")
f.HIGH_LATENCY_climb_rate = ProtoField.int8("mavlink_proto.HIGH_LATENCY_climb_rate", "climb_rate (int8)")
f.HIGH_LATENCY_gps_nsat = ProtoField.uint8("mavlink_proto.HIGH_LATENCY_gps_nsat", "gps_nsat (uint8)")
f.HIGH_LATENCY_gps_fix_type = ProtoField.uint8("mavlink_proto.HIGH_LATENCY_gps_fix_type", "gps_fix_type (uint8)")
f.HIGH_LATENCY_battery_remaining = ProtoField.uint8("mavlink_proto.HIGH_LATENCY_battery_remaining", "battery_remaining (uint8)")
f.HIGH_LATENCY_temperature = ProtoField.int8("mavlink_proto.HIGH_LATENCY_temperature", "temperature (int8)")
f.HIGH_LATENCY_temperature_air = ProtoField.int8("mavlink_proto.HIGH_LATENCY_temperature_air", "temperature_air (int8)")
f.HIGH_LATENCY_failsafe = ProtoField.uint8("mavlink_proto.HIGH_LATENCY_failsafe", "failsafe (uint8)")
f.HIGH_LATENCY_wp_num = ProtoField.uint8("mavlink_proto.HIGH_LATENCY_wp_num", "wp_num (uint8)")
f.HIGH_LATENCY_wp_distance = ProtoField.uint16("mavlink_proto.HIGH_LATENCY_wp_distance", "wp_distance (uint16)")

f.VIBRATION_time_usec = ProtoField.uint64("mavlink_proto.VIBRATION_time_usec", "time_usec (uint64)")
f.VIBRATION_vibration_x = ProtoField.float("mavlink_proto.VIBRATION_vibration_x", "vibration_x (float)")
f.VIBRATION_vibration_y = ProtoField.float("mavlink_proto.VIBRATION_vibration_y", "vibration_y (float)")
f.VIBRATION_vibration_z = ProtoField.float("mavlink_proto.VIBRATION_vibration_z", "vibration_z (float)")
f.VIBRATION_clipping_0 = ProtoField.uint32("mavlink_proto.VIBRATION_clipping_0", "clipping_0 (uint32)")
f.VIBRATION_clipping_1 = ProtoField.uint32("mavlink_proto.VIBRATION_clipping_1", "clipping_1 (uint32)")
f.VIBRATION_clipping_2 = ProtoField.uint32("mavlink_proto.VIBRATION_clipping_2", "clipping_2 (uint32)")

f.HOME_POSITION_latitude = ProtoField.int32("mavlink_proto.HOME_POSITION_latitude", "latitude (int32)")
f.HOME_POSITION_longitude = ProtoField.int32("mavlink_proto.HOME_POSITION_longitude", "longitude (int32)")
f.HOME_POSITION_altitude = ProtoField.int32("mavlink_proto.HOME_POSITION_altitude", "altitude (int32)")
f.HOME_POSITION_x = ProtoField.float("mavlink_proto.HOME_POSITION_x", "x (float)")
f.HOME_POSITION_y = ProtoField.float("mavlink_proto.HOME_POSITION_y", "y (float)")
f.HOME_POSITION_z = ProtoField.float("mavlink_proto.HOME_POSITION_z", "z (float)")
f.HOME_POSITION_q_0 = ProtoField.float("mavlink_proto.HOME_POSITION_q_0", "q[0] (float)")
f.HOME_POSITION_q_1 = ProtoField.float("mavlink_proto.HOME_POSITION_q_1", "q[1] (float)")
f.HOME_POSITION_q_2 = ProtoField.float("mavlink_proto.HOME_POSITION_q_2", "q[2] (float)")
f.HOME_POSITION_q_3 = ProtoField.float("mavlink_proto.HOME_POSITION_q_3", "q[3] (float)")
f.HOME_POSITION_approach_x = ProtoField.float("mavlink_proto.HOME_POSITION_approach_x", "approach_x (float)")
f.HOME_POSITION_approach_y = ProtoField.float("mavlink_proto.HOME_POSITION_approach_y", "approach_y (float)")
f.HOME_POSITION_approach_z = ProtoField.float("mavlink_proto.HOME_POSITION_approach_z", "approach_z (float)")

f.SET_HOME_POSITION_target_system = ProtoField.uint8("mavlink_proto.SET_HOME_POSITION_target_system", "target_system (uint8)")
f.SET_HOME_POSITION_latitude = ProtoField.int32("mavlink_proto.SET_HOME_POSITION_latitude", "latitude (int32)")
f.SET_HOME_POSITION_longitude = ProtoField.int32("mavlink_proto.SET_HOME_POSITION_longitude", "longitude (int32)")
f.SET_HOME_POSITION_altitude = ProtoField.int32("mavlink_proto.SET_HOME_POSITION_altitude", "altitude (int32)")
f.SET_HOME_POSITION_x = ProtoField.float("mavlink_proto.SET_HOME_POSITION_x", "x (float)")
f.SET_HOME_POSITION_y = ProtoField.float("mavlink_proto.SET_HOME_POSITION_y", "y (float)")
f.SET_HOME_POSITION_z = ProtoField.float("mavlink_proto.SET_HOME_POSITION_z", "z (float)")
f.SET_HOME_POSITION_q_0 = ProtoField.float("mavlink_proto.SET_HOME_POSITION_q_0", "q[0] (float)")
f.SET_HOME_POSITION_q_1 = ProtoField.float("mavlink_proto.SET_HOME_POSITION_q_1", "q[1] (float)")
f.SET_HOME_POSITION_q_2 = ProtoField.float("mavlink_proto.SET_HOME_POSITION_q_2", "q[2] (float)")
f.SET_HOME_POSITION_q_3 = ProtoField.float("mavlink_proto.SET_HOME_POSITION_q_3", "q[3] (float)")
f.SET_HOME_POSITION_approach_x = ProtoField.float("mavlink_proto.SET_HOME_POSITION_approach_x", "approach_x (float)")
f.SET_HOME_POSITION_approach_y = ProtoField.float("mavlink_proto.SET_HOME_POSITION_approach_y", "approach_y (float)")
f.SET_HOME_POSITION_approach_z = ProtoField.float("mavlink_proto.SET_HOME_POSITION_approach_z", "approach_z (float)")

f.MESSAGE_INTERVAL_message_id = ProtoField.uint16("mavlink_proto.MESSAGE_INTERVAL_message_id", "message_id (uint16)")
f.MESSAGE_INTERVAL_interval_us = ProtoField.int32("mavlink_proto.MESSAGE_INTERVAL_interval_us", "interval_us (int32)")

f.EXTENDED_SYS_STATE_vtol_state = ProtoField.uint8("mavlink_proto.EXTENDED_SYS_STATE_vtol_state", "vtol_state (uint8)")
f.EXTENDED_SYS_STATE_landed_state = ProtoField.uint8("mavlink_proto.EXTENDED_SYS_STATE_landed_state", "landed_state (uint8)")

f.ADSB_VEHICLE_ICAO_address = ProtoField.uint32("mavlink_proto.ADSB_VEHICLE_ICAO_address", "ICAO_address (uint32)")
f.ADSB_VEHICLE_lat = ProtoField.int32("mavlink_proto.ADSB_VEHICLE_lat", "lat (int32)")
f.ADSB_VEHICLE_lon = ProtoField.int32("mavlink_proto.ADSB_VEHICLE_lon", "lon (int32)")
f.ADSB_VEHICLE_altitude_type = ProtoField.uint8("mavlink_proto.ADSB_VEHICLE_altitude_type", "altitude_type (uint8)")
f.ADSB_VEHICLE_altitude = ProtoField.int32("mavlink_proto.ADSB_VEHICLE_altitude", "altitude (int32)")
f.ADSB_VEHICLE_heading = ProtoField.uint16("mavlink_proto.ADSB_VEHICLE_heading", "heading (uint16)")
f.ADSB_VEHICLE_hor_velocity = ProtoField.uint16("mavlink_proto.ADSB_VEHICLE_hor_velocity", "hor_velocity (uint16)")
f.ADSB_VEHICLE_ver_velocity = ProtoField.int16("mavlink_proto.ADSB_VEHICLE_ver_velocity", "ver_velocity (int16)")
f.ADSB_VEHICLE_callsign = ProtoField.string("mavlink_proto.ADSB_VEHICLE_callsign", "callsign (string)")
f.ADSB_VEHICLE_emitter_type = ProtoField.uint8("mavlink_proto.ADSB_VEHICLE_emitter_type", "emitter_type (uint8)")
f.ADSB_VEHICLE_tslc = ProtoField.uint8("mavlink_proto.ADSB_VEHICLE_tslc", "tslc (uint8)")
f.ADSB_VEHICLE_flags = ProtoField.uint16("mavlink_proto.ADSB_VEHICLE_flags", "flags (uint16)")
f.ADSB_VEHICLE_squawk = ProtoField.uint16("mavlink_proto.ADSB_VEHICLE_squawk", "squawk (uint16)")

f.COLLISION_src = ProtoField.uint8("mavlink_proto.COLLISION_src", "src (uint8)")
f.COLLISION_id = ProtoField.uint32("mavlink_proto.COLLISION_id", "id (uint32)")
f.COLLISION_action = ProtoField.uint8("mavlink_proto.COLLISION_action", "action (uint8)")
f.COLLISION_threat_level = ProtoField.uint8("mavlink_proto.COLLISION_threat_level", "threat_level (uint8)")
f.COLLISION_time_to_minimum_delta = ProtoField.float("mavlink_proto.COLLISION_time_to_minimum_delta", "time_to_minimum_delta (float)")
f.COLLISION_altitude_minimum_delta = ProtoField.float("mavlink_proto.COLLISION_altitude_minimum_delta", "altitude_minimum_delta (float)")
f.COLLISION_horizontal_minimum_delta = ProtoField.float("mavlink_proto.COLLISION_horizontal_minimum_delta", "horizontal_minimum_delta (float)")

f.V2_EXTENSION_target_network = ProtoField.uint8("mavlink_proto.V2_EXTENSION_target_network", "target_network (uint8)")
f.V2_EXTENSION_target_system = ProtoField.uint8("mavlink_proto.V2_EXTENSION_target_system", "target_system (uint8)")
f.V2_EXTENSION_target_component = ProtoField.uint8("mavlink_proto.V2_EXTENSION_target_component", "target_component (uint8)")
f.V2_EXTENSION_message_type = ProtoField.uint16("mavlink_proto.V2_EXTENSION_message_type", "message_type (uint16)")
f.V2_EXTENSION_payload_0 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_0", "payload[0] (uint8)")
f.V2_EXTENSION_payload_1 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_1", "payload[1] (uint8)")
f.V2_EXTENSION_payload_2 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_2", "payload[2] (uint8)")
f.V2_EXTENSION_payload_3 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_3", "payload[3] (uint8)")
f.V2_EXTENSION_payload_4 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_4", "payload[4] (uint8)")
f.V2_EXTENSION_payload_5 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_5", "payload[5] (uint8)")
f.V2_EXTENSION_payload_6 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_6", "payload[6] (uint8)")
f.V2_EXTENSION_payload_7 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_7", "payload[7] (uint8)")
f.V2_EXTENSION_payload_8 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_8", "payload[8] (uint8)")
f.V2_EXTENSION_payload_9 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_9", "payload[9] (uint8)")
f.V2_EXTENSION_payload_10 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_10", "payload[10] (uint8)")
f.V2_EXTENSION_payload_11 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_11", "payload[11] (uint8)")
f.V2_EXTENSION_payload_12 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_12", "payload[12] (uint8)")
f.V2_EXTENSION_payload_13 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_13", "payload[13] (uint8)")
f.V2_EXTENSION_payload_14 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_14", "payload[14] (uint8)")
f.V2_EXTENSION_payload_15 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_15", "payload[15] (uint8)")
f.V2_EXTENSION_payload_16 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_16", "payload[16] (uint8)")
f.V2_EXTENSION_payload_17 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_17", "payload[17] (uint8)")
f.V2_EXTENSION_payload_18 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_18", "payload[18] (uint8)")
f.V2_EXTENSION_payload_19 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_19", "payload[19] (uint8)")
f.V2_EXTENSION_payload_20 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_20", "payload[20] (uint8)")
f.V2_EXTENSION_payload_21 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_21", "payload[21] (uint8)")
f.V2_EXTENSION_payload_22 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_22", "payload[22] (uint8)")
f.V2_EXTENSION_payload_23 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_23", "payload[23] (uint8)")
f.V2_EXTENSION_payload_24 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_24", "payload[24] (uint8)")
f.V2_EXTENSION_payload_25 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_25", "payload[25] (uint8)")
f.V2_EXTENSION_payload_26 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_26", "payload[26] (uint8)")
f.V2_EXTENSION_payload_27 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_27", "payload[27] (uint8)")
f.V2_EXTENSION_payload_28 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_28", "payload[28] (uint8)")
f.V2_EXTENSION_payload_29 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_29", "payload[29] (uint8)")
f.V2_EXTENSION_payload_30 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_30", "payload[30] (uint8)")
f.V2_EXTENSION_payload_31 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_31", "payload[31] (uint8)")
f.V2_EXTENSION_payload_32 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_32", "payload[32] (uint8)")
f.V2_EXTENSION_payload_33 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_33", "payload[33] (uint8)")
f.V2_EXTENSION_payload_34 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_34", "payload[34] (uint8)")
f.V2_EXTENSION_payload_35 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_35", "payload[35] (uint8)")
f.V2_EXTENSION_payload_36 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_36", "payload[36] (uint8)")
f.V2_EXTENSION_payload_37 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_37", "payload[37] (uint8)")
f.V2_EXTENSION_payload_38 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_38", "payload[38] (uint8)")
f.V2_EXTENSION_payload_39 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_39", "payload[39] (uint8)")
f.V2_EXTENSION_payload_40 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_40", "payload[40] (uint8)")
f.V2_EXTENSION_payload_41 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_41", "payload[41] (uint8)")
f.V2_EXTENSION_payload_42 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_42", "payload[42] (uint8)")
f.V2_EXTENSION_payload_43 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_43", "payload[43] (uint8)")
f.V2_EXTENSION_payload_44 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_44", "payload[44] (uint8)")
f.V2_EXTENSION_payload_45 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_45", "payload[45] (uint8)")
f.V2_EXTENSION_payload_46 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_46", "payload[46] (uint8)")
f.V2_EXTENSION_payload_47 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_47", "payload[47] (uint8)")
f.V2_EXTENSION_payload_48 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_48", "payload[48] (uint8)")
f.V2_EXTENSION_payload_49 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_49", "payload[49] (uint8)")
f.V2_EXTENSION_payload_50 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_50", "payload[50] (uint8)")
f.V2_EXTENSION_payload_51 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_51", "payload[51] (uint8)")
f.V2_EXTENSION_payload_52 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_52", "payload[52] (uint8)")
f.V2_EXTENSION_payload_53 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_53", "payload[53] (uint8)")
f.V2_EXTENSION_payload_54 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_54", "payload[54] (uint8)")
f.V2_EXTENSION_payload_55 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_55", "payload[55] (uint8)")
f.V2_EXTENSION_payload_56 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_56", "payload[56] (uint8)")
f.V2_EXTENSION_payload_57 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_57", "payload[57] (uint8)")
f.V2_EXTENSION_payload_58 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_58", "payload[58] (uint8)")
f.V2_EXTENSION_payload_59 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_59", "payload[59] (uint8)")
f.V2_EXTENSION_payload_60 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_60", "payload[60] (uint8)")
f.V2_EXTENSION_payload_61 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_61", "payload[61] (uint8)")
f.V2_EXTENSION_payload_62 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_62", "payload[62] (uint8)")
f.V2_EXTENSION_payload_63 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_63", "payload[63] (uint8)")
f.V2_EXTENSION_payload_64 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_64", "payload[64] (uint8)")
f.V2_EXTENSION_payload_65 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_65", "payload[65] (uint8)")
f.V2_EXTENSION_payload_66 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_66", "payload[66] (uint8)")
f.V2_EXTENSION_payload_67 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_67", "payload[67] (uint8)")
f.V2_EXTENSION_payload_68 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_68", "payload[68] (uint8)")
f.V2_EXTENSION_payload_69 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_69", "payload[69] (uint8)")
f.V2_EXTENSION_payload_70 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_70", "payload[70] (uint8)")
f.V2_EXTENSION_payload_71 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_71", "payload[71] (uint8)")
f.V2_EXTENSION_payload_72 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_72", "payload[72] (uint8)")
f.V2_EXTENSION_payload_73 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_73", "payload[73] (uint8)")
f.V2_EXTENSION_payload_74 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_74", "payload[74] (uint8)")
f.V2_EXTENSION_payload_75 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_75", "payload[75] (uint8)")
f.V2_EXTENSION_payload_76 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_76", "payload[76] (uint8)")
f.V2_EXTENSION_payload_77 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_77", "payload[77] (uint8)")
f.V2_EXTENSION_payload_78 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_78", "payload[78] (uint8)")
f.V2_EXTENSION_payload_79 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_79", "payload[79] (uint8)")
f.V2_EXTENSION_payload_80 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_80", "payload[80] (uint8)")
f.V2_EXTENSION_payload_81 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_81", "payload[81] (uint8)")
f.V2_EXTENSION_payload_82 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_82", "payload[82] (uint8)")
f.V2_EXTENSION_payload_83 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_83", "payload[83] (uint8)")
f.V2_EXTENSION_payload_84 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_84", "payload[84] (uint8)")
f.V2_EXTENSION_payload_85 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_85", "payload[85] (uint8)")
f.V2_EXTENSION_payload_86 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_86", "payload[86] (uint8)")
f.V2_EXTENSION_payload_87 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_87", "payload[87] (uint8)")
f.V2_EXTENSION_payload_88 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_88", "payload[88] (uint8)")
f.V2_EXTENSION_payload_89 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_89", "payload[89] (uint8)")
f.V2_EXTENSION_payload_90 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_90", "payload[90] (uint8)")
f.V2_EXTENSION_payload_91 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_91", "payload[91] (uint8)")
f.V2_EXTENSION_payload_92 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_92", "payload[92] (uint8)")
f.V2_EXTENSION_payload_93 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_93", "payload[93] (uint8)")
f.V2_EXTENSION_payload_94 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_94", "payload[94] (uint8)")
f.V2_EXTENSION_payload_95 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_95", "payload[95] (uint8)")
f.V2_EXTENSION_payload_96 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_96", "payload[96] (uint8)")
f.V2_EXTENSION_payload_97 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_97", "payload[97] (uint8)")
f.V2_EXTENSION_payload_98 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_98", "payload[98] (uint8)")
f.V2_EXTENSION_payload_99 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_99", "payload[99] (uint8)")
f.V2_EXTENSION_payload_100 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_100", "payload[100] (uint8)")
f.V2_EXTENSION_payload_101 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_101", "payload[101] (uint8)")
f.V2_EXTENSION_payload_102 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_102", "payload[102] (uint8)")
f.V2_EXTENSION_payload_103 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_103", "payload[103] (uint8)")
f.V2_EXTENSION_payload_104 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_104", "payload[104] (uint8)")
f.V2_EXTENSION_payload_105 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_105", "payload[105] (uint8)")
f.V2_EXTENSION_payload_106 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_106", "payload[106] (uint8)")
f.V2_EXTENSION_payload_107 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_107", "payload[107] (uint8)")
f.V2_EXTENSION_payload_108 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_108", "payload[108] (uint8)")
f.V2_EXTENSION_payload_109 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_109", "payload[109] (uint8)")
f.V2_EXTENSION_payload_110 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_110", "payload[110] (uint8)")
f.V2_EXTENSION_payload_111 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_111", "payload[111] (uint8)")
f.V2_EXTENSION_payload_112 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_112", "payload[112] (uint8)")
f.V2_EXTENSION_payload_113 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_113", "payload[113] (uint8)")
f.V2_EXTENSION_payload_114 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_114", "payload[114] (uint8)")
f.V2_EXTENSION_payload_115 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_115", "payload[115] (uint8)")
f.V2_EXTENSION_payload_116 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_116", "payload[116] (uint8)")
f.V2_EXTENSION_payload_117 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_117", "payload[117] (uint8)")
f.V2_EXTENSION_payload_118 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_118", "payload[118] (uint8)")
f.V2_EXTENSION_payload_119 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_119", "payload[119] (uint8)")
f.V2_EXTENSION_payload_120 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_120", "payload[120] (uint8)")
f.V2_EXTENSION_payload_121 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_121", "payload[121] (uint8)")
f.V2_EXTENSION_payload_122 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_122", "payload[122] (uint8)")
f.V2_EXTENSION_payload_123 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_123", "payload[123] (uint8)")
f.V2_EXTENSION_payload_124 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_124", "payload[124] (uint8)")
f.V2_EXTENSION_payload_125 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_125", "payload[125] (uint8)")
f.V2_EXTENSION_payload_126 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_126", "payload[126] (uint8)")
f.V2_EXTENSION_payload_127 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_127", "payload[127] (uint8)")
f.V2_EXTENSION_payload_128 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_128", "payload[128] (uint8)")
f.V2_EXTENSION_payload_129 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_129", "payload[129] (uint8)")
f.V2_EXTENSION_payload_130 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_130", "payload[130] (uint8)")
f.V2_EXTENSION_payload_131 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_131", "payload[131] (uint8)")
f.V2_EXTENSION_payload_132 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_132", "payload[132] (uint8)")
f.V2_EXTENSION_payload_133 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_133", "payload[133] (uint8)")
f.V2_EXTENSION_payload_134 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_134", "payload[134] (uint8)")
f.V2_EXTENSION_payload_135 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_135", "payload[135] (uint8)")
f.V2_EXTENSION_payload_136 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_136", "payload[136] (uint8)")
f.V2_EXTENSION_payload_137 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_137", "payload[137] (uint8)")
f.V2_EXTENSION_payload_138 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_138", "payload[138] (uint8)")
f.V2_EXTENSION_payload_139 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_139", "payload[139] (uint8)")
f.V2_EXTENSION_payload_140 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_140", "payload[140] (uint8)")
f.V2_EXTENSION_payload_141 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_141", "payload[141] (uint8)")
f.V2_EXTENSION_payload_142 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_142", "payload[142] (uint8)")
f.V2_EXTENSION_payload_143 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_143", "payload[143] (uint8)")
f.V2_EXTENSION_payload_144 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_144", "payload[144] (uint8)")
f.V2_EXTENSION_payload_145 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_145", "payload[145] (uint8)")
f.V2_EXTENSION_payload_146 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_146", "payload[146] (uint8)")
f.V2_EXTENSION_payload_147 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_147", "payload[147] (uint8)")
f.V2_EXTENSION_payload_148 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_148", "payload[148] (uint8)")
f.V2_EXTENSION_payload_149 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_149", "payload[149] (uint8)")
f.V2_EXTENSION_payload_150 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_150", "payload[150] (uint8)")
f.V2_EXTENSION_payload_151 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_151", "payload[151] (uint8)")
f.V2_EXTENSION_payload_152 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_152", "payload[152] (uint8)")
f.V2_EXTENSION_payload_153 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_153", "payload[153] (uint8)")
f.V2_EXTENSION_payload_154 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_154", "payload[154] (uint8)")
f.V2_EXTENSION_payload_155 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_155", "payload[155] (uint8)")
f.V2_EXTENSION_payload_156 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_156", "payload[156] (uint8)")
f.V2_EXTENSION_payload_157 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_157", "payload[157] (uint8)")
f.V2_EXTENSION_payload_158 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_158", "payload[158] (uint8)")
f.V2_EXTENSION_payload_159 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_159", "payload[159] (uint8)")
f.V2_EXTENSION_payload_160 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_160", "payload[160] (uint8)")
f.V2_EXTENSION_payload_161 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_161", "payload[161] (uint8)")
f.V2_EXTENSION_payload_162 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_162", "payload[162] (uint8)")
f.V2_EXTENSION_payload_163 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_163", "payload[163] (uint8)")
f.V2_EXTENSION_payload_164 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_164", "payload[164] (uint8)")
f.V2_EXTENSION_payload_165 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_165", "payload[165] (uint8)")
f.V2_EXTENSION_payload_166 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_166", "payload[166] (uint8)")
f.V2_EXTENSION_payload_167 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_167", "payload[167] (uint8)")
f.V2_EXTENSION_payload_168 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_168", "payload[168] (uint8)")
f.V2_EXTENSION_payload_169 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_169", "payload[169] (uint8)")
f.V2_EXTENSION_payload_170 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_170", "payload[170] (uint8)")
f.V2_EXTENSION_payload_171 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_171", "payload[171] (uint8)")
f.V2_EXTENSION_payload_172 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_172", "payload[172] (uint8)")
f.V2_EXTENSION_payload_173 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_173", "payload[173] (uint8)")
f.V2_EXTENSION_payload_174 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_174", "payload[174] (uint8)")
f.V2_EXTENSION_payload_175 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_175", "payload[175] (uint8)")
f.V2_EXTENSION_payload_176 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_176", "payload[176] (uint8)")
f.V2_EXTENSION_payload_177 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_177", "payload[177] (uint8)")
f.V2_EXTENSION_payload_178 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_178", "payload[178] (uint8)")
f.V2_EXTENSION_payload_179 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_179", "payload[179] (uint8)")
f.V2_EXTENSION_payload_180 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_180", "payload[180] (uint8)")
f.V2_EXTENSION_payload_181 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_181", "payload[181] (uint8)")
f.V2_EXTENSION_payload_182 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_182", "payload[182] (uint8)")
f.V2_EXTENSION_payload_183 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_183", "payload[183] (uint8)")
f.V2_EXTENSION_payload_184 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_184", "payload[184] (uint8)")
f.V2_EXTENSION_payload_185 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_185", "payload[185] (uint8)")
f.V2_EXTENSION_payload_186 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_186", "payload[186] (uint8)")
f.V2_EXTENSION_payload_187 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_187", "payload[187] (uint8)")
f.V2_EXTENSION_payload_188 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_188", "payload[188] (uint8)")
f.V2_EXTENSION_payload_189 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_189", "payload[189] (uint8)")
f.V2_EXTENSION_payload_190 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_190", "payload[190] (uint8)")
f.V2_EXTENSION_payload_191 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_191", "payload[191] (uint8)")
f.V2_EXTENSION_payload_192 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_192", "payload[192] (uint8)")
f.V2_EXTENSION_payload_193 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_193", "payload[193] (uint8)")
f.V2_EXTENSION_payload_194 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_194", "payload[194] (uint8)")
f.V2_EXTENSION_payload_195 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_195", "payload[195] (uint8)")
f.V2_EXTENSION_payload_196 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_196", "payload[196] (uint8)")
f.V2_EXTENSION_payload_197 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_197", "payload[197] (uint8)")
f.V2_EXTENSION_payload_198 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_198", "payload[198] (uint8)")
f.V2_EXTENSION_payload_199 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_199", "payload[199] (uint8)")
f.V2_EXTENSION_payload_200 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_200", "payload[200] (uint8)")
f.V2_EXTENSION_payload_201 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_201", "payload[201] (uint8)")
f.V2_EXTENSION_payload_202 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_202", "payload[202] (uint8)")
f.V2_EXTENSION_payload_203 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_203", "payload[203] (uint8)")
f.V2_EXTENSION_payload_204 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_204", "payload[204] (uint8)")
f.V2_EXTENSION_payload_205 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_205", "payload[205] (uint8)")
f.V2_EXTENSION_payload_206 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_206", "payload[206] (uint8)")
f.V2_EXTENSION_payload_207 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_207", "payload[207] (uint8)")
f.V2_EXTENSION_payload_208 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_208", "payload[208] (uint8)")
f.V2_EXTENSION_payload_209 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_209", "payload[209] (uint8)")
f.V2_EXTENSION_payload_210 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_210", "payload[210] (uint8)")
f.V2_EXTENSION_payload_211 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_211", "payload[211] (uint8)")
f.V2_EXTENSION_payload_212 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_212", "payload[212] (uint8)")
f.V2_EXTENSION_payload_213 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_213", "payload[213] (uint8)")
f.V2_EXTENSION_payload_214 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_214", "payload[214] (uint8)")
f.V2_EXTENSION_payload_215 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_215", "payload[215] (uint8)")
f.V2_EXTENSION_payload_216 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_216", "payload[216] (uint8)")
f.V2_EXTENSION_payload_217 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_217", "payload[217] (uint8)")
f.V2_EXTENSION_payload_218 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_218", "payload[218] (uint8)")
f.V2_EXTENSION_payload_219 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_219", "payload[219] (uint8)")
f.V2_EXTENSION_payload_220 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_220", "payload[220] (uint8)")
f.V2_EXTENSION_payload_221 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_221", "payload[221] (uint8)")
f.V2_EXTENSION_payload_222 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_222", "payload[222] (uint8)")
f.V2_EXTENSION_payload_223 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_223", "payload[223] (uint8)")
f.V2_EXTENSION_payload_224 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_224", "payload[224] (uint8)")
f.V2_EXTENSION_payload_225 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_225", "payload[225] (uint8)")
f.V2_EXTENSION_payload_226 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_226", "payload[226] (uint8)")
f.V2_EXTENSION_payload_227 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_227", "payload[227] (uint8)")
f.V2_EXTENSION_payload_228 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_228", "payload[228] (uint8)")
f.V2_EXTENSION_payload_229 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_229", "payload[229] (uint8)")
f.V2_EXTENSION_payload_230 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_230", "payload[230] (uint8)")
f.V2_EXTENSION_payload_231 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_231", "payload[231] (uint8)")
f.V2_EXTENSION_payload_232 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_232", "payload[232] (uint8)")
f.V2_EXTENSION_payload_233 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_233", "payload[233] (uint8)")
f.V2_EXTENSION_payload_234 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_234", "payload[234] (uint8)")
f.V2_EXTENSION_payload_235 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_235", "payload[235] (uint8)")
f.V2_EXTENSION_payload_236 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_236", "payload[236] (uint8)")
f.V2_EXTENSION_payload_237 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_237", "payload[237] (uint8)")
f.V2_EXTENSION_payload_238 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_238", "payload[238] (uint8)")
f.V2_EXTENSION_payload_239 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_239", "payload[239] (uint8)")
f.V2_EXTENSION_payload_240 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_240", "payload[240] (uint8)")
f.V2_EXTENSION_payload_241 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_241", "payload[241] (uint8)")
f.V2_EXTENSION_payload_242 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_242", "payload[242] (uint8)")
f.V2_EXTENSION_payload_243 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_243", "payload[243] (uint8)")
f.V2_EXTENSION_payload_244 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_244", "payload[244] (uint8)")
f.V2_EXTENSION_payload_245 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_245", "payload[245] (uint8)")
f.V2_EXTENSION_payload_246 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_246", "payload[246] (uint8)")
f.V2_EXTENSION_payload_247 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_247", "payload[247] (uint8)")
f.V2_EXTENSION_payload_248 = ProtoField.uint8("mavlink_proto.V2_EXTENSION_payload_248", "payload[248] (uint8)")

f.MEMORY_VECT_address = ProtoField.uint16("mavlink_proto.MEMORY_VECT_address", "address (uint16)")
f.MEMORY_VECT_ver = ProtoField.uint8("mavlink_proto.MEMORY_VECT_ver", "ver (uint8)")
f.MEMORY_VECT_type = ProtoField.uint8("mavlink_proto.MEMORY_VECT_type", "type (uint8)")
f.MEMORY_VECT_value_0 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_0", "value[0] (int8)")
f.MEMORY_VECT_value_1 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_1", "value[1] (int8)")
f.MEMORY_VECT_value_2 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_2", "value[2] (int8)")
f.MEMORY_VECT_value_3 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_3", "value[3] (int8)")
f.MEMORY_VECT_value_4 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_4", "value[4] (int8)")
f.MEMORY_VECT_value_5 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_5", "value[5] (int8)")
f.MEMORY_VECT_value_6 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_6", "value[6] (int8)")
f.MEMORY_VECT_value_7 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_7", "value[7] (int8)")
f.MEMORY_VECT_value_8 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_8", "value[8] (int8)")
f.MEMORY_VECT_value_9 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_9", "value[9] (int8)")
f.MEMORY_VECT_value_10 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_10", "value[10] (int8)")
f.MEMORY_VECT_value_11 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_11", "value[11] (int8)")
f.MEMORY_VECT_value_12 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_12", "value[12] (int8)")
f.MEMORY_VECT_value_13 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_13", "value[13] (int8)")
f.MEMORY_VECT_value_14 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_14", "value[14] (int8)")
f.MEMORY_VECT_value_15 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_15", "value[15] (int8)")
f.MEMORY_VECT_value_16 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_16", "value[16] (int8)")
f.MEMORY_VECT_value_17 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_17", "value[17] (int8)")
f.MEMORY_VECT_value_18 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_18", "value[18] (int8)")
f.MEMORY_VECT_value_19 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_19", "value[19] (int8)")
f.MEMORY_VECT_value_20 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_20", "value[20] (int8)")
f.MEMORY_VECT_value_21 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_21", "value[21] (int8)")
f.MEMORY_VECT_value_22 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_22", "value[22] (int8)")
f.MEMORY_VECT_value_23 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_23", "value[23] (int8)")
f.MEMORY_VECT_value_24 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_24", "value[24] (int8)")
f.MEMORY_VECT_value_25 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_25", "value[25] (int8)")
f.MEMORY_VECT_value_26 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_26", "value[26] (int8)")
f.MEMORY_VECT_value_27 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_27", "value[27] (int8)")
f.MEMORY_VECT_value_28 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_28", "value[28] (int8)")
f.MEMORY_VECT_value_29 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_29", "value[29] (int8)")
f.MEMORY_VECT_value_30 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_30", "value[30] (int8)")
f.MEMORY_VECT_value_31 = ProtoField.int8("mavlink_proto.MEMORY_VECT_value_31", "value[31] (int8)")

f.DEBUG_VECT_name = ProtoField.string("mavlink_proto.DEBUG_VECT_name", "name (string)")
f.DEBUG_VECT_time_usec = ProtoField.uint64("mavlink_proto.DEBUG_VECT_time_usec", "time_usec (uint64)")
f.DEBUG_VECT_x = ProtoField.float("mavlink_proto.DEBUG_VECT_x", "x (float)")
f.DEBUG_VECT_y = ProtoField.float("mavlink_proto.DEBUG_VECT_y", "y (float)")
f.DEBUG_VECT_z = ProtoField.float("mavlink_proto.DEBUG_VECT_z", "z (float)")

f.NAMED_VALUE_FLOAT_time_boot_ms = ProtoField.uint32("mavlink_proto.NAMED_VALUE_FLOAT_time_boot_ms", "time_boot_ms (uint32)")
f.NAMED_VALUE_FLOAT_name = ProtoField.string("mavlink_proto.NAMED_VALUE_FLOAT_name", "name (string)")
f.NAMED_VALUE_FLOAT_value = ProtoField.float("mavlink_proto.NAMED_VALUE_FLOAT_value", "value (float)")

f.NAMED_VALUE_INT_time_boot_ms = ProtoField.uint32("mavlink_proto.NAMED_VALUE_INT_time_boot_ms", "time_boot_ms (uint32)")
f.NAMED_VALUE_INT_name = ProtoField.string("mavlink_proto.NAMED_VALUE_INT_name", "name (string)")
f.NAMED_VALUE_INT_value = ProtoField.int32("mavlink_proto.NAMED_VALUE_INT_value", "value (int32)")

f.STATUSTEXT_severity = ProtoField.uint8("mavlink_proto.STATUSTEXT_severity", "severity (uint8)")
f.STATUSTEXT_text = ProtoField.string("mavlink_proto.STATUSTEXT_text", "text (string)")

f.DEBUG_time_boot_ms = ProtoField.uint32("mavlink_proto.DEBUG_time_boot_ms", "time_boot_ms (uint32)")
f.DEBUG_ind = ProtoField.uint8("mavlink_proto.DEBUG_ind", "ind (uint8)")
f.DEBUG_value = ProtoField.float("mavlink_proto.DEBUG_value", "value (float)")

-- dissect payload of message type SENSOR_OFFSETS
function payload_fns.payload_150(buffer, tree, msgid, offset)
    tree:add_le(f.SENSOR_OFFSETS_mag_declination, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SENSOR_OFFSETS_raw_press, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SENSOR_OFFSETS_raw_temp, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SENSOR_OFFSETS_gyro_cal_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SENSOR_OFFSETS_gyro_cal_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SENSOR_OFFSETS_gyro_cal_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SENSOR_OFFSETS_accel_cal_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SENSOR_OFFSETS_accel_cal_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SENSOR_OFFSETS_accel_cal_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SENSOR_OFFSETS_mag_ofs_x, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SENSOR_OFFSETS_mag_ofs_y, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SENSOR_OFFSETS_mag_ofs_z, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type SET_MAG_OFFSETS
function payload_fns.payload_151(buffer, tree, msgid, offset)
    tree:add_le(f.SET_MAG_OFFSETS_mag_ofs_x, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SET_MAG_OFFSETS_mag_ofs_y, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SET_MAG_OFFSETS_mag_ofs_z, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SET_MAG_OFFSETS_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SET_MAG_OFFSETS_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MEMINFO
function payload_fns.payload_152(buffer, tree, msgid, offset)
    tree:add_le(f.MEMINFO_brkval, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MEMINFO_freemem, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type AP_ADC
function payload_fns.payload_153(buffer, tree, msgid, offset)
    tree:add_le(f.AP_ADC_adc1, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.AP_ADC_adc2, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.AP_ADC_adc3, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.AP_ADC_adc4, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.AP_ADC_adc5, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.AP_ADC_adc6, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type DIGICAM_CONFIGURE
function payload_fns.payload_154(buffer, tree, msgid, offset)
    tree:add_le(f.DIGICAM_CONFIGURE_extra_value, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.DIGICAM_CONFIGURE_shutter_speed, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.DIGICAM_CONFIGURE_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONFIGURE_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONFIGURE_mode, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONFIGURE_aperture, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONFIGURE_iso, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONFIGURE_exposure_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONFIGURE_command_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONFIGURE_engine_cut_off, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONFIGURE_extra_param, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type DIGICAM_CONTROL
function payload_fns.payload_155(buffer, tree, msgid, offset)
    tree:add_le(f.DIGICAM_CONTROL_extra_value, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.DIGICAM_CONTROL_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONTROL_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONTROL_session, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONTROL_zoom_pos, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONTROL_zoom_step, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONTROL_focus_lock, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONTROL_shot, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONTROL_command_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DIGICAM_CONTROL_extra_param, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MOUNT_CONFIGURE
function payload_fns.payload_156(buffer, tree, msgid, offset)
    tree:add_le(f.MOUNT_CONFIGURE_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MOUNT_CONFIGURE_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MOUNT_CONFIGURE_mount_mode, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MOUNT_CONFIGURE_stab_roll, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MOUNT_CONFIGURE_stab_pitch, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MOUNT_CONFIGURE_stab_yaw, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MOUNT_CONTROL
function payload_fns.payload_157(buffer, tree, msgid, offset)
    tree:add_le(f.MOUNT_CONTROL_input_a, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MOUNT_CONTROL_input_b, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MOUNT_CONTROL_input_c, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MOUNT_CONTROL_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MOUNT_CONTROL_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MOUNT_CONTROL_save_position, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MOUNT_STATUS
function payload_fns.payload_158(buffer, tree, msgid, offset)
    tree:add_le(f.MOUNT_STATUS_pointing_a, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MOUNT_STATUS_pointing_b, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MOUNT_STATUS_pointing_c, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MOUNT_STATUS_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MOUNT_STATUS_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type FENCE_POINT
function payload_fns.payload_160(buffer, tree, msgid, offset)
    tree:add_le(f.FENCE_POINT_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FENCE_POINT_lng, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FENCE_POINT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FENCE_POINT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FENCE_POINT_idx, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FENCE_POINT_count, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type FENCE_FETCH_POINT
function payload_fns.payload_161(buffer, tree, msgid, offset)
    tree:add_le(f.FENCE_FETCH_POINT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FENCE_FETCH_POINT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FENCE_FETCH_POINT_idx, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type FENCE_STATUS
function payload_fns.payload_162(buffer, tree, msgid, offset)
    tree:add_le(f.FENCE_STATUS_breach_time, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FENCE_STATUS_breach_count, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.FENCE_STATUS_breach_status, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FENCE_STATUS_breach_type, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type AHRS
function payload_fns.payload_163(buffer, tree, msgid, offset)
    tree:add_le(f.AHRS_omegaIx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS_omegaIy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS_omegaIz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS_accel_weight, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS_renorm_val, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS_error_rp, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS_error_yaw, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type SIMSTATE
function payload_fns.payload_164(buffer, tree, msgid, offset)
    tree:add_le(f.SIMSTATE_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIMSTATE_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIMSTATE_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIMSTATE_xacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIMSTATE_yacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIMSTATE_zacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIMSTATE_xgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIMSTATE_ygyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIMSTATE_zgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIMSTATE_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIMSTATE_lng, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type HWSTATUS
function payload_fns.payload_165(buffer, tree, msgid, offset)
    tree:add_le(f.HWSTATUS_Vcc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HWSTATUS_I2Cerr, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type RADIO
function payload_fns.payload_166(buffer, tree, msgid, offset)
    tree:add_le(f.RADIO_rxerrors, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RADIO_fixed, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RADIO_rssi, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RADIO_remrssi, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RADIO_txbuf, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RADIO_noise, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RADIO_remnoise, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type LIMITS_STATUS
function payload_fns.payload_167(buffer, tree, msgid, offset)
    tree:add_le(f.LIMITS_STATUS_last_trigger, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LIMITS_STATUS_last_action, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LIMITS_STATUS_last_recovery, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LIMITS_STATUS_last_clear, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LIMITS_STATUS_breach_count, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.LIMITS_STATUS_limits_state, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LIMITS_STATUS_mods_enabled, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LIMITS_STATUS_mods_required, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LIMITS_STATUS_mods_triggered, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type WIND
function payload_fns.payload_168(buffer, tree, msgid, offset)
    tree:add_le(f.WIND_direction, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.WIND_speed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.WIND_speed_z, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type DATA16
function payload_fns.payload_169(buffer, tree, msgid, offset)
    tree:add_le(f.DATA16_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_len, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA16_data_15, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type DATA32
function payload_fns.payload_170(buffer, tree, msgid, offset)
    tree:add_le(f.DATA32_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_len, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA32_data_31, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type DATA64
function payload_fns.payload_171(buffer, tree, msgid, offset)
    tree:add_le(f.DATA64_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_len, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA64_data_63, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type DATA96
function payload_fns.payload_172(buffer, tree, msgid, offset)
    tree:add_le(f.DATA96_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_len, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_69, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_70, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_71, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_72, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_73, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_74, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_75, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_76, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_77, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_78, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_79, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_80, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_81, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_82, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_83, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_84, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_85, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_86, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_87, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_88, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_89, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_90, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_91, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_92, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_93, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_94, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA96_data_95, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type RANGEFINDER
function payload_fns.payload_173(buffer, tree, msgid, offset)
    tree:add_le(f.RANGEFINDER_distance, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.RANGEFINDER_voltage, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type AIRSPEED_AUTOCAL
function payload_fns.payload_174(buffer, tree, msgid, offset)
    tree:add_le(f.AIRSPEED_AUTOCAL_vx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_vy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_vz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_diff_pressure, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_EAS2TAS, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_ratio, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_state_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_state_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_state_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_Pax, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_Pby, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AIRSPEED_AUTOCAL_Pcz, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type RALLY_POINT
function payload_fns.payload_175(buffer, tree, msgid, offset)
    tree:add_le(f.RALLY_POINT_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.RALLY_POINT_lng, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.RALLY_POINT_alt, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RALLY_POINT_break_alt, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RALLY_POINT_land_dir, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RALLY_POINT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RALLY_POINT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RALLY_POINT_idx, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RALLY_POINT_count, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RALLY_POINT_flags, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type RALLY_FETCH_POINT
function payload_fns.payload_176(buffer, tree, msgid, offset)
    tree:add_le(f.RALLY_FETCH_POINT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RALLY_FETCH_POINT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RALLY_FETCH_POINT_idx, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type COMPASSMOT_STATUS
function payload_fns.payload_177(buffer, tree, msgid, offset)
    tree:add_le(f.COMPASSMOT_STATUS_current, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMPASSMOT_STATUS_CompensationX, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMPASSMOT_STATUS_CompensationY, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMPASSMOT_STATUS_CompensationZ, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMPASSMOT_STATUS_throttle, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.COMPASSMOT_STATUS_interference, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type AHRS2
function payload_fns.payload_178(buffer, tree, msgid, offset)
    tree:add_le(f.AHRS2_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS2_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS2_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS2_altitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS2_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS2_lng, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type CAMERA_STATUS
function payload_fns.payload_179(buffer, tree, msgid, offset)
    tree:add_le(f.CAMERA_STATUS_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.CAMERA_STATUS_p1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_STATUS_p2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_STATUS_p3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_STATUS_p4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_STATUS_img_idx, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.CAMERA_STATUS_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.CAMERA_STATUS_cam_idx, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.CAMERA_STATUS_event_id, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type CAMERA_FEEDBACK
function payload_fns.payload_180(buffer, tree, msgid, offset)
    tree:add_le(f.CAMERA_FEEDBACK_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.CAMERA_FEEDBACK_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_FEEDBACK_lng, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_FEEDBACK_alt_msl, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_FEEDBACK_alt_rel, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_FEEDBACK_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_FEEDBACK_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_FEEDBACK_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_FEEDBACK_foc_len, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CAMERA_FEEDBACK_img_idx, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.CAMERA_FEEDBACK_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.CAMERA_FEEDBACK_cam_idx, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.CAMERA_FEEDBACK_flags, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type BATTERY2
function payload_fns.payload_181(buffer, tree, msgid, offset)
    tree:add_le(f.BATTERY2_voltage, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY2_current_battery, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type AHRS3
function payload_fns.payload_182(buffer, tree, msgid, offset)
    tree:add_le(f.AHRS3_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS3_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS3_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS3_altitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS3_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS3_lng, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS3_v1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS3_v2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS3_v3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AHRS3_v4, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type AUTOPILOT_VERSION_REQUEST
function payload_fns.payload_183(buffer, tree, msgid, offset)
    tree:add_le(f.AUTOPILOT_VERSION_REQUEST_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_REQUEST_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type REMOTE_LOG_DATA_BLOCK
function payload_fns.payload_184(buffer, tree, msgid, offset)
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_seqno, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_69, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_70, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_71, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_72, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_73, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_74, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_75, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_76, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_77, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_78, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_79, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_80, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_81, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_82, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_83, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_84, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_85, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_86, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_87, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_88, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_89, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_90, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_91, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_92, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_93, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_94, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_95, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_96, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_97, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_98, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_99, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_100, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_101, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_102, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_103, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_104, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_105, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_106, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_107, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_108, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_109, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_110, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_111, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_112, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_113, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_114, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_115, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_116, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_117, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_118, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_119, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_120, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_121, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_122, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_123, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_124, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_125, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_126, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_127, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_128, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_129, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_130, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_131, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_132, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_133, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_134, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_135, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_136, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_137, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_138, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_139, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_140, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_141, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_142, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_143, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_144, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_145, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_146, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_147, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_148, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_149, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_150, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_151, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_152, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_153, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_154, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_155, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_156, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_157, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_158, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_159, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_160, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_161, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_162, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_163, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_164, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_165, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_166, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_167, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_168, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_169, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_170, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_171, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_172, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_173, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_174, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_175, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_176, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_177, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_178, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_179, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_180, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_181, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_182, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_183, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_184, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_185, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_186, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_187, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_188, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_189, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_190, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_191, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_192, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_193, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_194, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_195, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_196, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_197, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_198, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_DATA_BLOCK_data_199, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type REMOTE_LOG_BLOCK_STATUS
function payload_fns.payload_185(buffer, tree, msgid, offset)
    tree:add_le(f.REMOTE_LOG_BLOCK_STATUS_seqno, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.REMOTE_LOG_BLOCK_STATUS_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_BLOCK_STATUS_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REMOTE_LOG_BLOCK_STATUS_status, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type LED_CONTROL
function payload_fns.payload_186(buffer, tree, msgid, offset)
    tree:add_le(f.LED_CONTROL_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_instance, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_pattern, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_len, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LED_CONTROL_custom_bytes_23, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MAG_CAL_PROGRESS
function payload_fns.payload_191(buffer, tree, msgid, offset)
    tree:add_le(f.MAG_CAL_PROGRESS_direction_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_PROGRESS_direction_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_PROGRESS_direction_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_PROGRESS_compass_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_cal_mask, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_cal_status, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_attempt, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_pct, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_mask_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_mask_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_mask_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_mask_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_mask_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_mask_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_mask_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_mask_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_mask_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_PROGRESS_completion_mask_9, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MAG_CAL_REPORT
function payload_fns.payload_192(buffer, tree, msgid, offset)
    tree:add_le(f.MAG_CAL_REPORT_fitness, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_REPORT_ofs_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_REPORT_ofs_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_REPORT_ofs_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_REPORT_diag_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_REPORT_diag_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_REPORT_diag_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_REPORT_offdiag_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_REPORT_offdiag_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_REPORT_offdiag_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MAG_CAL_REPORT_compass_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_REPORT_cal_mask, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_REPORT_cal_status, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MAG_CAL_REPORT_autosaved, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type EKF_STATUS_REPORT
function payload_fns.payload_193(buffer, tree, msgid, offset)
    tree:add_le(f.EKF_STATUS_REPORT_velocity_variance, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.EKF_STATUS_REPORT_pos_horiz_variance, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.EKF_STATUS_REPORT_pos_vert_variance, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.EKF_STATUS_REPORT_compass_variance, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.EKF_STATUS_REPORT_terrain_alt_variance, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.EKF_STATUS_REPORT_flags, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type PID_TUNING
function payload_fns.payload_194(buffer, tree, msgid, offset)
    tree:add_le(f.PID_TUNING_desired, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PID_TUNING_achieved, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PID_TUNING_FF, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PID_TUNING_P, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PID_TUNING_I, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PID_TUNING_D, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PID_TUNING_axis, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GIMBAL_REPORT
function payload_fns.payload_200(buffer, tree, msgid, offset)
    tree:add_le(f.GIMBAL_REPORT_delta_time, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_REPORT_delta_angle_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_REPORT_delta_angle_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_REPORT_delta_angle_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_REPORT_delta_velocity_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_REPORT_delta_velocity_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_REPORT_delta_velocity_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_REPORT_joint_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_REPORT_joint_el, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_REPORT_joint_az, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_REPORT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GIMBAL_REPORT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GIMBAL_CONTROL
function payload_fns.payload_201(buffer, tree, msgid, offset)
    tree:add_le(f.GIMBAL_CONTROL_demanded_rate_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_CONTROL_demanded_rate_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_CONTROL_demanded_rate_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GIMBAL_CONTROL_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GIMBAL_CONTROL_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GIMBAL_TORQUE_CMD_REPORT
function payload_fns.payload_214(buffer, tree, msgid, offset)
    tree:add_le(f.GIMBAL_TORQUE_CMD_REPORT_rl_torque_cmd, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GIMBAL_TORQUE_CMD_REPORT_el_torque_cmd, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GIMBAL_TORQUE_CMD_REPORT_az_torque_cmd, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GIMBAL_TORQUE_CMD_REPORT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GIMBAL_TORQUE_CMD_REPORT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GOPRO_HEARTBEAT
function payload_fns.payload_215(buffer, tree, msgid, offset)
    tree:add_le(f.GOPRO_HEARTBEAT_status, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_HEARTBEAT_capture_mode, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_HEARTBEAT_flags, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GOPRO_GET_REQUEST
function payload_fns.payload_216(buffer, tree, msgid, offset)
    tree:add_le(f.GOPRO_GET_REQUEST_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_GET_REQUEST_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_GET_REQUEST_cmd_id, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GOPRO_GET_RESPONSE
function payload_fns.payload_217(buffer, tree, msgid, offset)
    tree:add_le(f.GOPRO_GET_RESPONSE_cmd_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_GET_RESPONSE_status, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_GET_RESPONSE_value_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_GET_RESPONSE_value_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_GET_RESPONSE_value_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_GET_RESPONSE_value_3, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GOPRO_SET_REQUEST
function payload_fns.payload_218(buffer, tree, msgid, offset)
    tree:add_le(f.GOPRO_SET_REQUEST_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_SET_REQUEST_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_SET_REQUEST_cmd_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_SET_REQUEST_value_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_SET_REQUEST_value_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_SET_REQUEST_value_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_SET_REQUEST_value_3, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GOPRO_SET_RESPONSE
function payload_fns.payload_219(buffer, tree, msgid, offset)
    tree:add_le(f.GOPRO_SET_RESPONSE_cmd_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GOPRO_SET_RESPONSE_status, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type RPM
function payload_fns.payload_226(buffer, tree, msgid, offset)
    tree:add_le(f.RPM_rpm1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.RPM_rpm2, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type HEARTBEAT
function payload_fns.payload_0(buffer, tree, msgid, offset)
    tree:add_le(f.HEARTBEAT_custom_mode, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HEARTBEAT_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HEARTBEAT_autopilot, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HEARTBEAT_base_mode, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HEARTBEAT_system_status, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HEARTBEAT_mavlink_version, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SYS_STATUS
function payload_fns.payload_1(buffer, tree, msgid, offset)
    tree:add_le(f.SYS_STATUS_onboard_control_sensors_present, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SYS_STATUS_onboard_control_sensors_enabled, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SYS_STATUS_onboard_control_sensors_health, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SYS_STATUS_load, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SYS_STATUS_voltage_battery, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SYS_STATUS_current_battery, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SYS_STATUS_drop_rate_comm, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SYS_STATUS_errors_comm, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SYS_STATUS_errors_count1, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SYS_STATUS_errors_count2, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SYS_STATUS_errors_count3, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SYS_STATUS_errors_count4, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SYS_STATUS_battery_remaining, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SYSTEM_TIME
function payload_fns.payload_2(buffer, tree, msgid, offset)
    tree:add_le(f.SYSTEM_TIME_time_unix_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.SYSTEM_TIME_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type PING
function payload_fns.payload_4(buffer, tree, msgid, offset)
    tree:add_le(f.PING_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.PING_seq, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PING_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.PING_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type CHANGE_OPERATOR_CONTROL
function payload_fns.payload_5(buffer, tree, msgid, offset)
    tree:add_le(f.CHANGE_OPERATOR_CONTROL_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.CHANGE_OPERATOR_CONTROL_control_request, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.CHANGE_OPERATOR_CONTROL_version, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.CHANGE_OPERATOR_CONTROL_passkey, buffer(offset, 25))
    offset = offset + 25
    
    return offset
end


-- dissect payload of message type CHANGE_OPERATOR_CONTROL_ACK
function payload_fns.payload_6(buffer, tree, msgid, offset)
    tree:add_le(f.CHANGE_OPERATOR_CONTROL_ACK_gcs_system_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.CHANGE_OPERATOR_CONTROL_ACK_control_request, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.CHANGE_OPERATOR_CONTROL_ACK_ack, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type AUTH_KEY
function payload_fns.payload_7(buffer, tree, msgid, offset)
    tree:add_le(f.AUTH_KEY_key, buffer(offset, 32))
    offset = offset + 32
    
    return offset
end


-- dissect payload of message type SET_MODE
function payload_fns.payload_11(buffer, tree, msgid, offset)
    tree:add_le(f.SET_MODE_custom_mode, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_MODE_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SET_MODE_base_mode, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type PARAM_REQUEST_READ
function payload_fns.payload_20(buffer, tree, msgid, offset)
    tree:add_le(f.PARAM_REQUEST_READ_param_index, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.PARAM_REQUEST_READ_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.PARAM_REQUEST_READ_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.PARAM_REQUEST_READ_param_id, buffer(offset, 16))
    offset = offset + 16
    
    return offset
end


-- dissect payload of message type PARAM_REQUEST_LIST
function payload_fns.payload_21(buffer, tree, msgid, offset)
    tree:add_le(f.PARAM_REQUEST_LIST_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.PARAM_REQUEST_LIST_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type PARAM_VALUE
function payload_fns.payload_22(buffer, tree, msgid, offset)
    tree:add_le(f.PARAM_VALUE_param_value, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PARAM_VALUE_param_count, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.PARAM_VALUE_param_index, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.PARAM_VALUE_param_id, buffer(offset, 16))
    offset = offset + 16
    
    tree:add_le(f.PARAM_VALUE_param_type, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type PARAM_SET
function payload_fns.payload_23(buffer, tree, msgid, offset)
    tree:add_le(f.PARAM_SET_param_value, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PARAM_SET_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.PARAM_SET_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.PARAM_SET_param_id, buffer(offset, 16))
    offset = offset + 16
    
    tree:add_le(f.PARAM_SET_param_type, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GPS_RAW_INT
function payload_fns.payload_24(buffer, tree, msgid, offset)
    tree:add_le(f.GPS_RAW_INT_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.GPS_RAW_INT_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_RAW_INT_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_RAW_INT_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_RAW_INT_eph, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS_RAW_INT_epv, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS_RAW_INT_vel, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS_RAW_INT_cog, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS_RAW_INT_fix_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RAW_INT_satellites_visible, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GPS_STATUS
function payload_fns.payload_25(buffer, tree, msgid, offset)
    tree:add_le(f.GPS_STATUS_satellites_visible, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_prn_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_used_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_elevation_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_azimuth_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_STATUS_satellite_snr_19, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SCALED_IMU
function payload_fns.payload_26(buffer, tree, msgid, offset)
    tree:add_le(f.SCALED_IMU_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_IMU_xacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU_yacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU_zacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU_xgyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU_ygyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU_zgyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU_xmag, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU_ymag, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU_zmag, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type RAW_IMU
function payload_fns.payload_27(buffer, tree, msgid, offset)
    tree:add_le(f.RAW_IMU_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.RAW_IMU_xacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_IMU_yacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_IMU_zacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_IMU_xgyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_IMU_ygyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_IMU_zgyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_IMU_xmag, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_IMU_ymag, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_IMU_zmag, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type RAW_PRESSURE
function payload_fns.payload_28(buffer, tree, msgid, offset)
    tree:add_le(f.RAW_PRESSURE_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.RAW_PRESSURE_press_abs, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_PRESSURE_press_diff1, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_PRESSURE_press_diff2, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RAW_PRESSURE_temperature, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type SCALED_PRESSURE
function payload_fns.payload_29(buffer, tree, msgid, offset)
    tree:add_le(f.SCALED_PRESSURE_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_PRESSURE_press_abs, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_PRESSURE_press_diff, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_PRESSURE_temperature, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type ATTITUDE
function payload_fns.payload_30(buffer, tree, msgid, offset)
    tree:add_le(f.ATTITUDE_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_rollspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_pitchspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_yawspeed, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type ATTITUDE_QUATERNION
function payload_fns.payload_31(buffer, tree, msgid, offset)
    tree:add_le(f.ATTITUDE_QUATERNION_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_q1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_q2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_q3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_q4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_rollspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_pitchspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_yawspeed, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type LOCAL_POSITION_NED
function payload_fns.payload_32(buffer, tree, msgid, offset)
    tree:add_le(f.LOCAL_POSITION_NED_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_vx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_vy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_vz, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type GLOBAL_POSITION_INT
function payload_fns.payload_33(buffer, tree, msgid, offset)
    tree:add_le(f.GLOBAL_POSITION_INT_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_relative_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_vx, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GLOBAL_POSITION_INT_vy, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GLOBAL_POSITION_INT_vz, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GLOBAL_POSITION_INT_hdg, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type RC_CHANNELS_SCALED
function payload_fns.payload_34(buffer, tree, msgid, offset)
    tree:add_le(f.RC_CHANNELS_SCALED_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.RC_CHANNELS_SCALED_chan1_scaled, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_SCALED_chan2_scaled, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_SCALED_chan3_scaled, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_SCALED_chan4_scaled, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_SCALED_chan5_scaled, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_SCALED_chan6_scaled, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_SCALED_chan7_scaled, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_SCALED_chan8_scaled, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_SCALED_port, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RC_CHANNELS_SCALED_rssi, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type RC_CHANNELS_RAW
function payload_fns.payload_35(buffer, tree, msgid, offset)
    tree:add_le(f.RC_CHANNELS_RAW_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.RC_CHANNELS_RAW_chan1_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_RAW_chan2_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_RAW_chan3_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_RAW_chan4_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_RAW_chan5_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_RAW_chan6_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_RAW_chan7_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_RAW_chan8_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_RAW_port, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RC_CHANNELS_RAW_rssi, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SERVO_OUTPUT_RAW
function payload_fns.payload_36(buffer, tree, msgid, offset)
    tree:add_le(f.SERVO_OUTPUT_RAW_time_usec, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SERVO_OUTPUT_RAW_servo1_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SERVO_OUTPUT_RAW_servo2_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SERVO_OUTPUT_RAW_servo3_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SERVO_OUTPUT_RAW_servo4_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SERVO_OUTPUT_RAW_servo5_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SERVO_OUTPUT_RAW_servo6_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SERVO_OUTPUT_RAW_servo7_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SERVO_OUTPUT_RAW_servo8_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SERVO_OUTPUT_RAW_port, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_REQUEST_PARTIAL_LIST
function payload_fns.payload_37(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_REQUEST_PARTIAL_LIST_start_index, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_REQUEST_PARTIAL_LIST_end_index, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_REQUEST_PARTIAL_LIST_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_REQUEST_PARTIAL_LIST_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_WRITE_PARTIAL_LIST
function payload_fns.payload_38(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_WRITE_PARTIAL_LIST_start_index, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_WRITE_PARTIAL_LIST_end_index, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_WRITE_PARTIAL_LIST_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_WRITE_PARTIAL_LIST_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_ITEM
function payload_fns.payload_39(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_ITEM_param1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_param2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_param3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_param4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_seq, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_ITEM_command, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_ITEM_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_ITEM_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_ITEM_frame, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_ITEM_current, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_ITEM_autocontinue, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_REQUEST
function payload_fns.payload_40(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_REQUEST_seq, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_REQUEST_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_REQUEST_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_SET_CURRENT
function payload_fns.payload_41(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_SET_CURRENT_seq, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_SET_CURRENT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_SET_CURRENT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_CURRENT
function payload_fns.payload_42(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_CURRENT_seq, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type MISSION_REQUEST_LIST
function payload_fns.payload_43(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_REQUEST_LIST_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_REQUEST_LIST_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_COUNT
function payload_fns.payload_44(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_COUNT_count, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_COUNT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_COUNT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_CLEAR_ALL
function payload_fns.payload_45(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_CLEAR_ALL_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_CLEAR_ALL_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_ITEM_REACHED
function payload_fns.payload_46(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_ITEM_REACHED_seq, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type MISSION_ACK
function payload_fns.payload_47(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_ACK_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_ACK_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_ACK_type, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SET_GPS_GLOBAL_ORIGIN
function payload_fns.payload_48(buffer, tree, msgid, offset)
    tree:add_le(f.SET_GPS_GLOBAL_ORIGIN_latitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_GPS_GLOBAL_ORIGIN_longitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_GPS_GLOBAL_ORIGIN_altitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_GPS_GLOBAL_ORIGIN_target_system, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GPS_GLOBAL_ORIGIN
function payload_fns.payload_49(buffer, tree, msgid, offset)
    tree:add_le(f.GPS_GLOBAL_ORIGIN_latitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_GLOBAL_ORIGIN_longitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_GLOBAL_ORIGIN_altitude, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type PARAM_MAP_RC
function payload_fns.payload_50(buffer, tree, msgid, offset)
    tree:add_le(f.PARAM_MAP_RC_param_value0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PARAM_MAP_RC_scale, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PARAM_MAP_RC_param_value_min, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PARAM_MAP_RC_param_value_max, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.PARAM_MAP_RC_param_index, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.PARAM_MAP_RC_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.PARAM_MAP_RC_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.PARAM_MAP_RC_param_id, buffer(offset, 16))
    offset = offset + 16
    
    tree:add_le(f.PARAM_MAP_RC_parameter_rc_channel_index, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_REQUEST_INT
function payload_fns.payload_51(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_REQUEST_INT_seq, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_REQUEST_INT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_REQUEST_INT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SAFETY_SET_ALLOWED_AREA
function payload_fns.payload_54(buffer, tree, msgid, offset)
    tree:add_le(f.SAFETY_SET_ALLOWED_AREA_p1x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_SET_ALLOWED_AREA_p1y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_SET_ALLOWED_AREA_p1z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_SET_ALLOWED_AREA_p2x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_SET_ALLOWED_AREA_p2y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_SET_ALLOWED_AREA_p2z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_SET_ALLOWED_AREA_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SAFETY_SET_ALLOWED_AREA_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SAFETY_SET_ALLOWED_AREA_frame, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SAFETY_ALLOWED_AREA
function payload_fns.payload_55(buffer, tree, msgid, offset)
    tree:add_le(f.SAFETY_ALLOWED_AREA_p1x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_ALLOWED_AREA_p1y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_ALLOWED_AREA_p1z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_ALLOWED_AREA_p2x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_ALLOWED_AREA_p2y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_ALLOWED_AREA_p2z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SAFETY_ALLOWED_AREA_frame, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type ATTITUDE_QUATERNION_COV
function payload_fns.payload_61(buffer, tree, msgid, offset)
    tree:add_le(f.ATTITUDE_QUATERNION_COV_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_q_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_q_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_q_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_q_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_rollspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_pitchspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_yawspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_covariance_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_covariance_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_covariance_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_covariance_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_covariance_4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_covariance_5, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_covariance_6, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_covariance_7, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_QUATERNION_COV_covariance_8, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type NAV_CONTROLLER_OUTPUT
function payload_fns.payload_62(buffer, tree, msgid, offset)
    tree:add_le(f.NAV_CONTROLLER_OUTPUT_nav_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.NAV_CONTROLLER_OUTPUT_nav_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.NAV_CONTROLLER_OUTPUT_alt_error, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.NAV_CONTROLLER_OUTPUT_aspd_error, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.NAV_CONTROLLER_OUTPUT_xtrack_error, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.NAV_CONTROLLER_OUTPUT_nav_bearing, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.NAV_CONTROLLER_OUTPUT_target_bearing, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.NAV_CONTROLLER_OUTPUT_wp_dist, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type GLOBAL_POSITION_INT_COV
function payload_fns.payload_63(buffer, tree, msgid, offset)
    tree:add_le(f.GLOBAL_POSITION_INT_COV_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_relative_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_vx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_vy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_vz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_5, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_6, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_7, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_8, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_9, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_10, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_11, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_12, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_13, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_14, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_15, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_16, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_17, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_18, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_19, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_20, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_21, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_22, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_23, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_24, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_25, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_26, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_27, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_28, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_29, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_30, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_31, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_32, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_33, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_34, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_covariance_35, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_POSITION_INT_COV_estimator_type, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type LOCAL_POSITION_NED_COV
function payload_fns.payload_64(buffer, tree, msgid, offset)
    tree:add_le(f.LOCAL_POSITION_NED_COV_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_vx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_vy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_vz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_ax, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_ay, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_az, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_5, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_6, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_7, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_8, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_9, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_10, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_11, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_12, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_13, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_14, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_15, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_16, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_17, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_18, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_19, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_20, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_21, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_22, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_23, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_24, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_25, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_26, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_27, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_28, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_29, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_30, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_31, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_32, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_33, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_34, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_35, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_36, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_37, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_38, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_39, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_40, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_41, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_42, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_43, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_covariance_44, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_COV_estimator_type, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type RC_CHANNELS
function payload_fns.payload_65(buffer, tree, msgid, offset)
    tree:add_le(f.RC_CHANNELS_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.RC_CHANNELS_chan1_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan2_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan3_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan4_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan5_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan6_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan7_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan8_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan9_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan10_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan11_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan12_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan13_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan14_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan15_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan16_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan17_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chan18_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_chancount, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RC_CHANNELS_rssi, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type REQUEST_DATA_STREAM
function payload_fns.payload_66(buffer, tree, msgid, offset)
    tree:add_le(f.REQUEST_DATA_STREAM_req_message_rate, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.REQUEST_DATA_STREAM_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REQUEST_DATA_STREAM_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REQUEST_DATA_STREAM_req_stream_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.REQUEST_DATA_STREAM_start_stop, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type DATA_STREAM
function payload_fns.payload_67(buffer, tree, msgid, offset)
    tree:add_le(f.DATA_STREAM_message_rate, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.DATA_STREAM_stream_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA_STREAM_on_off, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MANUAL_CONTROL
function payload_fns.payload_69(buffer, tree, msgid, offset)
    tree:add_le(f.MANUAL_CONTROL_x, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MANUAL_CONTROL_y, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MANUAL_CONTROL_z, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MANUAL_CONTROL_r, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MANUAL_CONTROL_buttons, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MANUAL_CONTROL_target, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type RC_CHANNELS_OVERRIDE
function payload_fns.payload_70(buffer, tree, msgid, offset)
    tree:add_le(f.RC_CHANNELS_OVERRIDE_chan1_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_OVERRIDE_chan2_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_OVERRIDE_chan3_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_OVERRIDE_chan4_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_OVERRIDE_chan5_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_OVERRIDE_chan6_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_OVERRIDE_chan7_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_OVERRIDE_chan8_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RC_CHANNELS_OVERRIDE_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RC_CHANNELS_OVERRIDE_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MISSION_ITEM_INT
function payload_fns.payload_73(buffer, tree, msgid, offset)
    tree:add_le(f.MISSION_ITEM_INT_param1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_INT_param2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_INT_param3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_INT_param4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_INT_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_INT_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_INT_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MISSION_ITEM_INT_seq, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_ITEM_INT_command, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MISSION_ITEM_INT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_ITEM_INT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_ITEM_INT_frame, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_ITEM_INT_current, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MISSION_ITEM_INT_autocontinue, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type VFR_HUD
function payload_fns.payload_74(buffer, tree, msgid, offset)
    tree:add_le(f.VFR_HUD_airspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VFR_HUD_groundspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VFR_HUD_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VFR_HUD_climb, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VFR_HUD_heading, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.VFR_HUD_throttle, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type COMMAND_INT
function payload_fns.payload_75(buffer, tree, msgid, offset)
    tree:add_le(f.COMMAND_INT_param1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_INT_param2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_INT_param3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_INT_param4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_INT_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_INT_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_INT_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_INT_command, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.COMMAND_INT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.COMMAND_INT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.COMMAND_INT_frame, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.COMMAND_INT_current, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.COMMAND_INT_autocontinue, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type COMMAND_LONG
function payload_fns.payload_76(buffer, tree, msgid, offset)
    tree:add_le(f.COMMAND_LONG_param1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_LONG_param2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_LONG_param3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_LONG_param4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_LONG_param5, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_LONG_param6, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_LONG_param7, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COMMAND_LONG_command, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.COMMAND_LONG_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.COMMAND_LONG_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.COMMAND_LONG_confirmation, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type COMMAND_ACK
function payload_fns.payload_77(buffer, tree, msgid, offset)
    tree:add_le(f.COMMAND_ACK_command, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.COMMAND_ACK_result, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MANUAL_SETPOINT
function payload_fns.payload_81(buffer, tree, msgid, offset)
    tree:add_le(f.MANUAL_SETPOINT_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MANUAL_SETPOINT_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MANUAL_SETPOINT_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MANUAL_SETPOINT_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MANUAL_SETPOINT_thrust, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MANUAL_SETPOINT_mode_switch, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MANUAL_SETPOINT_manual_override_switch, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SET_ATTITUDE_TARGET
function payload_fns.payload_82(buffer, tree, msgid, offset)
    tree:add_le(f.SET_ATTITUDE_TARGET_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ATTITUDE_TARGET_q_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ATTITUDE_TARGET_q_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ATTITUDE_TARGET_q_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ATTITUDE_TARGET_q_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ATTITUDE_TARGET_body_roll_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ATTITUDE_TARGET_body_pitch_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ATTITUDE_TARGET_body_yaw_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ATTITUDE_TARGET_thrust, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ATTITUDE_TARGET_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SET_ATTITUDE_TARGET_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SET_ATTITUDE_TARGET_type_mask, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type ATTITUDE_TARGET
function payload_fns.payload_83(buffer, tree, msgid, offset)
    tree:add_le(f.ATTITUDE_TARGET_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_TARGET_q_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_TARGET_q_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_TARGET_q_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_TARGET_q_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_TARGET_body_roll_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_TARGET_body_pitch_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_TARGET_body_yaw_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_TARGET_thrust, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATTITUDE_TARGET_type_mask, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SET_POSITION_TARGET_LOCAL_NED
function payload_fns.payload_84(buffer, tree, msgid, offset)
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_vx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_vy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_vz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_afx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_afy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_afz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_yaw_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_type_mask, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SET_POSITION_TARGET_LOCAL_NED_coordinate_frame, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type POSITION_TARGET_LOCAL_NED
function payload_fns.payload_85(buffer, tree, msgid, offset)
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_vx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_vy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_vz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_afx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_afy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_afz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_yaw_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_type_mask, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.POSITION_TARGET_LOCAL_NED_coordinate_frame, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SET_POSITION_TARGET_GLOBAL_INT
function payload_fns.payload_86(buffer, tree, msgid, offset)
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_lat_int, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_lon_int, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_vx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_vy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_vz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_afx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_afy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_afz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_yaw_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_type_mask, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SET_POSITION_TARGET_GLOBAL_INT_coordinate_frame, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type POSITION_TARGET_GLOBAL_INT
function payload_fns.payload_87(buffer, tree, msgid, offset)
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_lat_int, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_lon_int, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_vx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_vy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_vz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_afx, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_afy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_afz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_yaw_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_type_mask, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.POSITION_TARGET_GLOBAL_INT_coordinate_frame, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET
function payload_fns.payload_89(buffer, tree, msgid, offset)
    tree:add_le(f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET_yaw, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type HIL_STATE
function payload_fns.payload_90(buffer, tree, msgid, offset)
    tree:add_le(f.HIL_STATE_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.HIL_STATE_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_rollspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_pitchspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_yawspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_vx, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_vy, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_vz, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_xacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_yacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_zacc, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type HIL_CONTROLS
function payload_fns.payload_91(buffer, tree, msgid, offset)
    tree:add_le(f.HIL_CONTROLS_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.HIL_CONTROLS_roll_ailerons, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_CONTROLS_pitch_elevator, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_CONTROLS_yaw_rudder, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_CONTROLS_throttle, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_CONTROLS_aux1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_CONTROLS_aux2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_CONTROLS_aux3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_CONTROLS_aux4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_CONTROLS_mode, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIL_CONTROLS_nav_mode, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type HIL_RC_INPUTS_RAW
function payload_fns.payload_92(buffer, tree, msgid, offset)
    tree:add_le(f.HIL_RC_INPUTS_RAW_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan1_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan2_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan3_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan4_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan5_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan6_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan7_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan8_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan9_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan10_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan11_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_chan12_raw, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_RC_INPUTS_RAW_rssi, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type HIL_ACTUATOR_CONTROLS
function payload_fns.payload_93(buffer, tree, msgid, offset)
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_flags, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_5, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_6, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_7, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_8, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_9, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_10, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_11, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_12, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_13, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_14, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_controls_15, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_ACTUATOR_CONTROLS_mode, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type OPTICAL_FLOW
function payload_fns.payload_100(buffer, tree, msgid, offset)
    tree:add_le(f.OPTICAL_FLOW_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.OPTICAL_FLOW_flow_comp_m_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_flow_comp_m_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_ground_distance, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_flow_x, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.OPTICAL_FLOW_flow_y, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.OPTICAL_FLOW_sensor_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.OPTICAL_FLOW_quality, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GLOBAL_VISION_POSITION_ESTIMATE
function payload_fns.payload_101(buffer, tree, msgid, offset)
    tree:add_le(f.GLOBAL_VISION_POSITION_ESTIMATE_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.GLOBAL_VISION_POSITION_ESTIMATE_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_VISION_POSITION_ESTIMATE_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_VISION_POSITION_ESTIMATE_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_VISION_POSITION_ESTIMATE_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_VISION_POSITION_ESTIMATE_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GLOBAL_VISION_POSITION_ESTIMATE_yaw, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type VISION_POSITION_ESTIMATE
function payload_fns.payload_102(buffer, tree, msgid, offset)
    tree:add_le(f.VISION_POSITION_ESTIMATE_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.VISION_POSITION_ESTIMATE_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VISION_POSITION_ESTIMATE_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VISION_POSITION_ESTIMATE_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VISION_POSITION_ESTIMATE_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VISION_POSITION_ESTIMATE_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VISION_POSITION_ESTIMATE_yaw, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type VISION_SPEED_ESTIMATE
function payload_fns.payload_103(buffer, tree, msgid, offset)
    tree:add_le(f.VISION_SPEED_ESTIMATE_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.VISION_SPEED_ESTIMATE_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VISION_SPEED_ESTIMATE_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VISION_SPEED_ESTIMATE_z, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type VICON_POSITION_ESTIMATE
function payload_fns.payload_104(buffer, tree, msgid, offset)
    tree:add_le(f.VICON_POSITION_ESTIMATE_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.VICON_POSITION_ESTIMATE_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VICON_POSITION_ESTIMATE_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VICON_POSITION_ESTIMATE_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VICON_POSITION_ESTIMATE_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VICON_POSITION_ESTIMATE_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VICON_POSITION_ESTIMATE_yaw, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type HIGHRES_IMU
function payload_fns.payload_105(buffer, tree, msgid, offset)
    tree:add_le(f.HIGHRES_IMU_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.HIGHRES_IMU_xacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_yacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_zacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_xgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_ygyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_zgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_xmag, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_ymag, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_zmag, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_abs_pressure, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_diff_pressure, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_pressure_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_temperature, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGHRES_IMU_fields_updated, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type OPTICAL_FLOW_RAD
function payload_fns.payload_106(buffer, tree, msgid, offset)
    tree:add_le(f.OPTICAL_FLOW_RAD_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.OPTICAL_FLOW_RAD_integration_time_us, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_RAD_integrated_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_RAD_integrated_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_RAD_integrated_xgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_RAD_integrated_ygyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_RAD_integrated_zgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_RAD_time_delta_distance_us, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_RAD_distance, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.OPTICAL_FLOW_RAD_temperature, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.OPTICAL_FLOW_RAD_sensor_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.OPTICAL_FLOW_RAD_quality, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type HIL_SENSOR
function payload_fns.payload_107(buffer, tree, msgid, offset)
    tree:add_le(f.HIL_SENSOR_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.HIL_SENSOR_xacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_yacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_zacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_xgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_ygyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_zgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_xmag, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_ymag, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_zmag, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_abs_pressure, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_diff_pressure, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_pressure_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_temperature, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_SENSOR_fields_updated, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type SIM_STATE
function payload_fns.payload_108(buffer, tree, msgid, offset)
    tree:add_le(f.SIM_STATE_q1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_q2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_q3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_q4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_roll, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_pitch, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_yaw, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_xacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_yacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_zacc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_xgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_ygyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_zgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_std_dev_horz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_std_dev_vert, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_vn, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_ve, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SIM_STATE_vd, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type RADIO_STATUS
function payload_fns.payload_109(buffer, tree, msgid, offset)
    tree:add_le(f.RADIO_STATUS_rxerrors, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RADIO_STATUS_fixed, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.RADIO_STATUS_rssi, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RADIO_STATUS_remrssi, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RADIO_STATUS_txbuf, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RADIO_STATUS_noise, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RADIO_STATUS_remnoise, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type FILE_TRANSFER_PROTOCOL
function payload_fns.payload_110(buffer, tree, msgid, offset)
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_target_network, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_69, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_70, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_71, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_72, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_73, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_74, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_75, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_76, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_77, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_78, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_79, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_80, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_81, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_82, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_83, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_84, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_85, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_86, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_87, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_88, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_89, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_90, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_91, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_92, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_93, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_94, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_95, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_96, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_97, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_98, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_99, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_100, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_101, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_102, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_103, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_104, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_105, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_106, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_107, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_108, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_109, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_110, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_111, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_112, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_113, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_114, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_115, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_116, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_117, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_118, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_119, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_120, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_121, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_122, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_123, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_124, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_125, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_126, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_127, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_128, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_129, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_130, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_131, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_132, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_133, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_134, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_135, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_136, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_137, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_138, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_139, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_140, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_141, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_142, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_143, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_144, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_145, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_146, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_147, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_148, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_149, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_150, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_151, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_152, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_153, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_154, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_155, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_156, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_157, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_158, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_159, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_160, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_161, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_162, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_163, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_164, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_165, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_166, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_167, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_168, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_169, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_170, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_171, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_172, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_173, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_174, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_175, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_176, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_177, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_178, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_179, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_180, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_181, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_182, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_183, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_184, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_185, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_186, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_187, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_188, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_189, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_190, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_191, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_192, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_193, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_194, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_195, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_196, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_197, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_198, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_199, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_200, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_201, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_202, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_203, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_204, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_205, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_206, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_207, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_208, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_209, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_210, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_211, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_212, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_213, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_214, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_215, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_216, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_217, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_218, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_219, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_220, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_221, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_222, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_223, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_224, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_225, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_226, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_227, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_228, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_229, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_230, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_231, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_232, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_233, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_234, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_235, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_236, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_237, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_238, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_239, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_240, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_241, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_242, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_243, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_244, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_245, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_246, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_247, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_248, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_249, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.FILE_TRANSFER_PROTOCOL_payload_250, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type TIMESYNC
function payload_fns.payload_111(buffer, tree, msgid, offset)
    tree:add_le(f.TIMESYNC_tc1, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.TIMESYNC_ts1, buffer(offset, 8))
    offset = offset + 8
    
    return offset
end


-- dissect payload of message type CAMERA_TRIGGER
function payload_fns.payload_112(buffer, tree, msgid, offset)
    tree:add_le(f.CAMERA_TRIGGER_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.CAMERA_TRIGGER_seq, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type HIL_GPS
function payload_fns.payload_113(buffer, tree, msgid, offset)
    tree:add_le(f.HIL_GPS_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.HIL_GPS_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_GPS_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_GPS_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_GPS_eph, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_GPS_epv, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_GPS_vel, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_GPS_vn, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_GPS_ve, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_GPS_vd, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_GPS_cog, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_GPS_fix_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIL_GPS_satellites_visible, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type HIL_OPTICAL_FLOW
function payload_fns.payload_114(buffer, tree, msgid, offset)
    tree:add_le(f.HIL_OPTICAL_FLOW_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.HIL_OPTICAL_FLOW_integration_time_us, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_OPTICAL_FLOW_integrated_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_OPTICAL_FLOW_integrated_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_OPTICAL_FLOW_integrated_xgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_OPTICAL_FLOW_integrated_ygyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_OPTICAL_FLOW_integrated_zgyro, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_OPTICAL_FLOW_time_delta_distance_us, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_OPTICAL_FLOW_distance, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_OPTICAL_FLOW_temperature, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_OPTICAL_FLOW_sensor_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIL_OPTICAL_FLOW_quality, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type HIL_STATE_QUATERNION
function payload_fns.payload_115(buffer, tree, msgid, offset)
    tree:add_le(f.HIL_STATE_QUATERNION_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.HIL_STATE_QUATERNION_attitude_quaternion_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_QUATERNION_attitude_quaternion_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_QUATERNION_attitude_quaternion_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_QUATERNION_attitude_quaternion_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_QUATERNION_rollspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_QUATERNION_pitchspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_QUATERNION_yawspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_QUATERNION_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_QUATERNION_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_QUATERNION_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIL_STATE_QUATERNION_vx, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_QUATERNION_vy, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_QUATERNION_vz, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_QUATERNION_ind_airspeed, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_QUATERNION_true_airspeed, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_QUATERNION_xacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_QUATERNION_yacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIL_STATE_QUATERNION_zacc, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type SCALED_IMU2
function payload_fns.payload_116(buffer, tree, msgid, offset)
    tree:add_le(f.SCALED_IMU2_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_IMU2_xacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU2_yacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU2_zacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU2_xgyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU2_ygyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU2_zgyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU2_xmag, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU2_ymag, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU2_zmag, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type LOG_REQUEST_LIST
function payload_fns.payload_117(buffer, tree, msgid, offset)
    tree:add_le(f.LOG_REQUEST_LIST_start, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.LOG_REQUEST_LIST_end, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.LOG_REQUEST_LIST_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_REQUEST_LIST_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type LOG_ENTRY
function payload_fns.payload_118(buffer, tree, msgid, offset)
    tree:add_le(f.LOG_ENTRY_time_utc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOG_ENTRY_size, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOG_ENTRY_id, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.LOG_ENTRY_num_logs, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.LOG_ENTRY_last_log_num, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type LOG_REQUEST_DATA
function payload_fns.payload_119(buffer, tree, msgid, offset)
    tree:add_le(f.LOG_REQUEST_DATA_ofs, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOG_REQUEST_DATA_count, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOG_REQUEST_DATA_id, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.LOG_REQUEST_DATA_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_REQUEST_DATA_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type LOG_DATA
function payload_fns.payload_120(buffer, tree, msgid, offset)
    tree:add_le(f.LOG_DATA_ofs, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LOG_DATA_id, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.LOG_DATA_count, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_69, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_70, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_71, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_72, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_73, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_74, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_75, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_76, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_77, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_78, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_79, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_80, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_81, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_82, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_83, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_84, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_85, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_86, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_87, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_88, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_DATA_data_89, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type LOG_ERASE
function payload_fns.payload_121(buffer, tree, msgid, offset)
    tree:add_le(f.LOG_ERASE_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_ERASE_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type LOG_REQUEST_END
function payload_fns.payload_122(buffer, tree, msgid, offset)
    tree:add_le(f.LOG_REQUEST_END_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LOG_REQUEST_END_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GPS_INJECT_DATA
function payload_fns.payload_123(buffer, tree, msgid, offset)
    tree:add_le(f.GPS_INJECT_DATA_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_len, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_69, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_70, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_71, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_72, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_73, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_74, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_75, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_76, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_77, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_78, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_79, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_80, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_81, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_82, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_83, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_84, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_85, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_86, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_87, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_88, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_89, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_90, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_91, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_92, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_93, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_94, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_95, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_96, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_97, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_98, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_99, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_100, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_101, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_102, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_103, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_104, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_105, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_106, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_107, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_108, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INJECT_DATA_data_109, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GPS2_RAW
function payload_fns.payload_124(buffer, tree, msgid, offset)
    tree:add_le(f.GPS2_RAW_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.GPS2_RAW_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RAW_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RAW_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RAW_dgps_age, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RAW_eph, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS2_RAW_epv, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS2_RAW_vel, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS2_RAW_cog, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS2_RAW_fix_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS2_RAW_satellites_visible, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS2_RAW_dgps_numch, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type POWER_STATUS
function payload_fns.payload_125(buffer, tree, msgid, offset)
    tree:add_le(f.POWER_STATUS_Vcc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.POWER_STATUS_Vservo, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.POWER_STATUS_flags, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type SERIAL_CONTROL
function payload_fns.payload_126(buffer, tree, msgid, offset)
    tree:add_le(f.SERIAL_CONTROL_baudrate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SERIAL_CONTROL_timeout, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SERIAL_CONTROL_device, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_flags, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_count, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SERIAL_CONTROL_data_69, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GPS_RTK
function payload_fns.payload_127(buffer, tree, msgid, offset)
    tree:add_le(f.GPS_RTK_time_last_baseline_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_RTK_tow, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_RTK_baseline_a_mm, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_RTK_baseline_b_mm, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_RTK_baseline_c_mm, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_RTK_accuracy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_RTK_iar_num_hypotheses, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_RTK_wn, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS_RTK_rtk_receiver_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTK_rtk_health, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTK_rtk_rate, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTK_nsats, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTK_baseline_coords_type, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GPS2_RTK
function payload_fns.payload_128(buffer, tree, msgid, offset)
    tree:add_le(f.GPS2_RTK_time_last_baseline_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RTK_tow, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RTK_baseline_a_mm, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RTK_baseline_b_mm, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RTK_baseline_c_mm, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RTK_accuracy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RTK_iar_num_hypotheses, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS2_RTK_wn, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS2_RTK_rtk_receiver_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS2_RTK_rtk_health, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS2_RTK_rtk_rate, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS2_RTK_nsats, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS2_RTK_baseline_coords_type, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SCALED_IMU3
function payload_fns.payload_129(buffer, tree, msgid, offset)
    tree:add_le(f.SCALED_IMU3_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_IMU3_xacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU3_yacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU3_zacc, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU3_xgyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU3_ygyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU3_zgyro, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU3_xmag, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU3_ymag, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.SCALED_IMU3_zmag, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type DATA_TRANSMISSION_HANDSHAKE
function payload_fns.payload_130(buffer, tree, msgid, offset)
    tree:add_le(f.DATA_TRANSMISSION_HANDSHAKE_size, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.DATA_TRANSMISSION_HANDSHAKE_width, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.DATA_TRANSMISSION_HANDSHAKE_height, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.DATA_TRANSMISSION_HANDSHAKE_packets, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.DATA_TRANSMISSION_HANDSHAKE_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA_TRANSMISSION_HANDSHAKE_payload, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DATA_TRANSMISSION_HANDSHAKE_jpg_quality, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type ENCAPSULATED_DATA
function payload_fns.payload_131(buffer, tree, msgid, offset)
    tree:add_le(f.ENCAPSULATED_DATA_seqnr, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.ENCAPSULATED_DATA_data_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_69, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_70, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_71, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_72, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_73, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_74, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_75, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_76, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_77, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_78, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_79, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_80, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_81, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_82, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_83, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_84, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_85, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_86, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_87, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_88, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_89, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_90, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_91, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_92, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_93, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_94, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_95, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_96, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_97, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_98, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_99, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_100, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_101, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_102, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_103, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_104, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_105, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_106, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_107, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_108, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_109, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_110, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_111, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_112, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_113, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_114, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_115, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_116, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_117, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_118, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_119, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_120, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_121, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_122, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_123, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_124, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_125, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_126, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_127, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_128, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_129, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_130, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_131, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_132, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_133, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_134, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_135, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_136, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_137, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_138, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_139, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_140, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_141, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_142, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_143, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_144, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_145, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_146, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_147, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_148, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_149, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_150, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_151, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_152, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_153, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_154, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_155, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_156, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_157, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_158, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_159, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_160, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_161, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_162, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_163, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_164, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_165, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_166, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_167, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_168, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_169, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_170, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_171, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_172, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_173, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_174, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_175, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_176, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_177, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_178, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_179, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_180, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_181, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_182, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_183, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_184, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_185, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_186, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_187, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_188, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_189, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_190, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_191, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_192, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_193, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_194, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_195, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_196, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_197, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_198, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_199, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_200, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_201, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_202, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_203, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_204, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_205, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_206, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_207, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_208, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_209, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_210, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_211, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_212, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_213, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_214, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_215, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_216, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_217, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_218, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_219, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_220, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_221, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_222, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_223, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_224, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_225, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_226, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_227, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_228, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_229, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_230, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_231, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_232, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_233, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_234, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_235, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_236, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_237, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_238, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_239, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_240, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_241, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_242, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_243, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_244, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_245, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_246, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_247, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_248, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_249, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_250, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_251, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ENCAPSULATED_DATA_data_252, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type DISTANCE_SENSOR
function payload_fns.payload_132(buffer, tree, msgid, offset)
    tree:add_le(f.DISTANCE_SENSOR_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.DISTANCE_SENSOR_min_distance, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.DISTANCE_SENSOR_max_distance, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.DISTANCE_SENSOR_current_distance, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.DISTANCE_SENSOR_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DISTANCE_SENSOR_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DISTANCE_SENSOR_orientation, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.DISTANCE_SENSOR_covariance, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type TERRAIN_REQUEST
function payload_fns.payload_133(buffer, tree, msgid, offset)
    tree:add_le(f.TERRAIN_REQUEST_mask, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.TERRAIN_REQUEST_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.TERRAIN_REQUEST_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.TERRAIN_REQUEST_grid_spacing, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type TERRAIN_DATA
function payload_fns.payload_134(buffer, tree, msgid, offset)
    tree:add_le(f.TERRAIN_DATA_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.TERRAIN_DATA_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.TERRAIN_DATA_grid_spacing, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_0, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_1, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_2, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_3, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_4, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_5, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_6, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_7, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_8, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_9, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_10, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_11, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_12, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_13, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_14, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_data_15, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_DATA_gridbit, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type TERRAIN_CHECK
function payload_fns.payload_135(buffer, tree, msgid, offset)
    tree:add_le(f.TERRAIN_CHECK_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.TERRAIN_CHECK_lon, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type TERRAIN_REPORT
function payload_fns.payload_136(buffer, tree, msgid, offset)
    tree:add_le(f.TERRAIN_REPORT_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.TERRAIN_REPORT_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.TERRAIN_REPORT_terrain_height, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.TERRAIN_REPORT_current_height, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.TERRAIN_REPORT_spacing, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_REPORT_pending, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.TERRAIN_REPORT_loaded, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type SCALED_PRESSURE2
function payload_fns.payload_137(buffer, tree, msgid, offset)
    tree:add_le(f.SCALED_PRESSURE2_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_PRESSURE2_press_abs, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_PRESSURE2_press_diff, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_PRESSURE2_temperature, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type ATT_POS_MOCAP
function payload_fns.payload_138(buffer, tree, msgid, offset)
    tree:add_le(f.ATT_POS_MOCAP_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.ATT_POS_MOCAP_q_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATT_POS_MOCAP_q_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATT_POS_MOCAP_q_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATT_POS_MOCAP_q_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATT_POS_MOCAP_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATT_POS_MOCAP_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ATT_POS_MOCAP_z, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type SET_ACTUATOR_CONTROL_TARGET
function payload_fns.payload_139(buffer, tree, msgid, offset)
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_controls_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_controls_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_controls_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_controls_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_controls_4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_controls_5, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_controls_6, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_controls_7, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_group_mlx, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.SET_ACTUATOR_CONTROL_TARGET_target_component, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type ACTUATOR_CONTROL_TARGET
function payload_fns.payload_140(buffer, tree, msgid, offset)
    tree:add_le(f.ACTUATOR_CONTROL_TARGET_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.ACTUATOR_CONTROL_TARGET_controls_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ACTUATOR_CONTROL_TARGET_controls_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ACTUATOR_CONTROL_TARGET_controls_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ACTUATOR_CONTROL_TARGET_controls_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ACTUATOR_CONTROL_TARGET_controls_4, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ACTUATOR_CONTROL_TARGET_controls_5, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ACTUATOR_CONTROL_TARGET_controls_6, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ACTUATOR_CONTROL_TARGET_controls_7, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ACTUATOR_CONTROL_TARGET_group_mlx, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type ALTITUDE
function payload_fns.payload_141(buffer, tree, msgid, offset)
    tree:add_le(f.ALTITUDE_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.ALTITUDE_altitude_monotonic, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ALTITUDE_altitude_amsl, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ALTITUDE_altitude_local, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ALTITUDE_altitude_relative, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ALTITUDE_altitude_terrain, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ALTITUDE_bottom_clearance, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type RESOURCE_REQUEST
function payload_fns.payload_142(buffer, tree, msgid, offset)
    tree:add_le(f.RESOURCE_REQUEST_request_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_69, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_70, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_71, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_72, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_73, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_74, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_75, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_76, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_77, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_78, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_79, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_80, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_81, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_82, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_83, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_84, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_85, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_86, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_87, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_88, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_89, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_90, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_91, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_92, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_93, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_94, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_95, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_96, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_97, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_98, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_99, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_100, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_101, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_102, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_103, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_104, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_105, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_106, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_107, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_108, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_109, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_110, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_111, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_112, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_113, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_114, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_115, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_116, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_117, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_118, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_uri_119, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_transfer_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_69, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_70, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_71, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_72, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_73, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_74, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_75, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_76, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_77, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_78, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_79, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_80, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_81, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_82, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_83, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_84, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_85, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_86, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_87, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_88, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_89, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_90, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_91, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_92, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_93, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_94, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_95, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_96, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_97, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_98, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_99, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_100, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_101, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_102, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_103, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_104, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_105, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_106, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_107, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_108, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_109, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_110, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_111, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_112, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_113, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_114, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_115, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_116, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_117, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_118, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.RESOURCE_REQUEST_storage_119, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type SCALED_PRESSURE3
function payload_fns.payload_143(buffer, tree, msgid, offset)
    tree:add_le(f.SCALED_PRESSURE3_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_PRESSURE3_press_abs, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_PRESSURE3_press_diff, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SCALED_PRESSURE3_temperature, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type FOLLOW_TARGET
function payload_fns.payload_144(buffer, tree, msgid, offset)
    tree:add_le(f.FOLLOW_TARGET_timestamp, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.FOLLOW_TARGET_custom_state, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.FOLLOW_TARGET_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_vel_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_vel_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_vel_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_acc_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_acc_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_acc_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_attitude_q_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_attitude_q_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_attitude_q_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_attitude_q_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_rates_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_rates_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_rates_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_position_cov_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_position_cov_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_position_cov_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.FOLLOW_TARGET_est_capabilities, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type CONTROL_SYSTEM_STATE
function payload_fns.payload_146(buffer, tree, msgid, offset)
    tree:add_le(f.CONTROL_SYSTEM_STATE_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_x_acc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_y_acc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_z_acc, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_x_vel, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_y_vel, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_z_vel, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_x_pos, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_y_pos, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_z_pos, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_airspeed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_vel_variance_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_vel_variance_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_vel_variance_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_pos_variance_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_pos_variance_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_pos_variance_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_q_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_q_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_q_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_q_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_roll_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_pitch_rate, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.CONTROL_SYSTEM_STATE_yaw_rate, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type BATTERY_STATUS
function payload_fns.payload_147(buffer, tree, msgid, offset)
    tree:add_le(f.BATTERY_STATUS_current_consumed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.BATTERY_STATUS_energy_consumed, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.BATTERY_STATUS_temperature, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_voltages_0, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_voltages_1, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_voltages_2, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_voltages_3, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_voltages_4, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_voltages_5, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_voltages_6, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_voltages_7, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_voltages_8, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_voltages_9, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_current_battery, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.BATTERY_STATUS_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.BATTERY_STATUS_battery_function, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.BATTERY_STATUS_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.BATTERY_STATUS_battery_remaining, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type AUTOPILOT_VERSION
function payload_fns.payload_148(buffer, tree, msgid, offset)
    tree:add_le(f.AUTOPILOT_VERSION_capabilities, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.AUTOPILOT_VERSION_uid, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.AUTOPILOT_VERSION_flight_sw_version, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AUTOPILOT_VERSION_middleware_sw_version, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AUTOPILOT_VERSION_os_sw_version, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AUTOPILOT_VERSION_board_version, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.AUTOPILOT_VERSION_vendor_id, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.AUTOPILOT_VERSION_product_id, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.AUTOPILOT_VERSION_flight_custom_version_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_flight_custom_version_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_flight_custom_version_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_flight_custom_version_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_flight_custom_version_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_flight_custom_version_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_flight_custom_version_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_flight_custom_version_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_middleware_custom_version_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_middleware_custom_version_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_middleware_custom_version_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_middleware_custom_version_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_middleware_custom_version_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_middleware_custom_version_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_middleware_custom_version_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_middleware_custom_version_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_os_custom_version_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_os_custom_version_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_os_custom_version_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_os_custom_version_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_os_custom_version_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_os_custom_version_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_os_custom_version_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.AUTOPILOT_VERSION_os_custom_version_7, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type LANDING_TARGET
function payload_fns.payload_149(buffer, tree, msgid, offset)
    tree:add_le(f.LANDING_TARGET_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.LANDING_TARGET_angle_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LANDING_TARGET_angle_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LANDING_TARGET_distance, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LANDING_TARGET_size_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LANDING_TARGET_size_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.LANDING_TARGET_target_num, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.LANDING_TARGET_frame, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type ESTIMATOR_STATUS
function payload_fns.payload_230(buffer, tree, msgid, offset)
    tree:add_le(f.ESTIMATOR_STATUS_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.ESTIMATOR_STATUS_vel_ratio, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ESTIMATOR_STATUS_pos_horiz_ratio, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ESTIMATOR_STATUS_pos_vert_ratio, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ESTIMATOR_STATUS_mag_ratio, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ESTIMATOR_STATUS_hagl_ratio, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ESTIMATOR_STATUS_tas_ratio, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ESTIMATOR_STATUS_pos_horiz_accuracy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ESTIMATOR_STATUS_pos_vert_accuracy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ESTIMATOR_STATUS_flags, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type WIND_COV
function payload_fns.payload_231(buffer, tree, msgid, offset)
    tree:add_le(f.WIND_COV_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.WIND_COV_wind_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.WIND_COV_wind_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.WIND_COV_wind_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.WIND_COV_var_horiz, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.WIND_COV_var_vert, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.WIND_COV_wind_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.WIND_COV_horiz_accuracy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.WIND_COV_vert_accuracy, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type GPS_INPUT
function payload_fns.payload_232(buffer, tree, msgid, offset)
    tree:add_le(f.GPS_INPUT_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.GPS_INPUT_time_week_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_alt, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_hdop, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_vdop, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_vn, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_ve, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_vd, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_speed_accuracy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_horiz_accuracy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_vert_accuracy, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.GPS_INPUT_ignore_flags, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS_INPUT_time_week, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.GPS_INPUT_gps_id, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INPUT_fix_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_INPUT_satellites_visible, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type GPS_RTCM_DATA
function payload_fns.payload_233(buffer, tree, msgid, offset)
    tree:add_le(f.GPS_RTCM_DATA_flags, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_len, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_69, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_70, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_71, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_72, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_73, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_74, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_75, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_76, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_77, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_78, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_79, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_80, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_81, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_82, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_83, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_84, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_85, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_86, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_87, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_88, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_89, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_90, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_91, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_92, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_93, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_94, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_95, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_96, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_97, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_98, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_99, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_100, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_101, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_102, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_103, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_104, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_105, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_106, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_107, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_108, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_109, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_110, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_111, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_112, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_113, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_114, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_115, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_116, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_117, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_118, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_119, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_120, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_121, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_122, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_123, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_124, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_125, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_126, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_127, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_128, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_129, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_130, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_131, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_132, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_133, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_134, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_135, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_136, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_137, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_138, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_139, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_140, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_141, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_142, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_143, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_144, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_145, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_146, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_147, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_148, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_149, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_150, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_151, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_152, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_153, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_154, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_155, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_156, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_157, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_158, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_159, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_160, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_161, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_162, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_163, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_164, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_165, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_166, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_167, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_168, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_169, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_170, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_171, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_172, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_173, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_174, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_175, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_176, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_177, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_178, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.GPS_RTCM_DATA_data_179, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type HIGH_LATENCY
function payload_fns.payload_234(buffer, tree, msgid, offset)
    tree:add_le(f.HIGH_LATENCY_custom_mode, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGH_LATENCY_latitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGH_LATENCY_longitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HIGH_LATENCY_roll, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIGH_LATENCY_pitch, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIGH_LATENCY_heading, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIGH_LATENCY_heading_sp, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIGH_LATENCY_altitude_amsl, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIGH_LATENCY_altitude_sp, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIGH_LATENCY_wp_distance, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.HIGH_LATENCY_base_mode, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_landed_state, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_throttle, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_airspeed, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_airspeed_sp, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_groundspeed, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_climb_rate, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_gps_nsat, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_gps_fix_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_battery_remaining, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_temperature, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_temperature_air, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_failsafe, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.HIGH_LATENCY_wp_num, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type VIBRATION
function payload_fns.payload_241(buffer, tree, msgid, offset)
    tree:add_le(f.VIBRATION_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.VIBRATION_vibration_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VIBRATION_vibration_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VIBRATION_vibration_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VIBRATION_clipping_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VIBRATION_clipping_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.VIBRATION_clipping_2, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type HOME_POSITION
function payload_fns.payload_242(buffer, tree, msgid, offset)
    tree:add_le(f.HOME_POSITION_latitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_longitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_altitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_q_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_q_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_q_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_q_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_approach_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_approach_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.HOME_POSITION_approach_z, buffer(offset, 4))
    offset = offset + 4
    
    return offset
end


-- dissect payload of message type SET_HOME_POSITION
function payload_fns.payload_243(buffer, tree, msgid, offset)
    tree:add_le(f.SET_HOME_POSITION_latitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_longitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_altitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_q_0, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_q_1, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_q_2, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_q_3, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_approach_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_approach_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_approach_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.SET_HOME_POSITION_target_system, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MESSAGE_INTERVAL
function payload_fns.payload_244(buffer, tree, msgid, offset)
    tree:add_le(f.MESSAGE_INTERVAL_interval_us, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.MESSAGE_INTERVAL_message_id, buffer(offset, 2))
    offset = offset + 2
    
    return offset
end


-- dissect payload of message type EXTENDED_SYS_STATE
function payload_fns.payload_245(buffer, tree, msgid, offset)
    tree:add_le(f.EXTENDED_SYS_STATE_vtol_state, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.EXTENDED_SYS_STATE_landed_state, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type ADSB_VEHICLE
function payload_fns.payload_246(buffer, tree, msgid, offset)
    tree:add_le(f.ADSB_VEHICLE_ICAO_address, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ADSB_VEHICLE_lat, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ADSB_VEHICLE_lon, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ADSB_VEHICLE_altitude, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.ADSB_VEHICLE_heading, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.ADSB_VEHICLE_hor_velocity, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.ADSB_VEHICLE_ver_velocity, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.ADSB_VEHICLE_flags, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.ADSB_VEHICLE_squawk, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.ADSB_VEHICLE_altitude_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ADSB_VEHICLE_callsign, buffer(offset, 9))
    offset = offset + 9
    
    tree:add_le(f.ADSB_VEHICLE_emitter_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.ADSB_VEHICLE_tslc, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type COLLISION
function payload_fns.payload_247(buffer, tree, msgid, offset)
    tree:add_le(f.COLLISION_id, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COLLISION_time_to_minimum_delta, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COLLISION_altitude_minimum_delta, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COLLISION_horizontal_minimum_delta, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.COLLISION_src, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.COLLISION_action, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.COLLISION_threat_level, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type V2_EXTENSION
function payload_fns.payload_248(buffer, tree, msgid, offset)
    tree:add_le(f.V2_EXTENSION_message_type, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.V2_EXTENSION_target_network, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_target_system, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_target_component, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_31, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_32, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_33, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_34, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_35, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_36, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_37, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_38, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_39, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_40, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_41, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_42, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_43, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_44, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_45, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_46, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_47, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_48, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_49, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_50, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_51, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_52, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_53, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_54, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_55, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_56, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_57, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_58, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_59, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_60, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_61, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_62, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_63, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_64, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_65, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_66, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_67, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_68, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_69, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_70, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_71, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_72, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_73, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_74, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_75, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_76, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_77, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_78, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_79, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_80, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_81, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_82, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_83, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_84, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_85, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_86, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_87, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_88, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_89, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_90, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_91, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_92, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_93, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_94, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_95, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_96, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_97, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_98, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_99, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_100, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_101, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_102, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_103, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_104, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_105, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_106, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_107, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_108, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_109, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_110, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_111, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_112, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_113, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_114, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_115, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_116, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_117, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_118, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_119, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_120, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_121, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_122, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_123, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_124, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_125, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_126, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_127, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_128, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_129, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_130, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_131, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_132, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_133, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_134, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_135, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_136, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_137, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_138, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_139, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_140, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_141, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_142, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_143, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_144, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_145, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_146, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_147, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_148, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_149, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_150, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_151, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_152, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_153, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_154, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_155, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_156, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_157, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_158, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_159, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_160, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_161, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_162, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_163, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_164, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_165, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_166, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_167, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_168, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_169, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_170, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_171, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_172, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_173, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_174, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_175, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_176, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_177, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_178, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_179, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_180, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_181, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_182, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_183, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_184, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_185, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_186, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_187, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_188, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_189, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_190, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_191, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_192, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_193, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_194, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_195, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_196, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_197, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_198, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_199, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_200, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_201, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_202, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_203, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_204, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_205, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_206, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_207, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_208, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_209, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_210, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_211, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_212, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_213, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_214, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_215, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_216, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_217, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_218, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_219, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_220, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_221, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_222, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_223, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_224, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_225, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_226, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_227, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_228, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_229, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_230, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_231, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_232, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_233, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_234, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_235, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_236, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_237, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_238, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_239, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_240, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_241, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_242, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_243, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_244, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_245, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_246, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_247, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.V2_EXTENSION_payload_248, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type MEMORY_VECT
function payload_fns.payload_249(buffer, tree, msgid, offset)
    tree:add_le(f.MEMORY_VECT_address, buffer(offset, 2))
    offset = offset + 2
    
    tree:add_le(f.MEMORY_VECT_ver, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_type, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_0, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_1, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_2, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_3, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_4, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_5, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_6, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_7, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_8, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_9, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_10, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_11, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_12, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_13, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_14, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_15, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_16, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_17, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_18, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_19, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_20, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_21, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_22, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_23, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_24, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_25, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_26, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_27, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_28, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_29, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_30, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.MEMORY_VECT_value_31, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end


-- dissect payload of message type DEBUG_VECT
function payload_fns.payload_250(buffer, tree, msgid, offset)
    tree:add_le(f.DEBUG_VECT_time_usec, buffer(offset, 8))
    offset = offset + 8
    
    tree:add_le(f.DEBUG_VECT_x, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.DEBUG_VECT_y, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.DEBUG_VECT_z, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.DEBUG_VECT_name, buffer(offset, 10))
    offset = offset + 10
    
    return offset
end


-- dissect payload of message type NAMED_VALUE_FLOAT
function payload_fns.payload_251(buffer, tree, msgid, offset)
    tree:add_le(f.NAMED_VALUE_FLOAT_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.NAMED_VALUE_FLOAT_value, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.NAMED_VALUE_FLOAT_name, buffer(offset, 10))
    offset = offset + 10
    
    return offset
end


-- dissect payload of message type NAMED_VALUE_INT
function payload_fns.payload_252(buffer, tree, msgid, offset)
    tree:add_le(f.NAMED_VALUE_INT_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.NAMED_VALUE_INT_value, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.NAMED_VALUE_INT_name, buffer(offset, 10))
    offset = offset + 10
    
    return offset
end


-- dissect payload of message type STATUSTEXT
function payload_fns.payload_253(buffer, tree, msgid, offset)
    tree:add_le(f.STATUSTEXT_severity, buffer(offset, 1))
    offset = offset + 1
    
    tree:add_le(f.STATUSTEXT_text, buffer(offset, 50))
    offset = offset + 50
    
    return offset
end


-- dissect payload of message type DEBUG
function payload_fns.payload_254(buffer, tree, msgid, offset)
    tree:add_le(f.DEBUG_time_boot_ms, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.DEBUG_value, buffer(offset, 4))
    offset = offset + 4
    
    tree:add_le(f.DEBUG_ind, buffer(offset, 1))
    offset = offset + 1
    
    return offset
end

lightdrone_msgName = {
    [1] = 'ANDLIGHT_MESSAGE_ID_DMX',
    [2] = 'ANDLIGHT_MESSAGE_ID_TIMESYNC_FINISH',
    [3] = 'ANDLIGHT_MESSAGE_ID_TIMESYNC',
    [4] = 'ANDLIGHT_MESSAGE_ID_RESEND',
    [5] = 'ANDLIGHT_MESSAGE_ID_EMERGENCY_PATH',
    [6] = 'ANDLIGHT_MESSAGE_ID_SELECT_RECEIVER',
    [7] = 'ANDLIGHT_MESSAGE_ID_VERSION',
    [8] = 'ANDLIGHT_MESSAGE_ID_BATTERY_STATUS',
    [9] = 'ANDLIGHT_MESSAGE_ID_BATTERY_CONFIG',
    [10] = 'ANDLIGHT_MESSAGE_ID_LIGHTBULB8',
    [11] = 'ANDLIGHT_MESSAGE_ID_LIGHTBULB16',
    [12] = 'ANDLIGHT_MESSAGE_ID___________',
    [13] = 'ANDLIGHT_MESSAGE_ID_RESTART_WIFI',
    [14] = 'ANDLIGHT_MESSAGE_ID_POSPLANNER',
    [18] = 'ANDLIGHT_MESSAGE_ID_SMART_DMX',
    [19] = 'ANDLIGHT_MESSAGE_ID_VERSION_WITH_TYPE',
    [20] = 'ANDLIGHT_MESSAGE_ID_STATUS',
    [21] = 'ANDLIGHT_MESSAGE_ID_GPSRTK',
}

lightdrone_proto = Proto("lightdrone_proto", "Lightdrone protocol")
l = lightdrone_proto.fields
l.payload = ProtoField.uint8("lightdrone_proto.payload", "Payload", base.DEC, lightdrone_msgName)

function lightdrone_type(buffer, offset)
    return buffer(offset + 1, 1):uint()
end

l.timesync_system_id = ProtoField.uint8("lightdrone_proto.timesync_system_id", "System id")
l.timesync_t0 = ProtoField.uint64("lightdrone_proto.T0", "T0")
l.timesync_t1 = ProtoField.uint64("lightdrone_proto.T1", "T1")
function lightdrone_msg_timesync(buffer, tree, offset)
    tree:add(l.timesync_system_id, buffer(offset+2, 1))
    tree:add(l.timesync_t0, buffer(offset+3, 8))
    tree:add(l.timesync_t1, buffer(offset+11, 8))
end

l.timesync_add_time = ProtoField.int64("lightdrone_proto.add_time", "Add time")
function lightdrone_msg_timesync_finish(buffer, tree, offset)
    tree:add(l.timesync_system_id, buffer(offset+2, 1))
    tree:add(l.timesync_add_time, buffer(offset+3, 8))
end

l.dmx_component_id = ProtoField.uint16("lightdrone_proto.component_id", "Component id")
l.dmx_value = ProtoField.uint8("lightdrone_proto.value", "Value")
l.dmx_time = ProtoField.uint32("lightdrone_proto.time", "Time")
l.dmx_send_id = ProtoField.uint16("lightdrone_proto.send_id", "Send id")
function lightdrone_msg_dmx(buffer, tree, offset)
    tree:add(l.dmx_component_id, buffer(offset+2, 2))
    tree:add(l.dmx_value, buffer(offset+4, 1))
    tree:add(l.dmx_time, buffer(offset+5, 4))
    tree:add(l.dmx_send_id, buffer(offset+9, 2))
end

function lightdrone_msg_smart_dmx(buffer, tree, offset)
    tree:add(l.dmx_time, buffer(offset+3, 4))
    tree:add(l.dmx_send_id, buffer(offset+7, 2))
    local amount = buffer(offset+2, 1):uint()
    for i=0,amount-1 do
        tree:add(l.dmx_component_id, buffer(offset+9+i*2, 1))
        tree:add(l.dmx_value, buffer(offset+9+i*2+1, 1))
    end
end

l.posplanner_time = ProtoField.uint32("lightdrone_proto.time", "Time")
function lightdrone_msg_posplanner_confirm(buffer, tree, offset)
    tree:add(l.posplanner_time, buffer(offset+3, 4))
end

function lightdrone_msg_posplanner_clear(buffer, tree, offset)
end

l.posplanner_segment_time = ProtoField.int64("lightdrone_proto.segment_time", "Segment time")
function lightdrone_msg_posplanner_segment_time(buffer, tree, offset)
    tree:add(l.posplanner_segment_time, buffer(offset+3, 8))
end

l.posplanner_amount = ProtoField.uint8("lightdrone_proto.x", "Amount")
l.posplanner_lat = ProtoField.float("lightdrone_proto.lat", "Reference Lat")
l.posplanner_lon = ProtoField.float("lightdrone_proto.lon", "Reference Lon")
l.posplanner_duration = ProtoField.uint32("lightdrone_proto.duration", "Bezier duration")
function lightdrone_msg_posplanner_plan(buffer, tree, offset)
    local amount = buffer(offset+3, 1):uint()
    tree:add(l.posplanner_amount, buffer(offset+3, 1))
    tree:add_le(l.posplanner_lat, buffer(offset+4, 4))
    tree:add_le(l.posplanner_lon, buffer(offset+8, 4))
    tree:add(l.posplanner_time, buffer(offset+12, 4))

    for i=0,amount-1 do
        tree:add(l.posplanner_duration, buffer(offset+16+i*34, 4))
    end
end

l.posplanner_state = ProtoField.uint8("lightdrone_proto.state", "State")
l.posplanner_starttime = ProtoField.uint16("lightdrone_proto.starttime", "Start time")
l.posplanner_bufsize = ProtoField.uint16("lightdrone_proto.bufsize", "Buffer size")
l.posplanner_speedx = ProtoField.int8("lightdrone_proto.speedx", "Speed X")
l.posplanner_speedy = ProtoField.int8("lightdrone_proto.speedy", "Speed Y")
l.posplanner_speedz = ProtoField.int8("lightdrone_proto.speedz", "Speed Z")
l.posplanner_distx = ProtoField.int8("lightdrone_proto.distx", "Dist X")
l.posplanner_disty = ProtoField.int8("lightdrone_proto.disty", "Dist Y")
l.posplanner_distz = ProtoField.int8("lightdrone_proto.distz", "Dist Z")
function lightdrone_msg_posplanner_status(buffer, tree, offset)
    tree:add(l.posplanner_state, buffer(offset+3, 1))
    tree:add(l.posplanner_starttime, buffer(offset+4, 2))
    tree:add(l.posplanner_bufsize, buffer(offset+6, 2))
    tree:add(l.posplanner_speedx, buffer(offset+8, 1))
    tree:add(l.posplanner_speedy, buffer(offset+9, 1))
    tree:add(l.posplanner_speedz, buffer(offset+10, 1))
    tree:add(l.posplanner_speedx, buffer(offset+11, 1))
    tree:add(l.posplanner_speedy, buffer(offset+12, 1))
    tree:add(l.posplanner_speedz, buffer(offset+13, 1))
end

lightdrone_posplanner_typeName = {
    [0] = 'POSPLANNER_TYPE_PLAN',
    [1] = 'POSPLANNER_TYPE_CONFIRM',
    [2] = 'POSPLANNER_TYPE_CLEAR',
    [3] = 'POSPLANNER_TYPE_SEGMENT_TIME',
    [4] = 'POSPLANNER_TYPE_STATUS',
}

function lightdrone_msg_posplanner(buffer, tree, offset)
    local subtype_ = buffer(offset+2, 1):uint()
    tree:add("Subtype: "..lightdrone_posplanner_typeName[subtype_]:lower(), buffer(offset+2, 1))

    if subtype_ == 0 then
        lightdrone_msg_posplanner_plan(buffer, tree, offset)
    elseif subtype_ == 1 then
        lightdrone_msg_posplanner_confirm(buffer, tree, offset)
    elseif subtype_ == 2 then
        lightdrone_msg_posplanner_clear(buffer, tree, offset)
    elseif subtype_ == 3 then
        lightdrone_msg_posplanner_segment_time(buffer, tree, offset)
    elseif subtype_ == 4 then
        lightdrone_msg_posplanner_status(buffer, tree, offset)
    end
end

function lightdrone_size(buffer, offset)
    local msg_id = lightdrone_type(buffer, offset)
    if msg_id == 1 then
      return 9 + 4;
    elseif msg_id == 2 then
      return 9 + 4;
    elseif msg_id == 3 then
      return 17 + 4;
    elseif msg_id == 4 then
      return 2 + 4;
    elseif msg_id == 6 then
      return 6 + 4;
    elseif msg_id == 7 then
      return 4 + 4;
    elseif msg_id == 19 then
      return 5 + 4;
    elseif msg_id == 20 then
      return 8 + 4;
    elseif msg_id == 10 then
      return 7;
    elseif msg_id == 21 then
      return buffer(offset+2,1):uint() + 4;
    elseif msg_id == 5 then
      return buffer(offset+3,1):uint() + 4;
    elseif msg_id == 14 then
      if buffer(offset+2, 1):uint() == 0 then
        return buffer(offset+3, 1):uint() * 34 + 14 + 4;
      elseif buffer(offset+2, 1):uint() == 1 then
          return 4 + 1 + 4;
      elseif buffer(offset+2, 1):uint() == 2 then
          return 5 + 4;
      elseif buffer(offset+2, 1):uint() == 3 then
          return 9 + 4;
      elseif buffer(offset+2, 1):uint() == 4 then
          return 11 + 1 + 4;
      end
    elseif msg_id == 18 then
      return 7 + buffer(offset+2,1):uint()*2 + 4;
   end
  return 1;
end

local msgCount = 0

function dissect_lightdrone(buffer, pinfo, tree, offset_start)
    local msg_id = lightdrone_type(buffer, offset_start)
    local offset = offset_start + lightdrone_size(buffer, offset_start)
    local subtree = tree:add(lightdrone_proto, "Lightdrone", buffer(offset_start, offset - offset_start))

    pinfo.cols.protocol = "Lightdrone"

    if msgCount == 1 then
        pinfo.cols.info = lightdrone_msgName[msg_id]
    else
        pinfo.cols.info:append("   "..lightdrone_msgName[msg_id])
    end

    subtree:add("Type: "..lightdrone_msgName[msg_id]:lower(), buffer(offset_start+1, 1))

    if msg_id == 1 then
        lightdrone_msg_dmx(buffer, subtree, offset_start)
    elseif msg_id == 2 then
        lightdrone_msg_timesync_finish(buffer, subtree, offset_start)
    elseif msg_id == 3 then
        lightdrone_msg_timesync(buffer, subtree, offset_start)
    elseif msg_id == 4 then
        lightdrone_msg_timesync(buffer, subtree, offset_start)
    elseif msg_id == 14 then
        lightdrone_msg_posplanner(buffer, subtree, offset_start)
    elseif msg_id == 18 then
        lightdrone_msg_smart_dmx(buffer, subtree, offset_start)
    end


    subtree:append_text(": "..lightdrone_msgName[msg_id]:lower():gsub("andlight_message_id_", ""))
    subtree:append_text(string.format(" (%d)", offset-offset_start))
    subtree:add (lightdrone_proto, buffer(offset_start,offset-offset_start), "raw bytes")

    return offset;
end

-- dissector function
function mavlink_proto.dissector(buffer,pinfo,tree)
    local offset = 0
    msgCount = 0
    
    -- loop through the buffer to extract all the messages in the buffer
    while (offset < buffer:len()) 
    do
        msgCount = msgCount + 1

        -- decode protocol version first
        local version = buffer(offset,1):uint()
        local protocolString = ""
    

        if (version == 0xfe) then
            protocolString = "MAVLink 1.0"
        elseif (version == 0x55) then
            protocolString = "MAVLink 0.9"
        elseif (version == 0xa8) then
            protocolString = "Lightdrone"
        else
            protocolString = "unknown"
            -- some unknown data found, record the begin offset
            if (unknownFrameBeginOffset == 0) then
                unknownFrameBeginOffset = offset
            end
           
            offset = offset + 1
        end	

        if protocolString == "Lightdrone" then
            offset = dissect_lightdrone(buffer, pinfo, tree, offset)

        
        elseif protocolString ~= "unknown" then

            local offset_start = offset
            local subtree = tree:add (mavlink_proto, "MAVLink Protocol")

            -- some Wireshark decoration
            pinfo.cols.protocol = "Lightdrone"

            -- HEADER ----------------------------------------
    
            local msgid
            if (buffer:len() - 2 - offset > 6) then
                -- normal header
                local header = subtree:add("Header")
                header:add(f.magic,version)
                offset = offset + 1
            
                local length = buffer(offset,1)
                header:add(f.length, length)
                offset = offset + 1
            
                local sequence = buffer(offset,1)
                header:add(f.sequence, sequence)
                offset = offset + 1
            
                local sysid = buffer(offset,1)
                header:add(f.sysid, sysid)
                offset = offset + 1
    
                local compid = buffer(offset,1)
                header:add(f.compid, compid)
                offset = offset + 1
            
                pinfo.cols.src = "System: "..tostring(sysid:uint())..', Component: '..tostring(compid:uint())
    
                msgid = buffer(offset,1)
                header:add(f.msgid, msgid)
                offset = offset + 1
            else 
                -- handle truncated header
                local hsize = buffer:len() - 2 - offset
                subtree:add(f.rawheader, buffer(offset, hsize))
                offset = offset + hsize
            end


            -- BODY ----------------------------------------
    
            -- dynamically call the type-specific payload dissector    
            local msgnr = msgid:uint()
            local dissect_payload_fn = "payload_"..tostring(msgnr)
            local fn = payload_fns[dissect_payload_fn]
    
            if (fn == nil) then
                pinfo.cols.info:append ("Unkown message type   ")
                subtree:add_expert_info(PI_MALFORMED, PI_ERROR, "Unkown message type")
                size = buffer:len() - 2 - offset
                subtree:add(f.rawpayload, buffer(offset,size))
                offset = offset + size
            else
                local payload = subtree:add(f.payload, msgid)
                pinfo.cols.dst:set(messageName[msgid:uint()])
                if (msgCount == 1) then
                -- first message should over wirte the TCP/UDP info
                    pinfo.cols.info = messageName[msgid:uint()]
                else
                    pinfo.cols.info:append("   "..messageName[msgid:uint()])
                end
                offset = fn(buffer, payload, msgid, offset)
            end

            -- CRC ----------------------------------------
            local crc = buffer(offset,2)
            subtree:add_le(f.crc, crc)
            offset = offset + 2

            subtree:append_text(string.format(" (%d)", offset-offset_start))
            subtree:add (mavlink_proto, buffer(offset_start,offset-offset_start), "raw bytes")
        end
    end
end

local function heuristic_checker(buffer, pinfo, tree)
    -- guard for length
    length = buffer:len()
    if length < 3 then
        return false
    end

    local potential_proto_flag = buffer(0,1):uint()
    if potential_proto_flag ~= 0xfe and potential_proto_flag ~= 0xa8 then
        return false
    end

    -- local potential_msg_id = buffer(1,2):uint()

    -- if get_message_name(potential_msg_id) ~= "Unknown" then
    --    return true
    --else 
    --    return false
    --end

    mavlink_proto.dissector(buffer, pinfo, tree)
    return true
end
   
mavlink_proto:register_heuristic("udp", heuristic_checker)
