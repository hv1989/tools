from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time

from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

from getpass import getpass
import os

if not os.path.exists("/tmp/invoices"):
    os.mkdir("/tmp/invoices")

def create_browser():
    fp = webdriver.FirefoxProfile()
    fp.set_preference("browser.download.folderList", 2)
    fp.set_preference("browser.download.manager.showWhenStarting", False)
    fp.set_preference("browser.download.dir", "/tmp/invoices")
    fp.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/csv")
    return webdriver.Firefox(executable_path=r'geckodriver', firefox_profile=fp)

def fill_slowly(el, text):
    for character in password:
        el.send_keys(character)
        time.sleep(0.3) # pause for 0.3 seconds

def wait_present(browser, what):
    time.sleep(1)
    WebDriverWait(browser, 30).until(
        expected_conditions.presence_of_element_located(
            what
        )
    )

def scroll_to(browser, element):
    x = element.location['x']
    y = element.location['y']
    scroll_by_coord = 'window.scrollTo(%s,%s);' % (
        x,
        y
    )
    scroll_nav_out_of_way = 'window.scrollBy(0, -120);'
    browser.execute_script(scroll_by_coord)
    browser.execute_script(scroll_nav_out_of_way)
    actions = ActionChains(browser)
    actions.move_to_element(element).perform();

def save_page(browser, name):
    import codecs
    with codecs.open("/tmp/site.html", "w", "utf-8") as f:
        f.write(browser.page_source)

    import pdfkit
    pdfkit.from_url('file:///tmp/site.html', name)

def inputYN(text):
    print text
    in_ = raw_input()
    return in_ in ["Y","y", ""]

do_upwork = inputYN("Do upwork? (Y/n)")
if do_upwork:
    password = getpass()

do_hobbyking = inputYN("Do hobbyking? (Y/n)")
if do_hobbyking:
    password_hk = getpass()

"""

Upwork

"""
if do_upwork:
    browser = create_browser()
    browser.get("https://www.upwork.com/ab/account-security/login")
    #browser.find_element(By.CSS_SELECTOR, ".d-none > .navbar-nav > li:nth-child(2) > a").click()
    browser.find_element(By.ID, "login_username").send_keys("hv1989@gmail.com")
    browser.find_element(By.ID, "login_password_continue").click()
    
    wait_present(browser, (By.ID, 'login_password'))
    time.sleep(4)
    
    browser.find_element(By.ID, "login_password").click()
    fill_slowly(browser.find_element(By.ID, "login_password"), password)
    browser.find_element(By.ID, "login_password").send_keys(Keys.ENTER)
    
    time.sleep(4)
    
    browser.get("https://www.upwork.com/ab/payments/reports/billing-history")

    wait_present(browser, (By.XPATH, "//button[contains(.,'Download Invoices')]"))
    time.sleep(4)
    browser.find_element(By.XPATH, "//button[contains(.,'Download Invoices')]").click()


"""

Hobbyking

"""
if do_hobbyking:
    browser = create_browser()
    browser.get("https://hobbyking.com/")
    browser.find_element(By.CSS_SELECTOR, ".form-language:nth-child(2) #current_store_name").click()
    browser.find_element(By.LINK_TEXT, "English").click()
    
    wait_present(browser, (By.LINK_TEXT, 'Sign in'))

    element = browser.find_element(By.LINK_TEXT, "Sign in")

    browser.find_element(By.LINK_TEXT, "Sign in").click()
    browser.find_element(By.ID, "email").click()
    browser.find_element(By.ID, "email").send_keys("hv1989@gmail.com")
    browser.find_element(By.CSS_SELECTOR, ".next").click()
    browser.find_element(By.ID, "pass").click()
    browser.find_element(By.ID, "pass").send_keys(password_hk)
    browser.find_element(By.ID, "pass").send_keys(Keys.ENTER)

    wait_present(browser, (By.CSS_SELECTOR, '.out'))

    browser.find_element(By.CSS_SELECTOR, ".nav-my-account-wrp > .collateraltab:nth-child(3) > .link").click()
    browser.find_element(By.CSS_SELECTOR, ".dashboard-container").click()


    wait_present(browser, (By.LINK_TEXT, 'VIEW ORDER'))
    els = browser.find_elements(By.LINK_TEXT, "VIEW ORDER")
    urls = [el.get_attribute("href") for el in els]
    for url in urls:
        browser.get(url)

        wait_present(browser, (By.LINK_TEXT, 'INVOICES'))
        browser.find_element(By.LINK_TEXT, "INVOICES").click()
  
        wait_present(browser, (By.LINK_TEXT, 'PRINT ALL INVOICES'))
        printbutton = browser.find_element(By.LINK_TEXT, "PRINT ALL INVOICES")

        url = printbutton.get_attribute("href")
        browser.get(url)


        os.system('xdotool search --sync --name "print" | xargs wmctrl -i -c')

        time.sleep(3)

        name = browser.find_element(By.CSS_SELECTOR, ".order-date").text
        id_ = url.split("/")[-2]
        
        save_page(browser, "/tmp/invoices/hk-"+name+"-"+id_+".pdf")

        time.sleep(10)

